<?php

/**
 *
 */
class itype {

    var $a_data = array(
        'textline' => array('desc' => 'Text line', 'html' => 'text', 'mysql' => 'char(255)'),
        'smalltext' => array('desc' => 'Short text line', 'html' => 'smalltext', 'mysql' => 'char(20)'),
        'name' => array('desc' => 'Name of person', 'html' => 'text', 'mysql' => 'char(150)'),
        'email' => array('desc' => 'Email', 'html' => 'text', 'mysql' => 'char(110)'),
        'tscreate' => array('desc' => 'Timestamp (insert)', 'html' => 'text', 'mysql' => 'timestamp DEFAULT CURRENT_TIMESTAMP'),
        'tsupdate' => array('desc' => 'Timestamp (update)', 'html' => 'text', 'mysql' => 'timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        'datetime' => array('desc' => 'Date and time', 'html' => 'text', 'mysql' => 'datetime'),
        'float' => array('desc' => 'Rational number', 'html' => 'text', 'mysql' => 'float'),
        'date' => array('desc' => 'Date', 'html' => 'date', 'mysql' => 'date'),
        'time' => array('desc' => 'Hours and minutes (HH:MM)', 'html' => 'text', 'mysql' => 'time'),
        'integer' => array('desc' => 'Number (integer, positive)', 'html' => 'integer', 'mysql' => 'int unsigned'),
        'smallint' => array('desc' => 'Number (small positive integer)', 'html' => 'text', 'mysql' => 'smallint unsigned'),
        'text' => array('desc' => 'Text', 'html' => 'textarea', 'mysql' => 'text'),
        'code' => array('desc' => 'Code', 'html' => 'code', 'mysql' => 'text'),
        'css' => array('desc' => 'CSS', 'html' => 'css', 'mysql' => 'text'),
        'richtext' => array('desc' => 'Rich Text (HTML)', 'html' => 'richtext', 'mysql' => 'text'),
        'password' => array('desc' => 'Password', 'html' => 'text', 'mysql' => 'char(40)'),
        'userfile' => array('desc' => 'File', 'html' => 'file', 'mysql' => 'char(20)'),
        'bool' => array('desc' => 'Checkbox (Yes / No)', 'html' => 'checkbox', 'mysql' => 'char(1)'),
        'fcheck' => array('desc' => 'Checkbox list', 'html' => 'fcheck', 'mysql' => 'char(255)'),
        'fradio' => array('desc' => 'Radio button list', 'html' => 'radio', 'mysql' => 'smallint unsigned'),
        'fselect' => array('desc' => 'Selection box', 'html' => 'select', 'mysql' => 'smallint unsigned'),
        'dbcheck' => array('desc' => 'Dynamic checkbox list', 'html' => 'checkbox', 'mysql' => 'text'),
        'dbradio' => array('desc' => 'Dynamic radio button list', 'html' => 'radio', 'mysql' => 'smallint unsigned'),
        'dbselect' => array('desc' => 'Dynamic select box', 'html' => 'select', 'mysql' => 'smallint unsigned'),
        'letters' => array('desc' => 'Letters', 'html' => 'text', 'mysql' => 'char(200)'),
        'textselect' => array('desc' => 'TextSelect', 'html' => 'select', 'mysql' => 'char(50)'),
        'integerlist' => array('desc' => 'Integer list', 'html' => 'checkbox', 'mysql' => 'char(255)'),
        'phone' => array('desc' => 'Phone', 'html' => 'phone', 'mysql' => 'char(255)'),
        'url' => array('desc' => 'URL', 'html' => 'url', 'mysql' => 'text'),
        'textlist' => array('desc' => 'Text list', 'html' => 'code', 'mysql' => 'text'),
        'html' => array('desc' => 'HTML code', 'html' => 'html', 'mysql' => 'text')
    );

    function map($s_type = NULL, $s_map = NULL) {

        if ($s_type == NULL) {
            return $this->a_data;
        } else {

            if (!empty($this->a_data[$s_type][$s_map])) {
                return $this->a_data[$s_type][$s_map];
            } else {
                throw new Exception('Type ' . $s_type . ' => ' . $s_map . ' does not exist. ');
            }
        }
    }

    /*
     *
     */

    function getControlType($sIobType) {
        if (!empty($this->a_data[$sIobType]['html'])) {
            return $this->a_data[$sIobType]['html'];
        } else {

            throw new Exception('Type ' . $sIobType . ' is unknown');
        }
    }

    function mysql_base_type($s_iobtype) {
        if (empty($this->a_data[$s_iobtype]['mysql']))
            return '';
        $s_type = $this->a_data[$s_iobtype]['mysql'];
        $s_ptype = '';
        $i_cnt = strlen($s_type);
        for ($i = 0; $i < $i_cnt; $i++) {
            if ($s_type[$i] == '(' || $s_type[$i] == ' ')
                break;

            $s_ptype .= $s_type[$i];
        }
        return $s_ptype;
    }

    //TODO: tem que aceitar $a_meta para validação superior

    function valid($m_value, $s_type, $b_optional = false, $a_options = null) {

        global $ob;

        if (empty($m_value) && $b_optional === true)
            return true;

        if (empty($m_value) && $b_optional === false)
            return false;

        switch ($s_type) {

            case 'lchars':

                return true;

            case 'textline':
            case 'textselect':

                //$s_res = preg_match ('/^[!-~ ¡-ÿ]{1,255}$/', $m_value);
                //if (!empty($s_res)) return true;

                if (empty($m_value))
                    return false;

                return true;

            case 'text':
            case 'richtext':
            case 'code':
            case 'html':
            case 'css':

                if (empty($m_value))
                    return false;

                return true;

            case 'integer':

                if (is_numeric($m_value)) {
                    if (strpos("$m_value", '.') == false)
                        return true;
                }

                break;

            case 'bool':

                if ("$m_value" == '1' || $m_value == '0')
                    return true;

                return false;

                break;

            case 'alpha':

                return true;

            case 'email':

                if (strpos($m_value, '@') !== false)
                    return true;
                //TODO: review: if (preg_match ('/^[a-z0-9\.\-\_]{1,60}@([a-z0-9\.\-\_]{2,63})\.([a-z]{2,6})$/', $m_value)) return true;

                break;

            case 'id':

                if (is_numeric($m_value) && !strpos($m_value, '.'))
                    return true;
                break;

            case 'username':

                if (preg_match('/^[a-z0-9]{1,15}$/', $m_value))
                    return true;
                break;

            case 'userfile':
            case 'pfile':

                $a_bad_ext = array('exe', 'com', 'pif');
                if (array_search($m_value, $a_bad_ext))
                    return false;
                return true;

            case 'timestamp':
            case 'datetime':
                //if (preg_match ('/^\d{1,14}$/', $m_value)) return true;
                //
				//TODO
                return true;

            case 'name':
                return true;


            case 'password':
                return true;

            case 'url':

                if (strpos($m_value, '://') !== false)
                    return true;
                break;

            case 'phone':

                if ((empty($m_value) || !is_string($m_value)) && !$b_optional)
                    return false;
                $ic = 0;
                for ($f = 0; $f < strlen($m_value); $f++) {
                    if (is_numeric($m_value[$f]))
                        $ic++;
                }
                if ($ic >= 9)
                    return true;
                break;

            case 'ip':

                if (preg_match('/^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})$/', $m_value))
                    return true;
                break;


            case 'domain':

                if (preg_match('/^[a-z0-9\.\-\_]{1,230}\.[a-z]{2,6}$/', $m_value))
                    return true;
                break;


            case 'float':

                if (preg_match('/^[-+]?\d*\.?\d*$/', $m_value))
                    return true;
                break;

            case 'date':

                // Note: this is for ISO format YYYY-MM-DD
                if (preg_match('/^\d{4}-(([1-9]{1})|([0][1-9]{1})|(10)|(11)|(12))-(([1-9]{1})|(0[1-9]{1})|(1[0-9]{1})|(2[0-9]{1})|(30)|(31))$/', $m_value))
                    return true;
                break;

            case 'time':
                if (preg_match('/^([0-1][0-9])|(2[0-3]):[0-5]\d(:[0-5]\d)?$/', $m_value))
                    return true;
                break;

            case 'smalltext':
                return true;

            case 'smallint':

                if (preg_match('/^\d{1,5}$/', $m_value) && $m_value < 65536)
                    return true;
                break;


            case 'dbcheck':
                if ((int) $m_value > 0 || $m_value == '0')
                    return true;
                break;

            case 'fcheck':

                return true;

            case 'fselect':
            case 'fradio':
            case 'dbselect':
            case 'dbradio':

                if (empty($m_value))
                    return false;

                // método ainda nao aceita $a_meta
                //DEV if (!empty($a_meta['select'][$m_value]) && is_numeric($a_meta['select'][$m_value])) return true;

                if ((int) $m_value > 0)
                    return true;

                break;

            case 'uid':

                if (preg_match('/^[a-zA-Z0-9]{16,}$/', $m_value))
                    return true;

                break;

            case 'digits':
                if (preg_match('/^[0-9]{1,}$/', $m_value))
                    return true;
                break;

            case 'letters':
                return true;

            case 'integerlist':
                $b = false;
                if (is_string($m_value)) {
                    $a = explode(',', $m_value);
                    $b = true;
                    foreach ($a as $i)
                        if (!empty($i) && !is_numeric($i))
                            $b = false;
                } elseif (is_array($m_value)) {
                    $b = true;
                    foreach ($m_value as $i)
                        if (!empty($i) && !is_numeric($i))
                            $b = false;
                }

                return $b;

            default:
                throw new exception('itype: type ' . $s_type . ' does not exist here.');
        }

        return false;
    }

    /*
     *
     */

    function filled($a) {
        foreach ($a as $m)
            if (empty($m))
                return false;

        return true;
    }

    /*
     *
     */

    function key_value() {
        $a = array();
        foreach ($this->a_data as $k => $av)
            $a[$k] = $av['desc'];
        asort($a);
        return $a;
    }

    /*
     *
     */

    function as_xhtml_select($s_element_name = "type", $s_key_selected = null) {
        $s = '<select name="' . $s_element_name . '">';
        foreach ($this->a_data as $k => $v) {
            $s .= '<option ';
            if ($k == $s_key_selected) {
                $s .= ' selected="selected" ';
            }
            $s .= 'value="' . $k . '">' . $v['desc'] . '</option>';
        }
        $s .= '</select>';
        return $s;
    }

}
