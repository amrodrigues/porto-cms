Porto CMS: Professional web content management system
=====================================================


Project Status
--------------

Porto CMS is production-quality, very stable and growing fast in the community, 
including some high-profile companies and institutions.

Many aspects, either architectural or module-specific, are not finished yet: 
the code is being refactored to a much more PHP5 OOP MVC approach from an old codebase.


About
-----

Depending on your background, vision and specific needs, Porto CMS might be:

- A professional web CMS for professional webdesigners.
- A serious tool to manage HTML5, CSS3, Jquery/Javascript and SQL content.
- A slick web CMS for commodity LAMP servers running PHP5 and MariaDB (MySQL).
- A way to escape lower quality open source Web CMS's.
- An easy way to bootstrap a complex web app.
- An integrated environment for web site development and management.
- A minimalistic lightweight abstraction of both a MariaDB database and a Linux directory tree.
- A metaphor of objects taken to the limits, reaching the end-users.


Who benefits?
-------------

- **Web designers**: create fast, professional mobile/desktop web sites.
- **Content managers**: easy and productive way of managing content.
- **Database administrators**: create SQL queries and build HTML5/CSS3 reports.
- **Developers**: a clear architecture written in simple PHP5 code, easy to extend in minutes.
- **Content strategists**: access all HTML/CSS/Javascript, split-test, edit metatags and layouts


Features
--------

- **Large content**: Designed to easily manage hundreds of thousand web documents in a single site.
- **E-commerce**: Paypal and Multibanco integration
- **Multisite**: a single install for dedicated/cloud servers or **monosite** for commodity web hosting.
- **Email marketing**: your newsletters ready to send.
- **Multilayout**: Different appearance across site sections.
- **Short or Friendly URLs**: call content units by /id or /friendly-url
- **Multiuser**: inspired on RBAC model, user profiles are created for different kinds of users.
- **Multidomain for white-label and co-branding**: same site can change visual behavior and contents for each domain.
- **Nice code**: Written in simple object-oriented PHP code.
- **Clear MVC-inspired orthogonal architecture** with a centralized controller.
- **Easy**: object metaphor that resembles a file system. Train users in few hours.
- **Integrated AJAX web forms generator**: make forms quickly.
- **Adaptive interface**: what you see is what you can use with your account.
- **Tested methodology**: previous versions still running on hundreds of sites.
- **Deploy in minutes**: copy to server, create a database, edit conf.php, paste some directives in the http server config file.
- **Lightweight**: minimalistic code and minimalistic interface for speed.
- **Open source MIT licence**: it's yours to use, modify, integrate, share or sell.


System requirements
-------------------

- A Linux/GNU distribution
- An HTTP server (like Apache) running PHP >= 5.4.1
- MariaDB or MySQL database system

License
-------

MIT Licence. You may use this software without any restriction. See LICENCE file.

Installation
------------

See INSTALL file.


Motivation
-----------

Thousands of web content professionals spend many hours each day using inadequate open source web software while creating and managing new-generation mobile responsive web apps.

As GLAMP platform is leading the web industry and content marketing is rising, Porto CMS aims to create a fresh look of what a 21st century web CMS might be.


History
---------

The first prototype of of Porto CMS was developed at Omnisinal (read *omnee-see-nal*), a communication/PR startup based at Porto, Portugal. Since then, it has been successfully used to power hundreds of web sites by the Omnisinal team.

Porto CMS has been totally rewritten in 2012 by the original author, Antonio Rodrigues, keeping the object metaphor while improving on the architecture and interface.

In 2013, Porto CMS was released as open source software on github.com and continues development.



Contributors
------------

- António Rodrigues: software development - antoniomotarodrigues@gmail.com
- Paulo Oliveira: Web strategy support  - paulo.oliveira@gmx.com
- José Meireles: User experience and multimedia
