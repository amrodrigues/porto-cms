<?php

/* --------------
 * A text loaded from a file or other source
 */

class IoTextLoader {

    protected $rawTxt = 'init';

    /*
     *
     */

    public function loadUrl($url) {
        $this->rawTxt = urldecode($this->getDataFromFileOrUrl($url));
    }

    /*
     *
     */

    protected function getDataFromFileOrUrl($uri) {
        if (strpos($uri, 'http') == 0) {
            return $this->getWebPageFromUrl($uri);
        }
        return file_get_contents($uri);
    }

    /*
     *
     */

    private function getWebPageFromUrl($url) {

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_ENCODING => "", // handle all encodings
            CURLOPT_USERAGENT => "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.114 Safari/537.36", // who am i
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 30, // timeout on connect
            CURLOPT_TIMEOUT => 30, // timeout on response
            CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
        );
        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
//$err = curl_errno($ch);
//$errmsg = curl_error($ch);
//$header = curl_getinfo($ch);
        curl_close($ch);
        return $this->convertToUtf8($content);
//$header['errno'] = $err;
//$header['errmsg'] = $errmsg;
//$header['content'] = $content;
//return $header;
    }

    /*
     * Converts any string to UTF-8
     */

    function convertToUtf8($s) {
        if (mb_check_encoding($s, 'UTF-8')) {
            return $s;
        } else {
            return utf8_encode($s);
        }
    }

    /*
     *
     */

    public function rawTextIsEmpty() {
        if (empty($this->rawTxt)) {
            return true;
        } else {
            return false;
        }
    }

    public function getRawText() {
        return $this->rawTxt;
    }

}

/* -------
 * Basic text processing
 */

class TextPreprocessor extends IoTextLoader {

    protected $purifiedTxt;

    /*
     *
     */

    public function getBetween($start, $end) {
        $i = strpos($this->rawTxt, $start);
        $i2 = strpos($this->rawTxt, $end, $i + strlen($start));
        if ($i > 0 && $i2 > 0) {
            return substr($this->rawTxt, $i + strlen($start), $i2 - $i + strlen($start));
        }
        return '';
    }

    /*
     *
     */

    static function linesToSet($s) {
        if (!is_string($s)) {
            throw new Exception("Not a string");
        }
        $a = explode("\n", $s);
        $st = new Set();
        foreach ($a as $s) {
            $st->add($s);
        }
        return $st;
    }

    /*
     *
     */

    static function linesToArray($s) {
        if (!is_string($s)) {
            throw new Exception("Not a string");
        }
        $a = explode("\n", $s);
        $ret = array();
        foreach ($a as $s) {
            $s = trim($s);
            if ($s !== '') {
                $ret[] = $s;
            }
        }
        return $ret;
    }

    /*
     *
     */

    public function rtrimSubstr($remove, $subject) {
        if (substr($subject, (0 - strlen($remove))) == $remove) {
            return substr($subject, 0, strlen($subject) - strlen($remove));
        }
        return $subject;
    }

    /*
     * Frequently HTML pages are broken, so we'll not use PHP's DOM objects
     */

    public function purifyToSimpleText() {
        $this->purifiedTxt = '';
        $this->purifiedTxt = $this->removeFormattingTags($this->rawTxt);
        $this->purifiedTxt = $this->separateWordsFromPunctuation($this->purifiedTxt);
    }

    /*
     *
     */

    public function extractHtmlTitle() {
        $i1 = strpos($this->rawTxt, '<title>');
        $i2 = strpos($this->rawTxt, '</title>', $i1);
        if ($i1 == false || $i2 == false) {
            return '';
        }
        return substr($this->rawTxt, ($i1 + 7), ($i2 - $i1 - 7));
    }

    /*
     * We need to remove HTML without using DOMDocument
     */

    private function removeFormattingTags($t) {

        $t = $this->removeSimpleTags(array('em', 'i', 'u', 'b', 'sub', 'sup', 'small', 'strong', 'abbr'), $t);
        $t = str_replace(array('<br>', '<br />', '<br/>'), ' ', $t);
        $t = str_replace(array("\r", "\t"), '', $t);
        $t = str_replace(array('      ', '     ', '    ', '   ', '  '), ' ', $t);

        $t = str_replace("\n", '', $t);
        $t = str_replace('>', ">\n", $t);
        $t = str_replace('<', "\n<", $t);

        $a = explode("\n", $t);
        foreach ($a as $k => $line) {
            if (empty($line) || substr($line, 0, 1) == '<') {
                unset($a[$k]);
            }
        }

        $t = implode("\n", $a);
        $t = html_entity_decode($t); // decode here because of "<" and ">" characters
        return $t;
    }

    /*
     *
     */

    private function removeSimpleTags($aTags, $t) {
        foreach ($aTags as $tag) {
            $t = str_replace('<' . $tag . '>', '', $t);
            $t = str_replace('</' . $tag . '>', '', $t);
        }
        return $t;
    }

    /*
     *
     */

    public function separateWordsFromPunctuation($t) {
        return $this->addBeforeAndAfter(array(';', ':', '!', '?', '.', ',', '\''), ' ', $t);
    }

    /*
     *
     */

    public function addBeforeAndAfter($mFind, $characterToAddBeforeAndAfter, $txt) {
        if (!is_array($mFind)) {
            $mFind = array($mFind);
        }
        foreach ($mFind as $find) {
            $txt = str_replace($find, $characterToAddBeforeAndAfter . $find . $characterToAddBeforeAndAfter, $this->purifiedTxt);
        }
        return $txt;
    }

    /*
     *
     */

    public function purifiedTextIsEmpty() {
        if (empty($this->purifiedTxt)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     *
     */

    public function getPurifiedText() {
        return $this->purifiedTxt;
    }

}

/* --------------
 * Regex matching
 */

class RegexHelper extends TextPreprocessor {

    private $matches;
    private $oCounter = false; // Counter object callback

    /*
     *
     */

    public function setMatches($regex) {
        $aMatches = array();
        preg_match_all($regex, $this->rawTxt, $aMatches);
        $this->matches = $aMatches[0];
    }

    /*
     * Tests purified text for patterns
     * Calls Counter
     */

    public function countPatternMatchesInPurifiedText($aRx) {

        foreach ($aRx as $regex) {
            if (preg_match($regex, $this->purifiedTxt) === 1) {
                $this->oCounter->add($regex, 1);
            }
        }
    }

    /*
     * test purified text with array('key' => array(regex,regex,...)...)
     */

    public function countPatternMatchesWithArraysOfRegexes($aaRx) {
        $ak = array();
        foreach ($aaRx as $k => $aRegex) {
            foreach ($aRegex as $rx) {
                if (preg_match($rx, $this->purifiedTxt) === 1) {
                    $this->oCounter->add($k, 1);
                    $ak[] = $k;
                }
            }
        }
        return $ak;
    }

    /*
     * Removes items in matches
     */

    public function discardMatches($aRx) {
        foreach ($this->matches as $k => $sMatch) {
            if ($this->matchesAnyInArray($aRx, $sMatch)) {
                unset($this->matches[$k]);
            }
        }
    }

    /*
     *
     */

    public function discardArrayItems($aRx, $aSubject) {
        foreach ($aSubject as $k => $txt) {
            if ($this->matchesAnyInArray($aRx, $txt)) {
                unset($aSubject[$k]);
            }
        }
    }

    /*
     *
     */

    public function matchesAnyInPurifiedText($aRx) {
        foreach ($aRx as $regex) {
            if (preg_match($regex, $this->purifiedTxt) === 1) {
                return true;
            }
        }
        return false;
    }

    /*
     *
     */

    public function explodeRegexes($sRx, $bAddSpaces = true) {
        $aRx = $this->linesToArray($sRx);
        foreach ($aRx as $k => $s) {
            $aRx[$k] = $this->checkDelimiters($s, 'i', $bAddSpaces);
        }
        return $aRx;
    }

    /*
     *
     */

    public function checkDelimiters($sRx, $sModifiers = '', $bAddSpacesToPattern = false) {
        if (substr($sRx, 0, 1) == '/') {
            return $sRx;
        }
        if ($bAddSpacesToPattern) {
            return '/ ' . $sRx . ' /' . $sModifiers;
        } else {
            return '/' . $sRx . '/' . $sModifiers;
        }
    }

    /*
     * key::regex1::regex2::regexN
     */

    public function explodeRegexesWithKey($sRx, $separator) {
        $aRx = $this->linesToArray($sRx);
        $a = array();
        foreach ($aRx as $line) {
            $aa = explode($separator, $line);
            $k = $aa[0];
            $a[$k] = array_slice($aa, 1);
            foreach ($a[$k] as $kk => $lineRx) {
                $a[$k][$kk] = $this->checkDelimiters($lineRx, 'i', true);
            }
        }
        return $a;
    }

    /*
     *
     */

    public function matchesAnyInArray($mRegex, $subject) {
        if (empty($mRegex)) {
            return false;
        }
        if (empty($subject)) {
            throw new Exception('Text empty');
        }
        if (!is_array($mRegex)) {
            $mRegex = array($mRegex);
        }
        foreach ($mRegex as $regex) {
            $test = preg_match($regex, $subject);
            if ($test == 1) {
                return true;
            }
        }
        return false;
    }

    /*
     *
     */

    public function registerCounter(&$oCounter) {
        $this->oCounter = $oCounter;
    }

    /*
     *
     */

    public function getMatches() {
        return $this->matches;
    }

}

/* ----------
 *
 */

class crawlerHelper extends RegexHelper {
    /*
     * Fetches a list of urls and aggregates unique links
     */

    public function getSerpLinks($stUrls, $aDiscardUrlsRegex) {

        $st = new Set(); // keeps aggregated list of urls

        foreach ($stUrls as $sUrl) {
            $this->loadUrl($sUrl);
            $this->setMatches('/https?\:\/\/[a-zA-Z0-9\-\.\&\/\?\=\_]+/i');
            $this->discardMatches($aDiscardUrlsRegex);
            $st->add($this->getMatches());
        }
        return $st;
    }

    /*
     *
     */

    public function expandRelativeUrls($sAFullUrlFromTheSite) {
        $this->rawTxt = preg_replace('/href=["\']([a-z0-9_\.;\&\?\-\+\(\)\/\=\%\$]+)["\']/i', 'href="' . $this->getBaseUrl($sAFullUrlFromTheSite) . '$1"', $this->rawTxt);
    }

    /*
     *
     */

    public function getBaseUrl($url) {
        $a = parse_url($url);
        return $a['scheme'] . '://' . $a['host'];
    }

}
