<?php

/*
 * This file should be executed by the system cron.
 * See INSTALL file for cron jobs configuration
 */
if (!empty($_GET)) {

    die('This code cannot be executed via web.');
} else {

    require('cron.php');
    if (empty($argv[1])) {
        die("Argument 1 needs to be the directory where websites reside\n");
    }
    if (empty($argv[2])) {
        die("Argument 2 needs to be the name of the method to call in each cron object\n");
    }
    $o = new cron();
    $o->exec($argv[1], $argv[2]);
}