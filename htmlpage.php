<?php

require ('dob.php');

/*
 * A recursive object html serializer with memcached and bootstrap support
 */

class htmlpage extends dob {

    function __construct() {
        // page content accumulator in global space
        //TODO: remove from global space
        $GLOBALS['html'] = array(
            'body' => array(1 => '', 2 => '', 3 => '', 4 => '', 5 => '', 6 => '', 7 => '', 8 => '', 9 => '', 10 => '', 11 => '', 12 => ''),
            'head' => array(),
            'title' => '',
            'bot' => array()
        );
        $this->dob();
    }

    /*
     * All GET requests for HTML go here:
     */

    function requestHandler() {

        global $ob, $conf;

        // --- short url:

        if (!empty($_GET['oid'])) {
            $oid = (int) $_GET['oid'];
        } else {
            $_GET['oid'] = $oid = $conf->defaultoid;
        }

        // --- long url:
        if (!empty($_GET['furl'])) {
            $oid = $this->getOidFromFurl($_GET['furl']);
        }
        $ob->page($oid);
    }

    /*
     *
     */

    function getUrl($aBr) {
        return $this->getFurlFromEasyname($aBr['easyname']);
    }

    /*
     *
     */

    function getFurlFromEasyname($easyname) {
        $s = trim(strtolower($easyname));
        $out = '';
        $i = strlen($s);
        $test = 'abcdefghijklmnopqrstuvwxyz0123456789';
        for ($f = 0; $f < strlen($s); $f++) {
            $ch = substr($s, $f, 1);
            if (strpos($test, $ch) !== false) {
                $out .= $ch;
            } else {
                $out .= '-';
            }
        }
        return str_replace('--', '-', $out);
    }

    /*
     *
     */

    function getOidFromFurl($furl) {
        global $conf;

        if (!$this->isClean($furl, 'alpha')) {
            return $conf->defaultoid;
        }

        $furl = str_replace('-', '%', $furl);
        $sql = 'select oid from broker where easyname like "' . str_replace('-', '%', $furl) . '" order by oid limit 1';
        $oid = $this->dbSelectCell($sql);

        if ($oid > 0) {
            $_GET['oid'] = $oid; //TODO: remove hack
            if ($furl != strtolower($furl)) {
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: /' . strtolower($furl));
                die();
            }
        }

        return $oid;
    }

    function errorPage($statusCode) {

        switch ($statusCode) {
            case '404':
                header("HTTP/1.0 404 Not Found");
                die();
            default:
                die();
        }
    }

    function getHtml($oid, $lay = '', $children = false, $omitfirst = false) {

        global $oAuth, $conf;

        // --- get broker data

        $ud = $this->get($oid);

        if ($ud === false)
            return '';

        $userIsAdmin = $oAuth->userIsAdmin();


        // --- Use cache? ---

        if ($_SERVER['REQUEST_URI'] != '' && $this->oCache !== null && empty($_POST) && $ud['br']['cache'] !== false) {
            // --- key ---

            $s_cache_key = $oid;

            if (!empty($_GET['op']) && $_GET['op'] == 'all')
                $s_cache_key .= '_opall';

            // --- fetch data --

            try {
                $html = $this->cacheGet($s_cache_key);
            } catch (Exception $e) {
                $html = false;
            }

            // --- update counter? ---

            if ($html !== false) {
                if ($conf->stats && !$userIsAdmin)
                    $GLOBALS['ob']->sql('update broker set cnt=(cnt+1) where oid=' . $oid);

                return $html;
            }
        }


        // No cache... Generate

        $cl = $ud['br']['class'];

        if ($cl == 'clone') {

            // It's a clone object

            $ids = explode(',', $ud['content']['rid']['value']);

            if (count($ids) == 1) {
                $lay = $this->getHtml($ids[0]);
            } else {

                $lay = '';

                foreach ($ids as $id) {
                    $lay .= $this->getHtml($id);
                }
            }
        } else { // --- It's a regular object:
            if ($omitfirst === false) {
                // next condition must only work in web environment:

                if (empty($_SERVER['REQUEST_URI']))
                    return;
                $lay = $this->asHtml($ud);
            }

            // using cache avoids getting sub-objects in database:

            if (strpos($lay, '{[') !== false) {

                if ($children === false)
                    $children = $this->children($ud['br']['oid']);


                if (!empty($children)) {
                    foreach ($children as $ch) {
                        if ($ch['class'] == 'folder')
                            continue; // prevent folder rendering

                        $sublay = $this->getHtml($ch['oid'], $lay, false, false); // recursive, checks perms

                        $needle = '{[' . $ch['laypos'] . ']}';

                        $lay = str_replace($needle, $sublay . $needle, $lay);
                    }
                }
            }
        }



        // --- remove content position tags, avoiding regexes for speed ---

        $lay = str_replace(array('{[1]}', '{[2]}', '{[3]}', '{[4]}', '{[5]}', '{[6]}', '{[7]}', '{[8]}', '{[9]}', '{[10]}', '{[11]}', '{[12]}'), '', $lay);


        // --- counter ---
        //if ($GLOBALS['conf']->stats && !$userIsAdmin)
        //    $GLOBALS['db']->sql('update broker set cnt=cnt+1 where oid=' . $oid);
        // --- save to memcache? ---

        if (isset($s_cache_key)) {
            $this->cacheAdd($s_cache_key, $lay);
        }
        return $lay;
    }

    /*
     * outputs or returns a web page
     */

    function page($oid = false, $lay = '', $embedCSS = false, $bOutput = true) {

        global $html, $conf;


        // --- folder id:

        $oid = (int) $oid;

        if (empty($oid))
            $oid = $GLOBALS['conf']->defaultoid;

        $oid_layout = 0;


        // --- layout switch per domain

        $switchlays = false;

        if (!empty($conf->domains)) {
            foreach ($conf->domains as $domain => $a) {

                if ($_SERVER['SERVER_NAME'] !== $domain)
                    continue;

                if ($_SERVER['REQUEST_URI'] == '/') {
                    $oid = (int) $a['defaultoid'];
                    $oid_layout = (int) $a['defaultlay'];
                    break;
                }

                if (!empty($a['switchlayouts']))
                    $switchlays = $a['switchlayouts'];

                break;
            }
        }

        //TODO: possibility of locking content not specified in the switch layouts option. this enables "virtual virtual sites", sort of.


        if ($lay != '')
            $html['body'][1] .= $lay; //'<div id="page-custom">' . $lay . '</div>';

        $ud = $this->get($oid);

        if ($ud === false) {
            //header("Status: 404 Not Found");
            die("Not found");
        }


        // --- use object layout or default layout:

        if ($oid_layout == 0) {

            if ($ud['br']['layout'] > 0)
                $oid_layout = $ud['br']['layout'];
            else
                $oid_layout = $conf->defaultlay;
        }


        if ($switchlays)
            foreach ($switchlays as $from => $to)
                if ($oid_layout == $from)
                    $oid_layout = $to;



        // --- get layout ---

        /*
         *  "behavior" objects placed inside layouts, like statusbehavior, change $conf var
         *  to affect behavior of other objects
         *  So we call them on layout before rendering page objects
         */

        $ud_layout = $this->get($oid_layout);

        if (!empty($ud_layout['content']['template']['value']))
            $lay = $ud_layout['content']['template']['value'];
        else
            die("please specify a layout for this object");


        // --- get all objects to put in page ---

        if ($ud['br']['class'] == 'folder') {
            $a_children = $this->childrenOf(array($oid_layout, $oid)); // folder and layout descendants, together
        } else {

            $a_children = $this->childrenWith($oid_layout, $oid); // joins oid to layout descendants
        }


        // --- behavior objects must be executed before any other:
        //TODO: review architecture to make it generic, specific objects cannot be enumerated here

        foreach ($a_children as $k => $a) {

            if ($a['class'] == 'behavior' || $a['class'] == 'status') {
                $this->asHtml($this->get($a['oid']));
                unset($a_children[$k]);
            }
        }

        // --- meta tags ---

        if (!$embedCSS && $ud['br']['class'] == 'folder') {
            if (!empty($ud['content']['metadescription']['value']))
                $html['head'][] = '<meta name="description" content="' . $ud['content']['metadescription']['value'] . '" />';

            if (!empty($ud['content']['metakeywords']['value']))
                $html['head'][] = '<meta name="keywords" content="' . $ud['content']['metakeywords']['value'] . '" />';
        }



        // --- CSS handling ---

        if (!empty($ud_layout['content']['idcss']['value'])) {

            if (!$embedCSS) {
                // --- usually we build a web page having css separated:
                $this->includeCss('/obj/css/get.php?id=' . $ud_layout['content']['idcss']['value']);
            } else {

                // --- but for email newsletters, we embed it:
                $css = $this->dbSelectCell('select code from css where oid=' . $ud['content']['idcss']['value']);
                $lay = str_replace('<head>', '<head><style type="text/css">' . $css . '</style>', $lay);
            }
        }


        $layOid = $ud_layout['br']['oid'];

        if (!empty($ud_layout['content']['framework']['value'])) {


            $this->includeJquery();
            $this->includeScriptTag('/obj/layout/mod/bootstrap.min.js');
            $this->includeCss('/obj/layout/mod/bootstrap.min.css');
            $this->addHead('bootstrap', '
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <!--[if lt IE 9]>
                            <script src="/obj/layout/mod/html5shiv.js"></script>
                            <script src="/obj/layout/mod/respond.min.js"></script>
                        <![endif]-->
                   ');

            $aContent = array();

            foreach ($a_children as $a) {
                if ($a['pid'] == $layOid || $a['class'] == 'folder')
                    continue;
                if ($a['laypos'] == '')
                    $iPos = 1;
                else
                    $iPos = (int) ($a['laypos']);

                $aContent[$iPos][] = $this->getHtml($a['oid']);
            }

            $aRows = explode(',', $ud_layout['content']['framework']['value']);
            $sublay = '';

            foreach ($aRows as $ixRow => $sColspan) {
                $aColspan = explode('-', $sColspan);
                $sublay .= '<div class="row">';

                foreach ($aColspan as $ixColspan => $iColspan) {
                    if (empty($aContent[$ixRow + 1][$ixColspan])) {
                        $sublay .= '<!-- missing object -->';
                        break;
                    }
                    $sublay .= '<div class="col-md-' . $iColspan . '">' . $aContent[$ixRow + 1][$ixColspan] . '</div>';
                }
                $sublay .= '</div>' . "\n";
            }

            $lay = str_replace('{[1]}', $sublay . '{[1]}', $lay);

            // merge layout objects:

            foreach ($a_children as $a) {
                if ($a['pid'] !== $layOid || $a['class'] == 'folder')
                    continue;
                $a['laypos'] == '' ? $pos = 1 : $pos = (int) $a['laypos'];
                $html['body'][$pos] .= $this->getHtml($a['oid']);
            }

            // Replace top layout positions with aggregated object content

            foreach ($html['body'] as $pos => $string_content) {
                $lay = str_replace('{[' . $pos . ']}', $string_content, $lay);
            }

            // end bootstrap content
        } else {

            // No bootstrap, do a simple distribution just by layout position

            if (!empty($a_children)) {
                foreach ($a_children as $a) {
                    if ($a['class'] == 'folder')
                        continue;
                    $a['laypos'] == '' ? $pos = 1 : $pos = (int) $a['laypos'];
                    $html['body'][$pos] .= $this->getHtml($a['oid']);
                }
            }

            // Replace top layout positions with aggregated object content

            foreach ($html['body'] as $pos => $string_content) {
                $lay = str_replace('{[' . $pos . ']}', $string_content, $lay);
            }
        }


        // page title

        if (!empty($ud['content']['title']['value'])) {
            $html['title'] = $ud['content']['title']['value'];
        } else {
            $html['title'] = $ud['br']['easyname'];
        }

        $lay = str_replace('<head>', '<head><title>' . $html['title'] . '</title>', $lay);

        if (!empty($html['head'])) {
            $lay = str_replace('</head>', implode("\n", $html['head']) . '</head>', $lay);
        }

        if (!empty($html['bot'])) {
            if (!empty($ud_layout['content']['mjquery']['value'])) {
                unset($html['bot']['jquery']);
            }
            $lay = str_replace('</body>', implode("\n", $html['bot']) . "\n" . '</body>', $lay);
        }

        // --- Output

        if ($bOutput) {

            header('Content-type: text/html; charset=UTF-8');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');  // always modified
            header('Cache-Control: no-store, no-cache, must-revalidate');  // HTTP/1.1
            header('Cache-Control: post-check=0, pre-check=0', false);
            header('Pragma: no-cache'); // HTTP/1.0

            print $lay;

            die();
        } else {

            return $lay; // this is used for email sending
        }
    }

    function includeScriptTag($path) {
        global $html;
        if (empty($html['bot'][$path])) {
            $html['bot'][$path] = '<script src="' . $path . '" type="text/javascript"></script>';
        }
    }

    function includeJquery() {
        global $html;
        if (empty($html['bot']['jquery'])) {
            $html['bot']['jquery'] = '<script src="/mod/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>';
        }
    }

    function includeJqueryUI() {
        $this->includeScriptTag('/mod/jquery/jquery-ui-1.9.2.custom.min.js');
        $this->includeCss('/mod/jquery/jquery-ui-1.9.2.custom.min.css');
    }

    function includeCss($path) {
        global $html;
        if (empty($html['head'][$path])) {
            $html['head'][$path] = '<link rel="stylesheet" type="text/css" href="' . $path . '" />';
        }
    }

    function includeScript($k, $script) {
        global $html;
        $html['bot'][$k] = '<script type="text/javascript">' . $script . '</script>';
    }

    function addBottom($k, $txt) {
        $GLOBALS['html']['bot'][$k] = $txt;
    }

    function addHead($k, $markup) {
        $GLOBALS['html']['head'][$k] = $markup;
    }

}
