<?php

require("files.php");
require("dob.php");

/*
 * cron manages all cron jobs of all objects in all web apps.
 * Objects may have a cron/ directory having classes with methods to be
 * executed at specific times.
 * See INSTALL file for crontab configuration.
 */

class cron {
    /*
     * For each server home directory
     */

    public function exec($websitesDirectory, $methodName) {
        $siteHomes = $this->getHomes($websitesDirectory);
        if (empty($siteHomes)) {
            throw new Exception('Site list empty.');
        }
        foreach ($siteHomes as $home) {
            $this->processHome($websitesDirectory . '/' . $home, $methodName);
        }
    }

    /*
     * Try to find Porto in each web site below $sitePath
     * Provide $ob and $conf context for cron obj execution
     */

    private function processHome($sitePath, $methodName) {

        global $ob, $conf;

        $conf = null;
        $confFilePath = "$sitePath/public_html/conf.php";
        if (!file_exists($confFilePath)) {
            print "No conf.php file found at $confFilePath\n";
            return;
        }

        // --- load conf
        require($confFilePath);
        if (empty($conf->dbname)) {
            print "No conf->dbname found\n";
            return;
        }
        if (empty($conf->dbuser)) {
            print "No conf->dbuser found\n";
            return;
        }
        if (empty($conf->dbpass)) {
            print "No conf->dbpass found\n";
            return;
        }

        // -- load dob

        $ob = new dob();

        print "\n\nProcess home: $sitePath Site root $conf->root  Database: " . $conf->dbname . "\n";
        $this->processCron($methodName);
    }

    /*
     * Executes $methodName() in all cron objects in a single site
     */

    private function processCron($methodName) {
        global $conf, $ob;
        print "At processCron($methodName)\n";
        $ao = $this->getCronObjects();
        if ($ao !== false) {
            $ob->log('Found ' . count($ao) . " cron classes");
        }
        if ($ao !== false) {
            foreach ($ao as $o) {
                if (method_exists($o, $methodName)) {
                    $o->$methodName();
                } else {
                    print "Method name $methodName was not found in cron object at $conf->root\n";
                }
            }
        }
    }

    /*
     * Get a directory list under /home
     */

    private function getHomes($websitesDirectory) {
        return files::fileList($websitesDirectory, true);
    }

    /*
     * Gets array of cron objects from a single web site
     */

    private function getCronObjects() {
        global $conf;
        print "At getCronObjects.\n";
        $aObjList = $this->getObjectList();
        if (empty($aObjList)) {
            print "No object list, skip.\n";
            return false;
        } else {
            print "Found " . count($aObjList) . " object directories.\n";
        }
        $ao = array();
        foreach ($aObjList as $objName) {
            $aCronObjs = $this->getCronObjNames($conf->root . '/obj/' . $objName);
            if (empty($aCronObjs)) {
                continue;
            }
            foreach ($aCronObjs as $sCronClassName) {
                $sCronClassFilePath = $conf->root . '/obj/' . $objName . '/cron/' . $sCronClassName . '.php';
                print "Cron class file path is $sCronClassFilePath\n";
                require($sCronClassFilePath);
                $ao[] = new $sCronClassName();
            }
        }
        if (empty($ao)) {
            return false;
        }
        return $ao;
    }

    /*
     * Gets array of porto object names
     */

    private function getObjectList() {
        global $conf;
        return files::fileList($conf->root . '/obj', true);
    }

    /*
     * Returns array with cron objects of a single porto object
     */

    private function getCronObjNames($objDir) {
        print "Object directory is $objDir\n";
        if (!file_exists($objDir . '/cron')) {
            return false;
        }
        print "Found cron directory.\n";
        $cronList = files::fileList($objDir . '/cron', false);
        if (empty($cronList)) {
            print "NOTICE: empty cron directory\n";
            return false;
        }
        foreach ($cronList as $k => $v) {
            $cronList[$k] = str_replace('.php', '', $v);
        }
        print_r($cronList);
        return $cronList;
    }

}
