<?php

/*
 * Front Controller
 * A gateway to each menu option
 * Checks if the user has access to each funcionality inside /ob/menu directory.
 */

require ($_SERVER['DOCUMENT_ROOT'] . '/conf.php');

require ($conf->root . '/htmlpage.php');
$ob = new htmlpage();


// --- Get menu option:

if (!empty($_GET['m'])) {
    $m = $_GET['m'];
} else {
    $m = 'frames';
}
$i = strpos($m, '.');

//TODO:review

if ($i === false) {
    $obj = 'broker';
    $menuClass = $m;
} else {
    $obj = substr($m, 0, $i);
    $menuClass = substr($m, $i + 1);
}


// --- sanitize:

if ($ob->isClean($menuClass, 'letters') !== true || $ob->isClean($obj, 'letters') !== true) {

    $ob->log('index: bad m parameter, going to error page');
    $obj = 'broker';
    $menuClass = 'errorpage';
}


$ob->log("ob/index: user called option " . $m);



// --- Only allowed users may pass... others will find the login form

if ($oAuth->checkSession() !== true) {

    $ob->log('index: checkSession failed, defaulting to "login" option');
    $obj = 'broker';
    $menuClass = 'login';
} elseif ($oAuth->pass('options', $m) !== true) {

    $ob->log('ob/index: user ' . $oAuth->userId() . ' has no permission for "' . $m . '", defaulting to "errorpage" option');
//print "<pre>"; print_r($_SESSION); print "</pre>";
    $obj = 'broker';
    $menuClass = 'errorpage';
}



// --- Gateway to menu option:

try {

    require($conf->root . '/obj/' . $obj . '/menu/' . $menuClass . '.php');
    $oOption = new $menuClass;
} catch (Exception $e) {

    $ob->log('ob/index: ERROR exec option ' . $m . ': ' . $e);
}

/*
 *  Upon execution, the menu option object will either:
 *  a) print a customized web document and return nothing
 *  b) return the body part
 *
 */

if (empty($_GET['do'])) {

    $out = $oOption->exec();
} else {

    $do = $ob->param('do', 'letters', false);
    if ($do)
        $out = $oOption->$do();
}



/*
 *  If content was returned, we suppose it wasn't printed.
 */

if (!empty($out)) { // let's make a default header and footer for this content:
    require_once('../ihtml.php');
    ihtml::backofficeHeader();
    print $out;
    ihtml::backofficeFooter();
}