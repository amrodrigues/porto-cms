
/*
 * Client data validation
 */

ivalid = new function() {


    /*
     * Test if a value entered by the user is OK or not
     * td is the table cell that changed
     */
    this.testCell = function(td) {

        var k = td.attributes[2].nodeValue; // meta key
        var ty = ud.content[k].type; // itype
        this[ty](td) ? td.style.backgroundColor = '#fff' : td.style.backgroundColor = '#eee';

    };


    // Cell testers ------------

    this.integer = function(td) {

        if (this.isInteger(td.textContent))
            return true;
        else
            return false;
    };

    this.textline = function(td) {

        var len = String(td.textContent).length;
        if (len < 256)
            return true;
        return false;
    };


    this.bool = function(td) {

        var v = $(td.children[0]).checked;
        if (v === true || v === false)
            return true;
        else
            return false;

        //TODO: value is true, not '1'. Check when saving data...
    };


    this.fselect = function(v) {
        //TODO: test with list
        if (this.isInteger(v))
            return true;
        return false;
    };

    this.dbselect = function(v) {
        //TODO: test
        if (this.isInteger(v))
            return true;
        return false;
    };

    this.date = function(v) {
        return true;
        //TODO: code
    };

    this.xhtml = function(v) {
        return true;
    };

    this.text = function(v) {
        return true;
    };

    this.email = function(v) {
        return true;
    };

    this.textselect = function(v) {
        return true;
    };

    this.code = function(v) {
        return true;
    };



    /*
     * Helpers -----
     */

    this.isInteger = function(sText) {
        var ValidChars = "0123456789";
        var IsNumber = true;
        var Char;
        for (i = 0; i < sText.length && IsNumber == true; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) === -1) {
                IsNumber = false;
            }
        }

        return IsNumber;
    };


    /*
     * Helps to test ud.br values
     */
    this.boolTrue = function(v) {
        if (v === '1')
            return true;
        return false;
    };


};