
/* 
 * itype
 * Defines client-side data types
 */

var itype = [{
        integer: {
            desc: 'Integer'
        },
        textline: {
            desc: 'Text line'
        },
        fselect: {
            desc: 'Static selection'
        },
        bool: {
            desc: 'Bool'
        },
        date: {
            desc: 'Date'
        },
        code: {
            desc: 'Code'
        },
        textselect: {
            desc: 'Text Select' //TODO:deprecate to fselect

        },
        dbselect: {
            desc: 'Dynamic selection'
        },
        email: {
            desc: 'Email'
        }

    }];
