
// Client functions

//TODO: remove frame?
//TODO: ajaxify all interface
//TODO: code better



// Hold current object browser pid:
browserOid = null;

// Holds ids of selected objects:

var selected = Array(200); // selected objects
for (f = 0; f < 200; f++)
    selected[f] = 0;


// Hold ids of selected objects:
var toCut = Array(200);
for (f = 0; f < 200; f++)
    toCut[f] = 0;


$(top).keypress(function(event) {
    top.left.keyboard(event);
});


function keyboard(ev) {

    ev.preventDefault();
    switch (ev.which) {

        case 92:
            top.right.history.back();
            break;

        case 98: //b
            top.right.location.href = "/do/?m=broker.browse";
            break;

        case 100: //d
            top.right.location.href = "/do/?m=broker.dashboard";
            break;

        case 97:
            top.right.location.href = "/do/?m=broker.add";
            break;

        case 116:
        case 104:
            top.right.location.href = "/do/?m=broker.browse&oid=1";
            break;

        case 17:
            if (ev.ctrlKey)
                top.right.location.href = "/do/?m=broker.logout";
            break;


    }
}


// Position in hierarchy

var oidbread = Array(0, 0, 0, 0, 0);
var bread = Array('', '', '', '', '');

function addBread(oid, easyname) {

    if (oid === oidbread[4])
        return;

    oidbread[4] = oid;
    bread[4] = easyname;

    s = '';

    for (var f = 0; f < 5; f++) {

        if (oidbread[f] > 0)
            s += '<div id="bread' + f + '"><a target="right" href="/do/?m=broker.browse&oid=' + oidbread[f] + '">' + bread[f] + '</a></div>';

        if (f < 4) {
            oidbread[f] = oidbread[f + 1];
            bread[f] = bread[f + 1];
        }
    }

    $("#bread").html(s);


}


// Disable selection

window.onload = function() {

    var element = document.getElementById('accordion');

    try {

        element.onselectstart = function() {
            return false;
        }; // ie

    } catch (e) {

        try {
            element.onmousedown = function() {
                return false;
            }; // mozilla

        } catch (e) {
        }
    }

};



/*
 *  Handler: When a browser page is loaded:
 */
function browserLoaded(bOid) {
    browserOid = bOid; // holds browser pid
    cutRestore();
}



/*
 *  Publish or unpublish object to the web
 */
function pub(oid) {

    var v = 0;
    var elAnchor = top.right.$('#publish' + oid);

    // --- color
    var cl = $(elAnchor).attr("class");
    if (cl === 'pub0')
        v = 1;

    // --- http

    $.get("/do/?m=broker.publish", 'oid=' + oid + '&op=' + v, function(responseText) {

        if (responseText === "OK") {
            if (v === 0)
                $(elAnchor).removeClass("pub1");
            else
                $(elAnchor).removeClass("pub0");

            $(elAnchor).addClass("pub" + v);

        } else {
            alert("Problems publishing :(");
        }

    }, 'text');

}



/*
 * Deletes an object
 */
function del(oid) {

    if (!confirm("Sure you want to DELETE this object?"))
        return false;

    res = $.get('/do/?m=broker.delete', 'oid=' + oid, function(data) {

        if (res.responseText === 'OK') {

            top.right.$("#li" + oid).fadeOut(100, function() {
            });
            return;
        }

        if (res.responseText === 'CHILD') {

            alert("Before deleting, remove the underlying object(s).");
            return;
        }

        alert("Unknown error: " + res.responseText);

    });

}


function myadd(sclass) {

    loc = '/do/?m=broker.objectadd&class=' + sclass + '&oid=' + top.right.oid;
    top.right.location = loc;
}



function changeIframeSrc(id, url) {

    var el = document.getElementById(id);

    if (el && el.src) {

        el.src = url;
        return false;
    }

    return true;
}


//TODO:deprecate
function row_delete(table, id) {

    go = false;
    eval('$.get("sys_row_delete.php", {table: "' + table + '", id: "' + id + '"}, function(data) { if (data == "OK") div_hide("' + table + '_' + id + '") })');
    if (go === true)
        div_hide(table + "_" + id);
    return true;
}



function div_hide(el) {
    top.right.document.getElementById(el).style.display = "none";
    return true;
}



function easynameSet(oid) {

    var txt = $("#input" + oid, top.right.document).val();

    eval('var res = $.get("/do/?m=broker.easyname", {oid:"' + oid + '", easyname:"' + txt + '"})');

    $("#input" + oid, top.right.document).attr("style", "display:none");

    $("#en" + oid, top.right.document).html('<a class="object-title" href="/do/?m=broker.browse&oid=' + oid + '">' + txt + '</a>');
}


function easynameAsk(oid, baseTxt) {

    var txt = prompt('Name this object', baseTxt);

    if (txt === null)
        return false;

    eval('var res = $.get("/do/?m=broker.easyname", {oid:"' + oid + '", easyname:"' + txt + '"})');

    $("#en" + oid, top.right.document).html('<a class="object-title" href="/do/?m=broker.browse&oid=' + oid + '">' + txt + '</a>');

}


/*
 * Marks or resets object to be moved
 */

function cut(obj, oid) {

    if (obj.className !== 'cut1') {

        // Cut single ---------

        for (var f = 0; f < 100; f++)
        {
            if (toCut[f] === 0)
            {
                toCut[f] = oid;
                break;
            }
        }

        obj.className = 'cut1';

        toggleSelectionMenu();

    } else {

        // Remove single -------

        for (var f = 0; f < 100; f++)
        {
            if (toCut[f] === oid)
            {
                toCut[f] = 0;
            }
        }

        obj.className = 'cut0';

        toggleSelectionMenu();

    }

}



/*
 * Place green on background of X links
 */
function cutRestore() {

    for (var f = 0; f < 100; f++) {
        if (toCut[f] > 0)
            top.right.$('#cut' + toCut[f]).css("backgroundColor", "gray");
    }
}



// -- paste all objects in toCut array

function move() {

    if (!confirm("MOVE selected objects here?"))
        return false;

    var loc = '/do/?m=broker.paste&pid=' + browserOid + '&oids=';

    for (var g = 0; g < 100; g++) {
        if (toCut[g] > 0)
            loc = loc + toCut[g] + ',';
    }

    $.get(loc, function(data) {

        // After moving, rebuilds divs and cleans object selection
        if (data !== 'OK') {
            alert(data);
            return;
        }
        clearSelection(); //reset cut array
        top.right.location.href = '/do/?m=broker.browse';

    });

}


function insertMenuItem(menuGroup, onclick, label, id) {

    if ($('#' + id).length === 0) { // insert all:

        $('#menu-' + menuGroup).prepend('<div id="' + id + '" class="menu-item" onclick="' + onclick + '">' + label + '</div>');

    } else { // just update label:

        $('#' + id).html(label);
    }
}



function duplicate() {

    if (!confirm("DUPLICATE objects here?"))
        return false;

    var loc = '/do/?m=broker.duplicate&pid=' + browserOid + '&oids=';

    for (var g = 0; g < 1000; g++) {

        if (toCut[g] > 0)
            loc = loc + toCut[g] + ',';
    }

    $.get(loc, function(data) {

        // After duplicating, rebuilds divs and cleans object selection

        if (data !== 'OK') {
            alert(data);
            return;
        }

        clearSelection(); //reset cut array
        top.right.location.href = '/do/?m=broker.browse';

    });

}



function clearSelection() {

    top.right.$(".cut1").addClass('cut0');
    for (var f = 0; f < 100; f++)
        toCut[f] = 0;
    hideSelectionMenu();
}

function cutCancel() {

    clearSelection();
    hideSelectionMenu();
}


function hideSelectionMenu() {

    $("#menuItemMove").remove();
    $("#menuItemDuplicate").remove();
    $("#menuItemClear").remove();

}


function countReal(arr) {

    var cnt = 0;
    for (f = 0; f < 100; f++)
        if (arr[f] > 0)
            cnt++;
    return cnt;
}


// --- show paste menu if has items or hide it if empty

function toggleSelectionMenu() {

    var cnt = countReal(toCut);

    if (cnt > 0) {

        var s = '';
        if (cnt > 1)
            s = 's';

        insertMenuItem(
                'main',
                'duplicate();',
                'Duplicate ' + cnt + ' object' + s + '...',
                'menuItemDuplicate'
                );

        insertMenuItem(
                'main',
                'move();',
                'Move ' + cnt + ' object' + s + '...',
                'menuItemMove'
                );

        insertMenuItem(
                'main',
                'clearSelection();',
                'Clear selection',
                'menuItemClear'
                );


    } else {

        hideSelectionMenu();

    }

}




function selectObject(checkBox) {

    var f = 0;

    if (checkBox.checked) {

        // Select single ---------

        for (f = 0; f < 100; f++) {

            if (selected[f] === 0) {
                selected[f] = checkBox.value;
                break;
            }
        }

        displayOrHideEditMenu();

    } else {


        // Remove single -------

        var b = false;

        for (f = 0; f < 100; f++) {

            if (selected[f] === checkBox.value) {
                selected[f] = 0;
            }

            if (selected[f] > 0) {
                b = true;
            }
        }

        displayOrHideEditMenu();
    }
}



// Sets focus on first input control
//TODO:refactor

function setFocus() {

    if (top.right.document.forms.length > 0) {

        var el, type, i = 0, j, els = top.right.document.forms[0].elements;

        while (el = els[i++]) {
            j = 0;
            while (type = arguments[j++])
                if (el.type == type && el.name != 'stxt' && el.name != 'oid')
                    return el.focus();
        }
    }

    return false;
}



// Select / Deselect all objects (checkboxes) -----

function selectAll(toCheck) {

    a = top.right.document.getElementsByName('sel');

    if (toCheck) {

        // Check all -------

        for (f = 0; f < a.length; f++) {

            a[f].checked = true;

            for (g = 0; g < 100; g++) {
                if (selected[g] === 0) {
                    selected[g] = a[f].value;
                    break;
                }
            }
        }

        displayOrHideEditMenu();

    } else {

        // Uncheck all
        // Uncheck controls

        for (f = 0; f < a.length; f++)
        {
            a[f].checked = false;
        }

        // Unckeck array ---

        for (g = 0; g < 100; g++) {
            if (selected[g] === a[f].value) {
                selected[g] = 0;
            }
        }

        displayOrHideEditMenu();
    }

}



// edits objet layout data (layout id and laypos)

function layoutMenu(oid) {

    $.getJSON('/do/?m=broker.brokerset&op=get&oid=' + oid, function(data) {

        var laypos = data.laypos; // object laypos
        var idlayout = data.layout; // object laypos
        var list = data.list;
        var lay = '<form method="get" type="multipart/form-data" action="">';

        // --- layout select box:

        lay += '<br><br> <select id="layout">';

        for (k in list) {
            if (list.hasOwnProperty(k)) {
                lay += '<option value="' + k + '"';
                if (idlayout === k)
                    lay += ' selected="selected"';
                lay += '>' + list[k] + '</option>';
            }
        }
        lay += '</select><br> <br>Layout position / Bootstrap row:<br><br>';


        // --- laypos radio list:

        for (var i = 1; i <= 12; i++) {

            lay += '<input id="laypos" type="radio" name="laypos" value="' + i + '"';
            if (i.toString() === laypos) {
                lay += ' checked';
            }
            lay += '>' + i + ' ';
        }


        lay += '<br><br><input type="submit" value="Modify" onclick="top.left.brokerSet(' + oid + '); return false;">';
        lay += '<input type="button" value="Cancel" onclick="top.left.layoutDialogClose();"> ';
        lay += '</form>';

        top.right.$("#layout-options").html(lay).css("display", "block");

    });

}



// Change broker data for an object

function brokerSet(oid) {

    var laypos = top.right.$('input:radio[name=laypos]:checked').val();
    $.get('/do/?m=broker.brokerset&op=set&oid=' + oid + '&laypos=' + laypos + '&layout=' + top.right.$("#layout").attr("value"), function(data, textStatus, jqXHR) {

        if (jqXHR.statusText === 'OK') {
            layoutDialogClose();

        } else {
            alert("A problem was found while trying to update layout info.");
        }
    });

}



// Close layout dialog box

function layoutDialogClose() {
    top.right.$("#layout-options").css("display", "none").html('');
}




// Deselects all chackboxes
// do not use in object browser

function deselectAllCheckboxes() {

    for (var i = 0; i < document.list.length; i++) {
        fld = document.list.elements[i];

        if (fld.type === 'checkbox') {
            fld.checked = false;
        }
    }
}




// selects all checkboxes
// do not use in object browser

function selectAllCheckboxes() {

    for (var i = 0; i < document.list.length; i++) {

        fld = document.list.elements[i];
        if (fld.type === 'checkbox') {
            fld.checked = true;
        }
    }
}



//TODO: review
function idrole(ob) {

    o = ob.parentNode.firstChild;

    do {
        o.style.backgroundColor = '#fff';
    } while (o = o.nextSibling);

    ob.style.backgroundColor = 'red';
}


//TODO: review
function previewWindow(href) {
    var w = window.open(this.href, 'preview', '');
    w.show();
}


// handle commands through ctrl key
// keycodeFunctions is a map of 'keyNN: function()' where NN is the keyCode

var ctrlKeyEvents = {};

function registerCTRLKeyEvents(keycodeFunctions) {
    ctrlKeyEvents = keycodeFunctions;
    $(top.right.document).keydown(function(ev) {
        //console.log(ev.keyCode);
        if (ev.ctrlKey !== true || ev.keyCode === 17)
            return true;
        ev.preventDefault();
        try {
            eval("ctrlKeyEvents.key" + ev.keyCode + "();");
        } catch (e) {
        }
    });
    $(top.left.document).keydown(function(ev) {
        console.log(ev.keyCode);
        if (ev.ctrlKey !== true || ev.keyCode === 17)
            return true;
        ev.preventDefault();
        try {
            eval("ctrlKeyEvents.key" + ev.keyCode + "();");
        } catch (e) {
        }
    });
}



// Restores checkboxes of selected object in browser

function checkSelectedObjects() {

    a = top.right.document.getElementsByName('sel');

    for (f = 0; f < a.length; f++)
    {
        for (g = 0; g < 100; g++)
        {
            if (selected[g] === a[f].value)
            {
                a[f].checked = true;

                break;
            }
        }
    }

}



function displayOrHideEditMenu() {

    var display = 0;

    for (g = 0; g < 100; g++) {

        if (selected[g] > 0) {
            display = 1;
            break;
        }
    }

    var state = top.left.$("#menu_edit").css('display');

    if (state === 'none')
        state = 0;

    else if (state === 'table-row' || state === 'block')
        state = 1;


    if (display === 1 && state === 0)
        $("#menu_edit").fadeIn();

    else if (display === 0 && state === 1)
        $("#menu_edit").fadeOut();

}





//--- Deletes all selected objects

function serverDeleteSelected() {

    loc = '/do/?m=broker.delete&oid=';

    for (g = 0; g < 100; g++) {
        if (selected[g] > 0) {
            loc = loc + selected[g] + ',';
        }
    }

    $.get(loc, function(data) {

        // After deletion, hides divs and cleans object selection
        if (data !== 'OK') {
            alert('An error was found deleting this object. Please try again later.');
            return;
        }


        // Hide deleted object divs
        for (g = 0; g < 100; g++) {
            if (selected[g] > 0) {
                $("#sel" + selected[g], top.right.document).value = '0';
                $('#' + selected[g], top.right.document).fadeOut('slow', function() {

                    // Animation complete

                });
                selected[g] = 0;
            }
        }

        displayOrHideEditMenu(); // Close edit menu

    });

}



// Unselect all selected objects

function unselectAll() {

    // clear array ---
    for (g = 0; g < 100; g++) {
        selected[g] = 0;
    }

    // clear visible checkboxes

    a = top.right.document.getElementsByName("sel");

    for (var i = 0; i < a.length; i++) {
        a[i].checked = false;
    }

    displayOrHideEditMenu();
}




function isInteger(sText) {

    var ValidChars = "0123456789";
    var IsNumber = true;
    var Char;

    for (i = 0; i < sText.length && IsNumber == true; i++) {
        Char = sText.charAt(i);

        if (ValidChars.indexOf(Char) === -1) {
            IsNumber = false;
        }
    }

    return IsNumber;
}



// For discussion and comments, see: http://remysharp.com/2009/01/07/html5-enabling-script/

var addEvent = (function() {
    if (document.addEventListener) {
        return function(el, type, fn) {
            if (el && el.nodeName || el === window) {
                el.addEventListener(type, fn, false);
            } else if (el && el.length) {
                for (var i = 0; i < el.length; i++) {
                    addEvent(el[i], type, fn);
                }
            }
        };
    } else {
        return function(el, type, fn) {
            if (el && el.nodeName || el === window) {
                el.attachEvent('on' + type, function() {
                    return fn.call(el, window.event);
                });
            } else if (el && el.length) {
                for (var i = 0; i < el.length; i++) {
                    addEvent(el[i], type, fn);
                }
            }
        };
    }
})();


var removeEvent = (function() {
    if (document.addEventListener) {
        return function(el, type, fn) {
            if (el && el.nodeName || el === window) {
                el.removeEventListener(type, fn, false);
            } else if (el && el.length) {
                for (var i = 0; i < el.length; i++) {
                    removeEvent(el[i], type, fn);
                }
            }
        };
    } else {
        return function(el, type, fn) {
            if (el && el.nodeName || el === window) {
                el.detachEvent('on' + type, function() {
                    return fn.call(el, window.event);
                });
            } else if (el && el.length) {
                for (var i = 0; i < el.length; i++) {
                    removeEvent(el[i], type, fn);
                }
            }
        };
    }
})();
