
/*
 * Js client-side table data editor
 */
var ieditor = new function() {

    this.createTable = function(menuClass, rid) {

        // --- Display table with data:
        var uri = '/do/?m=datatable.tableedit&do=getdata&rid=' + rid;

        $.ajax({
            type: "GET",
            url: uri,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: "{}",
            success: function(ud) {

                // Add toolbar header
                top.right.$('#ieditor').append('<div id="ieditorToolbar"></div>');

                // Add add button:
                if (ud.br.addbutton !== undefined) {
                    top.right.$('#ieditorToolbar').append(iform.button('Add', 'top.left.ieditor.onclickAddButton()'));
                }

                // Create+fill table with data:
                top.right.$('#ieditor').append(top.left.ieditor.tableView(ud));


                // make th's sortable ?
                if (ivalid.boolTrue(ud.br.columnsort))
                    top.right.$("#ieditorTable").tablesorter();


                // --- add sortable to last cell of each line:
                top.left.ieditor.makeSortable();

                // --- Detect changes in TD's
                addEvent(top.right.$("#ieditor td"), 'input', function(eventObject) {

                    // Mark element as changed:
                    top.right.$(eventObject.srcElement).addClass("changed");

                    // Change background for valid / invalid content
                    top.left.ivalid.testCell(eventObject.srcElement);
                });


                // --- Detect changes in components:
                // top.right.$("#ieditor select:first option:selected")

                addEvent(top.right.$("#ieditor select, #ieditor input"), 'change', function(eventObject) {

                    // Mark as changed
                    top.right.$(eventObject.srcElement.parentElement).addClass("changed");
                    // Change background for valid / invalid content

                    top.left.ivalid.testCell(eventObject.srcElement.parentElement);
                });

            }

        });

    };






    /*
     * Display table html with data
     */
    this.tableView = function(ud2) {

        if (typeof ud2 !== 'object')
            ud = JSON.parse(ud2);
        else
            ud = ud2;

        localStorage.setItem(top.right.location, ud);

        var str = '<table id="ieditorTable">';


        // --- table head --------------------------------------

        if (ud.br['tableheader'] === '1')
        {
            str += '<thead><tr>';

            // --- left-side buttons ----

            if (ud.br['sortable'] !== undefined)
            {
                str += '<th>Order</th>';
            }


            // --- show each content cell ---

            for (var k in ud.content)
            {
                if (ud.content[k].show === '0')
                    continue;

                str += '<th scope="col" title="' + ud.content[k].name + '">' + ud.content[k].desc + '</th>';
            }

            // --- right-side buttons ---

            if (ud.br['deletebutton'] === '1')
            {
                str += '<th>[x]</th>';
            }

            str += '</tr></thead>';
        }



        // --- table body ---------------------------------------

        str += '<tbody class="ieditorSort">';

        for (var row = 0; row < ud.data.length; row++) {

            str += '<tr id="' + row + '">';

            // ---- left-side buttons ---

            if (ud.br['sortable'] !== undefined)
            {
                str += '<td class="handle"><img src="/do/icons/prior.gif" /></td>';
            }

            // ---- td -------------------

            for (var k in ud.content)
            {
                if (ud.content[k].show === '0')
                    continue;

                str += '<td class="td-' + ud.content[k].type + '"';

                // add row/col info - practical to pass around td objects:

                str += ' ix="' + row + '" k="' + k + '"';

                // use HTML5 content editable or form inputs?:

                switch (ud.content[k].type)
                {
                    case 'textline':
                    case 'integer':
                    case 'smallint':
                    case 'smalltext':
                    case 'float':
                    case 'date':
                    case 'time':
                    case 'code':

                        if (ud.content[k].editable !== '0')
                            str += ' contenteditable="true"';

                        else
                            str += ' style="background-color: #eee"';
                }


                // invokes an iform method with the same name of the meta type
                // passing meta and value:

                str += '>' + iform[ud.content[k].type](ud.content[k], ud.data[row][k]) + '</td>';
            }


            // ---- right-side buttons ---

            if (ud.br['deletebutton'] === '1')
            {
                str += '<td>' + iform.button('x', 'top.left.ieditor.onclickDeleteButton(this)') + '</td>';
            }

            str += '</tr>';
        }

        str += '</tbody></table>';

        return str;
    };



    this.updateRow = function() {

    };

    this.deleteRow = function() {

    };

    this.updateCell = function() {

    };

    this.addRow = function() {

    };

    this.postData = function(url, postData) {
        //TODO:code!
    };

    this.localSave = function() {
        localStorage.setItem(top.right.location, top.right.$("#ieditor"));
        //TODO: udfiky()
    };





    /*
     *  ---- ADD button clicked ----
     */

    this.onclickAddButton = function() {

        ud = typeof ud !== 'object' ? JSON.parse(ud) : ud;

        str = '<tr id="-1">';

        // ---- left-side buttons ---

        if (ud.br['sortable'] !== undefined)        {
            str += '<td class="handle"><img src="/do/icons/prior.gif" /></td>';
        }

        for (var k in ud.content) {

            if (ud.content[k].show === '0')
                continue;

            str += '<td class="td-' + ud.content[k].type + '"';

            // use HTML5 content editable or form inputs?:

            switch (ud.content[k].type) {
                case 'textline':
                case 'integer':
                case 'smallint':
                case 'smalltext':
                case 'float':
                case 'date':
                case 'time':
                case 'code':

                    str += ' contenteditable="true"';
            }

            str += '>';

            if (ud.content[k]['default'])
                value = ud.content[k]['default'];

            else
                value = '';

            str += iform[ud.content[k].type](ud.content[k], value) + '</td>';
        }


        // ---- right-side buttons ---

        if (ud.br['deletebutton'] === '1')       {
            str += '<td>' + iform.button('x', 'top.left.ieditor.onclickDeleteButton(this)') + '</td>';
        }
        str += '</tr>';
        top.right.$('#ieditor tbody').prepend(str);

        //TODO: EVENTS!!!!!????
        //top.left.ieditor.makeSortable();

    };




    /*
     * Delete row. obj is the input element (delete button)
     */
    this.onclickDeleteButton = function(objDeleteButton) {

        //TODO: localStorage should maintain the original table
        //if (!confirm("Sure you want to delete?")) return false;

        $(objDeleteButton).parents("tr").map(function(el, ix) {

            //TODO: get oid of row to remove

            $(this).fadeOut(1000, function() {
                $(this).remove();
            });

        });
    };



    this.rowOid = function(el) {

    };


    // --- make table rows sortable ---
    // Server sends a $ud['br']['sortable'] = 'column';

    this.makeSortable = function() {

        top.right.$("#ieditor tbody").sortable(
                {
                    items: 'tr',
                    handle: '.handle',
                    cursor: 'hand',
                    helper: function(e, ui) {
                        ui.children().each(function() {
                            $(this).width($(this).width());
                        });
                        return ui;
                    }
                }

        );

    };

};