<?php

$conf = new stdClass();

$conf->dbserver		= 'localhost';
$conf->dbname		= '{{dbname}}';
$conf->dbuser		= '{{dbuser}}';
$conf->dbpass		= '{{dbpass}}';

$conf->root		= $_SERVER['DOCUMENT_ROOT'] . '/porto';
$conf->tmpdir		= '/tmp';
$conf->locale		= 'pt_PT';
$conf->timezone		= 'Europe/Lisbon';
$conf->defaultoid		= 2;
$conf->defaultlay		= 6;

$conf->sitename		= 'My Site Name';
$conf->siteurl		= 'http://127.0.0.1';
$conf->adminemail		= 'email@example.com';
$conf->cache		= false;
$conf->stats		= false;

$conf->smtphost		= 'localhost';
$conf->smtpuser		= '';
$conf->smtppass		= '';


?>
