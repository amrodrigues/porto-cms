-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: mydbname
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.13.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `broker`
--

DROP TABLE IF EXISTS `broker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `broker` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `easyname` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pid` int(10) unsigned DEFAULT NULL,
  `class` char(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iduser` int(10) unsigned DEFAULT NULL,
  `idrole` int(10) unsigned DEFAULT NULL,
  `publish` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prior` smallint(5) unsigned DEFAULT '1',
  `laypos` smallint(5) unsigned DEFAULT '1',
  `cnt` int(10) unsigned DEFAULT '0',
  `layout` smallint(5) unsigned DEFAULT NULL,
  `tscreate` datetime DEFAULT NULL,
  `tsupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `broker`
--

LOCK TABLES `broker` WRITE;
/*!40000 ALTER TABLE `broker` DISABLE KEYS */;
INSERT INTO `broker` VALUES (1,'Root object',0,'folder',5,4,NULL,1,1,NULL,NULL,NULL,'2013-09-14 12:53:50'),(2,'Homepage',1,'folder',5,4,'1',1,1,NULL,3,NULL,'2013-09-14 13:56:47'),(3,'Default layout',10,'layout',5,4,'1',2,1,NULL,NULL,NULL,'2013-09-14 13:02:43'),(4,'Admin Role',7,'role',5,4,NULL,2,1,NULL,NULL,NULL,'2013-09-14 13:21:48'),(5,'Admin default user',9,'user',5,4,NULL,3,1,NULL,NULL,NULL,'2013-09-14 13:22:07'),(6,'Settings',1,'folder',5,4,'0',3,1,0,3,'2013-09-14 13:57:29','2013-09-14 13:03:11'),(7,'Roles',6,'folder',5,4,'0',3,1,0,3,'2013-09-14 13:57:47','2013-09-14 13:03:40'),(8,'Content Manager Role',7,'role',5,4,'0',3,1,0,3,'2013-09-14 13:59:56','2013-09-14 13:21:48'),(9,'Users',6,'folder',5,4,'0',4,1,0,3,'2013-09-14 14:00:19','2013-09-14 13:03:40'),(10,'Layouts',6,'folder',5,4,'0',2,1,0,3,'2013-09-14 14:00:45','2013-09-14 13:03:40'),(11,'Default CSS',10,'css',5,4,'1',3,1,0,3,'2013-09-14 14:02:15','2013-09-14 13:02:43'),(12,'Menu',1,'folder',5,4,'0',2,1,0,3,'2013-09-14 14:03:02','2013-09-14 15:14:17'),(13,'Entities',6,'folder',5,4,'0',5,1,0,3,'2013-09-14 14:03:33','2013-09-14 13:03:40'),(14,'Contact form structure',13,'entity',5,4,'1',1,1,0,3,'2013-09-14 14:03:54','2013-09-14 13:06:22'),(15,'Name',14,'property',5,4,'1',2,1,0,3,'2013-09-14 14:04:23','2013-09-14 13:04:54'),(16,'Email',14,'property',5,4,'1',3,1,0,3,'2013-09-14 14:04:44','2013-09-14 13:04:54'),(17,'Message',14,'property',5,4,'1',5,1,0,3,'2013-09-14 14:05:19','2013-09-14 13:06:16'),(18,'Phone',14,'property',5,4,'1',4,1,0,3,'2013-09-14 14:06:08','2013-09-14 13:06:17'),(19,'Page 1',12,'folder',5,4,'1',2,1,0,3,'2013-09-14 14:07:18','2013-09-14 13:48:20'),(20,'Page 2',12,'folder',5,4,'1',3,1,0,3,'2013-09-14 14:07:37','2013-09-14 13:48:20'),(21,'Page 3',12,'folder',5,4,'1',4,1,0,3,'2013-09-14 14:07:49','2013-09-14 13:48:20'),(22,'Page 4',12,'folder',5,4,'1',5,1,0,3,'2013-09-14 14:08:00','2013-09-14 13:48:20'),(23,'Page 5',12,'folder',5,4,'1',6,1,0,3,'2013-09-14 14:08:11','2013-09-14 13:08:20'),(24,'Text',2,'text',5,4,'1',1,1,0,3,'2013-09-14 16:07:39','2013-09-14 15:07:41');
/*!40000 ALTER TABLE `broker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `css`
--

DROP TABLE IF EXISTS `css`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `css` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `css`
--

LOCK TABLES `css` WRITE;
/*!40000 ALTER TABLE `css` DISABLE KEYS */;
INSERT INTO `css` VALUES (11,'body {\r\n background-color:#555;\r\n color: white;\r\n}');
/*!40000 ALTER TABLE `css` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity`
--

DROP TABLE IF EXISTS `entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descr` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity`
--

LOCK TABLES `entity` WRITE;
/*!40000 ALTER TABLE `entity` DISABLE KEYS */;
INSERT INTO `entity` VALUES (14,'Contact');
/*!40000 ALTER TABLE `entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folder`
--

DROP TABLE IF EXISTS `folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folder` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metadescription` text COLLATE utf8_unicode_ci,
  `metakeywords` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folder`
--

LOCK TABLES `folder` WRITE;
/*!40000 ALTER TABLE `folder` DISABLE KEYS */;
INSERT INTO `folder` VALUES (2,NULL,NULL,NULL),(6,'','',''),(7,'','',''),(9,'','',''),(10,'','',''),(12,'','',''),(13,'','',''),(19,'','',''),(20,'','',''),(21,'','',''),(22,'','',''),(23,'','','');
/*!40000 ALTER TABLE `folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `layout`
--

DROP TABLE IF EXISTS `layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `layout` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` text COLLATE utf8_unicode_ci,
  `idcss` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `layout`
--

LOCK TABLES `layout` WRITE;
/*!40000 ALTER TABLE `layout` DISABLE KEYS */;
INSERT INTO `layout` VALUES (3,'<!DOCTYPE html>\r\n<html><head> </head><body>Default layout is working. Edit object 3.<br> {[1]}</body></html>',11);
/*!40000 ALTER TABLE `layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property`
--

DROP TABLE IF EXISTS `property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `xname` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xtype` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xoptional` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xdefault` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xminchars` int(10) unsigned DEFAULT NULL,
  `xmaxchars` int(10) unsigned DEFAULT NULL,
  `xpublish` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `xselect` text COLLATE utf8_unicode_ci,
  `xsql` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property`
--

LOCK TABLES `property` WRITE;
/*!40000 ALTER TABLE `property` DISABLE KEYS */;
INSERT INTO `property` VALUES (15,'name','textline','0','',3,100,'1','',''),(16,'email','email','0','',5,110,'1','',''),(17,'txt','text','0','',10,65535,'1','',''),(18,'tel','textline','1','',9,60,'1','','');
/*!40000 ALTER TABLE `property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perm` text COLLATE utf8_unicode_ci,
  `homeurl` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startobject` int(10) unsigned DEFAULT NULL,
  `logouturl` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (4,'Admin','*','/do/?m=broker.frames&right=/do/?m=broker.browse',1,'/'),(8,'Content Manager','options broker.frames,broker.logout,broker.browse,broker.layoutset,broker.publish,broker.menu,broker.edit,broker.prior,broker.refresh,broker.add,broker.edit,broker.easyname,broker.delete,broker.paste,broker.dashboard,xfile.uploadfiles,image.uploadimages,broker.duplicate\r\nadd text,image,folder,xfile,news,product,code\r\ntraverse text,image,folder,code,clist,reporter,xfile,news,product,code\r\nedit text,image,folder,xfile,news,product,code\r\nmove text,image,folder,xfile,news,product,code\r\ndelete text,image,folder,xfile,news,product,code\r\norder text,image,folder,xfile,news,product,code\r\nview text,image,folder,xfile,news,product,code\r\npublish text,image,folder,xfile,news,product,code','/do/?m=broker.frames&right=/do/?m=broker.browse',1,'/');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_post`
--

DROP TABLE IF EXISTS `sys_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_post` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `xname` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iduser` int(10) unsigned DEFAULT NULL,
  `txt` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_post`
--

LOCK TABLES `sys_post` WRITE;
/*!40000 ALTER TABLE `sys_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_stats`
--

DROP TABLE IF EXISTS `sys_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_stats` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `xtype` int(10) unsigned DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `xuser` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rid` int(10) unsigned DEFAULT NULL,
  `xlog` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_stats`
--

LOCK TABLES `sys_stats` WRITE;
/*!40000 ALTER TABLE `sys_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_status`
--

DROP TABLE IF EXISTS `sys_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_status` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `iduser` int(10) unsigned DEFAULT NULL,
  `xname` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `txt` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_status`
--

LOCK TABLES `sys_status` WRITE;
/*!40000 ALTER TABLE `sys_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idprofile` smallint(5) unsigned DEFAULT NULL,
  `name` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` char(110) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pass` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debugmode` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_userprofile`
--

DROP TABLE IF EXISTS `sys_userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_userprofile` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perm` text COLLATE utf8_unicode_ci,
  `homeurl` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startobject` int(10) unsigned DEFAULT NULL,
  `logouturl` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debugmode` char(1) COLLATE utf8_unicode_ci DEFAULT '0',
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_userprofile`
--

LOCK TABLES `sys_userprofile` WRITE;
/*!40000 ALTER TABLE `sys_userprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_userprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text`
--

DROP TABLE IF EXISTS `text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtitle` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `txt` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text`
--

LOCK TABLES `text` WRITE;
/*!40000 ALTER TABLE `text` DISABLE KEYS */;
INSERT INTO `text` VALUES (24,'Web site under construction','','This is a placeholder for the homepage.<br>');
/*!40000 ALTER TABLE `text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `oid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idrole` smallint(5) unsigned DEFAULT NULL,
  `name` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` char(110) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pass` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `debugmode` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (5,4,'Admin','admin@admin.com','admin','1');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-09-14 20:31:28
