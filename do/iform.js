
/*
 * Form HTML generator
*/

iform = new function() {
    
    this.form = function (action, content) {
       return '<form method="post" enctype="multipart/form-data" action="' + action + '">' + content + '</form>';
    };
    
    
    this.textline = function(meta, value) {
	return value;
	
        //var s = '<input type="text" name="' + meta.name + '" class="textline-text" ';
        //if (value != '') s += 'value="' + value + '"';
        //s += ' />'; 
        //return s;
    };


    this.email = function(meta, value) {
        var s = '<input type="text" name="' + meta.name + '" class="email-text" ';
        if (value !== '') s += 'value="' + value + '"';
        s += ' />';
        return s;
    };
    
    
    this.integer = function(meta, value) {
	return value;
	//return '<input type="text" name="' + meta.name + '" value="' + value + '" />';
    };
    
    
    this.fselect = function(meta, value) {
	var s = '<select name="' + meta.name + '">';
	
	for (var k in meta.select) {
	   s += '<option';

	   if (value === k) s += ' selected="selected"';

	   s += ' value="' + k + '">' + meta.select[k]  + '</option>';
	}

	s += '</select>';
	
	return s;
    };



    this.dbselect = function(meta, value) {
	var s = '<select name="' + meta.name + '">';

	for (var k in meta.select)
	{
	   s += '<option';

	   if (value === k) s += ' selected="selected"';

	   s += ' value="' + k + '">' + meta.select[k]  + '</option>';
	}

	s += '</select>';

	return s;
    };


    this.textselect = function(meta, value) {
	return this.fselect(meta,value);
    };
    
    
    this.smalltext = function(meta, value) {
	return value;
	//return '<input type="text" name="' + meta.name + '" value="' + value + '" />';
    };
    
    
    this.bool = function (meta, value) {
	var s = '<input type="checkbox" value="1" name="' + meta.name + '"';
	
	if (value === '1') s += ' checked="checked"';
	
	s += ' />';
	
	return s;
    };
    
    this.code = function(meta, value) {
	return value;
	//return '<textarea class="itype-code" wrap="soft" name="' + meta.name + '">' + value + "</textarea>";
    };
    
    this.smallint = function(meta,value) {
	return value;
	//return '<input type="text" name="' + meta.name + '" value="' + value + '" />';
	
    };
    
    this.button = function(value, onclick) {
	return '<input type="button" value="' + value + '" onclick="' + onclick + '" />';
	
    };
    
    
};