<?php

/*
 * DOB - Description Object Broker
 */

class dob {

    public $oCache = null;  // memcache client object
    public $enginePool = array(); // a pool of engine objects, for speed
    public $defPool = array(); //def arrays for speed
    public $brokerPool = array(); // broker rows pool
    public $debug = false;

    /*
     * Constructor: loads essential objects
     */

    function dob() {

        //TODO:make $db private to dob()

        global $conf, $oAuth, $oType, $db;
        if (empty($conf)) {
            die("dob: Conf file not loaded. Check if exists.");
        }
        setlocale(E_ALL, $conf->locale);
        date_default_timezone_set($conf->timezone);
        $db = new PDO('mysql:host=' . $conf->dbserver . ';dbname=' . $conf->dbname, $conf->dbuser, $conf->dbpass);
        require_once ('iauth.php');
        $oAuth = new iauth();
        if ($conf->cache) {
            $this->cacheInit(); // connect to memcached server
        }
        require_once ('itype.php');
        $oType = new itype();


        if (!empty($_GET['oid']) && !is_numeric($_GET['oid'])) {
            die("oid is NaN");
        }

        /*
         * Nr. 1: SECURITY: filter all $_GET and $_POST values for visitors.
         */
        if (!empty($_POST) && $oAuth->userIsVisitor()) {
            $this->loadPurifier();
            foreach ($_POST as $k => $v) {
                $_POST[$k] = $this->purifier->purify($v);
            }
            if (!empty($_GET) && count($_GET) > 1) {
                foreach ($_GET as $k => $v) {
                    $_GET[$k] = $this->purifier->purify($v);
                }
            }
        }
    }

    /*
     * Call asHtml to return HTML for an object
     */

    function asHtml($ud) {

        $cl = $ud['br']['class'];

        // --- use an object pool, recycling instantiated objects:
        if (!empty($this->enginePool[$cl])) {
            return $this->enginePool[$cl]->asHtml($ud);
        }

        // --- instantiate new engine, put it in pool, and call asHtml:
        require($GLOBALS['conf']->root . '/obj/' . $cl . '/' . $cl . '.php');
        $this->enginePool[$cl] = new $cl();
        return $this->enginePool[$cl]->asHtml($ud);
    }

    /*
     * A metadata skeleton for an object
     */

    function loadSkel($class) {

        $a_def = '';

        if (!empty($this->defPool[$class])) {
            return $this->defPool[$class];
        }

        require ($GLOBALS['conf']->root . '/obj/' . $class . '/def.php');
        $this->defPool[$class] = $a_def;
        return $a_def;
    }

    /*
     * Get an object as $ud representation
     */

    function get($oid) {

        global $db;

        // --- broker ---
        $oid = (int) $oid;
        $br = $this->getBr($oid);
        if ($br === false) {
            return false;
        }

        // SECURITY:
        if ($GLOBALS['oAuth']->pass('view', $br['class'], $br) !== true) {
            return false;
        }

        // --- Content
        $a_def = $this->loadSkel($br['class']);
        $a_def['br'] = $br;
        $sql = 'select * from ' . $br['class'] . ' where oid=' . $oid;
        $st = $db->query($sql);

        if ($st === false)
            return false;

        $a_content = $st->fetch(PDO::FETCH_ASSOC);

        if ($a_content !== false) {
            foreach ($a_def['content'] as $key => $dummy) {
                $a_def['content'][$key]['value'] = $a_content[$key];
            }
        }

        return $a_def;
    }

    /*
     * Get broker data array for an object
     */

    function getBr($oid) {

        global $oAuth, $db;

        if (!empty($this->brokerPool[$oid])) {
            return $this->brokerPool[$oid];
        }

        $sql = 'select * from broker where oid=' . $oid . $oAuth->sqlRestrict('view');
        $st = $db->query($sql);

        if ($st) {
            $br = $st->fetch(PDO::FETCH_ASSOC);
        }
        if (empty($br['class'])) {
            return false;
        }

        $this->brokerPool[$oid] = $br; // save to cache, for iauth
        return $br;
    }

    /*
     * Get object content
     */

    function getContents($s_class, $i_pid, $s_orderby = 'prior') {
        global $oAuth, $db;
        $sql = 'select ' . $s_class . '.*, broker.easyname from ' . $s_class . ',broker where ' . $s_class . '.oid = broker.oid and broker.pid=' . $i_pid . $oAuth->sqlRestrict('view') . ' order by ' . $s_orderby;
        $st = $db->query($sql);
        if ($st)
            return $st->fetchAll(PDO::FETCH_ASSOC);
        return false;
    }

    /*
     *
     */

    function objectUpdate($ud, $formType, $postData) {

        global $oAuth, $db;

        if ($formType == 'meta') { // object metadata+content
            // Update file extension on file properties
            //  or use the one for current file at server:
            foreach ($ud['content'] as $propName => $meta) {
                if ($meta['type'] == 'userfile' && empty($postData[$propName])) {
                    $postData[$propName] = $meta['value'];
                }
            }

            // $ud will have userfile properties updated (file extensions)

            $oFiles = &$this->helper('files');
            $oFiles->storeAllUploadedFiles($ud); //updates ud props

            $this->insertData($ud, $postData);
            if (!$oAuth->pass('edit', $ud['br']['class'], $ud['br']))
                return false;
            $sql = $this->sqlUpdate($ud);
            if ($this->debug)
                $this->log($sql);
            $db->exec($sql);
            $this->cacheRemove($ud['br']['oid']);
        } else if ($formType == 'br') { // broker table row
            if (!$oAuth->pass('edit', $ud['br']['class'], $ud['br']))
                return false;

            foreach ($ud as $k => $v)
                $ud['br'][$k] = $v;

            $sql = $this->sqlUpdateBroker($ud);

            if ($this->debug)
                $this->log($sql);

            $db->exec($sql);
            $this->cacheRemove($ud['br']['oid']);
        }
    }

    /*
     * Takes a key->value array and fills a $ud['content']
     * Only for data insertion / update
     */

    function insertData(&$ud, $a_data) {

        global $oType;

        if (empty($ud['content'])) {
            return;
        }

        $errors = '';

        foreach ($ud['content'] as $key => $meta) {

            $ud['content'][$key]['value'] = '';
            $value = '';

            if (isset($a_data[$key]))
                $value = $a_data[$key];

            //TODO: the following part is an old hack.
            //Types should be singletons with validation methods.

            $s_type = $meta['type'];
            if ($s_type == 'date') {
                $aa = explode('-', $value);
                if (empty($aa[2])) {
                    $value = '0000-00-00';
                } else {
                    if (strlen($aa[0]) == 2 && strlen($aa[2]) == 4)
                        $value = $aa[2] . '-' . $aa[1] . '-' . $aa[0];
                }
            }
            elseif ($s_type == 'richtext') {
                while (strpos($value, '  ') !== false)
                    $value = str_replace('  ', '', $value);
            } elseif ($s_type == 'textline') {

            } elseif ($s_type == 'bool' && empty($value))
                $value = '0';
            elseif ($s_type == 'id' && $value == '0')
                $value = '';

            elseif ($s_type == 'userfile' && empty($value)) {

                if (!empty($ud['content'][$key]['value']))
                    $value = $ud['content'][$key]['value'];
            }

            $optional = true;
            if (empty($ud['content'][$key]['optional']))
                $optional = false;

            // --- end hack ----
            // Filtering and validating
            //if ($ud['content'][$key]['type'] != 'code')
            //    $value = $this->purifier->purify($value);
            if ($oType->valid($value, $ud['content'][$key]['type'], $optional)) {
                $ud['content'][$key]['value'] = $value;
            } else {
                $errors .= ' ' . $ud['content'][$key]['desc'] . '; ';
            }
        }
        if (empty($errors))
            return true;
        return substr($errors, 0, -2) . '.';
    }

    /*
     * Inserts data into object table
     */

    private function objectInsert(&$ud, $oid = false) {

        if (!$this->dbTableExists($ud['br']['class'])) {
            $this->dbCheckIntegrity();
        }

        //TODO: is $oid param needed?? If so, add oid permission

        global $oAuth, $db;
        if (!$oAuth->pass('add', $ud['br']['class'], $ud['br'])) {
            return false;
        }

        foreach ($ud['content'] as $k => $v) {
            if (!isset($ud['content'][$k]['value'])) {
                $ud['content'][$k]['value'] = '';
            }
        }
        //TODO: remove hack
        foreach ($ud['content'] as $k => $v) {
            if ($v['type'] == 'tscreate' || $v['type'] == 'tsupdate')
                $ud['content'][$k]['value'] = date('YmdHis');
        }
        $sql = 'insert into ' . $ud['br']['class'] . ' (';
        if ($oid)
            $sql .= 'oid ,';
        $a_keys = array_keys($ud['content']);
        foreach ($a_keys as $k)
            $sql .= $ud['content'][$k]['name'] . ", ";
        $sql = substr($sql, 0, strlen($sql) - 2);
        $sql = $sql . ') values (';
        if ($oid)
            $sql .= $ud['br']['oid'] . ' ,';
        foreach ($a_keys as $k) {
            $value = $ud['content'][$k]['value'];
            if (is_array($value))
                $value = serialize($value);

            $sql .= $db->quote($value) . ', ';
        }
        $sql = substr($sql, 0, strlen($sql) - 2);
        $sql = $sql . ")";
        $iAffected = $db->exec($sql);
        $ud['br']['oid'] = $db->lastInsertId();
    }

    /*
     * Creates sql statement for updating an object content
     * Filters data
     */

    function sqlUpdate($ud) {

        global $db;
        $s_sql = 'update ' . $ud['br']['class'] . ' set ';
        foreach ($ud['content'] as $a) {
            if (empty($a['value']))
                $a['value'] = '';
            // serialization at sql level is good:
            if (is_array($a['value']))
                $a['value'] = serialize($a['value']);
            $s_sql .= $a['name'] . "=" . $db->quote($a['value']) . ", ";
        }
        $s_sql = substr($s_sql, 0, strlen($s_sql) - 2);
        $s_sql .= ' where oid=' . $ud['br']['oid'];
        return $s_sql;
    }

    /*
     * Creates an sql statement for updating a row of broker table
     */

    function sqlUpdateBroker($ud) {
        global $db;
        $s = 'update broker set ';
        foreach ($ud['br'] as $k => $v) {
            $s .= $k . "=" . $db->quote($v) . ", ";
        }
        $s = substr($s, 0, strlen($s) - 2);
        $s .= ' where oid=' . $ud['br']['oid'];
        $this->brokerPool[$ud['br']['oid']] = $ud['br'];
        return $s;
    }

    function childrenOf($a_oid) {

        $s_sql = 'select oid,pid,class,easyname,idrole,iduser,layout,laypos,prior from broker where (';
        foreach ($a_oid as $oid)
            $s_sql .= 'pid=' . $oid . ' or ';
        $s_sql = substr($s_sql, 0, strlen($s_sql) - 4);
        $s_sql .= ')' . $GLOBALS['oAuth']->sqlRestrict('view');
        $s_sql .= ' order by prior, oid desc limit 500';
        $st = $GLOBALS['db']->query($s_sql);
        if ($st) {
            $a = $st->fetchAll(PDO::FETCH_ASSOC);
            return $a;
        }
        return false;
    }

    /*
     *
     */

    function children($oid) {

        //TODO: not using broker pool ???

        $sql = 'select * from broker where (pid=' . $oid . $GLOBALS['oAuth']->sqlRestrict('view') . ') order by prior, oid desc limit 500';
        $st = $GLOBALS['db']->query($sql);
        if ($st === false)
            return false;
        $a = $st->fetchAll(PDO::FETCH_ASSOC);
        if (count($a) == 0)
            return false;
        foreach ($a as $aa)
            $this->brokerPool[$aa['oid']] = $aa;
        return $a;
    }

    /*
     *
     */

    function childrenOfClass($class, $pid) {
        $sql = 'select ' . $class . '.* from broker, ' . $class . ' where pid=' . $pid . ' and broker.class="' . $class . '" and broker.oid=' . $class . '.oid';
        $sql .= $GLOBALS['oAuth']->sqlRestrict('view') . ' order by prior';
        return $this->dbSelect($sql);
    }

    /*
     *
     */

    function childrenWith($pid, $oid) {
        $st = $GLOBALS['db']->query('select * from broker where (pid=' . $pid . ' or oid=' . $oid . ')' . $GLOBALS['oAuth']->sqlRestrict('view') . ' order by prior, oid desc');
        if ($st === false)
            return false;
        $a = $st->fetchAll(PDO::FETCH_ASSOC);
        if (count($a) == 0)
            return false;
        foreach ($a as $aa)
            $this->brokerPool[$aa['oid']] = $aa;
        return $a;
    }

    /*
     *
     */

    function getFilename($oid, $ext, $bAbsolute = false) {
        $s_file = "$oid";
        if (!empty($ext))
            $s_file .= '.' . $ext;
        return $this->getDirectory($oid, $bAbsolute) . '/' . $s_file;
    }

    /*
     * Returns a file extension
     */

    function getExtension($fn) {
        $ext = strrchr($fn, ".");
        return strtolower(substr($ext, 1, strlen($ext)));
    }

    /*
     *
     */

    function getDirectory($oid, $bAbsolute = false) {
        $odir = "$oid";
        if ($bAbsolute)
            $path = $_SERVER['DOCUMENT_ROOT'] . '/files';
        else
            $path = '/files';
        if (strlen($odir) == 1) {
            $odir = "$path/0$oid";
        } else {

            if (strlen($odir) > 1)
                $odir = "$path/" . substr("$oid", 0, 2);
        }
        return $odir;
    }

    /*
     *
     */

    function cacheInit() {
        if (empty($GLOBALS['conf']->cache))
            return false;

        if ($this->oCache == null) {
            try {
                $this->oCache = new Memcache();

                $this->oCache->connect('127.0.0.1', 11211);
            } catch (Exception $e) {

                $GLOBALS['conf']->cache = false;
            }
        }
    }

    function cacheAdd($k, $value) { // ext if for additional string
        if ($this->oCache == null)
            return false;
        $this->oCache->set($k . '_' . $GLOBALS['conf']->dbname, $value, false, false);
    }

    function cacheRemove($oid) {
        if ($this->oCache == null)
            return false;
        // crawl graph until top, deleting cache entries:
        while ($oid != 1 && !empty($oid)) {
            try {
                $this->oCache->delete($oid . '_' . $GLOBALS['conf']->dbname);
                $this->oCache->delete($oid . '_opall_' . $GLOBALS['conf']->dbname);
                $this->oCache->delete($oid . '_css_' . $GLOBALS['conf']->dbname);
            } catch (Exception $e) {

                //file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/test.txt', "error memcache $oid\n");
            }
            $oid = $GLOBALS['db']->selectCell('select pid from broker where oid=' . $oid);
        }
    }

    function cacheGet($key) {
        if ($this->oCache == null)
            return '';
        return $this->oCache->get($key . '_' . $GLOBALS['conf']->dbname);
    }

    function directoryList($dir) {
        return $this->fileList($dir, true);
    }

    function fileList($s_dir, $b_dir = false) {
        if (!($p = opendir($s_dir)))
            throw new Exception('Could not open directory ' . $s_dir);
        $a_res = array();
        while ($s_fn = readdir($p)) {
            $b_dir ? $b = is_dir($s_dir . '/' . $s_fn) : $b = is_file($s_dir . '/' . $s_fn);
            if ($b) {
                if (substr($s_fn, 0, 1) != '.' && substr($s_fn, 0, 1) != '_')
                    $a_res[] = $s_fn;
            }
        }
        closedir($p);
        return $a_res;
    }

    function objectGetDirectory($oid, $absolute = 0) {
        $odir = "$oid";
        if ($absolute)
            $path = $_SERVER['DOCUMENT_ROOT'] . '/files';
        else
            $path = '/files';
        if (strlen($odir) == 1) {
            $odir = "$path/0$oid";
        } else {
            if (strlen($odir) > 1)
                $odir = "$path/" . substr("$oid", 0, 2);
        }
        return $odir;
    }

    function objectGetFilename($oid, $ext, $absolute = 0) {
        $s_file = "$oid.$ext";
        $s_path = $this->objectGetDirectory($oid, $absolute) . '/' . $s_file;
        return $s_path;
    }

    function fsCheckDir($dir) {
        if (!is_dir($dir)) {
            $oldumask = umask(0);
            mkdir($dir, 0775);
            umask($oldumask);
        }
    }

    /*
     * ADMIN methods ---------------------------------------------------
     */

    function setObjectName($oid, $sObjectName) {
        global $db;
        $p = $db->prepare('update broker set easyname=:easyname where oid=:oid');
        $p->execute(array(':oid' => (int) $oid, ':easyname' => $sObjectName));
    }

    /*
     * Array of available classes
     */

    function classList($b_hidden = true, $b_shortlist = true, $b_broker = true, $b_data = true) {
        global $conf;
        $a_classes = $this->directoryList($conf->root . '/obj');
        sort($a_classes);
        $res = array();
        foreach ($a_classes as $s) {
            if (substr($s, 0, 1) == '_')
                continue;
            if ($b_hidden || substr($s, 0, 4) != 'sys_') {
                if ($b_broker || $s != 'broker') {
                    if ($b_data || !strpos($s, '_data')) {
                        $a_def = '';
                        require ($GLOBALS['conf']->root . "/obj/$s/def.php");
                        if ($b_shortlist) {
                            $res[$s] = $s;
                        } else {
                            $res[$s] = $a_def['br']['easyname'];
                        }
                    }
                }
            }
        }
        return $res;
    }

    /*
     *
     */

    function simpleNewObject($s_class, $s_easyname, $a_data, $i_pid = 1) {
        global $oAuth, $ob;
        $ud = $this->loadSkel($s_class);
        $ud['br']['easyname'] = $s_easyname;
        $ud['br']['pid'] = $i_pid;
        $ud['br']['user'] = $oAuth->userId();
        $ud['br']['prior'] = 1;
        // To make it easy for the user, by default behavior the parent's layout is applied to child objects
        $ud['br']['layout'] = $ob->dbSelectCell("select layout from broker where oid=$i_pid");
        $this->insertData($ud, $a_data);
        return $this->newObject($ud); // Returns new object id
    }

    /*
     *  Avoids if if if if....
     */

    private function setBrIfEmpty($propertyName, $newValueIfEmpty, &$ud) {
        if (empty($ud['br'][$propertyName])) {
            $ud['br'][$propertyName] = $newValueIfEmpty;
        }
    }

    /*
     * Adds a new object to broker table and object table, and uploads its files
     */

    public function newObject($ud) {

        global $oAuth, $conf;

        $this->setBrIfEmpty('prior', 1, $ud);
        $this->setBrIfEmpty('easyname', $this->defPool[$ud['br']['class']]['br']['easyname'], $ud);
        $this->setBrIfEmpty('layout', $conf->defaultlay, $ud);
        $this->setBrIfEmpty('laypos', 1, $ud);
        $this->setBrIfEmpty('publish', 0, $ud);

        try {

            $data = array(
                'pid' => $ud['br']['pid'],
                'class' => $ud['br']['class'],
                'easyname' => $ud['br']['easyname'],
                'iduser' => $oAuth->userId(),
                'idrole' => $oAuth->userRoleId(),
                'tscreate' => date("Y-m-d H:i:s"),
                'prior' => $ud['br']['prior'],
                'layout' => $ud['br']['layout'],
                'laypos' => $ud['br']['laypos'],
                'publish' => $ud['br']['publish']
            );

            $oidNew = $this->dbInsert('broker', $data);
        } catch (Exception $e) {
            return $this->log("newObject: error inserting: " . $e->__toString());
        }

        if (empty($oidNew)) {
            $this->log('newObject: ERROR: db did not return new id');
            return false;
        }

        $ud['br']['oid'] = $oidNew;

        /*
         * This is done before inserting into the object table because we need
         * to include the file extensions
         */
        $oFile = &$this->helper('files');
        $oFile->storeAllUploadedFiles($ud); // updates $ud with file exts. Needs oid in $ud

        $this->objectInsert($ud, true);
        return $oidNew;
    }

    /*
     *
     */

    function fileExtension($sFilename) {
        $s = strrchr($sFilename, '.');
        return strtolower(substr($s, 1));
    }

    /*
     * Checks data structures integrity.
     * integrity.php bootstraps a database at first login
     * calling this method
     */

    public function dbCheckIntegrity() {

        //TODO: move to menu/integrity.php? Must perform via command line

        $s = '';

        // --- Broker integrity

        $ud = $this->loadSkel('broker');
        $this->classCheckIntegrity($ud);

        // -- all classes
        $a_class_list = $this->classList(true, true, true, true);

        // -- just the ones used:
        $a_used = $this->dbSelectColumn("select distinct(class) from broker");
        $this->log('dbCheckIntegrity: system is using ' . count($a_used) . ' classes');

        // ---- Other classes integrity

        foreach ($a_class_list as $s_class) {

            if (array_search($s_class, $a_used) === false && substr($s_class, 0, 4) !== 'sys_')
                continue;

            $ud = $this->loadSkel($s_class);
            $this->log('dbCheckIntegrity: test class ' . $ud['br']['class']);
            $this->classCheckIntegrity($ud);


            //TODO: check data tables:

            /*
              $s_base_class = substr($s_class, 0, $ipos);

              $fh = $GLOBALS['db']->query('select oid from broker where class="' . $s_base_class . '"');

              $a_instances = $fh->fetchAll(PDO::FETCH_ASSOC);

              if (!empty($a_instances))
              {
              foreach ($a_instances as $a) $this->classCheckIntegrity($ud, $s_class . '_' . $a['oid']);
              }
             */
        }


        // delete unused tables
        //foreach ($a_class_list as $s_class) {
        //if (array_search($s_class, $a_used) === false && $s_class != 'broker' && substr($s_class, 0, 4) != 'sys_')
        //   $db->exec("drop table $s_class");
        //}
        // user

        $s .= "Checking basic roles<br />";

        //TODO: create admin user
        //$this->dbRowExists(false, 'select oid from role where perm="*"', "insert into role (title, perm) values (2, 'Admin')");
        //TODO: create external include for this type of ops...
        //TODO: add more checks


        return $s;
    }

    function dbRowExists($bExists, $sqlTest, $sqlIfCondition, $sqlElse = false) {
        global $db;
        $sp = $db->query($sqlTest);
        $res = $sp->fetch(PDO::FETCH_ASSOC);
        if (empty($res) == $bExists)
            $db->exec($sqlIfCondition);
        elseif (!empty($sqlElse))
            $db->exec($sqlElse);
    }

    /*
     *
     */

    function dbTableExists($sTableName) {
        global $db;
        $sp = $db->query('select 1 from ' . $sTableName);
        if (!$sp) {
            return false;
        }
        return true;
    }

    /*
     *  Get mysql primary type (int, char, etc)
     */

    function dbPrimaryType($s_type) {
        $a = explode('(', $s_type);
        return $a[0];
    }

    function dbGetTable($sTableName) {
        global $db;
        $sp = $db->query('select * from ' . $sTableName);
        if ($sp) {
            return $sp->fetchAll(PDO::FETCH_ASSOC);
        }
        return false;
    }

    function dbDelete($sTableName, $iOid) {
        global $db;
        return $db->exec('delete from ' . $sTableName . ' where oid=' . $iOid);
    }

    /*
     * Insert using a prepared statement for query cache
     */

    function dbInsert($sTableName, $aData) {
        global $db;
        $sql = 'insert into ' . $sTableName . '(';
        $aValues = array();
        $sValues = '';
        foreach ($aData as $k => $v) {
            $sql .= $k . ',';
            $sValues .= ':' . $k . ',';
            $aValues[':' . $k] = $v;
        }
        $sql = substr($sql, 0, -1);
        $sValues = substr($sValues, 0, -1);
        $sql .= ') values (' . $sValues . ')';
        $st = $db->prepare($sql);
        $st->execute($aValues);
        return $db->lastInsertId();
    }

    /*
     * db secure update statement
     */

    function dbUpdate($sTableName, $oidToUpdate, $aValues) {
        global $db;
        $sql = 'udpate ' . $sTableName . ' set ';
        foreach ($aValues as $k => $v) {
            $sql .= $k . ' = :' . $k . ', ';
        }
        $sql = substr($sql, 0, -2);
        $sql .= ' where oid=' . (int) $oidToUpdate;
        try {
            $st = $db->prepare($sql);
            $st->execute($aValues);
        } catch (Exception $e) {
            $this->log("dob:dbupdate $sTableName, $oidToUpdate failed: " . print_r($e, true));
        }
    }

    /*
     * Direct database select
     * Needed for dbselect def properties
     */

    function dbSelect($sql) {
        global $db;
        $q = $db->query($sql);
        if ($q) {
            return $q->fetchAll(PDO::FETCH_ASSOC);
        }
        return false;
    }

    function dbExec($sql) {
        global $db;
        return $db->exec($sql);
    }

    /*
     * SQL select returning a single row
     */

    function dbSelectRow($sql) {
        global $db;
        $q = $db->query($sql);
        if ($q)
            return $q->fetch(PDO::FETCH_ASSOC);
        return false;
    }

    function dbSelectColumn($sql) {
        global $db;
        $q = $db->query($sql);
        if ($q)
            return $q->fetchAll(PDO::FETCH_COLUMN);
        return false;
    }

    function dbSelectKeyValue($sql) {
        global $db;
        $q = $db->query($sql);
        if ($q)
            return $q->fetchAll(PDO::FETCH_KEY_PAIR);
        return false;
    }

    /*
     * Selects a single value from a row and a column
     */

    function dbSelectCell($sql) {
        global $db;
        $q = $db->query($sql);
        if ($q)
            return $q->fetchColumn();
        return $q;
    }

    /*
     * Gets a list of columns in a table
     */

    function dbColumnList($sTable) {
        /*
         * Example:
         *
         * [Field] => oid
         * [Type] => int(10) unsigned
         * [Null] => NO
         * [Key] => PRI
         * [Default] =>
         * [Extra] => auto_increment
         * [primary_type] => int
         */
        global $db;
        $sql = 'show columns from ' . $sTable;
        $q = $db->query($sql);
        if (!$q) {
            return false;
        }
        $aCols = $q->fetchAll(PDO::FETCH_ASSOC);
        if (empty($aCols))
            return false;
        $aRet = array();
        foreach ($aCols as $k => $a) {
            if (!empty($a['Field'])) {
                $sColName = $a['Field'];
                $aRet[$sColName] = $a;
                // -- add basic mysql type:
                $aRet[$sColName]['primary_type'] = $this->dbPrimaryType($a['Type']);
            }
        }
        return $aRet;
    }

    /*
     * Fixes db issues for a class:
     */

    function classCheckIntegrity($ud, $s_table = null) {
        global $db;
        if (empty($s_table))
            $s_table = $ud['br']['class'];
        if ($this->dbTableExists($s_table) === false) {
            return $this->classCreateTable($ud, $s_table);
        }
        if (empty($ud['content']))
            return true;
        $a_field_list = $this->dbColumnList($s_table, true);
        require_once($GLOBALS['conf']->root . '/itype.php');
        $o_type = new itype();
        foreach ($ud['content'] as $s_field => $a) {
            if (empty($o_type->a_data[$a['type']]))
                throw new Exception('cannot find type ' . $a['type'] . ' found in class ' . $ud['br']['class']);
            $s_itype = $o_type->a_data[$a['type']]['mysql'];
            if (empty($a_field_list[$s_field])) {
                // --- Create column
                $sql = 'alter table ' . $s_table . ' add ' . $s_field . ' ' . $s_itype;
                if (!empty($ud['content'][$s_field]['default'])) {
                    $sql .= " default '" . $ud['content'][$s_field]['default'] . "'";
                }
                $db->exec($sql);
            } else {
                // --- Check type mismatch
                $b_type_mismatch = $this->dbPrimaryType($s_itype) != $a_field_list[$s_field]['primary_type'];
                $b_defaultvalue_mismatch = isset($a['default']) && $a['default'] != $a_field_list[$s_field]['Default'];
                //print "type mism: " . $b_type_mismatch . "<br>";
                //print "defaultvalue mism: " . $b_defaultvalue_mismatch . "<br>";
                if ($b_type_mismatch || $b_defaultvalue_mismatch) {
                    $sql = 'alter table ' . $s_table . ' modify ' . $s_field . ' ' . $s_itype;
                    if (isset($a['default']))
                        $sql .= " default '" . $a['default'] . "'";
                    $db->exec($sql);
                }
            }
        }
        if (substr($ud['br']['class'], 0, 4) != 'data')
            $this->fsCheckDir($GLOBALS['conf']->root . '/obj/' . $ud['br']['class']);
        return true;
    }

    function objectMove($source, $newpid) {
        if ($source == $newpid)
            return false;

        //TODO: check: is object being moved below itself

        $sql = "update broker set pid=$newpid where oid=$source";
        $GLOBALS['db']->exec($sql);
        $this->cacheRemove($source);
        return true;
    }

    /*
     * Delete an object by oid
     */

    function objectDelete($id) {
        $id = (int) $id;
        global $oAuth;
        // --- permissions:
        $ud = $this->get($id);
        if (!$oAuth->pass('delete', $ud['br']['class'], $ud['br']))
            return false;
        // return if has child items
        $i = $this->countChild($id);
        if ($i > 0)
            return false;
        // Delete object files
        if ($this->userfileExists($id))
            $this->userfileDelete($id);
        // Delete object properties
        if (!empty($ud['content'])) {
            $s_sql = 'delete from ' . $ud['br']['class'] . ' where oid=' . $id;
            $GLOBALS['db']->exec($s_sql);
        }
        // Delete object registry
        $GLOBALS['db']->exec('delete from broker where oid=' . $id);
        // Delete data table
        $s_data_table = $ud['br']['class'] . $id;
        if ($this->dbTableExists($s_data_table))
            $GLOBALS['db']->exec("drop table $s_data_table");
        // Delete cache
        $this->cacheRemove($id);
    }

    /*
     * Deletes a userfile, including thumbnails
     */

    function userfileDelete($oid) {
        $sOid = "$oid";
        if (!class_exists('files'))
            require_once($GLOBALS['conf']->root . '/files.php');
        $dir = $this->getDirectory($oid, true);
        $aFileList = files::fileList($dir);
        $l = strlen($sOid);
        foreach ($aFileList as $sFn) {
            $part = substr($sFn, 0, $l + 1);
            if ($part == ($sOid . '_') || $part == ($sOid . '.'))
                unlink($dir . '/' . $sFn);
        }
    }

    /*
     * Counts objects below an object
     */

    function countChild($pid) {
        $sp = $GLOBALS['db']->query('select count(oid) as cnt from broker where pid=' . $pid . $GLOBALS['oAuth']->sqlRestrict('view'));
        $a = $sp->fetch(PDO::FETCH_ASSOC);
        if (!empty($a))
            return $a['cnt'];
    }

    function userfileExists($oid) {
        $ud = $this->get($oid);
        if (empty($ud['content']))
            return false;
        foreach ($ud['content'] as $a) {
            if ($a['type'] == 'userfile')
                return true;
        }
        return false;
    }

    function classOf($oid) {
        $sp = $GLOBALS['db']->query('select class from broker where oid=' . (int) $oid);
        $a = $sp->fetch();
        if (!empty($a))
            return $a['class'];
        return false;
    }

    function classCreateTable($ud, $s_table_name = '') {
        global $oType, $db;
        if (empty($ud['br']))
            return $this->log('classCreateTable: ERROR: ud[br] is empty.');
        if (empty($ud['content']))
            return $this->log('classCreateTable: ERROR: ud[content] is empty.');
        if (empty($s_table_name))
            $s_table_name = $ud['br']['class'];
        $ids = array();
        $sql = '';
        foreach ($ud['content'] as $name => $prop) {
            $type = $prop['type'];
            if (isset($prop['default']))
                $default = $prop['default'];
            else
                $default = "";
            if (empty($oType->a_data[$type]))
                throw new Exception('Data type $type does not exist in class ' . $ud['br']['class']);
            $sql .= "$name " . $oType->a_data[$type]['mysql'];
            if ($default)
                $sql .= " default '$default'";
            $sql .= ', ';
            if ($type == 'id')
                array_push($ids, $name);
        }
        $sql = substr($sql, 0, strlen($sql) - 2);
        if (count($ids)) {
            $k = ', key(';
            foreach ($ids as $field)
                if ($field)
                    $k = "$k$field, ";
            $k = substr($k, 0, strlen($k) - 2) . ")";
        } else {
            $k = '';
        }
        $tsql = 'create table ' . $s_table_name . ' (oid int unsigned not null auto_increment';
        if ($sql)
            $tsql = "$tsql, ";
        $sql = $tsql . "$sql, primary key (oid)$k)";
        $db->exec($sql);
        return true;
    }

    /*
     * Changes an object role
     */

    function setObjectRole($oid, $idRole) {
        $GLOBALS['db']->exec('update broker set idrole=' . (int) $idRole . ' where oid=' . (int) $oid);
        // if object becomes unpublished,
        // it must be deleted from memcache:
        //if ($idRole > 1)
        //    $this->cacheRemove($oid);
    }

    /*
     * gets an object role:
     */

    function objectRole($oid) {

        //TODO: is this method needed??!!!
        $sp = $GLOBALS['db']->query('select idrole from broker where oid=' . (int) $oid);
        if ($sp)
            return $sp->fetchColumn();
        return false;
        //TODO: check if this fetchColumn works...
    }

    /*
     * Vertical order of an object:
     */

    function setPrior($oid, $iPrior) {
        $ud = $this->get($oid);
        global $oAuth;
        if (!$oAuth->pass('order', $ud['br']['class'], $ud['br']))
            return false;
        $sql = 'update broker set prior=' . (int) $iPrior . ' where oid=' . (int) $oid;
        $GLOBALS['db']->exec($sql);
    }

    /*
     * Publish object to the web or unpublish it
     */

    function objectPublish($oid, $b = '1') {
        if ($b != '1' && $b != '0') {
            return false;
        }
        global $oAuth, $db, $ob;
        $br = $this->getBr($oid);
        if (!$oAuth->pass('publish', $br['class'], $br)) {
            return false;
        }

        //TODO: Extended permissions

        $sql = 'update broker set publish=' . $b . ' where oid=' . (int) $oid;
        $ob->log('objectPublish: ' . $sql);
        $db->exec($sql);
        $this->cacheRemove($oid); // must be called after db exec
        return true;
    }

    static function errorMessage($s) {
        return '<div class="error">' . $s . '</div>';
    }

    public function allowedClasses($oid) {
        $br = $this->getBr($oid);
        $def = $this->loadSkel($br['class']);
        return $def['classes'];
    }

    public function latestObjectsAdded() {
        global $db;
        $sp = $db->query('select * from broker order by oid desc limit 30');
        if ($sp)
            return $sp->fetchAll();
        return false;
    }

    public function latestObjectsEdited() {
        global $db;
        $sp = $db->query('select * from broker order by tsupdate desc limit 30');
        if ($sp)
            return $sp->fetchAll();
        return false;
    }

    /*
     * Simple logger
     * Production mode must disable this for performance and security reasons
     */

    function log($s, $fatalError = false) {

        global $conf;

        if (is_array($s)) {
            $s = print_r($s, true);
        }

        if ($this->debug) {
            $txt = date("Ymd H:i:s") . ': ' . $s . "\n";
            file_put_contents($conf->tmpdir . '/porto.log', $txt, FILE_APPEND);
            if (empty($_GET)) {
                print $txt . "\n";
            }
            if ($fatalError === true) {
                die();
            }
        }

        return false; // careful, many calls use this false value to return to their callers
    }

    /*
     * Get filtered URL POST parameter
     */

    function postParam($param, $type, $default) {
        if (empty($_POST[$param]))
            return $default;
        //htmlspecialchars prevents js injection:
        $p = htmlspecialchars($_POST[$param]);
        if ($this->isClean($p, $type))
            return $p;
        return $default;
    }

    /*
     * Get filtered URL GET parameter.
     * All input is already filtered by Purify
     */

    function param($param, $type, $default) {
        if (empty($_GET[$param])) {
            return $default;
        }
        //htmlspecialchars prevents js injection:
        $p = htmlspecialchars(urldecode($_GET[$param]));
        if ($this->isClean($p, $type)) {
            return $p;
        }
        return $default;
    }

    /*
     * For security reasons only digits, letters and _ are allowed
     */

    function isClean($s, $type) {
        if (empty($s))
            return false;
        $s = strtolower($s);
        switch ($type) {
            case 'digits':
                $allowed = '0123456789';
                break;
            case 'letters':
                $allowed = 'abcdefghijklmnopqrstuvwxyz';
                break;
            case 'dletters':
                $allowed = 'abcdefghijklmnopqrstuvwxyz-_';
                break;
            case 'alpha':
                $allowed = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_';
                break;
            case 'date':
                $allowed = '-0123456789';
                break;
            case 'digitscomma':
                $allowed = '0123456789,';
                break;
            case 'letterspace':
                $allowed = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ';
            case 'any':
                return true;
            default:
                die("isClean has no type $type");
        }
        $a = str_split($s);
        foreach ($a as $ch)
            if (strpos($allowed, $ch) === false)
                return false;
        return true;
    }

    /*
     * Enables debug mode or production mode
     */

    function debugMode($bDebug) {
        if ($bDebug === true) {
            global $conf;
            ini_set('display_errors', 1);
            ini_set('log_errors', 1);
            ini_set('error_log', $conf->tmpdir . '/porto.log');
            error_reporting(E_ALL);
            $this->debug = true;
        } else {
            ini_set('display_errors', 0);
            ini_set('log_errors', 0);
            error_reporting(E_NONE);
            $this->debug = false;
        }
    }

    /*
     *  "behavior" objects, usually placed inside layouts, like status object, change $conf var
     *  to register behavior for other objects
     *  We use $conf var to register dynamic settings, thus allowing for different settings across layouts
     */

    function registerBehavior($ud) {
        global $conf;
        $class = $ud['br']['class'];
        $a = array('oid' => $ud['br']['oid']);
        foreach ($ud['content'] as $k => $meta) {
            $a[$k] = $meta['value'];
        }
        $conf->$class = $a;
    }

    function behavior($class) {
        global $conf;
        if (!isset($conf->$class))
            return false;

        return $conf->$class;
    }

    function loadPurifier() {
        global $conf;
        if (isset($this->purifier)) {
            return;
        }
        require_once($conf->root . '/obj/broker/mod/htmlpurifier/HTMLPurifier.standalone.php');
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', 'ul,ol,li,b,strong,sup,em,sub,i,br,u,p,a[href]');
        $this->purifier = new HTMLPurifier($config);
    }

    /*
     * Returns obj reference
     */

    function &helper($className) {

        if (!isset($this->$className)) {
            try {
                require_once($className . '.php');
                $this->$className = new $className;
            } catch (Exception $e) {
                throw new Exception("$className does not seem to be a helper class");
            }
        }
        return $this->$className;
    }

}
