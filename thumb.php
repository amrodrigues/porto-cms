<?php

class thumb {

    var $image;
    var $sourceFn;
    var $originalWidth;
    var $originalHeight;
    var $ext;
    var $mw;
    var $mh;

    function source($sourceFn, $mw, $mh) {

        $this->sourceFn = $sourceFn;

        // Get image data

        list($this->originalWidth, $this->originalHeight, $ext) = getimagesize($sourceFn);

        if ($this->originalWidth == 0 || $this->originalHeight == 0)
            throw new Exception("Image dimensions contain zero");

        if ($ext == 2)
            $this->ext = 'jpg'; elseif ($ext == 3)
            $this->ext = 'png';
        else
            throw new Exception("Only png and jpg images supported");

        if (empty($mw))
            $mw = $this->originalWidth;

        if (empty($mh))
            $mh = $this->originalHeight;


        // Reduce size output if original is smaller:

        if ($mw > $this->originalWidth)
            $this->mw = $this->originalWidth;
        else
            $this->mw = $mw;

        if ($mh > $this->originalHeight)
            $this->mh = $this->originalHeight;
        else
            $this->mh = $mh;
    }

    function regularThumb($destFn) {

        $width_ratio = $this->mw / $this->originalWidth;

        if ($this->originalHeight * $width_ratio <= $this->mh) {
            $adjusted_width = $this->mw;
            $adjusted_height = $this->originalHeight * $width_ratio;
        } else {
            $height_ratio = $this->mh / $this->originalHeight;
            $adjusted_width = $this->originalWidth * $height_ratio;
            $adjusted_height = $this->mh;
        }

        $image_p = imagecreatetruecolor($adjusted_width, $adjusted_height);

        $this->loadSourceImage();

        imagecopyresampled($image_p, $this->image, 0, 0, 0, 0, ceil($adjusted_width), ceil($adjusted_height), $this->originalWidth, $this->originalHeight);
        imagejpeg($image_p, $destFn, 97);
    }

    function croppedThumb($w, $h, $destFn) {

        // Original code by Salman Arshad
        // http://salman-w.blogspot.pt/2009/04/crop-to-fit-image-using-aspphp.html

        $this->loadSourceImage();

        $source_width = $this->originalWidth;
        $source_height = $this->originalHeight;

        $source_aspect_ratio = $source_width / $source_height;
        $desired_aspect_ratio = $w / $h;

        if ($source_aspect_ratio > $desired_aspect_ratio) {
            /*
             * Triggered when source image is wider
             */
            $temp_height = $h;
            $temp_width = (int) ($h * $source_aspect_ratio);
        } else {
            /*
             * Triggered otherwise (i.e. source image is similar or taller)
             */
            $temp_width = $w;
            $temp_height = (int) ($w / $source_aspect_ratio);
        }

        /*
         * Resize the image into a temporary GD image
         */

        $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
        imagecopyresampled(
                $temp_gdim, $this->image, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height
        );

        /*
         * Copy cropped region from temporary image into the desired GD image
         */

        $x0 = ($temp_width - $w) / 2;
        $y0 = ($temp_height - $h) / 2;
        $desired_gdim = imagecreatetruecolor($w, $h);
        imagecopy(
                $desired_gdim, $temp_gdim, 0, 0, $x0, $y0, $w, $h
        );

        imagejpeg($desired_gdim, $destFn, 97);
    }

    function loadSourceImage() {

        if ($this->ext == 'jpg')
            $this->image = imagecreatefromjpeg($this->sourceFn);

        elseif ($this->ext == 'png')
            $this->image = imagecreatefrompng($this->sourceFn);
        else
            throw new Exception("Only png and jpg images supported");
    }

    /*
     * Place image inside a thumb having different aspect ratio (square or rectangular)
     */

    function squareThumb($destFn) {

        // Set a maximum height and width

        $width = $this->mw;
        $height = $this->mh;

        // Get dimensions

        list($this->originalWidth, $this->originalHeight) = getimagesize($this->sourceFn);
        if ($this->originalWidth == 0 || $this->originalHeight == 0)
            throw new Exception("Image dimensions contain zero");

        $ratio_orig = $this->originalWidth / $this->originalHeight;

        if ($width / $height < $ratio_orig)
            $width = $height * $ratio_orig;
        else
            $height = $width / $ratio_orig;

        // Resample

        $image_p = imagecreatetruecolor($this->mw, $this->mh);
        $this->loadSourceImage();


        // find center

        $sx = 0;
        $sy = 0;

        if ($this->originalWidth > $this->originalHeight)
            $sx = $width / 2;
        else
            $sy = $height / 2;


        // Output
        imagecopyresampled($image_p, $this->image, 0, 0, $sx, $sy, $width, $height, $this->originalWidth, $this->originalHeight);
        imagejpeg($image_p, $destFn, 95);
    }

    function fileExtension($fn) {
        $s = strrchr($fn, '.');
        return strtolower(substr($s, 1));
    }

    /*
     *
     */

    function deleteThumbs($oid) {
        global $ob;
        $oid = (int) $oid;
        if (empty($oid)) {
            throw new Exception('No oid supplied');
        }
        $dir = $ob->objectGetDirectory($oid, true);
        if (is_dir($dir)) {
            $af = $ob->fileList($dir);
            $ioid = strlen("$oid");
            foreach ($af as $fn) {
                $oidpart = substr($fn, $ioid + 1);
                if ($oidpart == ($oid . '.') || $oidpart == ($oid . '_')) {
                    unlink($dir . '/' . $fn);
                }
            }
        }
    }

    /*
     *
     */

    function resizeToWidth($oid, $ext, $w) {
        global $ob;

        $oid = (int) $oid;
        $dir = $ob->objectGetDirectory($oid, true);
        $fn = $dir . '/' . $oid . '.' . $ext;

        list($origw, $origh, $type, $attr) = getimagesize($fn);

        if ($origw < $w)
            return false;

        $this->source($fn, $w, 9999);
        $this->regularThumb($fn);
    }

    function checkThumb($oid, $ext, $w, $h) {

        if (empty($oid) || empty($ext)) {
            $GLOBALS['ob']->log("thumb->checkthumb: no oid or extension supplied, maybe image was not published.");
            return false;
        }

        $dir = $GLOBALS['ob']->getDirectory($oid, true) . '/';
        $fnthumb = $oid . '_' . $w . '_' . $h . '.' . $ext;
        if (file_exists($dir . $fnthumb))
            return true;
        $fnoriginal = $oid . '.' . $ext;

        if (!file_exists($dir . $fnoriginal))
            return false;

        $this->source($dir . $fnoriginal, $w, $h);
        $this->croppedThumb($w, $h, $dir . $fnthumb);

        return true;
    }

    function thumbUrl($oid, $ext, $w, $h) {


        $this->checkThumb($oid, $ext, $w, $h);
        return $GLOBALS['ob']->getDirectory($oid, false) . '/' . $oid . '_' . $w . '_' . $h . '.' . $ext;
    }

}
