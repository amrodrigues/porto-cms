<?php

/**
 * 	icaptcha
 */
require_once('ihtml.php');

class icaptcha {

    static function is_human() {

        if (empty($_SESSION['captcha']['is_human']))
            return false;

        return true;
    }

    static function place() {

        if (icaptcha::is_human() == true)
            return;

        $GLOBALS['ob']->include_jquery();

        return '
        <div class="xtfo-wrap">
                <div class="xtfo-image"><img alt="" src="/mod/xtfo/xtfo.php" /></div>
                <div class="xtfo-txt"><input class="xtfo-input" onkeyup="xtfotest(this);" type="text" name="xtfo" value="" /></div>
                <div style="width:150px;height:25px;line-height:25px" id="xtfo-success">Type letters in the box</div>
        </div>

        <script>
                function xtfotest(obj)
                {
                        if(obj.value.length == 4)
                        {
                                $("#xtfo-success").load("/mod/xtfo/test.php", { "code" : obj.value } );

                        } else {

                                $("#xtfo-success").html("Edit");
                        }
                }
        </script>

        ';
    }

    static function include_if_not_human() {
        if (icaptcha::is_human() == false) {
            return icaptcha::place();
        }
    }

    static function test_code() {
        if (icaptcha::is_human())
            return 'OK';

        $s_code = strtolower(ihtml::getParam('code', 'letters'));

        if ($s_code == $_SESSION['captcha']['code']) {
            $_SESSION['captcha']['is_human'] = true;

            return 'OK';
        } else {

            return "Edit...";
        }
    }

    static function kill_if_not_human() {
        if (icaptcha::is_human())
            return null;

        $s_code = strtolower(ihtml::getParam('xtfo', 'letters'));


        if ($s_code != $_SESSION['captcha']['code']) {
            $_SESSION['captcha']['is_human'] = false;

            die();
        } else {

            $_SESSION['captcha']['is_human'] = true;

            return true;
        }
    }

    static function has_code() {

        if (!empty($_SESSION['captcha']['code']))
            return true;

        return false;
    }

}

?>