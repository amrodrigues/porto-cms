<?php

/*
 * Smart image presenter
 */

class imagesmart {

    var $th;

    function __construct() {
        require_once($GLOBALS['conf']->root . '/thumb.php');
        $this->th = new thumb();
    }

    /*
     * Checks how many images exist at $pid and returns either the image or a nivoslider
     */

    function imageorslider($pid, $w, $h) {

        global $ob, $oAuth;

        $sql = 'select broker.oid, image.title, image.userfile, image.alt from broker, image where broker.oid=image.oid and pid=' . $pid . ' ' . $oAuth->sqlRestrict('view') . ' order by broker.prior';
        $a = $ob->dbSelect($sql);

        if (empty($a))
            return;

        if (count($a) == 1) {

            $res = $this->th->checkThumb($a[0]['oid'], $a[0]['userfile'], $w, $h);

            if ($res)
                return '<img src="' . $this->th->thumbUrl($a[0]['oid'], $a[0]['userfile'], $w, $h) . '">';

            return '<!-- something went wrong at imagesmart->get() -->';
        }

        return $this->nivoslider($w, $h, $a);
    }

    /*
     * Creates an image slider
     */

    function nivoslider($w, $h, $aImages) {

        global $ob;

        $lay = '';

        $ob->includeJquery();
        $ob->includeScriptTag('/obj/nivoslider/mod/jquery.nivo.slider.pack.js');
        $ob->includeCss('/obj/nivoslider/mod/nivo-slider.css');
        $ob->includeCss('/obj/nivoslider/mod/themes/default/default.css');

        $lay .= '
	 <script type="text/javascript">
	    $(window).load(function() {
                             $(\'#nivoslider\').nivoSlider();
                        });
	</script>
                    <div class="nivoslider theme-default nivoslider">
                    <div id="nivoslider" class="nivoSlider">';

        $cap = '';

        foreach ($aImages as $aa) {

            if ($this->th->checkThumb($aa['oid'], $aa['userfile'], $w, $h)) {

                $lay .= '<img src="' . $this->th->thumbUrl($aa['oid'], $aa['userfile'], $w, $h) . '"';

                if (!empty($aa['title'])) {
                    $lay .= ' title="#nivoslider-caption-' . $aa['oid'] . '"';
                    $cap .= '<div id="nivoslider-caption-' . $aa['oid'] . '" class="nivoslider-allcaptions" style="display:none">
		               <div class="nivoslider-title">' . $aa['title'] . '</div>
		               <div class="nivoslider-txt">' . $aa['alt'] . '</div>
		     </div>';
                }
                $lay .= ' />';
            }
        }

        $lay .= ' </div></div>' . $cap;
        return $lay;
    }

    /*
     * Method for other objects that need to generate html for yt videos
     */

    function youtube($w, $h, $uri) {

        $uri = str_replace('watch?v=', 'v/', $uri);
        $lay = '<object width="' . $w . '" height="' . $h . '">
       <param name="movie" value="' . $uri . '&hl=en&fs=1&"></param>
       <param name="allowFullScreen" value="true"></param>
       <param name="wmode" value="transparent"></param>
       <param name="allowscriptaccess" value="always"></param>
       <embed src="' . $uri . '&hl=en&fs=1&" type="application/x-shockwave-flash" wmode="transparent" allowscriptaccess="always" allowfullscreen="true" width="' . $w . '" height="' . $h . '"></embed></object>';
        return $lay;
    }

}
