<?php

/*
 * A template engine with lousy function parsing, but fast
 * Merges string templates with data replacing {{propertyName}}'s in string with data
 * Parses template functions [[functionName.param1.param2.paramN]]
 */

class itengine {

    var $aData = false; // data to present (either one dimensional array of key => values or a two dimensional array of key => array of key => values)
    var $k = false;     // points to a data key ("row") of $aData to be used by template functions

    /*
     * Sets data by SQL select optionally having [[function]]'s
     */

    function setDataSql($sql) { // sql may contain template function calls
        $disp = $this->dispatch($sql);
        $this->aData = $GLOBALS['ob']->dbSelect($disp);
    }

    /*
     *  Sets data to be merged:
     */

    function setData($a) { // Sets data to be merged:
        $this->aData = $a;
    }

    /*
     * Merges data into a string template having {{propertyName}}'s and/or [[function.param1.paramN]]'s
     */

    function merge($sTemplate) {
        if (empty($this->aData))
            return '';
        $lay = '';
        foreach ($this->aData as $k => $v) {
            $v = str_replace('.', '{*dot*}', $v);
            $templ = $sTemplate;
            if (!is_array($v)) {
                $templ = str_replace('{{' . $k . '}}', $v, $templ); //merge simple k=>v data items by property name
                if (strpos($templ, '[[') !== false) {
                    $this->k = $k; // make data available to template functions
                    $templ = $this->dispatch($templ);
                }
            } else { // assume $v is array of k => v
                foreach ($v as $kk => $vv) {
                    $templ = str_replace('{{' . $kk . '}}', $vv, $templ); //merge simple k=>v data items by property name
                }
                if (strpos($templ, '%{') !== false) {
                    $templ = $this->parseSQL($templ);
                }
                if (strpos($templ, '[[') !== false) {
                    $this->k = $k; // make data available to template functions
                    $templ = $this->dispatch($templ);
                }
                $lay .= $templ;
            }
        }
        return str_replace('{*dot*}', '.', $lay);
    }

    /*
     * Execute each functions embedded with [[functionName.parameter1.parameter2...]]
     * replacing function calls with it's returned values
     * Bool parameter values may be anything !empty()
     */

    function dispatch($s) {
        // if the current result row of data is to be used, $this->context has the value arrays
        do {
            $p1 = strpos($s, '[[');
            if ($p1 === false) {
                return $s;
            }
            $p2 = strpos($s, ']]', $p1);
            if ($p2 === false) {
                return $s;
            }
            $fun = substr($s, $p1 + 2, $p2 - $p1 - 2);
            $aFun = explode('.', $fun);
            if (method_exists($this, $aFun[0])) {
                $ret = $this->$aFun[0]($aFun);
                $s = str_replace('[[' . $fun . ']]', $ret, $s);
            } else {
                return "itengine: Template function not found.";
            }
        } while (true);
    }

    /*
     * Get an image relative url
     * imageurl.oid.userfile
     */

    function imageurl($a) {
        global $ob;
        $oid = $a[1];
        $ext = $a[2];
        return $ob->getFilename($oid, $ext);
    }

    /*
     * Get an image relative url below
     * thumbbelow.oid.width.height
     */

    function thumbbelow($a) {
        $sql = 'select broker.oid, image.userfile from broker, image where broker.pid = ' . $a[1] . ' and broker.oid=image.oid ' . $GLOBALS['oAuth']->sqlRestrict('view') . ' order by prior limit 1';
        $aImage = $GLOBALS['ob']->dbSelectRow($sql);
        require_once('thumb.php');
        $oth = new thumb();
        return $oth->thumbUrl($aImage['oid'], $aImage['userfile'], $a[2], $a[3]);
    }

    /*
     *
     */

    function furl($a) {
        $easyname = str_replace('{*dot*}', '.', $a[1]);
        global $ob;
        return $ob->getFurlFromEasyname($easyname);
    }

    /*
     * objectsbelow.oid.type
     */

    function objectsbelow($args) {
        global $ob;
        $oid = (int) $args[1];
        if ($oid < 1) {
            return '';
        }
        $sql = 'select oid from broker where pid = ' . $oid;
        if (!empty($args[2])) {
            $sql .= ' and class="' . $args[2] . '" ';
        }
        $sql .= $GLOBALS['oAuth']->sqlRestrict('view');
        $sql .= ' order by prior';
        $aChildren = $ob->dbSelectColumn($sql);
        $lay = '';
        if (!empty($aChildren)) {
            foreach ($aChildren as $oid) {

                $lay .= $ob->getHtml($oid, '', false, false);
            }
        }
        return $lay;
    }

    /*
     * object.oid
     */

    function object($args) {
        $oid = (int) $args[1];
        $s = $GLOBALS['ob']->getHtml($oid, '', false, false);
        return $s;
    }

    /*
     * Image icon
     * [[icon.oid.fileExtension.width.square]]
     */

    function iconurl($a) {
        global $ob;
        $url = $ob->getFilename($a[1], $a[2]);
        die("refactor itengine->iconurl");
        $out = '/base/guest/phpthumb/phpThumb.php?src=' . $url . '&w=' . $a[3] . '&f=jpg&q=97';
        if (!empty($a[4])) {
            $out .= '&zc=1&h=' . $a[3];
        }
        return $out;
    }

    /*
     * Gets image below
     * [[imagebelow.whSmall.whBig.Square.UseImageLink]]
     */

    function imagebelow($args) {
        global $ob;
        // context is a news, text or other image container
        $k = $this->k;
        if (empty($this->aData[$k]['oid'])) {
            return 'NO IMAGE PID';
        }
        $imagePid = $this->aData[$k]['oid'];
        // --- image data:
        $aImageData = $ob->dbSelectRow('select broker.oid, image.title, image.linkto, image.userfile from broker, image where broker.oid=image.oid and pid=' . $imagePid . ' order by broker.prior limit 1');
        // --- images below albums
        if (empty($aImageData)) {
            $sql = 'select oid from broker where pid=' . $imagePid . ' and (class="album" or class="galleria")';
            $a = $ob->dbSelect($sql);
            if (!empty($a)) {
                $sql = 'select broker.oid, image.title, image.linkto, image.userfile from broker, image where broker.oid=image.oid and pid=' . $a[0]['oid'] . ' order by broker.prior limit 1';
                $aImageData = $ob->dbSelectRow($sql);
            }
        }
        if (empty($aImageData))
            return '';
        $imageOid = $aImageData['oid'];
        if (!empty($args[1]))
            $whSmall = $args[1];
        else
            $whSmall = false;
        if (!empty($args[2]))
            $whBig = $args[2];
        else
            $whBig = false;
        // --- images directory:
        $dir = "$imageOid";
        if (strlen($dir) == 1)
            $dir = "0$dir";
        $url = '/files/' . substr($dir, 0, 2) . '/' . $imageOid . '.' . $aImageData['userfile'];
        $out = '';
        // Parameter 4 tells if image is to be linked
        $linkImage = false;
        if (!empty($args[4]) && empty($args[2]) && !empty($aImageData['linkto'])) {
            $linkImage = true;
            $out .= '<a href="' . $aImageData['linkto'] . '">';
        }
        if ($whSmall !== false) {
            // make a thumb
            die("refactor itengine->imagebelow");
            $out .= '<img title="' . $aImageData['title'] . '" src="/base/guest/phpthumb/phpThumb.php?src=' . $url . '&f=jpg&q=97&w=' . $whSmall;

            if (!empty($args[3]))
                $out .= '&h=' . $whSmall . '&zc=1';
            $out .= '" />';
            if ($linkImage)
                $out .= '</a>';
            if ($whBig === false)
                return $out;
            // $GLOBALS['ob']->include_html_header('lightbox', '<script type="text/javascript" src="/base/guest/lightbox/lightbox.js"></script><link rel="stylesheet" href="/base/guest/lightbox/lightbox.css" type="text/css" media="screen" />');
//TODO: no base dir anymore, refactor for thumb.php
            return '<a rel="lightbox" href="/base/guest/phpthumb/phpThumb.php?src=' . $url . '&w=' . $whBig . '&f=jpg&q=97">' . $out . '</a>';
        } else {
            // Serve unresized:
            $out .= '<img title="' . $aImageData['title'] . '" src="' . $url . '" />';
            if ($linkImage)
                $out .= '</a>';
            return $out;
        }
    }

    /*
     * Gets address of image below
     *
     */

    function imagebelowurl($args) {

        global $ob;
        // context is a news, text or other image container
        $k = $this->k;
        if (empty($this->aData[$k]['oid']))
            return 'NO IMAGE PID';
        $imagePid = $this->aData[$k]['oid'];
        // --- image data:
        $aImageData = $ob->dbSelectRow('select broker.oid, image.title, image.userfile from broker, image where broker.oid=image.oid and pid=' . $imagePid . ' limit 1');
        if (empty($aImageData))
            return '';
        $imageOid = $aImageData['oid'];
        // --- images directory:
        $dir = "$imageOid";
        if (strlen($dir) == 1)
            $dir = "0$dir";
        return '/files/' . substr($dir, 0, 2) . '/' . $imageOid . '.' . $aImageData['userfile'];
    }

    /*
     * Get an image thumbnail url
     * thumburl.oid.ext.width.height
     */

    function thumburl($a) {

        require_once('thumb.php');
        $oth = new thumb();
        $url = $oth->thumbUrl($a[1], $a[2], $a[3], $a[4]);
        return $url;
    }

    /*
     * Returns site url
     */

    function siteurl($a) {
        return $GLOBALS['conf']->siteurl;
    }

    /*
     * Long date
     */

    function datelong($args) {
        $a = explode('-', $this->aData[$k][$args[1]]);
        return (int) $a[2] . ' de ' . $this->monthname(array('', $a[1])) . ' de ' . $a[0];
    }

    /*
     *  Month name by nr
     */

    function monthname($args) {
        $iMonth = (int) $args[1];
        static $aMonths = array(
            1 => 'Janeiro',
            2 => 'Fevereiro',
            3 => 'Mar&ccedil;o',
            4 => 'Abril',
            5 => 'Maio',
            6 => 'Junho',
            7 => 'Julho',
            8 => 'Agosto',
            9 => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro'
        );
        if (!empty($iMonth))
            return $aMonths[$iMonth];
        return 'NULL';
    }

    /*
     *
     */

    function curmonthname($args) {
        $i = date("m");
        return $this->monthname(array(1 => $i));
    }

    /*
     *
     */

    function nextmonthname($args) {
        $i = (int) date("m");
        $i++;
        if ($i > 12)
            $i = 1;
        return $this->monthname(array(1 => $i));
    }

    /*
     * Inserts case ... when ... end in $sql
     * [[sqldesc.propName]]
     */

    function sqldesc($a) {
        global $ob;
        $propName = $a[1];
        if (!$ob->isClean($propName))
            return "itengine: Par&acirc;ametro 1 s&oacute; pode ter letras min&uacute;sculas e digitos.";
        $aKv = $this->aMeta['content'][$propName]['xselect'];
        $sq = 'case ' . $propName . ' ';
        foreach ($aKv as $k => $v)
            $sq .= 'when ' . (int) $k . ' then "' . trim($v) . '" ';
        $sq .= 'end ';
        return $sq;
    }

    /*
     * Return a HTTP get parameter
     * [[url.paramName.type]]
     */

    function url($a) {
        global $ob;
        $param = $a[1];
        $v = htmlentities(urldecode($_GET[$param]));
        if (empty($v)) {
            return 0;
        }
        if ($ob->isClean($v, $a[2]) === false) {
            return 0;
        }
        return $v;
    }

    /*
     * Returns pid of oid GET parameter
     * [[requestpid]]
     */

    function requestpid($a) {
        global $ob;
        $oid = (int) $_GET['oid'];
        if ($this->testChars("$oid", '1234567890') === false)
            return 0;
        $sql = 'select pid from broker where oid=' . $oid;
        return $ob->dbSelectCell($sql);
    }

    /*
     *
     */

    function parentname($a) {
        global $ob;
        return $ob->dbSelectCell('select easyname from broker where oid=' . $a[1]);
    }

    /*
     * fileurl.oid.ext
     */

    function fileurl($a) {
        return $GLOBALS['ob']->objectGetFilename($a[1], $a[2]);
    }

    /*
     * Tests if a string is exclusively a sequence of $sChars
     */

    private function testChars($s, $sChars) {
        if (empty($s))
            return false;
        $a = str_split($s);
        foreach ($a as $ch)
            if (strpos($sChars, $ch) === false)
                return false;
        return true;
    }

    /*
     *  Good for html tables or wrapper divs:
     */

    function mergeWrapped($sWrapperTemplate, $sTemplate) {
        if (strpos($sTemplate, '%{') !== false) {
            $sTemplate = $this->parseSQL($sTemplate);
        }
        return str_replace('{[1]}', $this->merge($sTemplate), $this->dispatch($sWrapperTemplate));
    }

    /*
     * Allows sql statements inside templates
     */

    public function parseSQL($templ) {
        $templ = $this->dispatch($templ);
        $i = -1;
        do {
            $i = strpos($templ, '%{', $i + 1);
            $ii = strpos($templ, '}%', $i);
            if ($i !== false && $ii !== false) {
                $sql = substr($templ, $i + 2, $ii - $i - 2);
                $aRows = $GLOBALS['ob']->dbSelectColumn($sql);
                if (empty($aRows) && $GLOBALS['oAuth']->userIsAdmin()) {
                    $templ .= "<!-- itengine::parseSQL: no results from embedded sql:\n$sql\n-->";
                }
                if (!empty($aRows)) {
                    // Concatenate sql result:
                    $templ = substr($templ, 0, $i) . '{{sys}}' . substr($templ, $ii + 2);
                    foreach ($aRows as $v) {
                        $templ = str_replace('{{sys}}', $v . '{{sys}}', $templ);
                    }
                    $templ = str_replace('{{sys}}', '', $templ);
                } else {
                    // Remove sql statement:
                    $templ = substr($templ, 0, $i) . substr($templ, $ii + 2);
                }
            } else {
                break;
            }
        } while (true);
        return $templ;
    }

}
