<?php

class junkfilter {

    var $aRegexes = array(
        array('aldactone', 4.9),
        array(' paxil ', 4.9),
        array('doxycycline', 4.9),
        array('lamisil', 4.9),
        array('document\.location', 5),
        array('<script ', 5),
        array('<form\ ', 5),
        array('\[url=', 5),
        array(' buy (a|your) ', 0.5),
        array(' great ', 0.5),
        array(' fatal ', 0.5),
        array(' visit our ', 0.5),
        array(' wassup ', 4),
        array(' whatsup ', 4),
        array(' fuck ', 5),
        array(' hi[!\,\-\ ]', 0.5),
        array(' leia com atenção', 0.5),
        array(' informativo ', 0.5),
        array(' she s(ays|aid)', 0.5),
        array(' say bye ', 0.5),
        array(' say goodbye', 0.5),
        array(' say bye bye', 0.5),
        array(' say by by', 0.5),
        array(' say good-bye', 0.5),
        array(' illegal . detected ', 0.5),
        array(' your wife ', 0.5),
        array(' cheap', 0.4),
        array(' do not wait', 0.3),
        array(' dont wait', 0.3),
        array(' don\'t wait ', 0.3),
        array('\ gambling', 1.5),
        array(' harvard ', 0.1),
        array(' i wanna ', 0.2),
        array(' i wana ', 0.8),
        array(' u wanna ', 0.9),
        array(' you wanna ', 0.9),
        array('\ gambler', 1.5),
        array('\ unique\ sale', 5.0),
        array('\ vida\ sexual', 2.0),
        array('\ sexual\ ', 3.0),
        array('\ sexy\ ', 2.0),
        array('louis vuitton', 2.5),
        array('\ sexual', 1.8),
        array('\ vigor\ ', 3.0),
        array('\ rod\ ', 1.0),
        array('\ pilules?\ ', 8.0),
        array('\ coupon\ ', 4.0),
        array('[vw]+[iy!|]+[a]+[g]+[r]+[a]+', 8.0),
        array('reloj\ de\ pulsera', 8.0),
        array('dream\ girl', 8.0),
        array('get\ hot', 3.0),
        array('\ sensations\ ', 0.5),
        array('\ fuck', 4.0),
        array('guaranteed', 0.2),
        array('diamond', 0.3),
        array('whispering', 5.0),
        array(' apps ', 3.0),
        array(' rolex', 4.0),
        array('pharma', 4.0),
        array('watches', 4.0),
        array(' save up to ', 0.4),
        array('tonight', 0.2),
        array('peepee', 8.0),
        array(' piss ', 4.5),
        array('\ birthday\ e?-?card', 8.0),
        array('russia[n]?', 2.5),
        array(' beauty ', 0.2),
        array('paradise', 1.5),
        array(' girls? ', 2.2),
        array('ladies', 2.2),
        array(' sex pill[s] ', 8.0),
        array('\ lady', 2.2),
        array('(want to see|wanna see) my picture ?\?', 0.5),
        array('[0-9][0-9]\%\ off', 5),
        array('[0-9][0-9]\%', 0.1),
        array('\ penis\ ', 5.0),
        array('vagina', 5),
        array('discount coupon', 2.0),
        array('update your subscription', 5.0),
        array('discount code', 4.0),
        array('weight cutting', 1.9),
        array('weight reduction', 1.9),
        array(' get yourself a', 0.8),
        array(' get your ', 1.0),
        array(' best rolex ', 8.0),
        array(' sex ', 4),
        array('vuitton', 4.0),
        array('lowest\ rates', 1.3),
        array('casino', 0.9),
        array('windows\ vista', 0.3),
        array('service\ pack', 0.3),
        array('lovemaking', 5),
        array('poker', 1),
        array('tablets', 1.0),
        array('\ dong[\ \.\?!-]', 2.0),
        array('pay\ attention', 2.0),
        array('jewelry', 4.7),
        array('jewels', 4.5),
        array('\ hot\ ', 2.0),
        array('\ armani \ ', 5.0),
        array(' oem soft', 4.0),
        array('\ vacancia[s]\ ', 0.5),
        array('dysfunction', 4.0),
        array('\ erectile\ ', 4.5),
        array('\ love\ ', 4.5),
        array(' best watch[es]', 4.0),
        array(' has recommended you', 2.0),
        array(' free\ trial', 3.5),
        array(' fatal ', 0.3),
        array(' sexmed ', 8.0),
        array(' erection ', 1.5),
        array('dating', 4.5),
        array('ultimate', 4.2),
        array('discount', 0.2),
        array('pills', 4.5),
        array('gucci', 4.5),
        array('enlargement', 2),
        array('\ penis\ enlargement', 5),
        array('\ lover', 0.3),
        array('\ status', 0.2),
        array('presentes', 0.2),
        array('monster size', 5.0),
        array(' can be yours', 2.5),
        array('healthy?(est)?', 0.3),
        array('ganhar\ dinheiro', 0.3),
        array('great\ price', 0.2),
        array('oferta\ gratuita', 0.2),
        array('drug\ ?store', 4.0),
        array(' following you on ', 5.0),
        array('conf(|ly)rm', 3.5),
        array('\ boleto\ ', 5.0),
        array('we\ offer\ you', 1.2),
        array('beautiful\ [russian\ ]*wom[ae]n', 4),
        array('xanax', 3.5),
        array('discrete\ packag', 4.5),
        array('banking', 0.3),
        array(' e-card ', 4.8),
        array(' voucher ', 0.3),
        array('click\ here', 1.5),
        array('\ cheap\ ', 1.5),
        array('\ luxury', 2.5),
        array('\ amazing', 2.5),
        array('\ howdy', 3.0),
        array('university\ degree', 3.0),
        array('\ rebate', 3.0),
        array('mr\.\ ', 1.5),
        array('\ beauties', 2.5),
        array('russian\ (wom[ea]n|lad(ie|ies|y|ys|yes))', 5.0),
        array('\ paradise', 1.5),
        array(' hey ', 1.5),
        array('\ charity', 2.5),
        array('temptation', 1.5),
        array('best\ sales', 3.7),
        array('\ award', 0.5),
        array('\ pic', 1, 8),
        array('% off', 2.0),
        array('\ sale\ ', 0.3),
        array('bradesco', 3.0),
        array('brand\ new', 5.0),
        array('\ potency', 2.0),
        array('\ watch', 1.0),
        array('\ joy\ ', 1.5),
        array(' dear user ', 1.0),
        array(' my pic ', 1.0),
        array('testosterone', 4.7),
        array(' your self ', 0.8),
        array('statement', 1.0),
        array('\ wife', 2.0),
        array('\ wives', 2.0),
        array('\ dhl', 1.0),
        array(' free ', 0.2),
        array('degree', 1.0),
        array('unbelievable', 1.0),
        array('exclusive', 0.2),
        array('online\ casino', 3.9),
        array('casino\ online', 2.2),
        array('office.professional', 4.5),
        array('pfizer', 3.4),
        array('\ masters\ ', 1.5),
        array('\ doctorate\ ', 1.5),
        array('\ lovely\ ', 1.4),
        array('\ customer\ satisfaction', 4.0),
        array(' suck ', 2.3),
        array('\ handbag\ ', 3.0),
        array('\ extreme\ ', 1.5),
        array('\ fantasies', 2.4),
        array('\ fantasy', 2.4),
        array('sent you a message on Facebook', 8.0),
        array('\ buy\ ', 2.0),
        array(' medical', 0.3),
        array('\ offer\ ', 0.5),
        array('\ fortune', 0.8),
        array('\ replica\ ', 0.9),
        array('\ bargain', 1.0),
        array('\ deal\ ', 0.8),
        array('\ rel[oó]gio[s]', 1.8),
        array('\ cialis\ ', 5.0),
        array('\ authentic', 0.4),
        array('\ swiss\ ', 0.4),
        array('\ great\ offer', 3.0),
        array('\ agreement', 0.4),
        array('\ gift\ ', 1.5),
        array('discount\ code', 1.0),
        array('\ night', 0.4),
        array('\ celebrity', 0.5),
        array('\ opportunity\ ', 0.5),
        array('\ breitling\ ', 4.5),
        array('\ game\ ', 0.5),
        array('\ compleat', 0.5),
        array('money\ back', 2.5),
        array('\ climax', 2.0),
        array('\ valium\ ', 5.0),
        array('special\ code', 2.0),
        array('\ adobe', 0.5),
        array('important notice', 2.0),
        array('habilite-se\ a\ ganhar', 5.0),
        array('gr[aá]tis', 1.2),
        array('\ marriage', 1.4),
        array('girlfriend', 1.2),
        array('\ cartier\ ', 3.8),
        array('this\ week\ only', 2.3),
        array('elegant', 1.0),
        array('luxury\ watch', 5.0),
        array('\ erect\ ', 2.0),
        array('massive', 1.0),
        array('exquisite', 1.4),
        array('impress\ your\ [girl]*friend'),
        array('insurance', 0.2),
        array('wom[ae]n', 1.2),
        array('greeting\ card', 5),
        array('\ eros\ ', 4.5),
        array('offer[s]\ you', 2.0),
        array('\ enjoy', 0.5),
        array('get\ a\ diploma', 3.5),
        array('get\ your\ diploma', 3.5),
        array('new\ year\ sales', 3.4),
        array('selected\ few', 3.8),
        array('jackpot', 4.5),
        array('lottery', 4.5),
        array('bonus', 2.0),
        array('\ sperm\ ', 3.0),
        array('patek\ philippe', 4.0),
        array('\ ems\ delivery', 2.0),
        array('sale\ day', 3.4),
        array('your\ dreams', 3.4),
        array('dream[s]', 0.3),
        array('save\ [0-9][0-9]\ ?%', 3.5),
        array('weenie', 3.4),
        array('\ sex\ performance', 5),
        array('medication', 2.2),
        array(' bags ', 1.0),
        array('shoes', 1.0),
        array('half price', 1.0),
        array('special[s]', 0.2),
        array('super\ ?sale', 3.4),
        array('g-spot', 4.5),
        array('\ gspot', 3.8),
        array('gee-spot', 4.8),
        array('forex', 2.3),
        array('\ trendy', 1.3),
        array('exchange\ rate', 1.0),
        array('\ libido', 3.5),
        array('updated', 0.4),
        array('account\ agreement', 1.8),
        array('\ breguet\ ', 4.5),
        array('\ postcard', 2.0),
        array('greeting\ card', 5),
        array('\ vicodin\ ', 4.0),
        array('\ percocet', 4.0),
        array('\ brand', 0.4),
        array('\ adderall', 3.5),
        array('\ shop', 0.2),
        array(' online ', 0.3),
        array('\ desconto', 0.3),
        array('go\ to\ bed', 3.4),
        array('in\ bed', 2.3),
        array('beautiful', 0.2),
        array('manhood', 2.0),
        array('promotion', 0.5),
        array('\ timepiece', 2.2),
        array('call\ us', 0.4),
        array('alert', 0.2),
        array('\ virus', 0.5),
        array('infection\ alert', 4),
        array('security', 0.2),
        array(' credit ', 0.4),
        array('\ porn\ ', 4.5),
        array('\ free\ porn[o]', 5),
        array('performance', 0.2),
        array('check\ our', 0.2),
        array('\ intimacy', 2.5),
        array('profitable', 0.3),
        array('\ sensual', 0.3),
        array('\ fashion', 0.5),
        array('\ trend', 0.5),
        array('\ meds\ ', 1.0),
        array('\ pay\ ', 0.1),
        array('\ erotic', 3.5),
        array('private', 0.3),
        array('private\ message', 2.5),
        array('ticket', 0.3),
        array('wonderful', 0.2),
        array(' model[s]? ', 0.1),
        array(' potent ', 0.2),
        array(' license[d]? software', 2.5),
        array('dreamweaver', 1.5),
        array('\.tw\/', 2.0),
        array('money\ back', 1.3),
        array('relationship', 0.2),
        array('couple', 0.2),
        array('parcel', 0.2),
        array('\ pene\ ', 4.6),
        array('desire', 0.2),
        array('romantic', 0.3),
        array('\ pillule', 4.2),
        array('\ male\ ', 0.2),
        array('virus\ alert', 5),
        array('boost', 0.4),
        array('free\ software', 4),
        array('satisfactory', 0.1),
        array('best\ [computer\ ]*software', 2.5),
        array('\ amourous\ ', 1.0),
        array('drugs', 0.5),
        array('income\ statement', 0.7),
        array('\ lust\ ', 3.2),
        array('shagging', 2.0),
        array('\ virility', 3.5),
        array(' weight losing ', 1.0),
        array('delivery', 0.3),
        array('wrist', 0.3),
        array('\ prick\ ', 5.0),
        array('\ regret', 0.4),
        array('passionate', 0.4),
        array(' claim ', 1.0),
        array(' health care', 0.5),
        array(' forget about', 0.3),
        array(' best-quality', 0.3),
        array(' best quality', 0.3),
        array(' gun ', 0.5),
        array(' oem software ', 2),
        array('\d\d% discount', 2)
    );

    /*
     * Detects common upper+lowercase gibberish words used by spam robots
     */

    function isUpperLowercaseGibberish($specimen) {
        if (empty($specimen) || strlen($specimen) < 8) {
            return false;
        }
        $score = 0;
        $words = explode(' ', $specimen);
        foreach ($words as $word) {
            $length = strlen($word);
            if ($length < 4) {
                continue;
            }
            $caseBefore = ctype_upper(substr($word, 1, 1));
            for ($i = 2; $i < $length; $i++) {
                $ch = substr($word, $i, 1);
                if (ctype_alpha($ch) !== true) {
                    continue;
                }
                $caseAfter = ctype_upper($ch);
                if ($caseBefore !== $caseAfter) {
                    $score++;
                }
                $caseBefore = $caseAfter;
            }
        }
        // A high score of upper/lowercase transitions in the same word triggers spam alarm
        if (abs($score / strlen($specimen)) > 0.3) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Test simple text and html spam using property types
     */

    function isSpam($aData, $aMetadata) {

        foreach ($aMetadata['content'] as $prop => $aMeta) {
            // if aData has property filled:

            if (!empty($aData[$prop])) {

                // Test html property:
                if (($aMeta['type'] == 'html' || $aMeta['type'] == 'richtext') && $this->score($_POST[$prop], true)) {
                    return true;
                }

                // Test:

                if ($this->scores($aData[$prop], false)) {
                    return true;
                }
            }
        }

        return false;
    }

    /*
     *
     */

    function scores($s, $bIsHtml = false) {

        $iScore = (float) 0;

        // if it's html, match href attribute with optional spaces:

        if (!$bIsHtml && (preg_match('/http:\/\//i', $s) > 0 || preg_match('/\ href[\ =]{1}/i', $s) > 0)) {
            $iScore += 5;
        }

        // Match regular text spam patterns:

        foreach ($this->aRegexes as $pat) {
            $i = preg_match('/' . $pat[0] . '/i', $s);

            if ($i > 0) {
                $iScore += $pat[1];
            }
        }


        if ($iScore > 5) {
            return true;
        }

        return false;
    }

}
