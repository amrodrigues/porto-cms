<?php

/*
 * Processes file uploads
 */

class files {
    /*
     * Creates a file or image object taking care of the uploaded file
     * $class is 'image' or 'xfile'
     * $pid is the parent id to where the object will be placed
     * $FILESdata is an item from $_FILES with uploaded file data
     */

    function newObject($class, $pid, $FILESdata) {

        $FILESdata['tmp_name'] = trim($FILESdata['tmp_name']);
        $this->checkFileUploadData($FILESdata);

        global $ob;

        $ud = $ob->loadSkel($class);
        $ud['br']['easyname'] = $FILESdata['name'];
        $ud['br']['pid'] = $pid;
        $ud['br']['prior'] = 1;
        $ud['content']['userfile']['value'] = $this->uploadedFileExtension($FILESdata);
        $ud['br']['oid'] = $ob->newObject($ud);
        $this->processUploadedFile($ud['br']['oid'], $FILESdata);

        return $ud;
    }

    /*
     * Gets the file extension in $_FILES data
     */

    function uploadedFileExtension($FILESdata) {

        $sExtension = $this->extension($FILESdata['name']);
        if ($sExtension == 'jpeg') {
            $sExtension = 'jpg';
        }
        return $sExtension;
    }

    /*
     * Blocks operations if there's something wrong with the uploaded file
     */

    function checkFileUploadData($FILESdata) {

        if (empty($FILESdata['tmp_name'])) {
            return false;
        }

        $sTmpFn = $FILESdata['tmp_name'];

        if (!empty($FILESdata['errors'])) {
            throw new Exception('Error on file arrival');
        }

        if (!is_uploaded_file($sTmpFn)) {
            throw new Exception('File ' . $sTmpFn . ' is not an uploaded file in FILES data.');
        }

        $sOriginalName = $FILESdata['name'];
        if (empty($sOriginalName)) {
            throw new Exception('Original file name is empty in FILES data.');
        }

        $s_type = trim($FILESdata['type']);
        if (empty($s_type)) {
            throw new Exception("No filename extension found in FILES data");
        }

        $sExtension = $this->uploadedFileExtension($FILESdata);

        // --- filter exec files

        $aNotAllowed = array('exe', 'bat', 'com', 'pif', 'vbs', 'php');
        if (array_search($sExtension, $aNotAllowed) !== false) {
            throw new Exception('Cannot upload ' . $sExtension . ' files');
        }

        if (is_executable($sOriginalName)) {
            throw new Exception('Cannot upload executable files');
        }
    }

    /*
     *
     */

    public function processUploadedFile($oid, $FILESdata) {

        global $ob;

        if ($this->checkFileUploadData($FILESdata) === false) {
            return false;
        }

        $srcFn = trim($FILESdata['tmp_name']);
        $sExtension = $this->uploadedFileExtension($FILESdata);
        $destFn = $ob->getFilename($oid, $sExtension, true);
        $this->deleteAllFiles($oid);
        $this->moveUploadedFile($srcFn, $destFn);
        return $sExtension;
    }

    /*
     * Stores all files of an object
     * Update $ud with file extensions
     */

    public function storeAllUploadedFiles(&$ud) {

        if (empty($ud['br']['oid'])) {
            throw new Exception('I need oid');
        }

        foreach ($ud['content'] as $aProp) {

            $propName = $aProp['name'];

            // ensure only uploaded files will have extension replaced in $ud

            if ($aProp['type'] !== 'userfile' || empty($_FILES[$propName])) {
                continue;
            }

            $ext = $this->processUploadedFile($ud['br']['oid'], $_FILES[$propName]);

            if ($ext !== false) {
                $ud['content'][$propName]['value'] = $ext;
            }
        }
    }

    /*
     *
     */

    function moveUploadedFile($srcFn, $destFn) {
        $b_result = move_uploaded_file($srcFn, $destFn);
        if (!$b_result) {
            throw new Exception("Cannot save the file you sent");
        }
        chmod($destFn, 0755);
    }

    /*
     *
     */

    function deleteAllThumbs($oid) {
        global $conf;
        require_once($conf->root . '/thumb.php');
        $ot = new thumb();
        $ot->deleteThumbs($oid);
    }

    /*
     *
     */

    function extension($sFilename) {
        $s = strrchr($sFilename, '.');
        return strtolower(substr($s, 1));
    }

    /*
     * Deletes a userfile, including thumbnails
     */

    function deleteAllFiles($oid) {
        global $ob;
        $sOid = "$oid";
        $dir = $ob->getDirectory($oid, true);
        $aFileList = $this->fileList($dir);
        $l = strlen($sOid);
        foreach ($aFileList as $sFn) {
            $part = substr($sFn, 0, $l + 1);
            if ($part == ($sOid . '_') || $part == ($sOid . '.')) {
                unlink($dir . '/' . $sFn);
            }
        }
    }

    /*
     * returns list of directories inside a directory
     */

    static function directoryList($dir) {
        return files::fileList($dir, true);
    }

    /*
     * returns list of files in a directory
     */

    static function fileList($sDir, $bDir = false) {
        if (!file_exists($sDir)) {
            return false;
        }
        $aRes = array();
        $p = opendir($sDir);
        while ($sFn = readdir($p)) {
            if ($bDir) {
                $b = is_dir($sDir . '/' . $sFn);
            } else {
                $b = is_file($sDir . '/' . $sFn);
            }
            if ($b && substr($sFn, 0, 1) != '.' && substr($sFn, 0, 1) != '_') {
                $aRes[] = $sFn;
            }
        }
        closedir($p);
        return $aRes;
    }

    /*
     *
     */

    static function touchDir($s_dir) {
        if (!is_dir($s_dir)) {
            $old_umask = umask(0);
            mkdir($s_dir, 0775);
            umask($old_umask);
        }
    }

    /*
     *
     */

    static function checkFile($s_filename, $s_path, $s_copy_from) {

        if (!file_exists($s_path . '/' . $s_filename)) {
            try {
                copy($s_copy_from . '/' . $s_filename, $s_path . '/' . $s_filename);
            } catch (Exception $e) {

            }
        }
    }

}
