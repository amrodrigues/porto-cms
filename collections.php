<?php

/*
 * Accumulates unique values
 */

class Set implements Iterator {

    private $array = array();
    private $position = 0;

    public function __construct() {
        $this->array = array();
        $this->position = 0;
    }

    public function add($v) {
        if (!is_array($v)) {
            if (array_search($v, $this->array) === false) {
                $this->array[] = $v;
            }
        } else {
            foreach ($v as $vv) {
                if (array_search($vv, $this->array) === false) {
                    $this->array[] = $vv;
                }
            }
        }
    }

    public function asArray() {
        return $this->array;
    }

    public function count() {
        return count($this->array);
    }

    function rewind() {
        $this->position = 0;
    }

    function current() {
        return $this->array[$this->position];
    }

    function key() {
        return $this->position;
    }

    function next() {
        ++$this->position;
    }

    function valid() {
        return isset($this->array[$this->position]);
    }

}

/*
 * Receives requests for counting keys
 */

class Counter {

    private $a = array();

    public function add($k, $cnt) {
        if (!isset($this->a[$k])) {
            $this->a[$k] = 0;
        }

        $this->a[$k] += $cnt;
    }

    public function asArray() {
        return $this->a;
    }

}
