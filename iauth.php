<?php

/*
 *  User access to resources
 *  An admin may have '*' permission (access to all functionality)
 */

class iauth {

    var $roleCache = false; // preloaded role data
    var $userCache = array();

    function __construct() {

        // --- check session:

        if (!empty($_SERVER['REQUEST_URI']) && !session_id())
            session_start();

        //TODO: console session
    }

    /*
     *  Supply proper sql restriction condition for broker queries:
     *  Not related to menu or backoffice access, just broker selects
     */

    function sqlRestrict($op) {

        global $ob;

        if (iauth::userIsAdmin())
            return '';
        if (!empty($_SESSION['role']['sqlrestrict']))
            return $_SESSION['role']['sqlrestrict'];

        // -- build sql restrict string:
        $s = '';

        // --- Visitor:
        if (empty($_SESSION['role'])) {
            $s = ' and broker.publish = "1"';
        } elseif (!empty($_SESSION['role']['perm'][$op]['obj'])) {
            $s = ' and (';
            foreach ($_SESSION['role']['perm'][$op]['obj'] as $class => $dummy) {
                $s .= 'class = "' . $class . '" or ';
            }
            $s = substr($s, 0, -4);
            $s .= ')';
        }


        // --- save in session for later

        $_SESSION['role']['sqlrestrict'] = $s;

        $ob->log("iauth: sqlrestrict: Setting role sql select restriction to: " . $s);

        return $s;
    }

    /*
     *  Setup session for a visitor:
     */

    function setVisitorSession() {
        $_SESSION['iauth'] = array('idrole' => 0); // Visitors have no role
        unset($_SESSION['role']);
        return true;
    }

    /*
     *  Role oid:
     */

    function userRoleId() {
        if (!empty($_SESSION['role']['oid']))
            return $_SESSION['role']['oid'];

        return false;
    }

    /*
     * Returns the role description
     */

    function roleDescr($idRole) {
        return $this->roleCache[$idRole]['title'];
    }

    /*
     * Fetches a role from database
     */

    function dbGetUserRoleData($idRole) {

        if (empty($idRole))
            die("no idRole");

        global $db;
        $sp = $db->query('select * from role where oid=' . $idRole);
        return $sp->fetch(PDO::FETCH_ASSOC);
    }

    function loadRoleCache() {
        if ($this->roleCache !== false)
            return true;

        $fp = $GLOBALS['db']->query('select * from role');

        $a = $fp->fetchAll(PDO::FETCH_ASSOC);

        $this->roleCache = array();

        foreach ($a as $row)
            $this->roleCache[$row['oid']] = $row;

        return true;
    }

    /*
     *  Returns user id
     */

    function userId() {
        if (!empty($_SESSION['iauth']['oid']))
            return $_SESSION['iauth']['oid'];

        return false;
    }

    function userName() {
        if (!empty($_SESSION['iauth']['name']))
            return $_SESSION['iauth']['name'];

        return false;
    }

    function userEmail() {
        if (!empty($_SESSION['iauth']['email']))
            return $_SESSION['iauth']['email'];

        return false;
    }

    /*
     * Preloads user data in userCache property
     */

    function cacheUserRow($userId) {
        if (!empty($this->userCache[$userId]))
            return true;

        global $ob;

        $sql = 'select * from user where oid=' . $userId;

        $ob->log($sql);

        $this->userCache[$userId] = $ob->dbSelectRow($sql);

        if (!empty($this->userCache[$userId]))
            return true;

        return false;
    }

    /*
     * Find a user name by id
     */

    function userNameById($userId) {
        if ($this->cacheUserRow($userId))
            return $this->userCache[$userId]['name'];
    }

    /*
     * Tells if current user is logged in
     */

    function userIsLoggedIn() {

        // Only logged in users have a role:

        if (!empty($_SESSION['iauth']['idrole']) && !empty($_SESSION['role']['oid'])) {

            // a bit of a double-check...

            if ($this->checkSession() === true)
                return true;
        } else {

            return false;
        }
    }

    /*
     *  Tells if user is an admin
     *  An admin can have any role id or user id, it just has full permissions
     */

    static public function userIsAdmin() {

        if (empty($_SESSION['role']['perm']))
            return false;

        if ($_SESSION['role']['perm'] == '*')
            return true;

        return false;
    }

    /*
     * Debugging is user-based, not role
     */

    static public function userIsDebugging() {

        if (!empty($_SESSION['iauth']['debugmode']))
            return true;

        return false;
    }

    /*
     * Tells if user is a visitor
     * Visitors don't have a row in user table
     */

    function userIsVisitor() {
        if (empty($_SESSION['role'])) {
            return true;
        }

        return false;
    }

    /**
     *  Gets a list of available menu options as obj.menuclass list
     */
    function getUserMenuList() {

        if ($this->userIsAdmin()) { // get all menu options
            require_once("files.php");

            global $conf;
            $a = array();

            // dir list:

            $objects = files::fileList($conf->root . '/obj', true);

            if ($objects !== false) {
                foreach ($objects as $object) {

                    $objMenuItems = files::fileList($conf->root . '/obj/' . $object . '/menu');
                    if ($objMenuItems === false)
                        continue;

                    foreach ($objMenuItems as $k => $v)
                        $a[] = $object . '.' . substr($v, 0, strpos($v, '.'));
                }
            }

            $GLOBALS['ob']->log("User menu list is: " . implode(',', $a));

            return $a;
        } else { // custom user, get all menus in role settings
            $a = array_keys($_SESSION['role']['perm']['options']['obj']);

            $GLOBALS['ob']->log("User menu list is: " . implode(',', $a));

            return $a;
        }
    }

    function userStartObject() {
        return $_SESSION['role']['startobject'];
    }

    /*
     * User login HTML form
     */

    function form($s_action) {
        global $ob;

        $ob->log('iauth: generating login form');

        // if uri is not login but was redirected, resume to url where we were

        if (!empty($_GET['m']) && strpos($_SERVER['REQUEST_URI'], 'm=login') === false)
            $_SESSION['iauth']['loginRequestUri'] = $_SERVER['REQUEST_URI'];


        // Start with a fresh session:

        $this->setVisitorSession();


        // Challenge strings for username and password:

        $uchal = $this->randomChars(1200);

        $pchal = $this->randomChars(1200);


        // Save in session for later checking against db:

        $_SESSION['iauth']['uchal'] = $uchal;

        $_SESSION['iauth']['pchal'] = $pchal;

        $_SESSION['iauth']['ip'] = $this->getUserIPAddress();

        $_SESSION['iauth']['ua'] = $_SERVER['HTTP_USER_AGENT'];


        $ob->log("iauth: delivering login form to " . $this->getUserIPAddress());


        // login form:

        return '

			    <img style="padding-left: 20px" src="/do/icons/logo.png" /> <br />

                            <script language="javascript" src="/mod/jquery/sha1.js" type="text/javascript"></script>

			    <div id="iauth-div">

                            <form name="fo" method="post" action="' . $s_action . '" onsubmit="if (typeof hex_sha1 !== \'function\') return false; this.usha.value=hex_sha1(this.u.value + \'' . $uchal . '\'); this.psha.value=hex_sha1(this.p.value + \'' . $pchal . '\'); this.u.value=\'\'; this.p.value = \'\'; return true;">

                            <div id="iauth-user">
                                    <p>Email </p><input type="text" name="u" size="40" autofocus>
                            </div>

                            <div id="iauth-pass">
                                    <p>Password </p> <input type="password" name="p" size="40">
                            </div>

                            <div id="iauth-submit">
                                    <input type="submit" value="Login"></td>
                            </div>

                                    <input type="hidden" name="usha">
                                    <input type="hidden" name="psha">
                            </form>

                    </div>

                    ';
    }

    /*
     * After posting username and password in form(), this checks if the user is registered in database:
     */

    function validate($s_client_user_hash, $s_client_pass_hash, $s_user_key = 'email', $s_pass_key = 'pass') {

        global $ob;

        $a_users = $ob->dbGetTable('user');

        foreach ($a_users as $a_user) {

            $usha = sha1($a_user[$s_user_key] . $_SESSION['iauth']['uchal']);
            $psha = sha1($a_user[$s_pass_key] . $_SESSION['iauth']['pchal']);

            // Test if this db user is the same as our client:

            if (
                    $usha == $s_client_user_hash &&
                    $psha == $s_client_pass_hash &&
                    $_SESSION['iauth']['ua'] == $_SERVER['HTTP_USER_AGENT'] &&
                    $_SESSION['iauth']['ip'] == $this->getUserIPAddress()
            ) {

                // Grant access:

                $ob->log("iauth: validate: ACCESS GRANTED");

                $this->registerPerm($a_user['idrole']);


                // --- Register user data in session preserving ip and user agent data already there:

                foreach ($a_user as $k => $v)
                    $_SESSION['iauth'][$k] = $v;

                session_regenerate_id();

                unset($_SESSION['iauth']['uchal']);
                unset($_SESSION['iauth']['pchal']);

                // --- redir to user home url or previous url

                if (!empty($_SESSION['iauth']['loginRequestUri']))
                    $url = $_SESSION['iauth']['loginRequestUri'];
                else
                    $url = $_SESSION['role']['homeurl'];


                $ob->log("iauth: validate: access granted, redir to $url");

                header('Location: ' . $url);

                die();
            }
        }

        $GLOBALS['ob']->log("iauth: validate: Access denied for ip " . $this->getUserIPAddress());

        return false;
    }

    /*
     * Checks if user is allowed to do something
     */

    function pass($op, $obj, $br = false) {

        // --- special cases:
        // public viewing for published objects

        if ($op == 'view' && $br['publish'] == '1')
            return true;

        // admin permissions, access all areas

        if ($_SESSION['role']['perm'] == '*') {

            $GLOBALS['ob']->log('iauth: pass: ADMIN perm, cleared for ' . $op . ' ' . $obj);
            return true;
        }

        // operation / line wildcard:

        if ($op != 'options' && !empty($_SESSION['role']['perm'][$op]) && $_SESSION['role']['perm'][$op] == '*') {

            $GLOBALS['ob']->log('iauth: pass: Operation wildcard perm, cleared for ' . $op . ' ' . $obj);
            return true;
        }

        // access to login form to everyone:

        if ($obj == 'login' || $obj == 'logout')
            return true;


        // ------ start regular analysis:

        if (empty($_SESSION['role']['perm'][$op]['when'])) {

            $GLOBALS['ob']->log("... no 'when' conditions, testing $op $obj");

            if (!empty($_SESSION['role']['perm'][$op]['obj'][$obj])) {
                $GLOBALS['ob']->log('iauth: granted');
                return true;
            }

            $GLOBALS['ob']->log("iauth: DENIED because ['perm'][$op]['obj'][$obj] is EMPTY:");
            $GLOBALS['ob']->log('iauth: ' . print_r($_SESSION['role']['perm'], true));

            return false;
        } else {

            $GLOBALS['ob']->log('...test each "when" condition: ' . print_r($_SESSION['role']['perm'][$op]['when'], true));

            if ($br !== false) {

                $GLOBALS['ob']->log('... br is not false, testing each condition');

                foreach ($_SESSION['role']['perm'][$op]['when'] as $a) {
                    if ($br[$a['k']] == $a['v'])
                        return true;
                }
            } else {

                $GLOBALS['ob']->log('... br is false, granted');
                return true;
            }

            return false;
        }
    }

    /*
     * User permissions. See DOCS for details about permissions format.
     * This method parses permissions from db and stores in $_SESSION
     */

    function registerPerm($idRole) {

        global $ob;

        $ob->log("iauth:registerPerm: enter");

        if (empty($idRole))
            die("iauth:registerPerm: no idRole");

        if (!is_numeric($idRole))
            die("iauth:registerPerm: idRole not numeric");

        $_SESSION['role'] = $this->dbGetUserRoleData($idRole);

        // will parse permission text lines:

        $sPerm = $_SESSION['role']['perm'];

        if (empty($sPerm))
            return false;

        if (trim($sPerm) == '*') {
            $_SESSION ['role']['perm'] = '*';
            return true;
        }

        $sPerm = str_replace('=', ' = ', $sPerm);
        $sPerm = str_replace('  ', ' ', $sPerm);

        $aLines = explode("\n", $sPerm);

        $perm = array();

        $r = "/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[\s,]+/";

        foreach ($aLines as $line) {

            $line = trim($line);
            if ($line == '' || substr($line, 0, 2) == '//')
                continue;

            $a = preg_split($r, $line, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

            $op = $a[0];

            // wildcard permission:

            if ($a[1] == '*') {
                $perm[$op] = '*';
                continue;
            }

            $perm[$op] = array('obj' => array(), 'when' => array());

            unset($a[0]);

            // parse objects:

            foreach ($a as $k => $v) {
                if ($v == 'when')
                    break;
                $perm[$op]['obj'][$v] = true;
            }

            // parse "when":

            $a = array_slice($a, $k);

            $key = true;

            foreach ($a as $v) {
                if ($key === true) {
                    $getkey = $v;
                    $key = false;
                    continue;
                }
                if ($v == 'or') {
                    $key = true;
                    continue;
                }
                if ($v == '=') {
                    $value = true;
                    continue;
                }
                if ($value === true) {
                    $perm[$op]['when'][] = array('k' => $getkey, 'v' => $v);
                    $value = false;
                }
            }

            // to avoid many function calls, will test if false:

            if (empty($perm[$op]['when']))
                $perm[$op]['when'] = false;
        }

        $_SESSION['role']['perm'] = $perm;

        $ob->log('iauth: registerPerm: ' . print_r($_SESSION['role'], true));
    }

    /**
     *  Checks session vars, client ip & user agent string
     */
    function checkSession() {

        global $ob;

        if (empty($_SESSION['iauth']['idrole'])) {
            $ob->log(print_r($_SESSION, true));
            return $ob->log("checkSession: NO idrole in iauth session var");
        }

        if (empty($_SESSION['iauth']['oid']))
            return $ob->log("checkSession: NO user oid in iauth session var");

        if (
                $_SERVER['HTTP_USER_AGENT'] == $_SESSION['iauth']['ua'] &&
                $this->getUserIPAddress() == $_SESSION['iauth']['ip'] &&
                !empty($_SESSION['iauth']['idrole'])
        )
            return true;

        return $ob->log("checkSession: SECURITY: user agent or ip address not equal to those in session vars");
    }

    /*
     *  Destroys all session data
     */

    function userLogout() {
        global $ob;

        $ob->log("iauth:userLogout");

        session_destroy();
    }

    /*
     *  Destroys all session data
     */

    function logoutUrl() {
        if (!empty($_SESSION['role']['logouturl']))
            return $_SESSION['role']['logouturl'];

        return false;
    }

    /**
     *  @return string Client IP address
     */
    function getUserIPAddress() {

        if (isset($_SERVER)) {

            if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
                $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            } elseif (isset($_SERVER["HTTP_CLIENT_IP"])) {
                $ip = $_SERVER["HTTP_CLIENT_IP"];
            } else {

                $ip = $_SERVER["REMOTE_ADDR"];
            }
        } else {

            if (getenv('HTTP_X_FORWARDED_FOR')) {
                $ip = getenv('HTTP_X_FORWARDED_FOR');
            } elseif (getenv('HTTP_CLIENT_IP')) {
                $ip = getenv('HTTP_CLIENT_IP');
            } else {

                $ip = getenv('REMOTE_ADDR');
            }
        }

        return $ip;
    }

    /**
     *
     *  @param $i_length integer Size of random string
     *  @return string  Random string of characters
     */
    function randomChars($i_lenght) {
        $s_possible = '23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';

        $i_poslen = strlen($s_possible);

        $s = '';

        for ($f = 0; $f < $i_lenght; $f++) {
            $s .= substr($s_possible, (rand() % $i_poslen), 1);
        }

        return ($s);
    }

}
