<?php

require_once('itype.php');

/*
 * Generates html forms
 */

class editor {

    private $sCancelCaption = 'Cancel';
    private $sCancelUrl = '';
    private $sSubmitCaption = ' OK ';
    private $aData = false;
    private $sAction = '';
    private $iStoreOid = false; //??
    private $datatableOid = false;
    private $sFormType = false;
    private $bInsertRte = false;
    private $bAjax = false;
    private $topButtons = true;
    private $formOid = null;
    private $bIntegrateAceEditor = false;

    /*
     *
     */

    function asHtml($ud) {

        global $ob, $conf;

        // needed to fill <form id> ,for ajax

        if ($this->datatableOid === false) {
            $this->datatableOid = $ud['br']['oid'];
        }

        $formId = 'i' . $this->formOid;

        if ($this->sFormType == 'br') {  // Object broker data - must fetch metadata because $ud['br'] is a simple array
            $ud2 = $ob->loadSkel('broker');

            $aMetadata = $ud2['content']; // br metadata is in content
            foreach ($aMetadata as $k => $v) {
                $aMetadata[$k]['value'] = $ud['br'][$k];
            }
        } elseif ($this->sFormType == 'meta') { // object content
            $aMetadata = $ud['content'];

            foreach ($ud['content'] as $k => $meta) {
                if (!empty($meta['value'])) {
                    $this->aData[$k] = $meta['value']; //TODO makes sense??
                }
            }
        } else {

            throw new Exception('Unknown form type');
        }

        // --- get html form

        $oType = new itype();
        $s = '';
        $bHasError = false;

        foreach ($aMetadata as $sColumn => $aMeta) {

            $s .= '<label class="form-label">' . $aMeta['desc'];

            // Ask for correction
            if (isset($aMeta['error']) && $aMeta['error'] === true) {
                $bHasError = true;
                $s .= '&nbsp;&nbsp;<span class="form-error">[Please check]</span>';
            }

            // Optional property?
            if (!empty($aMeta['optional'])) {

                $s .= '&nbsp;<span class="form-optional">';
                if (!empty($conf->behavior['form.label.optional'])) {
                    $s .= $conf->behavior['form.label.optional'];
                } else {
                    $s .= 'Optional';
                }
                $s .= '</span>';
            } else {

                // Use default value?
                if ($this->sFormType == 'meta' && empty($this->aData[$sColumn]) && !empty($aMeta['default']))
                    $this->aData[$sColumn] = $aMeta['default'];
            }

            $s .= '</label>';
            $sControlType = $oType->getControlType($aMeta['type']);

            if (!isset($this->aData[$sColumn])) {
                $this->aData[$sColumn] = ''; // fill with a key
            } else {
                $this->aData[$sColumn] = str_replace('"', '&quot;', $this->aData[$sColumn]);    // Escape quotes for value="text here with quotes..."
            }

            $s .= $this->getControl($aMeta, $sControlType, $sColumn);

            //TODO:switch form groups
        }


        // Form envelope --------------

        $sRet = '<div class="form">';

        if ($bHasError) {
            $sRet .= '<div class="error">Please review you data.</div>';
        }

        $sRet .= '<form id="i' . $this->formOid . '" method="post" action="' . $this->sAction . '" enctype="multipart/form-data">';

        // for multiple forms in a single page, we must identify the target form.
        // navigation buttons:

        $sNav = '<input class="form-control" type="submit" name="submit" value="' . $this->sSubmitCaption . '"> ';
        if (!empty($this->sCancelUrl)) {
            $sNav .= '<input class="form-control" type="button" value="' . $this->sCancelCaption . '" onclick="document.location=\'' . $this->sCancelUrl . '\'; return true;"> ';
            $sNav .= '<br>';
        }

        if ($this->topButtons) {
            $sRet .= $sNav . '<br>';
        }

        $sRet .= $s;
        $sRet .= '<div class="form-buttons">' . $sNav . "</div>";
        $sRet .= '<input type="hidden" name="oid" value="' . $this->formOid . '">'; // distinguish multiple forms in same page
        $sRet .= '</form>';

        if ($this->bInsertRte === true) {
            $sRet .= $this->integrateRteEditor();
        }

        if ($this->bIntegrateAceEditor) {
            $sRet .= $this->integrateAceEditor();
        }

        if ($this->bAjax === true) {
            $this->setAjaxHeaders($formId);
            $sRet .= '<div id="' . $formId . '-result" class="alert"></div>';
        }

        return $sRet . '</div>';
    }

    /*
     * Ajaxifies any form
     * Does not return data.
     */

    static function setAjaxHeaders($formId) {

        global $ob;

        $ob->includeJquery();
        $ob->includeScript('editor_' . $formId, '
	    $(function(){
	        $("#' . $formId . '").submit(function(){
	           $.post($("#' . $formId . '").attr("action"), $(this).serialize(), function(resp) {
                            if (resp.code == 1) {
                              $("#' . $formId . '-result").addClass("alert-success");
                              $("#' . $formId . '").each(function(){ this.reset();});
                            } else {
                              $("#' . $formId . '-result").addClass("alert-warning");
                            }
                            $("#' . $formId . '-result").html(\'<strong>\' + resp.title + "</strong> " + resp.txt);
                        }, "json");
                        return false;
                     });
                    });
                   ');
    }

    /*
     *
     */

    function getControl($aMeta, $sControlType, $sColumn) {

        $s = '';

        // Note: xsql keeps just the sql statement. xselect keeps options (static or dynamically generated).

        if (!empty($aMeta['xsql'])) {
            $aMeta['xselect'] = $GLOBALS['ob']->dbSelectKeyValue($aMeta['xsql']);
        }

        if (!isset($aMeta['value'])) {
            $aMeta['value'] = ''; // new objects
        }

        switch ($sControlType) {

            case 'integer':

                $s .= '<input class="form-control form-integer" type="text" name="' . $sColumn . '" value="' . $this->aData[$sColumn] . '">';

                break;

            case 'date':

                global $ob;

                $ob->includeJquery();
                $ob->includeCss('/mod/jquery/jquery-ui-1.9.2.custom.min.css');
                $ob->includeScriptTag('/mod/jquery/jquery-ui-1.9.2.custom.min.js');
                $ob->includeScriptTag('/mod/jquery/date.js');


                $s .= '
                                <script>
                                $(function() {
                                       $.datepicker.setDefaults($.datepicker.regional[\'pt_PT\']);
                                       $(".form-date").datepicker({ dateFormat: \'yy-mm-dd\' });
                                });
                                </script>

                                <input class="form-control form-date" type="text" name="' . $sColumn . '" value="' . $this->aData[$sColumn] . '">';
                break;

            case 'text':
                $s .= '<input class="form-control form-text" type="text" name="' . $sColumn . '" value="' . $this->aData[$sColumn] . '">';
                break;

            case 'smalltext':
                $s .= '<input class="form-control form-smalltext" type="text" name="' . $sColumn . '" value="' . $this->aData[$sColumn] . '">';
                break;

            case 'textarea':

                $s .= "<textarea class=\"form-control form-textarea\" onkeydown=\"if(event.keyCode==9) {value+='\t'; return false;}\" wrap=\"soft\" name=\"" . $sColumn . "\">" . $this->aData[$sColumn] . "</textarea>";
                break;

            case 'code':

                //TODO: find better way than this.
                $this->aData[$sColumn] = str_replace('<textarea', '<xtextarea', $this->aData[$sColumn]);
                $this->aData[$sColumn] = str_replace('</textarea', '</xtextarea', $this->aData[$sColumn]);
                $s .= "<textarea class=\"form-control form-code\" onkeydown=\"if(event.keyCode==9) {value+='\t'; return false;}\" wrap=\"soft\" name=\"" . $sColumn . "\">" . $this->aData[$sColumn] . "</textarea>";
                break;

            case 'css':

                $s .= '<textarea class="form-control form-css" data-editor="css" id="form-' . $aMeta['name'] . '" name="' . $aMeta['name'] . '">' . $aMeta['value'] . '</textarea>' . $this->integrateAceEditor();
                $this->bIntegrateAceEditor = true;
                break;

            case 'richtext':

                $s .= '<textarea class="form-control form-richtext" id="form-' . $aMeta['name'] . '" name="' . $aMeta['name'] . '">' . $aMeta['value'] . '</textarea>';
                $this->bInsertRte = true;
                break;

            case 'html':

                $s .= '<textarea class="form-control form-html" data-editor="html" id="form-' . $aMeta['name'] . '" name="' . $aMeta['name'] . '">' . $aMeta['value'] . '</textarea>' . $this->integrateAceEditor();
                $this->bIntegrateAceEditor = true;
                break;

            case 'checkbox':
                $s .= '<input class="form-control form-checkbox" type="checkbox" value="1" name="' . $sColumn . '"';
                if ($this->aData[$sColumn]) {
                    $s .= ' checked';
                }
                $s .= '>';
                break;

            case 'select':

                if (!empty($aMeta['xselect'])) {

                    $s .= '<select class="form-control form-select" name="' . $sColumn . '">';
                    if (!empty($aMeta['optional'])) {
                        $s .= '<option value="">...</option>';
                    }
                    foreach ($aMeta['xselect'] as $idkey => $value) {
                        $s .= '<option';
                        if ($this->aData[$sColumn] == $idkey) {
                            $s .= ' selected="selected"';
                        }
                        $s .= ' value="' . $idkey . '">' . $value . '</option>';
                    }
                    $s .= '</select>';
                } else {

                    if (empty($aMeta['optional'])) {
                        $s .= '<div class="error">Note: property needs value for selection.</div>';
                    }
                }

                break;


            case 'radio':

                if (!empty($aMeta['xselect'])) {

                    if (!empty($aMeta['optional'])) {
                        $s .= '<input class="form-control form-radio-item" type="radio" name="' . $sColumn . '" value="" ';
                        if (empty($this->aData[$sColumn]))
                            $s .= ' checked="checked"';
                        $s .= '>&nbsp;No choice<br>';
                    }

                    foreach ($aMeta['xselect'] as $i_id => $s_desc) {
                        $s .= '<input class="form-control form-radio-item" type="radio" value="' . $i_id . '" ';
                        if ($this->aData[$sColumn] == $i_id) {
                            $s .= ' checked="checked"';
                        }
                        $s .= ' name="' . $sColumn . '">&nbsp;' . $s_desc . '<br>';
                    }
                } else {

                    if (empty($aMeta['optional'])) {
                        $s .= '<div class="error">Note: Please edit property object and fill options in format 1:option-A 2:Option-B ... separated by line breaks.</div>';
                    }
                }

                break;


            case 'fcheck':

                if (!empty($aMeta['xselect'])) { //user created options:
                    $s .= '<br />';

                    foreach ($aMeta['xselect'] as $i_id => $s_desc) {
                        $s .= '<input class="form-control form-' . $aMeta['type'] . '-item" style="border:0px" name="' . $sColumn . '[]" type="checkbox"';

                        if (!empty($aMeta['value'])) {
                            //TODO: remove this hack, data should be unserialized already:

                            if (!is_array($aMeta['value']) && strpos($aMeta['value'], ';}') !== false) {
                                $aMeta['value'] = unserialize(str_replace('&quot;', '"', $aMeta['value']));
                            }

                            if (is_array($aMeta['value']))
                                foreach ($aMeta['value'] as $i) {
                                    if ($i == $i_id)
                                        $s .= ' checked="checked"';
                                }
                        }

                        $s .= ' value="' . $i_id . '" />&nbsp;' . $s_desc . '<br />' . "\n";
                    }

                    $s .= '<br>';
                } else {
                    // user didn't fill options:
                    if (!empty($aMeta['optional']))
                        $s .= '<div class="error">Note: Please edit property object and fill options in format 1:option-A 2:Option-B ... separated by line break.</div>';
                }
                break;


            case 'file':

                $s .= '<input class="form-control form-file" type="file" name="' . $sColumn . '">';
                $extension = $this->aData[$sColumn];

                if (!empty($extension) && ($extension == 'png' || $extension == 'jpg' || $extension == 'gif')) {

                    $s .= '<br>';
                    $fn = $GLOBALS['ob']->getFilename($this->datatableOid, $this->aData[$sColumn], false);

                    list($width, $height, $ext) = getimagesize($_SERVER['DOCUMENT_ROOT'] . $fn);
                    $s .= '<small><strong>' . $width . 'x' . $height . ' px ' . $extension . '</strong> image.</small><br><br>';

                    $s .= '<div class="form-image-show"><img src="' . $fn . '?rand=' . rand(1, 100000) . '" /></div>';
                }


                /*
                  // Pixlr option

                  //TODO: rbac

                  if ($this->aData[$sColumn] == 'jpg' || $this->aData[$sColumn] == 'png' || $this->aData[$sColumn] == 'gif')
                  {
                  $s .= '<a target="_new" href="/do/pixlr/edit.php?service=express&id=' . $this->iObjectOid . '&ext=' . $this->aData[$sColumn] . '">B&aacute;sico</a> | ';

                  $s .= '<a target="_new" href="/do/pixlr/edit.php?service=editor&id=' . $id . '&ext=' . $this->aData[$sColumn] . '">Avan&ccedil;ado</a> <br /> <br />';
                  }
                 */

                break;


            case 'captcha':

                require_once('./mod/xtfo/captcha.php');
                $s .= captcha::includeIfNotHuman();
                break;

            case 'phone':

                $s .= '<input class="form-control form-phone" type="text" name="' . $sColumn . '" value="' . $this->aData[$sColumn] . '">';
                break;

            default:

                throw new Exception('html control type ' . $aMeta['type'] . ' on ' . $aMeta['desc'] . ' is not supported.');
        }


        return $s;
    }

    function setTopButtons($b) {
        $this->topButtons = $b;
    }

    function setSubmitCaption($s) {
        $this->sSubmitCaption = $s;
    }

    function setAction($s) {
        $this->sAction = $s;
    }

    function setData($a) {
        $this->aData = $a;
    }

    function setDatatableOid($oid) {
        $this->datatableOid = $oid;
    }

    function setFormOid($oid) {
        $this->formOid = $oid;
    }

    function setStoreOid($oid) {
        $this->iStoreOid = $oid;
    }

    function setFormType($s) {
        $this->sFormType = $s;
    }

    function setCancelUrl($s) {
        $this->sCancelUrl = $s;
    }

    function setAjax($b) {
        $this->bAjax = $b;
    }

    /*
     *
     */

    function integrateAceEditor() {

        if (!empty($GLOBALS['integrateAceEditor'])) {
            return '';
        }
        $GLOBALS['integrateAceEditor'] = true;

        //CREDIT: https://gist.github.com/duncansmart/5267653

        return '
            <script src="/obj/broker/mod/ace/ace.js"></script>
            <script>

                // Hook up ACE editor to all textareas with data-editor attribute
                $(function () {

                    $("textarea[data-editor]").each(function () {

                        var textarea = $(this);
                        var mode = textarea.data("editor");
                        var editDiv = $("<div>", {
                            position: "absolute",
                            width: textarea.width(),
                            height: textarea.height(),
                            "class": textarea.attr("class")
                        }).insertBefore(textarea);
                        textarea.css("visibility", "hidden");
                        var editor = ace.edit(editDiv[0]);
                        editor.renderer.setShowGutter(false);
                        editor.getSession().setValue(textarea.val());
                        editor.getSession().setMode("ace/mode/" + mode);
                        editor.setTheme("ace/theme/clouds");
                        // copy back to textarea on form submit...
                        textarea.closest("form").submit(function () {
                            textarea.val(editor.getSession().getValue());
                        })
                    });

                });
            </script>
            <style>
             .ace_scrollbar {
               display: none !important;
             }
            </style>
             ';
    }

    /*
     *
     */

    function integrateRteEditor() {
        return '
                <script type="text/javascript" src="/mod/rte/jquery.rte.js"></script>
                <script type="text/javascript">
                  $(".form-richtext").rte({
                      content_css_url: "/mod/rte/rte.css",
                      media_url: "/mod/rte/",
                  });
                </script>';
    }

}
