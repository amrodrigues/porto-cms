<?php
	
	header('Content-Type: image/jpeg');
	
	session_start();
	
	require_once ($_SERVER['DOCUMENT_ROOT'] . '/conf.php');
	
	
	// Words to avoid...
	
	$a_mau = array(
		
		'fode', 'foda', 'fodo', 'fuck', 'fodi', 'fudi', 'fede', 'fake', 'foke',
		'cago', 'caga', 'shit',
		'pito', 'pita', 'cona',
		'puta', 'puto', 
		'mijo', 'mija', 'pila',
		'tusa', 'tuza', 'tusa', 'come',
		'cock', 'rata'
	);
	
	
	
	
	
	// Set code
	
	$s_allowed = 'abcdefghijklmnopqrstuvwxyz';
	
	
	do {
		
		$s_code = '';
		
		while(strlen($s_code) < 4) $s_code .= substr($s_allowed, rand(0, 26), 1);
		
		$ok = true;
		
		foreach ($a_mau as $s_mau) if ($s_code == $s_mau) $ok = false;
		
		if ($ok) break;

	} while (true);
	
	
	$_SESSION['captcha']['code'] = $s_code;
	
	
	
	
	
	// Image dimensions
	
	$w = 160;
	
	$h = 40;
	
	
	
	
	
	// Set font
	
	$s_font = $GLOBALS['conf']->base . '/core/xtfo/captcha347.ttf';
	
	$font_size = $h * 0.6;
	
	
	
	
	
	// Create image
	
	$image = @imagecreate($w, $h) or die('Cannot create GD image stream');
	
	$white = imagecolorallocate($image, 255, 255, 255); // Background color
	
	$black = imagecolorallocate($image, 0, 0, 0);
	
	
	
	// Noise
	
	for($i = 0; $i < 150; $i++ )
	{
		$x = mt_rand(0, $w);
		
		$y = mt_rand(0, $h);
		
		imageline($image, $x , $y + rand(-5,5), $x + rand(-5,5), $y, $black);
	}
	
	
	
	
	
	$textbox = imagettfbbox($font_size, 0, $s_font, $s_code);
	
	$x = ($w - $textbox[4]) / 2;
	
	$y = ($h - $textbox[5]) / 2;
	
	imagettftext($image, $font_size, 0, $x, $y, $black, $s_font , $s_code);
	
	
	
	
	
	imagejpeg($image);
	
	imagedestroy($image);
	
?>
