<?php


$a_def = array(

    'classes' => '*',

    'br' => array(
            'class' => 'broker',
            'easyname' => 'Object broker',
            'cache' => false
    ),
    'content' => array(

        'easyname' => array(
            'name' => 'easyname',
            'type' => 'textline',
            'desc' => 'Object name'
        ),
        'pid' => array(
            'name' => 'pid',
            'type' => 'integer',
            'desc' => 'Parent id'
        ),
        'class' => array(
            'name' => 'class',
            'desc' => 'Class name',
            'type' => 'letters'
        ),
        'iduser' => array(
            'name' => 'iduser',
            'type' => 'integer',
            'desc' => 'User Id'
        ),
        'idrole' => array(
            'name' => 'idrole',
            'type' => 'integer',
            'desc' => 'Role Id'
        ),
        'publish' => array(
            'name' => 'publish',
            'type' => 'bool',
            'desc' => 'Publish'
        ),
        'prior' => array(
            'name' => 'prior',
            'type' => 'smallint',
            'desc' => 'Show order',
            'default' => '1',
            'optional' => true,
            'show' => false
        ),
        'laypos' => array(
            'name' => 'laypos',
            'type' => 'smallint',
            'desc' => 'Layout position',
            'optional' => true,
            'default' => 1,
            'show' => true
        ),
        'cnt' => array(
            'name' => 'cnt',
            'type' => 'integer',
            'desc' => 'View counter',
            'optional' => true,
            'default' => '0',
            'show' => false
        ),
        'layout' => array(
            'name' => 'layout',
            'type' => 'dbselect',
            'desc' => 'Layout',
            'xsql' => 'select oid, easyname from broker where class="layout"',
            'optional' => true,
            'default' => 0
        ),
        'tscreate' => array(
            'name' => 'tscreate',
            'type' => 'datetime',
            'desc' => 'Creation date-time',
            'optional' => true,
            'show' => false
        ),
        'tsupdate' => array(
            'name' => 'tsupdate',
            'type' => 'tsupdate',
            'desc' => 'Update date-time',
            'optional' => true,
            'show' => false
        )
    )

);


?>
