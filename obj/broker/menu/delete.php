<?php

/*
 * Menu option: delete object
 */
class delete {

    var $group = "none";
    var $descr = '';
    var $show = false;
    var $public = false;

    function exec() {
        global $ob;

        if (!empty($_GET['oid']))
            $oid = $_GET['oid'];
        else
            return "no oid(s)";

        $aOids = explode(',', $oid);

        if (empty($aOids))
            return "invalid oid(s)";

        foreach ($aOids as $oid) {
            $aCh = $ob->children($oid);

            if (!empty($aCh))
                die("CHILD");

            if ((int) $oid < 1)
                continue;

            $ob->objectDelete($oid);
        }

        print 'OK';
    }

}

?>