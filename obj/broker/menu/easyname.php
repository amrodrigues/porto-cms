<?php

/*
 * Menu option: Set object Name
 */

class easyname {

    var $group = "none";
    var $descr = "";
    var $show = false;
    var $public = false;

    function exec() {

        global $ob, $oAuth;

        $oid = (int) $_GET['oid'];

        if (empty($oid))
            return 'KO';

        $br = $ob->getBr($oid);

        if ($oAuth->pass('edit', $br['class'], $br))
            $ob->setObjectName($oid, $_GET['easyname']);

        print 'OK';
    }

}

?>