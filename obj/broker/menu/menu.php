<?php

/*
 * Generates left side menu
 */

class menu {

    var $group = "none";
    var $descr = "Left menu";
    var $show = false;
    var $public = false;

    function exec() {

	$s = '';

	$s .= '<script language="javascript" type="text/javascript" src="/do/ieditor.js"></script>';
	$s .= '<script language="javascript" type="text/javascript" src="/do/ivalid.js"></script>';
	$s .= '<script language="javascript" type="text/javascript" src="/do/iform.js"></script>';
	$s .= '<script language="javascript" type="text/javascript" src="/do/client.js"></script>';
	$s .= '<script language="javascript" type="text/javascript" src="/do/isessionrefresh.js"></script>';

	$s .= $this->getMenu();

	/*
	  $s .= '<script language="javascript">
	  top.left.addEvent(top.right.document, \'load\', function (ev) {
	  top.left.document.body.style.backgroundColor="#888";
	  }
	  </script>';
	 */

	return $s;
    }

    //--- Generates left menu for backoffice

    function getMenu() {

	global $conf, $oAuth, $ob;

	$ob->log('menu enter');

	//-- Load menu objects

	$aMenus = $oAuth->getUserMenuList();

	foreach ($aMenus as $m) {
            
            $i = strpos($m, '.');
            
            if ($i === false) {
                $obj = 'broker';
                $menuClass = $m;
            } else {
                $obj = substr($m, 0, $i);
                $menuClass = substr($m, $i + 1);
            }

            
            if (empty($m)) die ("menu: empty op");
            
	    $ob->log("menu:getMenu: $m");

	    $sFn = $conf->root . '/obj/' . $obj . '/menu/' . $menuClass . '.php';
            
            if (!file_exists($sFn)) continue;
            
	    require_once($sFn);

	    if (class_exists($menuClass))
		$this->aoMenu[$menuClass] = new $menuClass;
                $this->aoMenu[$menuClass]->obj = $obj;

	    // if item is not supposed to be used as visible menu option, delete it

	    if (empty($this->aoMenu[$menuClass]->show))
		unset($this->aoMenu[$menuClass]);

	    // menu items without description are automatically ignored

	    if (empty($this->aoMenu[$menuClass]->descr))
		unset($this->aoMenu[$menuClass]);
	}

	//TODO: remove hardcoded categories
	// Build html separated by menu groups

	$aGroups = array(
	    'main' => 'Menu',
	    'add' => 'Add',
	    'edit' => 'Edit',
	    'newsletter' => 'Newsletters',
	    'store' => 'Store',
            'payments' => 'Payments',
	    'admin' => 'Administration',
	    'helpers' => 'Helpers'
	);


	//-- create html for each menu group

	$aSubMenu = array();

	foreach ($aGroups as $k => $v)
	    $aSubMenu[$k] = '';

	if (isset($this->aoMenu))
	    foreach ($this->aoMenu as $menuClass => $oMenu) {
            
		if (empty($oMenu->group))
		    $sg = 'main';
		else
		    $sg = $oMenu->group;
                    
		$s = $this->itemHtml('/do/?m=' . $oMenu->obj . '.' . $menuClass, $oMenu->descr);

		if (array_key_exists($sg, $aGroups))
		    $aSubMenu[$sg] .= $s;
	    }


	//-- generate menu

	$sMenu = '<div id="accordion" class="accordion">';

	foreach ($aSubMenu as $sGroup => $sSubMenu) {
	    if (!empty($sSubMenu))
		$sMenu .= $this->groupHTML($aGroups[$sGroup], $sSubMenu, $sGroup);
	}

	$sMenu .= '</div>

            <script type="text/javascript">
                $(document).ready(function(){

                    $("h3").click(
                        function() {
                            $(this).next().toggle("slow");
                            return false;
                        }
                    ).next().hide();

                    // Open first menu item
                    top.left.$("h3:contains(\'Menu\')").next().toggle("slow");
                });
            </script>

            ';



	//-- Search object by id

	if ($oAuth->pass('options', 'browse'))
	    $sMenu .= '

            <div id="searchbox-div">
               <form id="formsearch" method="get" action="/do/" target="right">
                 <input name="m" type="hidden" value="browse" />
                 <input id="searchbox" name="oid" type="text" placeholder="id..." title="Jump to id..." name="oid" />
               </form>
            </div>

            <script type="text/javascript">

                $("#formsearch").bind("keydown", function(evt) {

                     var n = evt.which;
                     if (evt.which >= 96 && evt.which <= 105) return;   // digits - side keys
                     if (evt.which >= 48 && evt.which <= 57) return;    // digits - regular keys
                     if (n == 8 || n == 37 || n == 39) return;          // backspace, left, right
                     if (n == 8 || n == 37 || n == 39) return;          // backspace, left, right
                     if (n == 46 || n == 13) return;                    // Delete, Enter

                     evt.stopPropagation();
                     return false;                                      // prevents reloading frame
                  }
                );


            </script>';



	$sMenu .= '

            <div id="bread"></div>

        ';





	return $sMenu;
    }

    // --- Create menu item

    function itemHtml($s_url, $s_txt) {

	$s = '<div class="menu-item" onclick="';

	$s .= "top.right.document.location='" . $s_url . "';";

	$s .= "\">";

	$s .= $s_txt;

	$s .= '</div>';

	return $s;
    }

    // --- Create mmenu group

    function groupHtml($sTitle, $sContent, $sGroup) {

	$s = '<h3><a href="#">' . $sTitle . '</a></h3>';

	$s .= '<div class="menu-group" id="menu-' . $sGroup . '">';

	$s .= $sContent . '</div>';

	return $s;
    }

}

?>