<?php

/*
 * Receives files by drag'n'drop
 * and creates image and xfile objects for each one
 */

class dropzoneupload {

    var $group = "admin";
    var $descr = '';
    var $show = false;
    var $public = false;
    var $oFiles = false;

    /*
     * Creates one object for each file uploaded
     */

    function exec() {

        //file_put_contents("/tmp/drop.txt", print_r($_FILES, true));

        global $ob;

        $this->oFiles = &$ob->helper('files');
        $pid = $_SESSION['object'];

        foreach ($_FILES as $FILESdata) {
            $class = $this->objectType($FILESdata);
            $this->oFiles->newObject($class, $pid, $FILESdata);
        }

        //header('Location:/do/?m=broker.browse');
    }

    /*
     * Common object type for a file extension
     */

    private function objectType($FILESdata) {

        $ext = $this->oFiles->extension($FILESdata['name']);

        switch ($ext) {
            case 'jpg':
            case 'png':
            case 'gif':
                $class = 'image';
                break;
            default:
                $class = 'xfile';
        }

        return $class;
    }

}
