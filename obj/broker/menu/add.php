<?php

/*
 * Menu option: Add object
 */

class add {

    var $group = "main";
    var $descr = "Add object";
    var $show = true;
    var $public = false;

    function exec() {

        if (empty($_SESSION['object'])) {
            return "Use object browser to find the place where you wish to add an object. Then you can select this option.";
        }

        global $ob, $oAuth;

        $mouse = 'onmouseover="style.backgroundColor=\'#eee\'; style.cursor=\'pointer\';" onmouseout="style.backgroundColor=\'#fff\'"';

        $br = $ob->getBr($_SESSION['object']);

        if (!$oAuth->pass('traverse', $br['class'], $br)) {
            return "Cannot add objects here.";
        }

        $aAllowed = $ob->allowedClasses($_SESSION['object']);

        if (empty($aAllowed)) {
            return 'I don\'t need more objects here. <br><a href="javascript:window.history.back()">Go back...</a>';
        }

        $aAvailable = $ob->classList(false, false, false, false);


        // get child class count:
        $aChildCnt = $ob->dbSelectKeyValue('select class, count(class) from broker where pid=' . $br['oid'] . ' group by class');

        global $oAuth;

        /*
         * In this area we have to:
         *
         * - Check if class exists (i.e. exists in obj directory)
         * - Check if it's possible to add below current object
         * - Check if user has permissions to add this kind of object at current place
         */

        // trim allowed until we have the final list:

        $aList = array();

        if ($aAllowed == '*') {
            $aAllowed = $aAvailable;
        }

        foreach ($aAllowed as $class => $maxObj) {

            // classes should all be available, or def is wrong...

            if (empty($aAvailable[$class])) {
                continue;
            }
            if ($maxObj !== '*' && !empty($aChildCnt[$class]) && $aChildCnt[$class] >= $maxObj) {
                continue;
            }
            if ($oAuth->pass('add', $class, false) === false) {
                continue;
            }

            // add cleared class to list:
            $aList[$class] = $aAvailable[$class];
        }

        asort($aList);

        /* Until here, we've found objects that we have permission to use
         *  Now, we split the actually already used and those inexistent in db
         *  The idea is to hide objects the user does not regularly use
         */

        $aSeparatedList = $this->separateUsedobjects($aList);

        $s = '<script>
            function addObject(className) {
              document.location="/do/?m=broker.edit&class=" + className + "&form_type=meta&op=new&oid=' . $_SESSION['object'] . '&cancel=/do/?m=broker.browse&ret=/do/?m=broker.browse";
        }
        </script>';

        foreach ($aSeparatedList['using'] as $sClass => $sDesc) {
            $s .= '
                <div class="add-item-div" ' . $mouse . ' onclick="addObject(\'' . $sClass . '\');">
                    <img style="clear:right;margin:10px;" title="" border="0" src="/obj/' . $sClass . '/icon.png"  />
                    <br>
                    <span class="menu-item">' . $sDesc . '</span>
                </div>
            ';
        }

        // select box with unused objects

        if (!empty($aSeparatedList['notusing'])) {
            global $conf;
            require_once($conf->root . '/ihtml.php');
            $ohtml = new ihtml();
            $s .= '<div class="add-unused-objects"><form>More objects: ' . $ohtml->select($aSeparatedList['notusing'], 'notusing', 'addObject(this.value);') . '</form></div>';
        }

        $s .= '</div>';
        return $s;
    }

    /*
     *
     */

    private function separateUsedObjects($aList) {
        $aUsing = $this->objectsInUse();
        $aNotUsed = array();
        foreach ($aList as $className => $descr) {
            if (array_search($className, $aUsing) === false) {
                $aNotUsed[$className] = $descr;
                unset($aList[$className]);
            }
        }
        return array('using' => $aList, 'notusing' => $aNotUsed);
    }

    /*
     *
     */

    private function objectsInUse() {
        global $ob;
        $sql = 'select distinct(class) from broker order by class';
        return $ob->dbSelectColumn($sql);
    }

}
