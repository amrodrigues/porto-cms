<?php

/*
 * Menu option to publish a object
 */

class publish {

    var $group = 'none';
    var $descr = 'Publish object';
    var $show = false;
    var $public = false;

    function exec() {
        global $ob;

        $ob->log('m.publish: enter');


        // --- parse input oid and op:

        $oid = (int) $_GET['oid'];

        if (!$oid)
            return 'ERROR: no oid';

        $op = $_GET['op'];

        if ($op != '0' && $op != '1')
            return 'ERROR: Bad op, must be 0 or 1';


        // --- do it:

        $bRes = $ob->objectPublish($oid, $op);

        // no need to control permissions and cache (dob does it, this is just a controller)
        
        // --- response:

        if ($bRes) {
            die("OK"); // answer to client js
        } else {

            $ob->log('publish: Error publishing object ' . $oid . ' to ' . $op);

            die("ERROR");
        }
    }

}

?>