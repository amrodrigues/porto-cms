<?php

/*
 * Object browser
 */

class browse {

    var $group = "main";
    var $descr = "Object browser";
    var $show = true;
    var $public = false;

    /*
     *
     */

    function exec() {

        global $ob, $oAuth, $conf;

        $s = '';


        // Show object ----

        if (!empty($_GET['oid'])) {
            $oid = (int) $_GET['oid'];
        } elseif (!empty($_SESSION['object'])) {

            $oid = $_SESSION['object'];
        } else {

            $oid = $_SESSION['object'] = $oAuth->userStartObject();
        }


        // --- location object:

        $ud = $ob->get($oid);


        // save position in hierarchy for menu options

        $_SESSION['object'] = $oid;
        $_SESSION['br'] = $ud['br'];

        require_once($conf->root . '/ihtml.php');

        $s .= ihtml::backofficeHeader(false);

        $s .= '
            </head>
            <body style="overflow:auto; overflow-x: hidden;" onload="top.left.browserLoaded(' . $oid . ')">
            <form id="dropzone" class="dropzone" action="/do/?m=broker.dropzoneupload">
        ';

        $ch = $ob->children($oid);

        // --- Header with location

        if ($ud['br']['oid'] != 1) {

            $s .= '<div class="location">';
            $s .= '<div class="location-back"><a title="Parent object" href="index.php?m=broker.browse&oid=' . $ud['br']['pid'] . '"><img class="img-back" src="icons/back.png"></a></div>';
            $s .= $this->getObjectRow($ud['br']);
            $s .= "</div>";
        }


        // Object list

        if (count($ch) > 0) {

            $s .= '<div class="main">';

            //TODO: page nav

            if (!empty($ch)) {

                $s .= "<ul id=\"sortable\">";

                foreach ($ch as $a) {
                    $s .= $this->getObjectRow($a);
                }

                $s .= '</ul></div>'; // end main
                $s .= '<div class="link-add">';

                if (count($ch) > 5) {
                    $s .= count($ch) . ' objects here.<br />';
                }

                $s .= '<div>Click to <a href="/do/?m=broker.add" style="text-decoration:underline">add another object</a>.<br>Drag and drop files above.</div>';
                $s .= '</div>';
            } else {

                $allowed = $ob->allowedClasses($oid);

                if ($allowed !== false) {
                    $s .= '<div class="link-add">Empty object.<br>Click to <a href="/do/?m=broker.add" style="text-decoration:underline">add an object here</a>...<br>Drag and drop files above.</div>';
                } else {

                    $s .= '<div class="link-add">No need for objects here.</div>';
                }
            }

            $s .= '</div>';
        }

        $s .= '</form>';


        // layout menu option dialog:

        $s .= '<div id="layout-options"></div>';


        // --- script includes

        $s .= '<script src="/do/dropzone/dropzone.min.js"></script>';


        // --- include sortable if user has permission to order items

        if ($oAuth->pass('view', $ud['br']['class'], $ud['br'])) {
            $s .= '
                <script type="text/javascript" language="javascript">

                    $(document).ready(function() {
                        $(top.left.document).ready(function() {

                          $(top.left).focus();
                          var olist = new Array();
                          $("#sortable").sortable({ revert: true, delay: 500 });
                          $("#sortable").sortable({
                            stop: function(event, ui) {
                                $(\'li\').children().each( function(index) {
                                        olist.push($(this).attr(\'id\'));
                                });
                                eval(\'$.get("/do/?m=broker.prior", {prior:"\' + olist.toString() + \'"}, function(data) {  })\');
                                olist = new Array();
                            }
                          });
                        });
                      });
                </script>
        ';
        }

        $s .= '
        <script>

        $(document).ready(function() {
            $(top.left.document).ready(function() {

                // --- CTRL key events

                //top.left.registerCTRLKeyEvents({
                //        key65: function() { top.right.location.href = "/do/?m=broker.add"; },
                //        key90: function() { top.right.location.href = "/do/?m=broker.browse&oid=1"; }
                //});


                // --- side links to previous objects

                var oid = ' . $oid . ';
                var easyname = "' . str_replace('"', '\"', $ud['br']['easyname']) . '";
                top.left.addBread(oid,easyname);
em
                // --- Drop zone for file uploading by drag\'n\'drop

                Dropzone.options.dropzone = {
                    paramName: "file",
                    maxFilesize: 30,
                    dictDefaultMessage: "",
                    init: function() {
                            this.on("complete", function(file) { document.location.href=document.location.href; });
                    }
                 };

                 // --- close "ready" events
            });
        });
        </script>
        ';

        $s .= '</body></html>';
        print $s;
    }

    /*
     *
     */

    function getObjectRow($a) {

        global $conf, $oAuth;
        $id = $a['oid'];
        $s = '
                <li id="li' . $id . '" value="' . $id . '">
                <div class="object-info" id="id' . $id . '">
            ';

        // --- Image ---
        $s .= '<a href="index.php?m=broker.browse&oid=' . $id . '">' . $this->getIcon($a) . '</a>';


        // --- change name form
        // Ask for object easyname
        if (empty($a['easyname'])) {

            $s .= '<div style="display:inline" id="easyname' . $id . '">';
            $s .= '<form><input autofocus="autofocus" onkeydown="if(event.keyCode == 13) { top.left.easynameSet(' . $id . ');return false;}" id="input' . $id . '" type="text" style="vertical-align:middle;margin:0px;padding:0px;width:220px;height:22px;font-size:12px" name="easyname" placeholder="Object name"></form>';
            $s .= '</div>';
        }


        // --- easyname ---
        $s .= '<div class="obj-title"><a id="en' . $id . '" href="index.php?m=broker.browse&oid=' . $id . '">' . $a['easyname'] . '</a></div>';

        // --- additional options:

        $s .= '<div class="obj-info">';
        $s .= $a['oid'];

        if ($conf->stats) {
            $s .= ' cnt ' . $a['cnt'];
        }

        $s .= ' ' . $oAuth->userNameById($a['iduser']) . ' ';
        $s .= '</div>';


        //---

        $s .= '<div class="object-options">';

        if ($oAuth->pass('edit', $a['class'], $a))
            $s .= " <a title=\"Edit\" class=\"op\" href=\"/do?m=broker.edit&amp;cancel=index.php?m=broker.browse&amp;op=edit&amp;form_type=meta&oid=$id&amp;ret=index.php?m=broker.browse\">Edit</a>";
        else
            $s .= ' <div class="emptyop">Edit</div>';

        if ($oAuth->pass('edit', $a['class'], $a))
            $s .= "<a title=\"Rename\" class=\"op\" href=\"javascript:top.left.easynameAsk(" . $id . ', \'' . str_replace("'", '', $a['easyname']) . "');\">Name</a>";
        else
            $s .= '<div class="emptyop">Name</div>';

        if ($oAuth->pass('view', $a['class'], $a))
            $s .= " <a title=\"View\" class=\"op\" href=\"/index.php?oid=$id\">View</a>";
        else
            $s .= ' <div class="emptyop">View</div>';

        if ($oAuth->pass('move', $a['class'], $a))
            $s .= ' <a title="Move" id="cut' . $id . '" class="cut0" onclick="top.left.cut(this,' . $id . ')">Select</a>';
        else
            $s .= ' <div class="emptyop">Select</div>';

        if ($oAuth->pass('delete', $a['class'], $a))
            $s .= ' <a title="Delete" class="op" onclick="top.left.del(' . $id . ')">Delete</a>';
        else
            $s .= ' <div class="emptyop">Delete</div>';

        if ($oAuth->pass('edit', $a['class'], $a))
            $s .= ' <a title="Layout" class="op" onclick="top.left.layoutMenu(' . $id . ')">Layout</a>';
        else
            $s .= ' <div class="emptyop">Layout</div>';

        // prevent empty value:

        if (!empty($a['publish']))
            $pub = '1';
        else
            $pub = '0';

        if ($oAuth->pass('publish', $a['class'], $a))
            $s .= ' <a title="Publish" id="publish' . $id . '" class="pub' . $pub . '" href="javascript:top.left.pub(' . $id . ');">Publish</a>';
        else
            $s .= ' <div class="emptyop">Publish</div>';

        $s .= '</div>';

        // ----

        $s .= "</div></li>"; // end object

        return $s;
    }

    /*
     *
     *
     *
     * get object icon
     */

    function getIcon($a) {

        if ($a['class'] == 'image') {
            global $ob;

            $sDir = $ob->getDirectory($a['oid'], false);

            $fn = '/th' . $a['oid'] . '_48_48_1.jpg';

            if (file_exists($_SERVER['DOCUMENT_ROOT'] . $sDir . $fn)) {
                return '<img class="obj-icon" src="' . $sDir . $fn . '">';
            }
        }

        return '<img class="obj-icon" src="/obj/' . $a['class'] . '/icon.png">';
    }

}
