<?php

/*
 * Sets object vertical order when user drags and drops
 */

class prior {

    var $group = "admin";
    var $descr = 'Object vertical order (priority)';
    var $show = false;
    var $public = false;

    function exec() {

        global $ob, $oAuth;

        $aPrior = $_GET['prior'];

        $ob->log('prior: list is ' . print_r($aPrior, true));

        $a = explode(',', $aPrior);

        foreach ($a as $k => $oid) $a[$k] = (int) str_replace('id', '', $oid);

        foreach ($a as $k => $oid) {

            $br = $ob->getBr($oid);

            if ($oAuth->pass('order', $br['class'], $br))
                $ob->setPrior($oid, $k + 1);
        }

        print "OK";
    }

}

?>