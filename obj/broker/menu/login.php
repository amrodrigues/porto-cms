<?php

class login {

    var $descr = 'Login';
    var $show = false;
    var $public = true;

    
    function exec() {
	
	global $conf;
	
	if (empty($_POST)) {
	    
	    // check if porto is installed:
	    require_once('integrity.php');
	    $oi = new integrity();
	    if ($oi->dbIsPrepared() === false) $oi->bootstrap(); // minimum install
	    
	    return $this->loginForm();
	    
	} else {
	    return $this->validateLogin();
	}
    }

    
    function loginForm() {
	
	// Present login form:
	return $GLOBALS['oAuth']->form('/do/?m=broker.login');
    }

    
    function validateLogin() {
	// --- user entered username & password:

	if (!empty($_POST['usha']) && !empty($_POST['psha'])) {
	    /*
	     * Jump to the role home url if login was successful.
	     * Role home url is defined in table role
	     */

	    $GLOBALS['oAuth']->validate($_POST['usha'], $_POST['psha']);

	    // Script exists here.
	} else {

	    return 'Problem at login...';
	}
    }

}

?>