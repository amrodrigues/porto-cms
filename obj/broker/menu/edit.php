<?php

/*
 *  Form presenter
 *  All Object Browser data edition is done here
 */

class edit {

    var $group = 'none';
    var $descr = 'Editor';
    var $show = false;
    var $public = false;

    function exec() {

        global $ob, $conf, $oAuth;


        $s = '';


        // --- Extract + sanitize

        $oid = (int) $_GET['oid'];

        $class = $ob->param('class', 'letters', false);
        $storeOid = $ob->param('store_oid', 'digits', false);
        $formType = $ob->param('form_type', 'letters', false);
        $op = $ob->param('op', 'letters', false);
        $returnUrl = $ob->param('ret', 'any', false);
        $cancelUrl = $ob->param('cancel', 'any', '');



        // --- Set form

        require_once($conf->root . '/editor.php');

        $fo = new editor();
        $fo->setFormType($formType);
        $fo->setDatatableOid($oid);
        $fo->setStoreOid($storeOid);
        $fo->setCancelUrl($cancelUrl);


        // --- Get object to edit

        if ($op == 'new' || $op == 'insert') { // New object
            $ud = $ob->loadSkel($class);
        } elseif ($op == 'edit' || $op == 'update') {  // Update existing object
            $ud = $ob->get($oid);
            $class = $ud['br']['class'];
        }


        switch ($op) {

            case 'new':

                if (!$oAuth->pass('traverse', $class))
                    return false;
                if (!$oAuth->pass('add', $class, $ud['br']))
                    return false;

                $this->show = true;
                $fo->setAction('/do/?m=broker.edit&op=insert&form_type=' . $formType . '&oid=' . $oid . '&class=' . $class . '&cancel=' . $cancelUrl . '&ret=' . $returnUrl);
                $fo->setSubmitCaption('Add');
                $s .= '<h2>Insert new ' . $class . ' below id ' . $oid . '</h2><br>' . $fo->asHtml($ud);

                break;



            case 'insert':

                if (!$oAuth->pass('traverse', $class, $ud['br']))
                    return false;
                if (!$oAuth->pass('add', $class, $ud['br']))
                    return false;

                if ($formType == 'meta') {

                    //TODO: remove hack, remove also in /editor.php

                    foreach ($_POST as $k => $v) {
                        $_POST[$k] = str_replace('xtextarea', 'textarea', $v);
                    }

                    $oidNew = $ob->simpleNewObject($class, "", $_POST, $oid); // data is sanitized and validated here

                    if (empty($oidNew)) { // there were errors inserting - $ud was just updated with error info
                        // Has errors, ask to correct
                        $this->show = true;

                        $fo->setAction('/do/?m=broker.edit&cancel=' . $cancelUrl . '&ret=' . $returnUrl . '&form_type=' . $formType . '&op=insert&oid=' . $oid . '&class=' . $class);

                        $fo->setSubmitCaption('Corrigir');

                        $s .= $fo->asHtml($ud);
                    } else {

                        if (!empty($returnUrl))
                            header('Location: ' . $returnUrl);
                    }
                }

                //TODO: formType == 'br'  ?
                //TODO: formType == 'datatable' ?

                break;




            case 'edit':

                if (!$oAuth->pass('edit', $ud['br']['class'], $ud['br']))
                    return false;


                $this->show = true;

                $fo->setAction('/do/?m=broker.edit&cancel=' . $cancelUrl . '&ret=' . $returnUrl . '&form_type=' . $formType . '&op=update&oid=' . $oid . '&class=' . $class);

                $fo->setSubmitCaption('Modify');

                $s .= '<h2>Edit ' . $ud['br']['easyname'] . ' data (id ' . $oid . ')</h2><br>' . $fo->asHtml($ud);

                break;



            case 'update': // ---

                if (!$oAuth->pass('traverse', $ud['br']['class'], $ob->getBr($ud['br']['pid'])))
                    return false;

                if (!$oAuth->pass('edit', $ud['br']['class'], $ud['br']))
                    return false;

                //TODO: remove hack, remove also in insert option in this file and /editor.php

                foreach ($_POST as $k => $v) {
                    $_POST[$k] = str_replace('xtextarea', 'textarea', $v);
                }



                $bRes = $ob->objectUpdate($ud, $formType, $_POST); // sanitizes and validates data, provides access control

                if ($bRes === false) {

                    // Has ERRORS, ask to correct

                    $this->show = true;

                    $fo->setAction('/do/?m=broker.edit&cancel=' . $cancelUrl . '&ret=' . $returnUrl . '&form_type=' . $formType . '&op=update&oid=' . $oid . '&class=' . $class);

                    $fo->setSubmitCaption('Review');

                    $s .= $fo->asHtml($ud);
                } else {

                    if (!empty($returnUrl))
                        header('Location: ' . $returnUrl);
                }

                break;


            default:

                throw new Exception('Edit operation unknown.');
        }


        return $s;
    }

}
