<?php

/*
 * Refresh a session, so that user doesn't get logged out
 */

class refresh {

    var $group = "none";
    var $descr = "Refresh session";
    var $show = true;
    var $public = false;

    function exec() {
        
        // Do some stuff to say we're alive:

        if (empty($_SESSION['refresh']))
            $_SESSION['refresh'] = 0;

        $_SESSION['refresh'] = 1;
        print 'OK';
    }

}

?>