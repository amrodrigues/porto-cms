<?php

    class logout {

        var $group = "admin";
        var $descr = "Logout";
        var $show = true;
        var $public = false;

        function exec() {

            global $oAuth;

            $url = $oAuth->logoutUrl(); // before logout, or else we don't get it

            $oAuth->userLogout();

            if ($url === false)
                $url = '/';

            return '<script type="text/javascript">top.window.document.location.href=\'' . $url . '\';</script>';
        }

    }

?>