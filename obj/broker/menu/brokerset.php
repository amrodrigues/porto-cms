<?php

/*
 * Menu option: Executes ajax requests for layout change and placeholder change of a single object
 */

class brokerset {

    var $group = "none";
    var $descr = 'Get and set broker data';
    var $show = false;
    var $public = false;

    function exec() {

        global $ob, $oAuth;

        // --- oid

        if (empty($_GET['oid']))
            die("oid not set");
        $oid = (int) $_GET['oid'];
        if (empty($oid))
            die("need oid");

        // --- op

        if (empty($_GET['op']))
            die("op get|set not found");
        $op = $_GET['op'];
        if ($op != 'get' && $op != 'set')
            die("bad op");
        
       
       if ($op == 'get') { // send layout list + layout id + layout position of requested object
            
            $ob->log('menu:brokerset: get layout data for oid ' . $oid);
            $aLays = $ob->dbSelectKeyValue('select oid, easyname from broker where class="layout" order by easyname');
            $aSelected = $ob->dbSelectRow('select layout, laypos from broker where oid=' . $oid);
            print json_encode(array('layout' => $aSelected['layout'], 'laypos' => $aSelected['laypos'], 'list' => $aLays));
            die();
            
        } elseif ($op == 'set') { // set layout id + layout position for requested object
            
            $ob->log('menu:brokerset: set layout data for oid ' . $oid);
            $layout = (int) trim($_GET['layout']);
            $laypos = (int) trim($_GET['laypos']);

            //TODO: permissions

            $ud = $ob->get($oid);
            if (empty($ud))
                die("KO");

            if ($oAuth->pass('edit', $ud['br']['class'], $ud['br'])) {
                $ob->dbExec('update broker set laypos=' . $laypos . ', layout=' . $layout . ' where oid=' . $oid);
                print "OK";
                die();
            }

            print "KO";

            die();
        }
    }

}

?>