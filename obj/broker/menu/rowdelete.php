<?php

/*
 * Menu option: delete table row
 * Note: only for datatables, saved as datatableId
 */

class rowdelete {

    var $group = "none";
    var $descr = "Delete table row";
    var $show = true;
    var $public = false;

    function exec() {

        global $ob;

        require_once ($GLOBALS['conf']->root . '/ihttp.php');

        $rowId = ihttp::getParam('id', 'integer');
        $sTable = ihttp::getParam('table', 'textline');

        $a = explode('_', $sTable);

        $oid = (int) $a[2];
        if (!is_numeric($oid))
            die('table ' . $sTable . ' seems not to be a data table');

        // limit to datatables and sys_ tables:
        if (strpos($sTable, 'datatable') !== 0  && strpos($sTable, 'sys_') !== 0)
            die('cannot change that table');

        try {
            $ob->exec('delete from ' . $sTable . ' where oid=' . $rowId);
            print "OK";
            
        } catch (Exception $e) {
            die('database error: ' . $e);
        }
    }

}

?>