<?php

/*
 * Menu option: duplicate selected objects
 */

class duplicate {

    var $group = "admin";
    var $descr = 'Duplicate';
    var $show = false;
    var $public = false;

    function exec() {

        global $ob, $oAuth;

        // list of object ids to duplicate:
        $oids = $ob->param('oids', 'digitscomma', false);
        $aOid = explode(',', $oids);

        // the place (parent id) where all duplicated objects will be:
        $pid = $ob->param('pid', 'digits', false);

        foreach ($aOid as $oid) {
            if (!is_numeric($oid))
                continue;

            $this->objDuplicate($oid, $pid);
        }

        print 'OK';
    }

    
    /*
     * 
     */

    function objDuplicate($oid, $pid) {

        global $ob, $oAuth;
        
        $ob->log("dob:objDuplicate $oid at $pid");

        $ud = $ob->get($oid);

        if (empty($ud))
            die("ud is empty");

        if (!$oAuth->pass('edit', $ud['br']['class'], $ud['br']))
            return false;

        $ud['br']['pid'] = $pid;

        $newOid = $ob->newObject($ud);

        if (empty($newOid))
            die("no new oid");

        // Also copy object's files(thumbs and regular files)
        // We will do a hard copy because of thumbs:

        if (!empty($ud['content']))
            foreach ($ud['content'] as $meta) {

                if ($meta['type'] !== 'userfile')
                    continue;

                $dir1 = $ob->getDirectory($oid, true);
                $dir2 = $ob->getDirectory($newOid, true);

                $files = $ob->fileList($dir1);

                if (!empty($files))
                    foreach ($files as $fn) {

                        $i1 = strpos($fn, $oid . '_');
                        $i2 = strpos($fn, $oid . '.');

                        if ($i1 === 0) // if oid_ or oid. is at pos 0...
                            $newFn = $newOid . substr($fn, $i1 + strlen("$oid"));
                        elseif ($i2 === 0)
                            $newFn = $newOid . substr($fn, $i2 + strlen("$oid"));
                        else
                            continue;  // not the right file to copy, ignore.


                        $src = $dir1 . '/' . $fn;
                        $dest = $dir2 . '/' . $newFn;

                        copy($src, $dest);
                    }
            }

        // Object was duplicated. Go for the descendants:

        $aChildren = $ob->children($oid);

        if (empty($aChildren))
            return $newOid;

        foreach ($aChildren as $ach)
            $this->objDuplicate($ach['oid'], $newOid);

        return 'OK';
    }

}

?>
