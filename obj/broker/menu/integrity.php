<?php

/*
 * Menu option: check system integrity
 */

class integrity {

    var $group = "admin";
    var $descr = "Check system";
    var $show = true;
    var $public = false;

    /*
     * Check if database is installed
     */

    function dbIsPrepared() {

        global $ob;
        if ($ob->dbTableExists('user') &&
                $this->dbTableHasData('user')) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Creates database minimum data for login
     * A better alternative is to load a database skel in /do/install/
     */

    function bootstrap() {

        global $ob;

        // we assume we have access to
        // a mysql UTF-8 empty database

        $ud = $ob->loadSkel('broker');
        $ob->classCheckIntegrity($ud);

        // bare minimum objects
        $sql = 'insert into broker (oid, pid, class, easyname, iduser, idrole) values (1,0,"folder", "Root object", 5, 4)';
        $ob->dbExec($sql);
        $sql = 'insert into broker (oid, pid, class, easyname, iduser, idrole) values (2,1,"folder", "Homepage", 5,4)';
        $ob->dbExec($sql);
        $sql = 'insert into broker (oid, pid, class, easyname, iduser, idrole) values (3,1,"layout", "Default layout", 5, 4)';
        $ob->dbExec($sql);
        $sql = 'insert into broker (oid, pid, class, easyname, iduser, idrole) values (4, 1,"role", "Admin role", 5, 4)';
        $ob->dbExec($sql);
        $sql = 'insert into broker (oid, pid, class, easyname, iduser, idrole) values (5,1,"user", "Admin default user", 5, 4)';
        $ob->dbExec($sql);

        $ob->dbCheckIntegrity(); // Must be below broker, it ignores tables of classes not found in broker
        // default homepage
        $sql = 'insert into folder (oid) values (2)';
        $ob->dbExec($sql);

        // default layout
        $sql = 'insert into layout (oid, template) values (3, "<!DOCTYPE html>\n<html><head> </head><body>Default layout is working. <br> {[1]}</body></html>")';
        $ob->dbExec($sql);

        // an admin role:
        $sql = 'insert into role(oid, title, perm, homeurl, startobject, logouturl) values (4,"Admin", "*", "/do/?m=broker.frames&right=/do/?m=broker.browse", 1, "/")';
        $ob->dbExec($sql);

        // an admin user
        $sql = 'insert into user(oid,name, email, pass, debugmode, idrole) values(5,"Admin", "admin@admin.com", "admin", 0, 4)';
        $ob->dbExec($sql);
    }

    /*
     *
     */

    function dbTableHasData($table) {

        global $ob;
        $sql = 'select oid from ' . $table . ' limit 1';
        $a = $ob->dbSelect($sql);
        if (!empty($a))
            return true;
        return false;
    }

    /*
     *
     */

    function exec() {

        global $ob, $conf;

        $docroot = $_SERVER['DOCUMENT_ROOT'];

        $s = '<h2>Checking conf.php file</h2>';

        // if conf file does not exist, route to conf file creator
        if (!file_exists($docroot . '/conf.php')) {
            return $s . $this->createConfFile();
        }

        // if there are essential conf file items missing stop checking and return error(s)
        $cs = $this->checkConfFile();
        if (!empty($cs))
            return $s . $cs;

        $s .= 'OK.<br>';
        $s .= '<h2>Checking database...</h2>';
        $s .= $ob->dbCheckIntegrity();

        // --- filesystem check

        $s .= '<h2>Checking public /files directory</h2>';

        require ($conf->root . '/files.php');
        files::touchDir($docroot . '/files');

        for ($i = 0; $i <= 99; $i++) {
            $s_dir = "$i";

            if (strlen($s_dir) == 1) {
                $s_dir = '0' . $s_dir;
            }

            files::touchDir($_SERVER['DOCUMENT_ROOT'] . '/files/' . $s_dir);
        }

        // --- specific files check

        $s .='Checking public files index.php, robots.txt, favicon.ico<br />';
        files::checkFile('index.php', $_SERVER['DOCUMENT_ROOT'], $GLOBALS['conf']->root . '/install/skel');
        files::checkFile('robots.txt', $_SERVER['DOCUMENT_ROOT'], $GLOBALS['conf']->root . '/install/skel');
        files::checkFile('favicon.ico', $_SERVER['DOCUMENT_ROOT'], $GLOBALS['conf']->root . '/install/skel');
        files::checkFile('conf.php', $_SERVER['DOCUMENT_ROOT'], $GLOBALS['conf']->root . '/install/skel');

        // --- check datatables

        $s .= '<h2>Checking datatables</h2>';
        $this->checkDatatables();
        $s .= 'Done.</div>';
        return $s;
    }

    /*
     * Checks critical conf values
     */

    static function checkConfFile() {

        require ($_SERVER['DOCUMENT_ROOT'] . '/conf.php');

        // all conf values must be present, some critical, some may be empty.
        // conf items that shouldn't ever empty:
        $aConf = array('dbserver', 'dbuser', 'dbpass', 'dbname', 'root', 'tmpdir', 'locale', 'timezone', 'defaultoid', 'defaultlay', 'sitename', 'siteurl', 'adminemail');
        $s = '';
        foreach ($aConf as $k) {
            if (empty($conf->$k)) {
                $s .= 'Please fill $conf->' . $k . ' in your conf.php file located at site root directory.<br>';
            }
        }

        // conf items that can be zero, empty string or bool
        $aConf2 = array('cache', 'stats', 'smtphost', 'smtpuser', 'smtppass');
        foreach ($aConf2 as $k) {
            if (!isset($conf->$k)) {
                $s .= 'Please create a $conf->' . $k . ' value in your conf.php file (even if zero, empty or false).<br>';
            }
        }

        return $s;
    }

    /*
     *
     */

    public function checkDatatables() {
        global $ob, $conf;
        $sql = 'select oid from broker where class="datatable"';
        $a = $ob->dbSelectColumn($sql);
        if (empty($a)) {
            return;
        }
        $s = 'Found datatables: ' . implode(',', $a) . '<br>';
        require_once($conf->root . '/obj/datatable/datatable.php');
        $dt = new datatable();
        foreach ($a as $oid) {
            $dt->checkIntegrity($oid);
        }
        return $s;
    }

}
