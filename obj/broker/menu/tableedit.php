<?php

/*
 * Generates a Javascript backoffice table editor for datatables
 */

class tableedit {

    var $group = "admin";
    var $descr = 'Javascript table editor';
    var $show = false;
    var $public = false;
    
    var $datatableOid = false;
    var $table = false;
    
    function exec() {
        
        global $ob, $conf;
        require_once($conf->root . '/ihtml.php');

        $this->datatableOid = $ob->param('oid', 'digits', false);
        $this->table = 'datatable' . $datatableOid;
        
        if ($ob->dbTableExists($this->table) === false) $ob->log('tableedit: Table does not exist.', true);
        
        //TODO: permissions
        
        $jsTableEditor = ihtml::ieditor($this->table, 0);  // menu class, rid
        return ihtml::titleDescContentBox('Editing...', 'Description here.', $jsTableEditor);
    }

    
    /*
     * Custom script calls editor
     */

    function editor($startoid) {
        $_GET['startoid'] = $startoid;
        return $this->exec();
    }

    
    /*
     * jsTableEditor calls this to get table data
     */

    public function getdata() {
        
        global $ob, $conf;

        $startOid = $ob->param('startoid', 'digits', false);
        
        if (empty($startOid))
            $startOid = 1;

        // get metadata
        
        require_once($conf->root . '/obj/datatable/datatable.php');
        $odt = new datatable();
        $ud = $odt->createSkel($odt->getProperties($odt->entityOid($this->datatableOid)));
        
        // add oid
        
        $ud['content']['oid'] = array(
            'name' => 'oid',
            'type' => 'integer',
            'desc' => 'Id'
        );
        
        // build sql select
        
        $sql = 'select oid,';

        foreach ($ud['content'] as $k => $a) {
            //TODO:advanced profile permissions
            $sql .= $k . ',';
        }

        $sql = substr($sql, 0, strlen($sql) - 1) . ' from ' . $this->table . ' where oid >=' . $startOid . ' order by oid limit 50';

        // add data
        
        $ud['data'] = $ob->select($sql);

        // add configurations to br ("broker") data:
        
        $ud['br']['addbutton'] = 1;
        $ud['br']['sortable'] = ''; // rows can be ordered
        $ud['br']['deletebutton'] = '1';
        $ud['br']['tableheader'] = '1';
        $ud['br']['columnsort'] = '1';

        // hidden columns
        
        $ud['content']['oid']['show'] = '0';
        $ud['content']['xprior']['show'] = '0';
        $ud['content']['rid']['show'] = '0';

        // db selects
        // dynamic properties like dbselect must load options from db

        foreach ($ud['content'] as $k => $a) {
            if ($a['type'] == 'dbselect')
                $a_def['content'][$k]['select'] = $ob->select($a['sql']);
        }

        // read-only columns
        //$ud['content']['xname']['editable'] = '0';
        
        print json_encode($ud);
        return '';
    }

    
    
    
    /*
     *  This function is called by the jsTableEditor
     *  Creates a new empty row and returns new oid to jsTableEditor
     */

    function insertRow() {

        global $ob;
        
        $data = $_POST['data']; // array with key=>value pairs
        
        //TODO: security for non-admin users here on in $ob ?
        
        $oid = $ob->dbInsert($this->table, $data);
        print json_encode($oid);
    }

    /*
     * This function is called by the jsTableEditor
     * It updates a whole row of data in a table
     */

    function updateRow() {
        global $ob;
        $data = $_POST['data']; // array with key=>value pairs
        $ob->dbUpdate($this->table, $ob->param('id', 'digits', false), $data);
    }

    /*
     * This function is called by the jsTableEditor
     * It updates a single item of data in a specified row
     */

    function updateCell() {
        global $ob;
        $ob->updateRow('dob_user', $_POST['key'], $_POST['value'], $_POST['oid']);
    }

    /*
     * This function is called by the jsTableEditor
     * It deletes a whole row
     */

    function deleteRow() {
        $GLOBALS['ob']->deleteRow('dob_user', $_POST['oid']);
    }

}

?>