<?php

/*
 * Menu option: dashboard
 */

class dashboard {

    var $group = "main";
    var $descr = "Dashboard";
    var $show = true;
    var $public = false;

    function exec() {

        //TODO: finish
        // latest objects added

        global $ob;

        $s = '<h1>Dashboard</h1>';
        $a = $ob->latestObjectsAdded();
        $s .= $this->box("Latest added", $this->objectList($a, 0));


        // latest objects edited
        $a = $ob->latestObjectsEdited();
        $s .= $this->box("Latest edited", $this->objectList($a, 1));

        return $s;
    }

    function box($title, $content) {
        return '<div class="dashboard-box"><div class="dashboard-title">' . $title . '</div>' . $content . '</div>';
    }

    function objectList($a, $op = 0) {

        if (empty($a))
            return '<div>You don\'t have objects yet.</div>';

        global $oAuth;

        $s = '';

        if ($op == 0)
            $ts = 'tscreate';
        else
            $ts = 'tsupdate';

        foreach ($a as $aa) {

            $s .= '<div><a href="/do/?m=broker.browse&oid=' . $aa['oid'] . '"><span class="dash-ts">' . substr($aa[$ts], 0, -3) . '</span> ' . $aa['easyname'] . '</a> <span class="dash-author"> ' . $oAuth->userNameById($aa['iduser']) . '</span></div>';
        }

        return $s;
    }

}

?>