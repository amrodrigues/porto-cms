<?php

/*
 * Displays an error page
 */

class errorpage {

    var $group = "none";
    var $descr = '';
    var $show = true;
    var $public = true;

    function exec() {
        return '<div class="error">
                <h1>Oops!... Something went wrong.</h1>
                Help us solve it :)
            ';
    }

}

?>