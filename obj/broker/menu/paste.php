<?php

/*
 * Menu option: Move selected objects
 */

//TODO: refactor name

class paste {

    var $group = "";
    var $descr = 'Paste';
    var $show = false;
    var $public = false;

    function exec() {

        global $ob, $oAuth;

        if (!empty($_GET['pid']))
            $pid = (int) $_GET['pid'];
        else
            return "no pid";

        if (!empty($_GET['oids']))
            $oids = $_GET['oids'];
        else
            return "no oids";

        $ob->log('broker.paste: at ' . $pid . ' oids ' . $oids);

        $aOids = explode(',', $oids);

        if (empty($aOids))
            return "invalid oids";

        foreach ($aOids as $id) {

            if ((int) $id < 1)
                continue;

            $br = $ob->getBr($id);

            if ($oAuth->pass('move', $br['class'], $br))
                $ob->objectMove($id, $pid);
        }

        print 'OK';
    }

}

?>