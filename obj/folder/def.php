<?php

$a_def = array(
    'classes' => '*',
    'br' => array(
        'class' => 'folder',
        'easyname' => 'Folder',
        'cache' => false
    ),
    'content' => array(
        'title' => array(
            'name' => 'title',
            'type' => 'textline',
            'desc' => 'Title',
            'optional' => true,
            'show' => true
        ),
        'metadescription' => array(
            'name' => 'metadescription',
            'type' => 'text',
            'desc' => 'Description metatag',
            'optional' => true,
            'show' => true
        ),
        'metakeywords' => array(
            'name' => 'metakeywords',
            'type' => 'text',
            'desc' => 'Keywords metatag',
            'optional' => true,
            'show' => true
        )
    )
);
