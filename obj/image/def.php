<?php

$a_def = array(
    'classes' => false,
    'br' => array(
        'class' => 'image',
        'easyname' => 'Image',
        'cache' => true
    ),
    'content' => array(
        'userfile' => array(
            'name' => 'userfile',
            'type' => 'userfile',
            'desc' => 'Image filename',
            'optional' => true
        ),
        'title' => array(
            'name' => 'title',
            'type' => 'textline',
            'desc' => 'Title',
            'optional' => true,
            'show' => true
        ),
        'alt' => array(
            'name' => 'alt',
            'type' => 'textline',
            'desc' => 'Alt. description',
            'optional' => true
        ),
        'align' => array(
            'name' => 'align',
            'type' => 'fradio',
            'desc' => 'Align',
            'optional' => true,
            'default' => 1,
            'xselect' => array(1 => 'Left', 2 => 'Right', 3 => 'Center', 4 => 'None (inside a div)')
        ),
        'linkto' => array(
            'name' => 'linkto',
            'type' => 'textline',
            'desc' => 'Hyperlink to',
            'optional' => true
        ),
        'thumb' => array(
            'name' => 'thumb',
            'type' => 'integer',
            'desc' => 'Width',
            'optional' => true
        ),
        'thumby' => array(
            'name' => 'thumby',
            'type' => 'integer',
            'desc' => 'Height',
            'optional' => true
        ),
        'popup' => array(
            'name' => 'popup',
            'type' => 'bool',
            'desc' => 'Zoom',
            'optional' => true
        ),
        'label' => array(
            'name' => 'label',
            'type' => 'textline',
            'desc' => 'Label',
            'optional' => true
        ),
        'txt' => array(
            'name' => 'txt',
            'type' => 'richtext',
            'desc' => 'Long description',
            'optional' => true
        ),
        'imgmap' => array(
            'name' => 'imgmap',
            'type' => 'code',
            'desc' => 'Image map (just area elements)',
            'optional' => true
        ),
        'cssclass' => array(
            'name' => 'cssclass',
            'type' => 'textline',
            'desc' => 'Add CSS class',
            'optional' => true
        )
    )
);
