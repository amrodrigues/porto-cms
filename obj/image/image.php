<?php

class image {

    function asHtml($ud) {

        global $ob, $conf;

        $lay = '';
        $oid = $ud['br']['oid'];
        $ext = $ud['content']['userfile']['value'];
        $url = $ob->objectGetFilename($oid, $ext);

        // --- optional width / height

        list($origw, $origh, $type, $attr) = getimagesize($_SERVER['DOCUMENT_ROOT'] . $url);

        // --- width

        if ($ud['content']['thumb']['value'] > 0) {
            $tw = $ud['content']['thumb']['value'];
            $width = $tw;
        } else {

            $tw = 0;
            $width = $origw;
        }

        // --- height

        if ($ud['content']['thumby']['value'] > 0) {
            $th = $ud['content']['thumby']['value'];
            $height = $th;
        } else {
            $th = 0;
            $height = $origh;
        }


        switch ($ud['content']['align']['value']) {

            case 1:
                $align = 'left';
                break;
            case 2:
                $align = 'right';
                break;
            case 3:
                $align = 'center';
                break;
            default:
                $align = false;
                break;
        }

        if ($align !== false) {
            $lay .= '<div class="image-' . $align . '">';
        } elseif ($ud['content']['align']['value'] == 4) {
            $lay = '<div>';
        }


        // --- start link ----

        if (!empty($ud['content']['linkto']['value'])) {
            $lay .= '<a href="' . $ud['content']['linkto']['value'] . '">';
        }

        // --- thumb? ---

        if ($tw > 0 || $th > 0) {
            // use thumbnail

            $thumb_url = $ob->getDirectory($oid) . '/' . $oid . '_' . $width . '_' . $height . '.' . $ext;

            // --- generate thumb?

            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $thumb_url)) {

                global $oThumb;

                if (empty($oThumb)) {
                    require_once($conf->root . '/thumb.php');

                    $oThumb = new thumb();
                }

                $sourceFn = $ob->getFilename($oid, $ext, true);
                $oThumb->source($sourceFn, $width, $height);
                $destFn = $ob->getDirectory($oid, true) . '/' . $oid . '_' . $width . '_' . $height . '.' . $ext;
                $oThumb->regularThumb($destFn);
            }
        } else {

            $thumb_url = '';
            $src = $url; // use direct url
        }


        // --- IMG tag

        $lay .= '<img ';

        if (!empty($ud['content']['cssclass']['value'])) {
            $lay .= ' class="' . $ud['content']['cssclass']['value'] . '" ';
        }

        if (!empty($ud['content']['popup']['value'])) {
            $lay .= 'onmouseover="style.cursor=\'pointer\'" ';
        }

        $thumb_url != '' ? $lay .= 'src="' . $thumb_url . '" ' : $lay .= 'src="' . $src . '" ';
        $lay .= 'title="' . $ud['content']['title']['value'] . '" alt="' . $ud['content']['alt']['value'] . '" ';

        if ($tw == 0 && $th == 0)
            $lay .= $attr;

        // image map:

        if (!empty($ud['content']['imgmap']['value'])) {
            $lay .= ' usemap="#imagemap' . $oid . '"><map name="imagemap' . $oid . '">' . $ud['content']['imgmap']['value'] . '</map>';
        } else {

            $lay .= '>';
        }

        // --- end link

        if (!empty($ud['content']['linkto']['value']) || !empty($ud['content']['popup']['value']))
            $lay .= '</a>';




        // --- label below image

        if (!empty($ud['content']['label']['value'])) {
            $lay .= '<div class="image-label">' . $ud['content']['label']['value'] . '</div>';
        }



        if ($align !== false || $ud['content']['align']['value'] == 4)
            $lay .= '</div>';

        return $lay;
    }

}

?>