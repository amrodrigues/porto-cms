<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'xfile',
        'easyname' => 'File',
        'cache' => true
      ),
      'content' => array(
        'userfile' => array(
          'name' => 'userfile',
          'type' => 'userfile',
          'desc' => 'Local file path'
        )
      )
    );
?>