<?php

class xfile {

    function asHtml($ud) {

        global $ob;

        $ext = $ud['content']['userfile']['value'];
        $fn = $ob->objectGetFilename($ud['br']['oid'], $ext);
        $fn_abs = $ob->objectGetFilename($ud['br']['oid'], $ext, 1);
        return xfile::present($fn, $fn_abs, $ext, $ud['br']['easyname'], 'xfile');
    }

    static function present($fn, $fn_abs, $ext, $easyname, $cssprefix) {
        global $conf;
        $lay = '';
        if (file_exists($fn_abs)) {
            $size = (filesize($fn_abs) / 1024);
            if ($size > 1024) {
                $size = number_format($size / 1024, 1) . ' MB';
            } else {
                $size = number_format($size) . ' KB';
            }
            if ($size > 0) {
                $lay = '
                <div class="' . $cssprefix . '">
                  <div class="' . $cssprefix . '-title"><a href="' . $fn . '">' . $easyname . '</a></div>
                  <div class="' . $cssprefix . '-info">' . $size . '</div>
                  <div class="' . $cssprefix . '-icon"><a href="' . $fn . '"><img alt="' . $ext . '" src="/obj/xfile/icons/';
                if (file_exists($conf->root . '/obj/xfile/icons/' . $ext . '.png'))
                    $lay .= $ext;
                else
                    $lay .= 'default';
                $lay .= '.png" /></a></div>

                </div>
            ';
            }
        }
        return $lay;
    }

}
