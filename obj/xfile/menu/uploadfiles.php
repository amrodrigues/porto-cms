<?php

/*
 * File upload helper
 * Uploads a zip file, extracts it, and creates one xfile object for each file
 */

class uploadfiles {

    var $group = "helpers";
    var $descr = "Upload files";
    var $show = true;
    var $public = false;

    function exec() {

	global $ob, $conf;

	require ($conf->root . '/files.php');

	$oid = $_SESSION['object'];
	$ud = $ob->get($oid);

	$s = '';

	if (empty($_FILES['sys_zip']['tmp_name'])) {

	    $s .= '<h2>Upload multiple files at ' . $ud['br']['easyname'] . '</h2>';
	    $s .= '<form method="post" enctype="multipart/form-data" action="/do/?m=xfile.uploadfiles">';
	    $s .= '<input type="file" name="sys_zip"><br> <br>';
	    $s .= '<input type="submit" value="Upload ZIP file and extract...">';
	    $s .= '</form>';
	    
	    return $s;
	    
	} else {

	    $o_file = new files();
	    $sid = session_id();
	    $tmpdir = $conf->tmpdir . '/' . $sid;
	    $ob->fsCheckDir($tmpdir);

	    exec("cd $tmpdir; unzip " . $_FILES['sys_zip']['tmp_name']);

	    $a_list = $o_file->findAllFiles($tmpdir);
	    
	    rsort($a_list);

	    $i_prior = 1;

	    foreach ($a_list as $s_fn) {

		$s_path = $tmpdir . '/' . $s_fn;

		$ud = $ob->loadSkel('xfile');
		$ud['br']['easyname'] = $s_fn;
		$ud['br']['pid'] = $_SESSION['object'];
		$ud['br']['prior'] = $i_prior;
		foreach ($ud['content'] as $k => $a) {
		    $ud['content'][$k]['value'] = '';
		}
		$ud['content']['userfile']['value'] = strtolower($o_file->extension($s_fn));
		$ud['br']['oid'] = $ob->newObject($ud);

		$dest_path = $ob->getFilename($ud['br']['oid'], $ud['content']['userfile']['value'], true);
		rename($s_path, $dest_path);
		chmod($dest_path, 0755);

		$i_prior++;
	    }

	    exec('rm -rf ' . $tmpdir);
	    header('Location: /do/?m=broker.browse');
	}
    }

}

?>