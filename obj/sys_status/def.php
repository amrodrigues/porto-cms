<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'sys_status',
        'easyname' => 'Status post',
        'cache' => false
      ),
      'content' => array(
        'rid' => array(
          'name' => 'rid',
          'type' => 'integer',
          'desc' => 'Object oid',
          'indexed' => true
        ),
        'ts' => array(
          'name' => 'ts',
          'type' => 'datetime',
          'desc' => 'Timestamp'
        ),
        'iduser' => array(
          'name' => 'iduser',
          'type' => 'integer',
          'desc' => 'Name',
          'optional' => true
        ),
        'xname' => array(
          'name' => 'xname',
          'type' => 'textline',
          'desc' => 'Name',
          'optional' => true
        ),
          'email' => array(
          'name' => 'email',
          'type' => 'textline',
          'desc' => 'Email',
          'optional' => true
        ),
        'iduser' => array(
          'name' => 'iduser',
          'type' => 'integer',
          'desc' => 'User id',
          'optional' => false
        ),
        'txt' => array(
          'name' => 'txt',
          'type' => 'text',
          'desc' => 'Post',
          'optional' => true
        )
      )
    );
?>