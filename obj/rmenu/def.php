<?php

$a_def = array(
    'classes' => array('folder' => '*'),
    'br' => array(
	'class' => 'rmenu',
	'easyname' => 'Responsive menu'
    ),
    'content' => array(
	'pid' => array(
	    'name' => 'pid',
	    'type' => 'integer',
	    'desc' => 'Id of menu items folder'
	)
    )
);
?>
