
  manageMenuWidth(); // initial menu

  $(window).resize(manageMenuWidth);


  // --- manage menu placing or removing visible items:

  function manageMenuWidth() {

    // --- When screen is smaller, move items to a collapsed menu:
    var addw = 310;

    while (
            $("#rmenu-items div").length > 0 &&
            $("#rmenu").outerWidth() + addw + 35 >= $(window).width()
    ) {
            $("#rmenu-items div:last-child").prependTo("#rmenu-collapsed-items");
    }


    // --- When screen is larger, add items from collapsed menu:

    while (
            $("#rmenu-collapsed-items div").length > 0 &&
            $("#rmenu").outerWidth() + addw + $("#rmenu-collapsed-items div:first-child").outerWidth() <= $(window).width()
    ) {

        $("#rmenu-collapsed-items div:first-child").appendTo("#rmenu-items");
    }

    rmenuIcon();

    highlightMenuOption();

    //console.log("----------------");

    //console.log("#top childrenTotalWidth: " + childTotalWidth($("#top")) + "px");
    //console.log("#rmenu-items nr. items:" + $("#rmenu-items div").length);
    //console.log("#top outerwidth:" + $("#top").outerWidth());
    //console.log("Document width: " + $(document).width());

    //console.log("#rmenu-items: " + $("#rmenu-items").outerWidth() + "px.");
    //console.log("#rmenu-items: " + $("#rmenu-items div").length + " items");
    //console.log("#rmenu-collapsed-items: " + $("#rmenu-collapsed-items div").length + " items");
    //console.log("#rmenu is " + $("#rmenu").outerWidth() + "px");
    //console.log("---");


}



// --- collapsed menu ICON:

function rmenuIcon() {

    if ($("#rmenu-collapsed-items div").length > 0) {

        //console.log("collapsed items exist");
        $("#rmenu-collapsed-icon").css("display", "table-cell"); //appendTo("#rmenu-collapsed-icon-place");
        $("#rmenu-collapsed-items").appendTo("#rmenu-hidden"); // hide collapsed menu

    } else {

       //console.log("collapsed items do NOT exist");
       $("#rmenu-collapsed-icon").css("display", "none"); //appendTo("#rmenu-hidden");
       $("#rmenu-collapsed-items").appendTo("#rmenu-hidden"); // hide collapsed menu
    }
 }


// --- colapsed menu:

function showCollapsedMenu() {
    $("#rmenu-collapsed-items").appendTo("#rmenu-collapsed");
}


function hideCollapsedMenu() {
    $("#rmenu-collapsed-items").appendTo("#rmenu-hidden");
}


function toggleCollapsedMenu() {
    if ($("#rmenu-hidden #rmenu-collapsed-items").length > 0) {
      showCollapsedMenu();
    } else {
      hideCollapsedMenu();
    }
}


// returns sum of pixel width of child elements:

function childTotalWidth(elParent) {
    var totalWidth = 0;
    $(elParent).children().each(
         function(ix,el) {
            totalWidth += $(el).outerWidth();
         }
     );
    return totalWidth;
}


function urlParam(s) {

  var url = window.location.search.substring(1);
  var vars = url.split('/');
  for (var i = 0; i < vars.length; i++)  {
    var param = vars[i].split('=');
    if (param[0] == s)  {
      return param[1];
    }
  }
  return '';

}


function highlightMenuOption() {

  var url = window.location.href;
  var vars = url.split('/');
  var href = '/' + vars[3];

  $(".rmenu-item a[href='" + href + "']").attr("class", "rmenu-item-active");

}
