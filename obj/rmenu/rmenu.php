<?php

/* Responsive menu
 * - Integrate site logo? (does not belong to the concept of a menu)
 */
class rmenu {

    function asHtml($ud) {

	$sql = 'select oid, easyname from broker where pid=' . $ud['content']['pid']['value'] . ' and publish=1 order by prior';
	$aItems = $GLOBALS['ob']->dbSelect($sql);

	$GLOBALS['ob']->includeJquery();

	$s = '
            <div id="rmenu">
               <div id="rmenu-bar">
                  <div id="rmenu-items">
        ';

	if (!empty($aItems))
	    foreach ($aItems as $a)
		$s .= '<div class="rmenu-item"><a href="/' . $a['oid'] . '">' . $a['easyname'] . '</a></div>';

	$s .= '
                 </div>

               <div id="rmenu-collapsed-icon">
                  <a href="#" onclick="toggleCollapsedMenu();return false;"><img src="/obj/rmenu/menu.png" /></a>
               </div>

              </div>
            </div>

            <div id="rmenu-collapsed"></div>
            <div id="rmenu-hidden"><div id="rmenu-collapsed-items"></div>

          </div>
          <script type="text/javascript" src="/obj/rmenu/rmenu.js"></script>
            ';

	return $s;
    }

}

?>