<?php

class jchart {

    function asHtml($ud) {

        global $ob;

        $ob->includeJquery();
        $ob->includeScriptTag('/obj/jchart/mod/chart.min.js');
        $ob->includeScriptTag('/obj/jchart/mod/chartjs-option.js');

        // ----- prepare plot data

        $sql = $ud['content']['xsql']['value'];
        if (strpos($sql, '[[') !== false) {
            $ot = &$ob->helper('itengine');
            $sql = $ot->parseSQL($sql);
        }
        $data = $ob->dbSelect($sql);
        $groupby = $plotdata = array();

        foreach ($data as $row) {
            $plotdata[$row['label']] = array();
            $groupby[$row['groupby']] = $row['groupby'];
        }

        foreach ($data as $row) {
            $plotdata[$row['label']][$row['groupby']] = $row['value'];
        }

        $aColors = $plotdata;
        if (empty($ud['content']['pcolors']['value'])) {
            $aDefaultColors = array(
                array("rgba(255,0,0,0.1)", "rgba(255,0,0,1)", "rgba(255,0,0,1)", "#fff"),
                array("rgba(0,255,0,0.1)", "rgba(0,255,0,1)", "rgba(0,255,0,1)", "#fff"),
                array("rgba(0,0,255,0.1)", "rgba(0,0,255,1)", "rgba(0,0,255,1)", "#fff"),
                array("rgba(100,100,100,0.1)", "rgba(100,100,100,1)", "rgba(100,100,100,1)", "#fff"),
                array("rgba(255,255,0,0.1)", "rgba(255,255,0,1)", "rgba(255,255,0,1)", "#fff"),
                array("rgba(0,255,255,0.1)", "rgba(0,255,255,1)", "rgba(0,255,255,1)", "#fff"),
                array("rgba(128,0,0,0.1)", "rgba(128,0,0,1)", "rgba(128,0,0,1)", "#fff"),
                array("rgba(0,128,0,0.1)", "rgba(0,128,0,1)", "rgba(0,128,0,1)", "#fff"),
                array("rgba(0,0,128,0.1)", "rgba(0,0,128,1)", "rgba(0,0,128,1)", "#fff"),
                array("rgba(128,128,128,0.1)", "rgba(128,128,128,1)", "rgba(128,128,128,1)", "#fff"),
                array("rgba(0,128,128,0.1)", "rgba(0,128,128,1)", "rgba(0,128,128,1)", "#fff")
            );
            $i = 0;
            foreach ($aColors as $k => $v) {
                $aColors[$k] = $aDefaultColors[$i];
                $i++;
            }
        } else { // Custom colors:
            $ac = explode("\n", trim($ud['content']['pcolors']['value']));
            $i = 0;
            foreach ($aColors as $k => $v) {
                $aColors[$k] = explode(';', $ac[$i]);
                $i++;
            }
        }

        // ----- build embedded js
        $bot = '
            <script>
                var data = {
                    labels : [';
        foreach ($groupby as $label) {
            $bot .= '"' . $label . '",';
        }
        $bot = substr($bot, 0, -1);
        $bot .= '],
                    datasets : [
        ';
        // ------ For each line to plot:
        foreach ($plotdata as $label => $values) {

            $bot .= '
                {
                   fillColor : "' . $aColors[$label][0] . '",
                   strokeColor : "' . $aColors[$label][1] . '",
                   pointColor : "' . $aColors[$label][2] . '",
                   pointStrokeColor : "' . $aColors[$label][3] . '",
                   data : [';
            foreach ($groupby as $xlabel) {
                if (isset($values[$xlabel]))
                    $bot .= $values[$xlabel] . ',';
                else
                    $bot .= '0,';
            }
            $bot = substr($bot, 0, -1);
            $bot .= ']
                      },';
        }
        $bot = substr($bot, 0, -1);
        $bot .='
                    ]
                };
                respChart($("#i' . $ud['br']['oid'] . '"), data, ';
        if (!empty($ud['content']['coptions']['value'])) {
            $bot .= $ud['content']['coptions']['value'] . ', ';
        } else {
            $bot .= 'false, ';
        }
        switch ($ud['content']['ctype']['value']) {
            case 1:
                $bot .= '"line"';
                break;
            case 2:
                $bot .= '"bar"';
                break;
            case 3:
                $bot .= '"radar"';
                break;
        }
        $bot .= ');
              </script>
            ';
        $ob->addBottom('i' . $ud['br']['oid'], $bot);

        // ------
        $lay = '<div class="jchart"><canvas class="jchart" id="i' . $ud['br']['oid'] . '" width="800"';
        if (!empty($ud['content']['h']['value']))
            $lay .= ' height="' . $ud['content']['h']['value'] . '"';
        $lay .= '></canvas></div>';
        return $lay;
    }

}
