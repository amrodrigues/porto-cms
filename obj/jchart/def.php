<?php

$a_def = array(
    'classes' => false,
    'br' => array(
        'class' => 'jchart',
        'easyname' => 'Chart',
        'cache' => true
    ),
    'content' => array(
        'ctype' => array(
            'name' => 'ctype',
            'type' => 'fselect',
            'desc' => 'jChart type',
            'xselect' => array(
                1 => 'Line',
                2 => 'Bar',
                3 => 'Radar'
            ),
            'default' => 1
        ),
        'xsql' => array(
            'name' => 'xsql',
            'type' => 'code',
            'desc' => 'SQL select with 3 cols: label,value,groupby'
        ),
        'pcolors' => array(
            'name' => 'pcolors',
            'type' => 'code',
            'desc' => 'fillColor;strokeColor;pointColor;pointStrokeColor for each line',
            'optional' => true
        ),
        'coptions' => array(
            'name' => 'coptions',
            'type' => 'code',
            'desc' => 'Chart options js',
            'optional' => true
        ),
        'h' => array(
            'name' => 'h',
            'type' => 'integer',
            'desc' => 'Force height',
            'optional' => true
        )
    )
);
