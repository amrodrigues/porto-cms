<?php

class media {

    function asHtml($ud) {
        
        global $ob;
        
        $ob->includeJquery();
        
        $ob->addBottom('oembed', '
        <script defer="defer" src="/obj/media/mod/oembed/jquery.oembed.js"></script>
        <script defer="defer" language="javascript">
            $(function(){
                $("a.embed").oembed();
            });
        </script>
        ');

        return '<div class="media"><a class="embed" href="' . $ud['content']['url']['value'] . '"></a></div>';
    }

}

?>