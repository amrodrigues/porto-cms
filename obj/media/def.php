<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'media',
        'easyname' => 'Media',
        'cache' => true
      ),
      'content' => array(
        'url' => array(
          'name' => 'url',
          'type' => 'textline',
          'desc' => 'Media webpage URL',
          'optional' => true
        )
      )
    );
?>