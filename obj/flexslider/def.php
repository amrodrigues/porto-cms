<?php

$a_def = array(
    'classes' => array('image' => '*'),
    'br' => array( 
	'class' => 'flexslider',
	'easyname' => 'Flex slider'
    ),
    'content' => array(
        
	'w' => array(
	    'name' => 'w',
	    'type' => 'integer',
	    'optional' => true,
	    'desc' => 'Resize to width'
	),
        'h' => array(
	    'name' => 'h',
	    'type' => 'integer',
	    'optional' => true,
	    'desc' => 'resize to height'
	),
	'ijs' => array(
	    'name' => 'ijs',
	    'desc' => 'Override default js',
	    'type' => 'code',
	    'optional' => true
	),
	'icss' => array(
	    'name' => 'icss',
	    'desc' => 'Include Flexslider default CSS',
	    'type' => 'bool',
	    'optional' => true
	)
    )
);