<?php

class flexslider {

    function asHtml($ud) {

        global $ob, $oAuth, $conf, $oThumb;

        $lay = '';

        $ob->includeJquery();
        $ob->includeScriptTag('/obj/flexslider/mod/jquery.flexslider.js');

        if (!empty($ud['content']['icss']['value'])) {
            $ob->includeCss('/obj/flexslider/mod/flexslider.css');
        }

        if (!empty($ud['content']['ijs']['value'])) {
            $ob->includeScript('i' . $ud['br']['oid'], $ud['content']['ijs']['value']);
        } else {

            $ob->includeScript('flexslider' . $ud['br']['oid'], '
                $(window).load(function(){
                  $(\'#i' . $ud['br']['oid'] . '\').flexslider({
                    animation: "fade",
                    start: function(slider){
                      $(\'body\').removeClass(\'loading\');
                    }
                  });
                });
	');
        }

        $sql = 'select broker.oid, image.title, image.userfile, image.alt, image.linkto from broker, image where broker.oid = image.oid and broker.pid = ' . $ud['br']['oid'] . $oAuth->sqlRestrict('view') . ' order by prior';
        $a = $ob->dbSelect($sql);

        if (empty($a)) {
            return '';
        }

        $lay .= '
         <div class="flexslider" id="i' . $ud['br']['oid'] . '">
          <ul class="slides">
          ';

        if (!empty($ud['content']['w']['value']) && !empty($ud['content']['h']['value'])) {
            require_once($conf->root . '/thumb.php');
            $oThumb = new thumb();
            foreach ($a as $aa) {
                $oThumb->checkThumb($aa['oid'], $aa['userfile'], $ud['content']['w']['value'], $ud['content']['h']['value']);
            }
        }

        foreach ($a as $aa) {

            if (!empty($aa['linkto'])) {
                $link = $aa['linkto'];
            } else {
                $link = false;
            }

            if (is_object($oThumb)) {
                $src = $oThumb->thumbUrl($aa['oid'], $aa['userfile'], $ud['content']['w']['value'], $ud['content']['h']['value']);
            } else {
                $src = $ob->getFilename($aa['oid'], $aa['userfile']);
            }

            $lay .= '<li data-thumb="' . $src . '">';

            if ($link) {
                $lay .= '<a href="' . $link . '">';
            }

            $lay .= '<img src="' . $src . '">';

            if ($link) {
                $lay .= '</a>';
            }
            $lay .= '</li>';
        }

        $lay .= '</ul></div>';
        return $lay;
    }

}
