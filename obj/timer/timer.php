<?php

/*
 * A timer show objects below itself between two dates
 */

class timer {

    function asHtml($ud) {

        $startdate = strtotime($ud['content']['startdate']['value']);
        $enddate = strtotime($ud['content']['enddate']['value']) + 86400;
        $today = time();
        
        if($today >= $startdate && $today <= $enddate) {
            return '{[1]}';
        }
	return '';
    }

}

?>