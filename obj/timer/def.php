<?php

$a_def = array(
    'classes' => '*',
    'br' => array(
        'class' => 'timer',
        'easyname' => 'Timer',
        'cache' => true
    ),
    'content' => array(
        'startdate' => array(
            'name' => 'startdate',
            'type' => 'date',
            'desc' => 'Start date',
            'optional' => false,
            'show' => true
        ),
        'enddate' => array(
            'name' => 'enddate',
            'type' => 'date',
            'desc' => 'End date',
            'optional' => false,
            'show' => true
        )
        
    )
);
?>