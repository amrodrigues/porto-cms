<?php

$a_def = array(
    'classes' => '*',
    'br' => array(
        'class' => 'box',
        'easyname' => 'Box',
        'cache' => true
    ),
    'content' => array(
        'title' => array(
            'name' => 'title',
            'type' => 'textline',
            'desc' => 'Title',
            'optional' => true
        ),
        'align' => array(
            'name' => 'align',
            'type' => 'fradio',
            'desc' => 'Align',
            'optional' => true,
            'xselect' => array(1 => 'Left', 2 => 'Right', 3 => 'Center')
        ),
        'txt' => array(
            'name' => 'txt',
            'type' => 'richtext',
            'desc' => 'Text',
            'optional' => false
        )
    )
);
?>