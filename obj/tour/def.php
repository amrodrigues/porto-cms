<?php

$a_def = array(
    'classes' => array('image' => 50, 'xfile' => 50, 'youtube' => 50),
    'br' => array(
        'class' => 'tour',
        'easyname' => 'Tour',
        'cache' => true
    ),
    'content' => array(
        'title' => array(
            'name' => 'title',
            'type' => 'textline',
            'desc' => 'Title',
            'show' => true
        ),
        'shortdesc' => array(
            'name' => 'shortdesc',
            'type' => 'textline',
            'desc' => 'Short description',
            'show' => true,
            'optional' => true
        ),
        'txt' => array(
            'name' => 'txt',
            'type' => 'richtext',
            'desc' => 'Description',
            'optional' => true
        ),
        'price' => array(
            'name' => 'price',
            'type' => 'textline',
            'desc' => 'Price',
            'optional' => true
        ),
        'bookicon' => array(
            'name' => 'bookicon',
            'type' => 'fradio',
            'desc' => 'Icon',
            'select' => array(
                1 => 'Book now',
                2 => 'Ask for a quote'
            ),
            'optional' => true
        ),
        'bookurl' => array(
            'name' => 'bookurl',
            'type' => 'textline',
            'desc' => 'Book / Quote URL',
            'optional' => true
        ),
        'featured' => array(
            'name' => 'featured',
            'type' => 'bool',
            'desc' => 'Featured tour',
            'optional' => true
        ),
        'ispack' => array(
            'name' => 'ispack',
            'type' => 'bool',
            'desc' => 'Classify as pack',
            'optional' => true
        ),
        'isevent' => array(
            'name' => 'isevent',
            'type' => 'bool',
            'desc' => 'Classify as event',
            'optional' => true
        )
    )
);
?>