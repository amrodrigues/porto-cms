<?php

class tour {

    function asHtml($ud) {

        global $ob, $oAuth, $conf;

        $lay = '
	    
	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_PT/all.js#xfbml=1&appId=107613032683702";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>    
    
<div class="tour">';

        // ---- head

        $lay .= ' <div class="tour-head"><h1>' . $ud['content']['title']['value'] . '</h1></div>';
        if (!empty($ud['content']['shortdesc']['value']))
            $lay .= '<div class="tour-shortdesc">' . $ud['content']['shortdesc']['value'] . '</div>';


        // ---- left

        $lay .='<div class="tour-body">
	    <div class="tour-left">';

        if (empty($conf->behavior['tour.imagewidth']) || empty($conf->behavior['tour.imageheight']))
            return 'Please insert a tour.imagewidth and tour.imageheight in a behavior object inside the layout';

        require_once($conf->root . '/imagesmart.php');
        $is = new imagesmart();
        $lay .= $is->imageorslider($ud['br']['oid'], $conf->behavior['tour.imagewidth'], $conf->behavior['tour.imageheight']);


        // ---- youtube video

        $sql = 'select broker.oid, youtube.* from broker, youtube where broker.oid = youtube.oid and broker.pid = ' . $ud['br']['oid'] . $oAuth->sqlRestrict('view') . ' order by prior';
        $aVideos = $ob->dbSelect($sql);
        if (!empty($aVideos)) {

            foreach ($aVideos as $av) {
                $lay .= '<div class="tour-video">' . $is->youtube($conf->behavior['tour.imagewidth'], $conf->behavior['tour.imageheight'], $av['url']) . '</div>';
            }
        }

        $lay .= '</div>';


        // ---- right

        $lay .= '<div class="tour-right">';

        $lay .= '<div class="tour-txt">' . $ud['content']['txt']['value'] . '</div>';

        if (!empty($ud['content']['price']['value']))
            $lay .= '<div class="tour-price">' . $ud['content']['price']['value'] . '&euro;</div>';

        // ---- add book button:

        if (!empty($ud['content']['bookurl']['value']) && !empty($ud['content']['bookicon']['value'])) {

            switch ($ud['content']['bookicon']['value']) {

                case 2:
                    if (empty($conf->behavior['tour.askquoteicon.url']))
                        return 'Please edit or place a behavior object inside the layout, having a <b>tour.askquoteicon.url</b> key-value.';
                    $iicon = $conf->behavior['tour.askquoteicon.url'];
                    break;
                case 1:
                    if (empty($conf->behavior['tour.bookicon.url']))
                        return 'Please edit or place a behavior object inside the layout, having a <b>tour.bookicon.url</b> key-value.';
                    $iicon = $conf->behavior['tour.bookicon.url'];
                    break;
            }

            $ob->includeReveal();

            // ---- ref to pass via url:
            
            //TODO: should be complete domain, not left part.

            $aa = explode('.', $_SERVER['SERVER_NAME']);
            $serverName = $aa[0];

            // ---- build url with ref

            $url = trim($ud['content']['bookurl']['value']);
            if (strpos($url, '?') !== false)
                $url .= '&';
            else
                $url .= '?';
            $url .= 'ref=' . $serverName;

            // ----

            $lay .= '<div class="tour-bookurl"><a data-reveal-id="bookmodal" data-animation="fade"><img src="' . $iicon . '"></a></div>';

            $lay .= '<div id="bookmodal" class="reveal-modal" style="width:985px">
		    <iframe src="' . $url . '" class="book-iframe"></iframe>
	                  </div>';
        }


        // ---- Files

        $sql = 'select broker.oid from broker where broker.pid = ' . $ud['br']['oid'] . ' and broker.class="xfile" ' . $oAuth->sqlRestrict('view') . ' order by prior';
        $aFiles = $ob->dbSelect($sql);

        if (!empty($aFiles)) {
            foreach ($aFiles as $af) {
                $udfile = $ob->get($af['oid']);
                $lay .= $ob->asHtml($udfile);
            }
        }


        $lay .= '
	    <div class="tour-comments">
	    <div class="fb-comments" data-href="http://' . $_SERVER['SERVER_NAME'] . '/' . $ud['br']['oid'] . '" data-width="360" data-num-posts="4"></div>
		</div>
	';


        $lay .= '</div>';



        $lay .= '</div></div>';

        return $lay;
    }

}

?>