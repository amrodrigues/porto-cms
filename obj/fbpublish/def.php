<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'fbpublish',
        'easyname' => 'Facebook publisher',
        'cache' => false
      ),
      'content' => array(
        'appid' => array(
          'name' => 'appid',
          'type' => 'textline',
          'desc' => 'App Id',
          'optional' => false
        ),
        'secret' => array(
          'name' => 'secret',
          'type' => 'textline',
          'desc' => 'Secret',
          'optional' => false
        ),
        'token' => array(
          'name' => 'token',
          'type' => 'textline',
          'desc' => 'Token',
          'optional' => false
        ),
        'publish' => array(
          'name' => 'publish',
          'type' => 'fcheck',
          'desc' => 'Publish',
          'optional' => true,
          'xselect' => array(
            1 => 'News',
            2 => 'Events',
            3 => 'Products',
            4 => 'Services'
          )
        ),
        'idpresenter' => array(
          'name' => 'idpresenter',
          'type' => 'integer',
          'desc' => 'Publish feed from a reporter object (id)',
          'optional' => true
        )
      )
    );
?>