<?php

    /*
     * Role
     * Defines a "user profile"
     */

    $a_def = array(
      'classes' => array('user' => '*'),
      'br' => array(
        'class' => 'role',
        'easyname' => 'Role',
        'cache' => false
      ),
      'content' => array(
        'title' => array(
          'name' => 'title',
          'type' => 'textline',
          'optional' => false,
          'desc' => 'Role name',
          'show' => true
        ),
        'perm' => array(
          'name' => 'perm',
          'type' => 'code',
          'optional' => true,
          'desc' => 'Permissions'
        ),
        'homeurl' => array(
          'name' => 'homeurl',
          'type' => 'textline',
          'desc' => 'After login jumps to URL...'
        ),
        'startobject' => array(
          'name' => 'startobject',
          'type' => 'integer',
          'desc' => 'Start object id'
        ),
        'logouturl' => array(
          'name' => 'logouturl',
          'type' => 'textline',
          'desc' => 'After logout jumps to URL...'
        )
      )
    );
?>