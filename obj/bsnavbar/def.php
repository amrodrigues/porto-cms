<?php

$a_def = array(
    'classes' => array('code' => 1, 'image' => 1),
    'br' => array(
        'class' => 'bsnavbar',
        'easyname' => 'Bootstrap Navbar'
    ),
    'content' => array(
        'rootid' => array(
            'name' => 'rootid',
            'type' => 'integer',
            'desc' => 'Root id',
            'optional' => true
        ),
        'brandhref' => array(
            'name' => 'brandhref',
            'type' => 'textline',
            'desc' => 'Href in brand image',
            'optional' => true
        )
    )
);
