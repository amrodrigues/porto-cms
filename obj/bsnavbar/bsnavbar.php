<?php

class bsnavbar {

    private $a = array(); // all folder items
    private $cnt = 0;     // $a count
    private $requestOid = false;

    /*
     *
     */

    function asHtml($ud) {
        global $ob;
        $this->requestOid = $ob->param('oid', 'digits', false);
        $lay = '
          <div role="navigation" class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                </button>
                ';

        if (!empty($ud['content']['brandhref']['value'])) {
            $lay .= '<a class="navbar-brand" href="' . $ud['content']['brandhref']['value'] . '">{[1]}</a>';
        }

        $lay .= '
              </div>
              <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">';
        $this->loadFolders();
        $aCh = $this->child($ud['content']['rootid']['value']);
        if (empty($aCh)) {
            return "";
        }
        foreach ($aCh as $aChItem) {
            $lay .= $this->get($aChItem, $this->child($aChItem['oid'])) . "\n";
        }
        return $lay . '</ul></div></div></div>';
    }

    /*
     * Load all folders
     */

    function loadFolders() {
        $sql = 'select broker.oid, broker.pid, broker.easyname from broker,folder where folder.oid = broker.oid ' . $GLOBALS['oAuth']->sqlRestrict('view') . ' order by pid';
        $this->a = $GLOBALS['ob']->dbSelect($sql);
        if (empty($this->a)) {
            return '';
        }
        $this->cnt = count($this->a);
    }

    /*
     * recursive function to merge all items
     */

    function get($br, $aChild) { //a = current row, aChild = row descendants
        $s = '';

        if (count($aChild) == 0) { //single menu item
            $s .= '<li';
            if ($br['oid'] == $this->requestOid) {

                $s .= ' class="active"';
            }
            $s .= '><a href="' . $GLOBALS['ob']->getUrl($br) . '">' . $br['easyname'] . '</a></li>';
            return $s;
        } else { //dropdown
            $s .= '
            <li class="dropdown">
                   <a href="' . $GLOBALS['ob']->getUrl($br) . '" class="dropdown-toggle" data-toggle="dropdown">' .
                    $br['easyname'] . ' <b class="caret"></b></a>
                   <ul class="dropdown-menu">';

            foreach ($aChild as $br2) {
                $aChild2 = $this->child($br2['oid']);
                $s .= $this->get($br2, $aChild2);
            }

            return $s . '
   		</ul>
                 </li>
          ';
        }
    }

    /*
     * Find item's descendants
     */

    function child($oid) {
        $a = array();
        for ($f = 0; $f < $this->cnt; $f++) {
            if ($this->a[$f]['pid'] == $oid) {
                $a[] = $this->a[$f];
            }
        }
        return $a;
    }

}
