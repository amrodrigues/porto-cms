<?php

$a_def = array(
    'classes' => false,
    'br' => array(
        'class' => 'property',
        'easyname' => 'Entity property',
        'cache' => false
    ),
    'content' => array(
        'xname' => array(
            'name' => 'xname',
            'type' => 'smalltext',
            'desc' => 'DB column name (lowercase, no spaces or punctuation)'
        ),
        'xtype' => array(
            'name' => 'xtype',
            'type' => 'textselect',
            'desc' => 'Data type',
            'xselect' => $GLOBALS['oType']->key_value()
        ),
        'xoptional' => array(
            'name' => 'xoptional',
            'type' => 'bool',
            'desc' => 'Optional'
        ),
        'xdefault' => array(
            'name' => 'xdefault',
            'type' => 'textline',
            'desc' => 'Default value'
        ),
        'xminchars' => array(
            'name' => 'xminchars',
            'type' => 'integer',
            'desc' => 'Minimum characters'
        ),
        'xmaxchars' => array(
            'name' => 'xmaxchars',
            'type' => 'integer',
            'desc' => 'Maximum characters'
        ),
        'xpublish' => array(
            'name' => 'xpublish',
            'type' => 'bool',
            'desc' => 'Public'
        ),
        'xselect' => array(
            'name' => 'xselect',
            'type' => 'code',
            'desc' => 'Static options (format 1:Option1 2:Option2 etc. separated by line break)'
        ),
        'xsql' => array(
            'name' => 'xsql',
            'type' => 'code',
            'desc' => 'Dynamic options (an SQL select having 2 columns: oid and some column for label)'
        )
    )
);
