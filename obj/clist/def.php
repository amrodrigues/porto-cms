<?php

$a_def = array(

	'classes' => '*',

	'br' => array(
		'class' => 'clist',
		'easyname' => 'Content list',
          'cache' => true
	),

	'content' => array(

		'showtitle' => array(
			'name' => 'showtitle',
			'type' => 'bool',
			'desc' => 'Show title',
			'show' => true
		),
                'rid' => array(
			'name' => 'rid',
			'type' => 'integer',
			'desc' => 'Get content from another id',
			'show' => true,
                        'optional' => true
		)
	)

);


?>