<?php

class clist {

    function asHtml($ud) {

	global $ob, $oAuth;

	$lay = '<div class="clist">';

	// get data 
        
        $oid = $ud['br']['oid'];
        if (!empty($ud['content']['rid']['value'])) $oid = $ud['content']['rid']['value'];
        
	$s_sql = 'select oid, easyname, idrole, publish from broker where pid = ' . $oid . ' order by prior';
	$a = $ob->dbSelect($s_sql);

	if (empty($a))
	    return '';


	// Present

	if (!empty($ud['content']['showtitle']['value']))
	    $lay .= '<h1>' . $ud['br']['easyname'] . '</h1>';

        $userIsAdmin = $oAuth->userIsAdmin();
        $userRoleId = $oAuth->userRoleId();
        
	foreach ($a as $aa) {
            if ($aa['publish'] == '1' || $userIsAdmin || $userRoleId == $aa['idrole'])
	    $lay .= '<a href="/' . $aa['oid'] . '"><div class="clist-item">' . $aa['easyname'] . '</div></a>';
	}


	return $lay . '</div>';
    }

}

?>