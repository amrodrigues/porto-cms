<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'rss',
        'easyname' => 'RSS Newsfeed',
        'cache' => false
      ),
      'content' => array(
        'descr' => array(
          'name' => 'descr',
          'type' => 'text',
          'desc' => 'Feed description',
          'optional' => true,
          'show' => true
        ),
        'sqlt' => array(
          'name' => 'sqlt',
          'type' => 'code',
          'desc' => 'SQL select having "title", "link" and "description" columns',
          'optional' => true
        ),
        'url' => array(
          'name' => 'url',
          'type' => 'code',
          'desc' => 'Use data from this URL',
          'optional' => true
        ),
        'titles' => array(
          'name' => 'titles',
          'type' => 'bool',
          'desc' => 'Only show titles',
          'optional' => true
        )
      )
    );
?>
