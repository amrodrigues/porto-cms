<?php

require ($_SERVER['DOCUMENT_ROOT'] . '/conf.php');

require_once ($conf->base . '/core/uz/uzrss.php');

require_once ($conf->base . '/core/uz/uzhttp.php');

require_once ($conf->root . '/dob.php');



$id = uzhttp::getvar('id', 'integer');

if (empty($id))
    die();


$o = new dob();

$ud = $o->get($id);



if (empty($ud['br']['class']) || $ud['br']['class'] != 'rss')
    die();


$s_sql = trim($ud['content']['sqlt']['value']);

if (strpos(strtolower($s_sql), 'select') !== false) {
    $a_channel = array
	(
	'title' => $ud['br']['easyname'],
	'link' => $GLOBALS['conf']->siteurl . '/obj/rss/feed.php?id=' . $ud['br']['oid'],
	'site_uri' => $GLOBALS['conf']->siteurl,
	'site_name' => $GLOBALS['conf']->sitename,
	'description' => $ud['content']['descr']['value'],
	'pubdate' => gmdate("D, d M Y H:i:s") . ' GMT',
	'language' => 'pt',
	'xsql' => $s_sql
    );


    $o = new uzrss();

    print $o->make($a_channel);
}
?>
