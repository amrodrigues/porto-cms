<?php

    $a_def = array(
      'classes' => '*',
      'br' => array(
        'class' => 'masonry',
        'easyname' => 'Masonry',
        'cache' => true
      ),
      'content' => array(
        'columnwidth' => array(
          'name' => 'columnwidth',
          'type' => 'integer',
          'desc' => 'Column width',
          'show' => true
        )
      )
    );
?>