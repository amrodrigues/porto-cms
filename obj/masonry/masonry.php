<?php

class masonry {

    function asHtml($ud) {

        global $ob;

        $ob->includeJquery();
        $ob->includeScriptTag('/obj/masonry/masonry.pkgd.min.js');
        $ob->includeScriptTag('/obj/masonry/imagesloaded.js');


        $cw = $ud['content']['columnwidth']['value'];
        if (empty($cw)) $cw = 220;

        
        $lay = '<div id="masonry" class="js-masonry">';
        
        $lay .= '<div class="grid-sizer"></div>';
        
        $a = $ob->dbSelect('select broker.oid from broker where pid=' . $ud['br']['oid'] . ' order by broker.prior');
        if (empty($a))
            return '';
        foreach ($a as $aa) {
            $lay .= '<div class="masonry-item">' . $ob->getHtml($aa['oid']) . '</div>';
        }

        $lay .= '</div>';
        

                $lay .= '
            <script type="text/javascript">
                
                var container = document.querySelector(\'#masonry\');
                var msnry;
                
                // initialize Masonry after all images have loaded
                imagesLoaded( container, function() {
                  msnry = new Masonry( container );
                });
            </script>            
        ';
        
        
        return $lay;
    }

}

?>