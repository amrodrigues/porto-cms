<?php

    $a_def = array(
      'classes' => array('property' => '*'),
      'br' => array(
        'class' => 'entity',
        'easyname' => 'Entity',
        'cache' => false
      ),
      'content' => array(
        'descr' => array(
          'name' => 'descr',
          'type' => 'textline',
          'desc' => 'Entity description'
        )
      )
    );
?>