<?php

class blogpost_posts {


   function bydate() {

        global $ob, $oAuth;

        // --- params

        $dt = $ob->param('date', 'date', false);
        if (!$dt) return '';
        $adt = explode('-', $dt);

        $start = $ob->param('start', 'digits', 0);
        $len =   $ob->param('len'  , 'digits', 6);
        $pid =   $ob->param('pid', 'digits', false);

        // --- data

        $sql = '
          select blogpost.oid as oid from broker, blogpost, user
          where
            blogpost.oid=broker.oid ' . $oAuth->sqlRestrict('view') . '
            and user.oid = broker.iduser';

            if ($pid) $sql .= ' and broker.pid=' . $pid;

            $sql .= ' and day(broker.tscreate) = ' . $adt[2] . '
            and month(broker.tscreate) = ' . $adt[1] . '
            and year(broker.tscreate) = ' . $adt[0] . '
            order by blogpost.oid desc
            limit ' . $start . ', ' . $len;


         $a = $ob->dbSelectColumn($sql);

         // --- html

         $lay = '';

         if (!empty($a)) foreach ($a as $oid) {
             $lay .= $ob->getHtml($oid);
         }

         return $lay;

    }



      function archive() {

        global $ob, $oAuth;

        // --- params

        $pid =   $ob->param('pid', 'digits', false);

        // --- data

        $sql = 'select broker.oid, blogpost.title, broker.tscreate
          from broker, blogpost
          where
            blogpost.oid=broker.oid ' . $oAuth->sqlRestrict('view');

            if ($pid) $sql .= ' and broker.pid=' . $pid;

            $sql .= ' order by broker.oid desc';

         $a = $ob->dbSelect($sql);


         // --- html

         $lay = '';

         if (!empty($a))
         {
             $lay = '<ul>';

             foreach ($a as $aa) {
               $lay .= '<li><a href="/' . $aa['oid'] . '">' . $aa['title'] . '</a> <small>' . substr($aa['tscreate'], 0, -3) . '</small></li>' . "\n";
             }

             $lay .= '</ul>';
         }

         return $lay;

    }


}


?>