<?php

/*
 * Blogpost methods related to tag handling
 * Methods having $asArray are accessible by reporter objects as data source for templating
 */
class blogpost_tags {


    /*
     * Show latest posts by tag
     */
    function tag($asArray = false) {

        global $ob, $oAuth;

        // --- params

        $tag = $ob->param('tag', 'alpha', false);
        if (!$tag) return '';

        $pid = $ob->param('pid', 'digits', false);

        // --- data

        $sql = 'select blogpost.* from blogpost,broker where blogpost.oid=broker.oid';

        if ($pid) $sql .= ' and broker.pid=' . $pid; // restrict to some parent id, if needed

        $sql .= ' and blogpost.tags like "%' . $tag . '%" ' .  $oAuth->sqlRestrict('view') . ' order by oid desc limit 40';

        $a = $ob->dbSelectColumn($sql);

        if ($asArray) return $a;

        // --- html

        $lay = '';

        if (!empty($a)) foreach ($a as $oid) $lay .= $ob->getHtml($oid);

        return $lay;

    }



    /*
     * Tag list
     * Returns array (array('tag' => string, 'cnt' => integer), ...)
     */
    function categories($asArray = false) {

        global $ob, $oAuth;

        // --- params

        $pid = 0;

        $ob->param('pid', 'digits', false);

        // --- data

        $sql = 'select tags from blogpost, broker where broker.oid=blogpost.oid ' .  $oAuth->sqlRestrict('view');

        if ($pid) $sql .= ' and pid=' . $pid;

        $a = $ob->dbSelectColumn($sql);

        $aTags = array();

        if (!empty($a)) foreach ($a as $s) {

            $aa = explode(',', $s);

            if (!empty($aa)) foreach ($aa as $ss) {

                $ss = trim($ss);
                if (empty($aTags[$ss])) $aTags[$ss] = array('tag' => $ss, 'cnt' => 1);
                else $aTags[$ss]['cnt']++;
            }
        }

        if ($asArray) return $aTags;


        // --- html

        $lay = '<div class="blogpost-tags-categories">';

        foreach ($aTags as $a) {

            $lay .= '<div><a href="/?go=blogpost-tags-tag&tag=' . urlencode($a['tag']) . '">' . $a['tag'] . '(' . $a['cnt'] . ')</a></div>';
        }

        return $lay . '</div>';
    }






}

?>