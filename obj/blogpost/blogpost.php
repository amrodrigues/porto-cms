<?php

class blogpost {


    function asHtml($ud) {


        global $ob, $conf;

        $lay = '<div class="blogpost">';
            $lay .= '<h1>' . $ud['content']['title']['value'] . '</h1>';
            $lay .= '<div class="blogpost-date">' . substr($ud['br']['tscreate'], 0, -3) . '</div>';
            $lay .= '<div class="blogpost-author">' . $ob->dbSelectCell('select name from user where oid=' . $ud['br']['iduser']) . '</div>';

        $lay .= '<div class="blogpost-body">';

        // --- children: process folder

        $aChild = $ob->children($ud['br']['oid']);

        $afterLead = '';

        if (!empty($aChild))
            foreach ($aChild as $a) {
                // --- images go top:

                if ($a['class'] == 'image' || $a['class'] == 'galleria' || $a['class'] == 'album') {
                    if ($a['laypos'] == 1)
                        $lay .= $ob->getHtml($a['oid']);

                    // --- text parts go after lead text
                } elseif ($a['class'] == 'text') {

                    $afterLead .= $ob->getHtml($a['oid']);

                    // --- folder contents are listed below lead + text parts:
                } elseif ($a['class'] == 'folder') {

                    $aList = $ob->children($a['oid']);

                    $i = 0;

                    $linkList = '';

                    foreach ($aList as $aa) {
                        if ($showAll) {
                            $afterLead .= $ob->getHtml($aa['oid']);
                        } else {

                            $linkList .= '<div class="blogpost-attach-wrapper"><a href="/index.php?oid=' . $aa['oid'] . '">' . $aa['easyname'] . '</a></div>';

                            $i++;
                        }
                    }

                    if ($i > 0)
                        $afterLead .= '<div class="blogpost-attach">' . $linkList . '</div>';
                }
            }

        if (!empty($ud['content']['txt']['value']))
            $lay .= $ud['content']['txt']['value'];

        $lay .= '</div>';

        $lay .= $afterLead;




        // --- insert tags

/*
 * note: cannot insert because url loses folder context

        $tags = explode(',', $ud['content']['tags']['value']);

        if (!empty($tags)) {

            $lay .= '<div class="blogpost-tags">';

            foreach ($tags as $tag) {

                $tag = trim($tag);
                $lay .= '<a href="/?go=blogpost-tags-tag?tag=' . $tag . '">' . $tag  . '</a> ';
            }

            $lay .= '</div>';
        }

*/
        $lay .= '</div>';



        // --- comments

        /*
         * We use a behavior object, status, that has to be in the layout
         */

        if (!empty($conf->status)) {

            require_once($conf->root . '/obj/status/status.php');

            $oStatus = new status();

            $bm = $conf->status;

            $lay .= '<h2>' . $bm['labellist'] . '</h2>' . $oStatus->statusList($ud['br']['oid']);

            $lay .= $oStatus->statusForm($ud['br']['oid']);
        }


        return $lay;
    }


}

?>