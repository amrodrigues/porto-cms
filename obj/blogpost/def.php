<?php

$a_def = array(
    'classes' => array('image' => 1, 'text' => '*'),
    'br' => array(
        'class' => 'blogpost',
        'easyname' => 'Blog post',
        'cache' => true
    ),
    'content' => array(
        'title' => array(
            'name' => 'title',
            'type' => 'textline',
            'desc' => 'Title',
            'show' => true
        ),
        'txt' => array(
            'name' => 'txt',
            'type' => 'richtext',
            'desc' => 'Body'
        ),
        'tags' => array(
            'name' => 'tags',
            'type' => 'textline',
            'desc' => 'Comma-separated tags'
        )
    )
);
?>