<?php

$a_def = array(
    'classes' => array('image' => 1, 'album' => 1, 'imageslider' => 1),
    'br' => array(
        'class' => 'event',
        'easyname' => 'Event',
        'cache' => true
    ),
    'content' => array(
        'title' => array(
            'name' => 'title',
            'type' => 'textline',
            'desc' => 'Title',
            'show' => true
        ),
        'startdate' => array(
            'name' => 'startdate',
            'type' => 'date',
            'desc' => 'Start date'
        ),
        'enddate' => array(
            'name' => 'enddate',
            'type' => 'date',
            'desc' => 'End date'
        ),
        'time' => array(
            'name' => 'time',
            'type' => 'textline',
            'desc' => 'Schedule',
            'optional' => true
        ),
        'place' => array(
            'name' => 'place',
            'type' => 'textline',
            'desc' => 'Place',
            'optional' => true
        ),
        'txt' => array(
            'name' => 'txt',
            'type' => 'richtext',
            'desc' => 'Description',
            'optional' => true
        ),
        'price' => array(
            'name' => 'price',
            'type' => 'textline',
            'desc' => 'Price',
            'optional' => true
        )
    )
);
