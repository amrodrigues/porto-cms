<?php

    $a_def = array(
      'classes' => '*',
      'br' => array(
        'class' => 'clone',
        'easyname' => 'Clone',
        'cache' => false
      ),
      'content' => array(
        'rid' => array(
          'name' => 'rid',
          'type' => 'textline',
          'desc' => 'Object ids (comma separated)',
          'show' => true
        )
      )
    );
?>
