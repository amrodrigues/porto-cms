<?php

$a_def = array(
    'classes' => '*',
    'br' => array(
        'class' => 'css',
        'easyname' => 'CSS',
        'cache' => true
    ),
    'content' => array(
        'code' => array(
            'name' => 'code',
            'type' => 'css',
            'desc' => 'CSS code',
            'optional' => false
        )
    )
);
