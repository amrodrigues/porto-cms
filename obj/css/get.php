<?php

/*
 * Css spitter
 */

if (!session_id())
    session_start();

if (!empty($_GET['id']))
    $id = (int) $_GET['id'];
else
    die("ERROR: no id");

require ($_SERVER['DOCUMENT_ROOT'] . '/conf.php');

$css = $err = $mcKey = false;

// --- Load from cache without executing dob:
if (!empty($conf->cache)) {
    try {

        $c = new Memcache();
        $mcKey = $id . '_css_' . $conf->dbname;
        $c->connect('127.0.0.1', 11211);
        $css = $c->get($mcKey);
    } catch (Exception $e) {
        $err = 'obj css get.php: ERROR: could not fetch key ' . $mcKey;
    }
}

// Generate and cache it:

if ($css === false) {
    require ($conf->root . '/dob.php');

    $ob = new dob();
    if ($err) {
        $ob->log($err);
    }
    $css = $ob->dbSelectCell('select code from css where oid=' . $id);
    if ($mcKey !== false) { // keep in cache
        $c = new Memcache();
        $c->connect('127.0.0.1', 11211);
        $c->set($mcKey, $css, false, false);
    }
}

header('Content-Type: text/css');
print $css;
die();
