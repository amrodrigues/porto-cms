<?php

class importdata {

    var $group = "helpers";
    var $descr = "Import data";
    var $show = true;
    var $public = false;

    function exec() {

        global $ob, $conf;

        $br = $_SESSION['br'];

        if (empty($br['class']))
            die('I need br session variable with current object browser position.');
        
        if ($br['class'] != 'datatable')
            return ('To import data:<br><br>Place object browser inside a "Data table" object. Data will be inserted into the data table you choose. <br><br>If you don\'t have a datatable, create an "Entity" object and place "Properties" inside. Then create a "Data table". <br><br>Read the documentation.');

        $s = '<h1>Import data into ' . $br['easyname'] . ' (' . $br['oid'] . ')</h1>';

        // --- object to query datatable structure:

        require_once($conf->root . '/obj/datatable/datatable.php');
        $odt = new datatable();
        $aProperties = $odt->getProperties($odt->entityOid($br['oid']));


        // --- 1. Ask for data -----------------------------

        if (empty($_POST['dat']) && empty($_POST['map']) && !empty($aProperties)) {

            $s .= '<form method="post" action="/do/?m=datatable.importdata">';
            $s .= '  <textarea class="form-textarea" name="dat"></textarea><br><br>';
            $s .= '  <h2>Column separator character:</h2>';
            $s .= '  <input type="radio" name="sep" value="1" checked="checked"> Tab<br>';
            $s .= '  <input type="radio" name="sep" value="2"> Comma<br>';
            $s .= '  <input type="radio" name="sep" value="3"> Semicolon<br><br>';
            $s .= '  <input type="checkbox" name="distinctemail" value="1" checked="checked"> Update rows having similar email address<br>';
            $s .= '  <input type="checkbox" name="ignorenoemail" value="1" checked="checked"> Ignore rows without an email address<br><br>';
            $s .= '  <input type="submit" value="Next...">';
            $s .= '</form>';
        }






        // --- 2. Map columns --------------------------------

        if (!empty($_POST['dat'])) {

            // TODO: centralize session variables

            $_SESSION['datatable'] = array();

            // keep vars:

            if (!empty($_POST['distinctemail']))
                $_SESSION['datatable']['distinctemail'] = 1;
            else
                $_SESSION['datatable']['distinctemail'] = false;

            if (!empty($_POST['ignorenoemail']))
                $_SESSION['datatable']['ignorenoemail'] = 1;
            else
                $_SESSION['datatable']['ignorenoemail'] = false;


            switch ($_POST['sep']) {
                case '1':
                    $sep = "\t";
                    break;
                case '2':
                    $sep = ',';
                    break;
                case '3':
                    $sep = ';';
                    break;
                default:
                    die("No separator specified.");
            }


            // Explode table

            $a_table = explode("\n", $_POST['dat']);
            
            foreach ($a_table as $i => $s_row) {
                if (trim($s_row) != '') {
                    $a_table[$i] = explode($sep, $s_row);
                    foreach ($a_table[$i] as $k => $v)
                        $a_table[$i][$k] = trim($v);
                }
            }

            $_SESSION['datatable']['table'] = $a_table;


            // Map properties of data uploaded to datatable db columns

            $s .= '<form method="post" action="/do/?m=datatable.importdata">';
            $s .= '<table cellspacing="0" cellpadding="5">';
            $s .= '<tr><th>Examples</th><th>Classify as...</th></tr>';

            $i_cols = count($a_table[0]);

            for ($i = 0; $i < $i_cols; $i++) {

                $s .= '<tr>';

                // Column nr
                $s .= '<td>';

                // Example data
                $cnt = 0;

                for ($ii = 0; $ii < $i_cols; $ii++) {
                    if (!empty($a_table[$ii][$i])) {
                        $s .= trim($a_table[$ii][$i]) . '; ';

                        $cnt++;

                        if ($cnt > 4)
                            break;
                    }
                }

                $s .= ' etc...</td><td><select name="map[]">';
                $s .= '<option value="sys_ignore">Ignore property</option>';

                foreach ($aProperties as $aProp)
                    $s .= '<option value="' . $aProp['xname'] . '">' . $aProp['easyname'] . '</option>';

                $s .= '</select></td>';
                $s .= '</tr>';
            }

            $s .= '</table>';
            $s .= '<input type="submit" value="Import..." />';
            $s .= '</form>';
        }





        // --- 3. Import data ---------------------------------------

        if (!empty($_POST['map'])) {

            require_once($conf->root . '/obj/email/emailparse.php');
            $oEmailParse = new emailparse();

            $tableName = $odt->tableName($br['oid']);

            // check datatable db table, it may not be initialized:
            $odt->checkIntegrity($br['oid']);

            // ---- for each row

            $cnt = 0;

            foreach ($_SESSION['datatable']['table'] as $i_row => $a_row) {

                if (empty($a_row))
                    continue;

                $a_datainsert = array();
                $a_cols = array();
                $b_insert = true; // row will be inserted via sql
                // ---- for each col: ----

                foreach ($_POST['map'] as $i_col => $sColumnName) {

                    if ($sColumnName == 'sys_ignore')
                        continue;

                    $datum = trim(str_replace('  ', ' ', $a_row[$i_col]));

                    // find property type:

                    $sType = false;
                    foreach ($aProperties as $aProp) {
                        if ($aProp['xname'] == $sColumnName) {
                            $sType = $aProp['xtype'];
                            break;
                        }
                    }

                    if ($sType === false)
                        die("error: property type for $sColumnName not found.");
                    if ($sType == 'email')
                        $datum = $oEmailParse->correct($datum);

                    // ---- ignore row if email is empty

                    if ($sType == 'email' && !empty($_SESSION['datatable']['ignorenoemail'])) {
                        if (strpos($datum, '@') == false) {
                            continue 2; // ignore row, jump to next row
                        }
                    }


                    // ---- check if email exists ----

                    if (!empty($_SESSION['datatable']['distinctemail'])) {

                        $bEmailExists = $oEmailParse->dbEmailExists($datum, $tableName, $sColumnName);

                        if ($sType == 'email' && !empty($bEmailExists)) {
                            $b_insert = false;
                            $sEmail = $datum;
                            $sEmailColName = $sColumnName;
                        }
                    } else {

                        $sEmail = '';
                        $sEmailColName = '';
                    }


                    //TODO date hack #20110920 ------------------------------
                    // match: Array([0] => 06-05-1973, [1] => 06, [2] => 05, [3] => 1973 )

                    $matches = false;

                    preg_match('/(\\d\\d)[\\-\\/](\\d\\d)[\\-\\/](\\d\\d\\d\\d)/', $datum, $matches);

                    if (!empty($matches[1])) {
                        $datum = $matches[3] . '-' . $matches[2] . '-' . $matches[1];
                    }

                    // must be pushed together, ix dependent
                    $a_cols[] = $sColumnName;
                    $a_datainsert[$sColumnName] = $datum;
                }

                if ($b_insert) {
                    
                    $newid = $ob->dbInsert($tableName, $a_datainsert);

                    if ($newid > 0) {
                        $cnt++;
                    } else {

                        $s .= 'Could not insert: ' . print_r($a_datainsert, true) . '<br>';
                    }
                    
                } else { // update by email, must be custom sql:
                    
                    $sql = 'update ' . $tableName . ' set ';
                    foreach ($a_datainsert as $k => $v) {
                        $sql .= mysql_real_escape_string($k) . '="' . mysql_real_escape_string($v) . '",';
                    }
                    $sql = substr($sql, 0, strlen($sql) - 1) . ' where ' . $sEmailColName . '="' . mysql_real_escape_string($sEmail) . '"';
                    $ob->exec($sql);
                }
            }

            unset($_SESSION['datatable']);
            $s .= '<br><br>' . $cnt . ' items were imported to the datatable object.';
        }

        return $s;
    }

}

?>