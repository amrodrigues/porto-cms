<?php

/*
 *
 */

class datatable {

    function asHtml($ud) {
        return '';
    }

    /*
     * An array of properties of an entity
     */

    //TODO: gets properties that user may access (idrole column in properties)

    function getProperties($entityOid) {
        global $ob, $oAuth;
        $sql = 'select property.*, broker.easyname from property,broker where broker.oid=property.oid and broker.pid=' . (int) $entityOid . ' ' . $oAuth->sqlRestrict('view') . ' order by broker.prior';
        $a = $ob->dbSelect($sql);
        return $a;
    }

    /*
     *
     */

    function entityOid($datatableOid) {
        global $ob;
        if (!is_numeric($datatableOid)) {
            throw new Exception('No datatableOid to get entity oid');
        }
        $sql = 'select rid from datatable where oid=' . (int) $datatableOid;
        $entityOid = $ob->dbSelectCell($sql);
        return $entityOid;
    }

    /*
     * Db table name of a datatable object
     */

    function tableName($oid) {
        return 'datatable' . $oid;
    }

    /*
     * Check and repair a db table of a datatable object
     */

    function checkIntegrity($datatableOid) { // $oid of a datatable object
        if (empty($datatableOid)) {
            throw new Exception('oid is empty');
        }
        global $ob;
        $entityOid = $this->entityOid($datatableOid);
        if (empty($entityOid)) {
            throw new Exception("Could not get entity oid from datatable oid $datatableOid");
        }
        $tableName = $this->tableName($datatableOid);
        $ud = $this->createSkel($this->getProperties($entityOid), $tableName);
        if (empty($ud['content'])) {
            return null;
        }
        $ob->classCheckIntegrity($ud, $tableName);
        return $ud;
    }

    /*
     * Creates a ud array for dob compatibility.
     * Allows dob to check integrity of datatable db tables
     */

    function createSkel($aProperties, $tableName) {
        // empty skel:
        $ud = array(
            'br' => array(
                'class' => $tableName
            ),
            'content' => array(
            )
        );

        // fill:
        if (!empty($aProperties)) {
            foreach ($aProperties as $meta) {
                $name = $meta['xname'];
                //TODO: min max chars
                $ud['content'][$name] = array(
                    'name' => $name,
                    'type' => $meta['xtype'],
                    'desc' => $meta['easyname'],
                    'default' => $meta['xdefault'],
                    'optional' => $meta['xoptional'],
                    'xsql' => $meta['xsql'],
                    'xselect' => $this->explodeKVstring($meta['xselect'])
                );
            }
        }

        return $ud;
    }

    /*
     * Explodes k:v strings separated by line breaks
     */

    function explodeKVstring($lines) {
        if (is_array($lines)) {
            return $lines;
        }
        $lines = explode("\n", $lines);
        $aRet = array();
        foreach ($lines as $line) {
            $a = explode(':', $line);
            if (empty($a[0]) || empty($a[1])) {
                continue;
            }
            $aRet[trim($a[0])] = trim($a[1]);
        }
        return $aRet;
    }

    /*
     * Insert to db one or multidimensional data array
     */

    public function insertData($aaData, $datatableOid) {
        if (!is_numeric($datatableOid)) {
            throw new Exception('datatableOid parameter is not numeric');
        }
        global $ob;
        $table = $this->tableName($datatableOid);
        try {
            $this->checkIntegrity($datatableOid);
        } catch (Exception $e) {
            $ob->log("Could not check integrity for $datatableOid");
        }
        if (count($aaData) == count($aaData, COUNT_RECURSIVE)) {
            $ob->dbinsert($table, $aaData);
        } else {
            foreach ($aaData as $a) {
                $ob->dbinsert($table, $a);
            }
        }
    }

}
