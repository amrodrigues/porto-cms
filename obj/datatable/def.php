<?php

$a_def = array(
    'classes' => false,
    'br' => array(
        'class' => 'datatable',
        'easyname' => 'Data table',
        'cache' => false
    ),
    'content' => array(
        'rid' => array(
            'name' => 'rid',
            'type' => 'dbselect',
            'desc' => 'Entity',
            'xsql' => 'select oid, easyname from broker where class="entity" ' . $GLOBALS['oAuth']->sqlRestrict('view') . ' order by easyname'
        )
    )
);
