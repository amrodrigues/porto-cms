<?php
	//TODO: wrap in a class with a front controlller

        /*
         * IMPORTANT: any script outside the control of /do/index.php or /index.php must use only public data (broker.publish = 1)
         */

	require_once ($_SERVER['DOCUMENT_ROOT'] . '/conf.php');


	require($conf->root . '/dob.php');

        $ob = new dob();


	// json object id: -------------------------------------

	$oid = $ob->param('oid', 'digits', false);

	if (!$oid) die ('[]');





	// Object data ----------------------------

	$sSql = $ob->dbSelectCell('select json.xsql from broker, json where broker.oid=' . $oid . ' and json.oid = broker.oid and broker.publish=1');



        header('Content-Type: application/json');

	if (empty($sSql)) die ('[]');

        // has cms functions, load template engine:

        if (strpos($sSql, '[[') !== false) {

           require ($conf->root . '/itengine.php');
           $oe = new itengine();
           $sSql = $oe->dispatch($sSql);
        }

	$aContent = $ob->dbSelect($sSql);

	if (empty($aContent)) die ('[]');


//TODO: review
	// Images ------------------------
/*
	if (!empty($aObject['addimages']))
	{
		$sSql = 'select broker.oid, broker.pid, image.userfile from broker,image where broker.oid = image.oid and broker.rlevel = 1 and (';

		foreach ($aContent as $a)
		{
			$sSql .= 'pid=' . $a['oid'] . ' or ';
		}

		$sSql = substr($sSql, 0, strlen($sSql) - 4) . ')';

		$aImages = $db->select($sSql);


		 // Add images to data ---------------

		 foreach ($aContent as $k => $a)
		 {
		 	if (empty($aContent[$k]['channels'])) $aContent[$k]['channels'] = 'a:18:{i:0;s:1:"4";i:1;s:1:"5";i:2;s:1:"6";i:3;s:1:"7";i:4;s:1:"8";i:5;s:1:"9";i:6;s:2:"10";i:7;s:2:"11";i:8;s:2:"12";i:9;s:2:"13";i:10;s:2:"14";i:11;s:2:"15";i:12;s:2:"16";i:13;s:2:"17";i:14;s:2:"18";i:15;s:2:"19";i:16;s:2:"20";i:17;s:2:"21";}';

		 	if (empty($aContent[$k]['link'])) $aContent[$k]['link'] = $GLOBALS['conf']->siteurl . '/index.php?oid=' . $a['oid'];

		 	$b = false;

		 	foreach ($aImages as $aa)
		 	{
				if ($aa['pid'] == $a['oid'])
				{
					$aContent[$k]['img'] = '/files/' . substr($aa['oid'], 0,2) . '/' . $aa['oid'] . '.' . $aa['userfile'];

					$b = true;

					break;
				}
		 	}

		 	if (!$b) $aContent[$k]['img'] = '';

		 }
	}

*/


//TODO: review
        // Sanitize -------------------
/*
    foreach ($aContent as $k => $v)
	{
		 foreach ($v as $kk => $vv)
		 {
		 	  $aContent[$k][$kk] = str_replace(array('<br>', '<br />', '<br/>'), "\n", $aContent[$k][$kk]);
		 	  $aContent[$k][$kk] = str_replace("\r", '', $aContent[$k][$kk]);
		 	  $aContent[$k][$kk] = str_replace("\n\n\n", "\n\n", $aContent[$k][$kk]);
		 	  $aContent[$k][$kk] = str_replace("\n\n\n", "\n\n", $aContent[$k][$kk]);

	          $aContent[$k][$kk] = htmlentities($aContent[$k][$kk], ENT_NOQUOTES);
         }
	}
*/

    // Print data as json -------------

    if (!empty($aContent))
    {
	print json_encode($aContent);

    } else {

        print '[]';
    }


?>