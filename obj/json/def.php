<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'json',
        'easyname' => 'JSON publisher',
        'cache' => false
      ),
      'content' => array(
        'xsql' => array(
          'name' => 'xsql',
          'type' => 'code',
          'desc' => 'SQL select',
          'optional' => false
        ),
        'addimages' => array(
          'name' => 'addimages',
          'type' => 'bool',
          'desc' => 'Insert images',
          'optional' => true
        )
      )
    );
?>
