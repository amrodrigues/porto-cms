<?php

class filebrowser {


    function asHtml($ud) {

        $id = $ud['content']['idtop']['value'];

        if (empty($id))
            $id = $ud['br']['oid'];

        $aTree = $this->tree($id);

        if (empty($aTree))
            return '';

        $lay = '';

        global $ob;

        $lay .= '
		<div class="filebrowser">
                   ' . $this->getHtml($aTree) . '
		</div>
		';

        $ob->includeScript('filebrowser', '
        $(document).ready(function() {
            $(".filebrowser-title").click(
               function() {

                if ($(this).next().css("display") == "none") {
                    $(this).next().css("display", "table")
                    } else {
                    $(this).next().css("display", "none");
                    }
                    return false;
                }
            )
            });
        ');

        return $lay;
    }


    function tree($id) {

        global $oAuth, $ob;

        //TODO: structure globally represented in objects?
        //TODO: special table with hierarchical data?
        //TODO: objects keep child references?

        $sSql = 'select oid, class, easyname from broker where pid=' . (int) $id . $oAuth->sqlRestrict('view') . ' order by prior';

        $aList = $ob->dbSelect($sSql);

        foreach ($aList as $k => $aItem) {
            if ($aItem['class'] == 'folder')
                $aList[$k]['child'] = $this->tree($aItem['oid']); else
                $aList[$k]['child'] = false;
        }

        return $aList;
    }



    function getHtml($aNode) {

        global $ob, $conf;

        $s = '';

        if (!class_exists('xfile'))
            require_once($conf->root . '/obj/xfile/xfile.php');

        foreach ($aNode as $a) {

            if (!empty($a['child'])) { // get child objects:

                $s .= '
                    <div class="filebrowser-title">' . $a['easyname'] . '</div>

                    <div class="filebrowser-node" style="display:none">
                        ' . $this->getHtml($a['child']) . '
                    </div>

                ';

            } else { // no child objects:

                switch ($a['class']) {

                    case 'xfile':
                    case 'image':

                        $ext = $ob->dbSelectCell('select userfile from ' . $a['class'] . ' where oid=' . $a['oid']);

                        $s .= '<div class="filebrowser-item">' . xfile::present($ob->objectGetFilename($a['oid'], $ext), $ob->objectGetFilename($a['oid'], $ext, true), $ext, $a['easyname'], 'xfile') . '</div>';

                        break;

                    default:

                        $s .= '<div class="filebrowser-item"><a href="/' . $a['oid'] . '">' . $a['easyname'] . '</a>';
                }
            }


        }

        return $s;
    }

}

?>
