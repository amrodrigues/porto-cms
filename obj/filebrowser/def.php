<?php

$a_def = array(

	'classes' => '*',

	'br' => array(
		'class' => 'filebrowser',
		'easyname' => 'File Browser'
	),
	'content' => array(
		'idtop' => array(
			'name' => 'idtop',
			'type' => 'integer',
			'desc' => 'Root id',
			'optional' => true
		)
	)

);

?>
