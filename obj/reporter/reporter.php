<?php

class reporter {

    function asHtml($ud) {

        require_once($GLOBALS['conf']->root . '/itengine.php');
        $it = new itengine();
        $lay = '';
        
        //if (!empty($ud['content']['mcall']['value'])) $it->setDataByMethodCall($ud['content']['mcall']['value']);
        
        $sql = $ud['content']['sqls']['value'];
        
        if (!empty($sql)) {
            
            $sql = str_replace('[[self.oid]]', $ud['br']['oid'], $sql); //TODO: remove hack
            $sql = str_replace('[[url.oid.digits]]', (int) $_GET['oid'], $sql); //TODO: remove hack
            
            $it->setDataSql($sql);
            
            if (empty($it->aData) && $GLOBALS['oAuth']->userIsAdmin()) { 
                    $lay .= "<!--\n\nReporter id " . $ud['br']['oid'] . ": no data from sql query:\n" . $sql . "\n\n-->";
            }
            
        } else {
            return 'No data source';
        }

        $lay .= $it->merge($ud['content']['rowtmpl']['value']);
        
        if (!empty($lay)){
            $lay = str_replace('{[1]}', $lay, $it->parseSQL($ud['content']['tmpl']['value']));
            return $lay;
        } else {
            return '';
        }
        
    }

}