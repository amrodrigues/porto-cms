<?php

$a_def = array(
    'classes' => '*',
    'br' => array(
        'class' => 'reporter',
        'easyname' => 'Reporter',
        'cache' => false
    ),
    'content' => array(
        'sqls' => array(
            'name' => 'sqls',
            'type' => 'code',
            'desc' => 'Get data by SQL expression with optional functions',
            'optional' => true
        ),
        'tmpl' => array(
            'name' => 'tmpl',
            'type' => 'html',
            'desc' => 'Wrapper template (use {[1]} to mark item insertion place)',
            'optional' => false,
        ),
        'rowtmpl' => array(
            'name' => 'rowtmpl',
            'type' => 'html',
            'desc' => 'Item template',
            'optional' => false,
        )
    )
);
