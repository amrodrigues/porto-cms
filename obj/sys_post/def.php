<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'sys_post',
        'easyname' => 'Post',
        'cache' => false
      ),
      'content' => array(
        'rid' => array(
          'name' => 'rid',
          'type' => 'integer',
          'desc' => 'Object oid',
          'indexed' => true
        ),
        'ts' => array(
          'name' => 'ts',
          'type' => 'datetime',
          'desc' => 'Timestamp'
        ),
        'xname' => array(
          'name' => 'xname',
          'type' => 'textline',
          'desc' => 'Name',
          'optional' => true
        ),
        'iduser' => array(
          'name' => 'iduser',
          'type' => 'integer',
          'desc' => 'User id',
          'optional' => false
        ),
        'txt' => array(
          'name' => 'txt',
          'type' => 'text',
          'desc' => 'Post',
          'optional' => true
        )
      )
    );
?>