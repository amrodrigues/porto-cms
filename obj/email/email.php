<?php

class box
{

    function asHtml($ud)
    {

        $lay = '<div class="box ';

        switch ($ud['content']['align']['value'])
        {
            case 1:

                $lay .= 'box-left';
                break;

            case 2:

                $lay .= 'box-right';
                break;

            default:

                $lay .= 'box-center';
        }

        $lay .= '">{[1]}';

        if (!empty($ud['content']['title']['value']))
        {
            $lay .= '<div class="box-title">' . $ud['content']['title']['value'] . '</div>';
        }

        $lay .= '<div class="box-txt">' . $ud['content']['txt']['value'] . '</div></div>';

        return $lay;
    }
}

?>