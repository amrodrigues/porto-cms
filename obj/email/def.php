<?php

$a_def = array(
    'classes' => '*',
    'br' => array(
        'class' => 'email',
        'easyname' => 'Email',
        'cache' => true
    ),
    'content' => array(
        'subject' => array(
            'name' => 'subject',
            'type' => 'textline',
            'desc' => 'Subject',
            'optional' => true
        ),
        'body' => array(
            'name' => 'body',
            'type' => 'text',
            'desc' => 'Message body with optional properties {{}} and positions {[]}',
            'optional' => true
        ),
        'sendto' => array(
            'name' => 'sendto',
            'type' => 'code',
            'desc' => 'Send to some people',
            'optional' => false
        ),
        'dtoid' => array(
            'name' => 'dtoid',
            'type' => 'dbradio',
            'desc' => 'Also send to emails in data tables',
            'xsql' => 'select oid, easyname from broker where class="datatable" ' . $GLOBALS['oAuth']->sqlRestrict('view') . ' order by easyname',
            'optional' => false
        ),
        'sent' => array(
            'name' => 'sent',
            'type' => 'bool',
            'desc' => 'Sent (locked)',
            'optional' => true
        ),
        'tssend' => array(
            'name' => 'tssend',
            'type' => 'datetime',
            'desc' => 'Scheduled send',
            'optional' => true,
            'default' => date("Y-m-d H:i:s")
        )
    )
);
?>
