<?php

class emailparse {

    private $accent = array(
        'ã' => 'a', 'ẽ' => 'e', 'ĩ' => 'i', 'õ' => 'o', 'ũ' => 'u',
        'á' => 'a', 'é' => 'e', 'í' => 'i', 'ó' => 'o', 'ú' => 'u',
        'â' => 'a', 'ê' => 'e', 'î' => 'i', 'ô' => 'o', 'û' => 'u',
        'à' => 'a', 'è' => 'a', 'ì' => 'i', 'ò' => 'o', 'ù' => 'u',
        'ä' => 'a', 'ë' => 'e', 'ï' => 'i', 'ö' => 'o', 'ü' => 'u'
    );
    
    private $domain = array(
        '@htmail.com'   => '@hotmail.com',
        '@hotimail.com' => '@hotmail.com',
        '@hotmeil.com'  => '@hotmail.com',
        '@hotamil.com'  => '@hotmail.com',
        '@notmail.com'  => '@hotmail.com',
        '@gotmail.com'  => '@hotmail.com',
        '@hotmaial.com' => '@hotmail.com',
        '@homtail.com'  => '@hotmail.com',
        '@totmail.com'  => '@hotmail.com',
        '@gotmail.com'  => '@hotmail.com',
        '@hormail.com'  => '@hotmail.com',
        '@hotmal.com'   => '@hotmail.com',
        '@otmail.com'   => '@hotmail.com',
        '@ohtmail.com'  => '@hotmail.com',
        '@otemail.com'  => '@hotmail.com',
        '@htomail.com'  => '@hotmail.com',
        '@hotmail.com.com'  => '@hotmail.com',
        '@gmai.com'     => '@gmail.com',
        '@gmal.com'     => '@gmail.com',
        '.gove.br'      => '.gov.br'
    );


    /*
     * Returns oids of table where an email exists
     */
    public function dbEmailExists($email, $table, $column) {
        global $ob;
        $sql = "select oid from $table where $column like '%" . $email . '%';
        return $ob->dbSelect($sql);
    }
    
    public function correct($email) {

        foreach ($this->accent as $from => $to)
            $email = str_replace($from, $to, $email);

        foreach ($this->domain as $from => $to)
            $email = str_replace($from, $to, $email);

        
        $email = str_replace(array(' ', 'mailto:'), '', $email);
        
        return trim(strtolower($email));
    }
    
   
    

}

?>
