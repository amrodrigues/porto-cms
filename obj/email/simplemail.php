<?php

/*
 * Sends a single email with optional inline media
 * Copyright (c) 2011 antoniomotarodrigues@gmail.com
 */

require ('mod/swift/lib/swift_required.php');

class simpleMail {

    function send($fromName, $fromEmail, $toName, $toEmail, $replyTo, $subject, $textMessage, $htmlMessage, $smtpServer, $smtpUser, $smtpPass, $sBasePath) {

        // --- message object

        $message = Swift_Message::newInstance();

        $message->setSubject($subject);

        $message->setFrom(array($fromEmail => $fromName));

        $message->setTo(array($toEmail => $toName));

        if (!empty($replyTo))
            $message->setReplyTo($replyTo);


        // --- inline media

        if (!empty($sBasePath)) {
            preg_match_all('/\ src="(\/files\/[0-9]*\/[0-9]*\.(jpg|png|gif))"/', $htmlMessage, $matches);

            if (!empty($matches[1])) {
                $aCid = array(); // accumulates Cids

                foreach ($matches[1] as $sUrl) {

                    $sFileToEmbed = $sBasePath . $sUrl;

                    $sCid = array_search($sFileToEmbed, $aCid);


                    if (empty($sCid) && file_exists($sFileToEmbed)) {
                        $sCid = $message->embed(Swift_Image::fromPath($sFileToEmbed));

                        $aCid[] = $sCid; // add new Cid to array
                    }

                    if (!empty($sCid))
                        $htmlMessage = str_replace(' src="' . $sUrl . '"', ' src="' . $sCid . '"', $htmlMessage);
                }
            }
        }

        $message->setBody($htmlMessage, 'text/html');

        $message->addPart('HTML message', 'text/plain');


        // --- set transport

        $transport = Swift_SmtpTransport::newInstance($smtpServer, 25);

        if (!empty($smtpUser))
            $transport->setUsername($smtpUser);

        if (!empty($smtpPass))
            $transport->setPassword($smtpPass);


        // --- send

        $mailer = Swift_Mailer::newInstance($transport);

        $result = $mailer->send($message);

        return $result;
    }

}

?>