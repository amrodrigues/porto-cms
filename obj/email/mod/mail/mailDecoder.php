<?php


// Decodes raw email messages

class mailDecoder
{

    
    // Decode using a raw email message ---

    function asDecodedMessage($rawMessage)
    {
        require_once("mimedecode.php");

        $myDecoder = new Mail_mimeDecode($rawMessage);

        $aMessage = $myDecoder->decode(array('include_bodies' => true, 'decode_bodies' => true, 'decode_headers' => true));

        return $aMessage;
    } 


    
    
    // --- $a = decoded message in parts
    
    function asText($aDecodedMessage) 
    {

        // --- $to:

        if (!empty($aDecodedMessage->headers['to'])) $to = extractEmail($aDecodedMessage->headers['to']); else $to = '';


        // --- $from:

        $from = extractEmail($aDecodedMessage->headers['from']);

        $fromRaw = $aDecodedMessage->headers['from']; // includes sender name


        // --- $subject:

        if (!empty($aDecodedMessage->headers['subject'])) $subject = $aDecodedMessage->headers['subject']; else $subject = '';


        // --- $text: keeps all text found in message

        // --- Add subject and text message to $text for analysis ----

        $text = " $fromRaw \n $to $subject \n ";

        if (!empty($aDecodedMessage->body)) $text .= $aDecodedMessage->body . " \n";

        
        // --- Iterate message parts appending text ----

        if (!empty($aDecodedMessage->parts)) foreach ($aDecodedMessage->parts as $ap)
        {
            if ($ap->ctype_primary == 'text')
            {
                $t = $ap->body;
                $text .= $t;
                continue;
            }
        }

        $text = html_entity_decode($text);

        return $text;
    }




    function asSimplifiedText($aDecodedMessage)
    {
        
        $text = $this->asText($aDecodedMessage);

        $text = strip_tags($text);

        $text = strtolower($text);

        $text = $this->removePunctuation($text);

        $text = str_replace(array('    ', '    ', '   ', '  '), ' ', $text);

        return $text;
    }




    // --- English emotional analysis ----

    // --- Detect sales pitch discourse - scores little

    function emotionAnalysis($text)
    {
        $a = array(
            ' super ', ' superb ', ' great', ' best', ' amazing', ' amaze ', ' fantastic', ' joyful', ' beautiful', ' danger', ' amaz[ei]ng ',
            ' howdy', ' strange', ' lovely', ' pretty', ' mega', ' boost', ' fantastic ',
            ' regret', ' lucky', ' paradise', 'now!', '!', ' ugly', 'important', 'laughable', ' power[ful]?',
            ' boost', ' happ[iy]', ' sad\ ', ' success', ' safe', ' good ', ' nice ', ' luxury ', ' exquisite ', 'monster ',
            ' kill ', ' extra ', ' huge '
        );

        $iscore = 0;

        $comment = '';

        foreach ($a as $regex)
        {
                if (preg_match('/' . $regex . '/i', $text) > 0)
                {
                        $iscore += 0.1;

                        $comment .= $regex . ' ';
                }
        }

        if ($iscore > 0) score($iscore, "EMOTION_EN", $comment);
    }






    function removePunctuation($s)
    {
        return str_replace(array('~', '^', '\\', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '"', '=', '+', "'", '{', '}','$','[', ']','*', '/', '<', '>', ':', ';', ',','&', '#', '@', '(', ')','_','.',  '!', '?', "\n"), ' ', $s);
        // '|', '-'
    }








    function domainList($text)
    {
        preg_match('/http:\/\/[a-z0-9\.\-_\/~\?=@#%;$\(\)\+&]+/i', $text, $matches);

        foreach ($matches as $k => $match) $matches[$k] = domain($match);

        $matches = array_unique($matches);

        return $matches;
    }




    function extractEmail($s)
    {
        $pattern = "/([\s]*)([_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*([ ]+|)@([ ]+|)([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,}))([\s]*)/i";

        preg_match_all($pattern, $s, $matches);

        if (!empty($matches[2][0])) return $matches[2][0];

        return '';
    }




    /*
     * Extract a second-level or third level domain from a url
     */
    function domain($url)
    {

        $url = parse_url($url, PHP_URL_HOST); // using PHP_URL_HOST will return a string

        $a = explode('.', $url);

        $parts = count($a);

        if ($parts < 2) return '';

        if ($parts == 2) return $url;

        $i = ($parts - 2);

        $co = array('com', 'co', 'net', 'org');

        if (array_search($a[$i], $co) !== false) return $a[$i - 1] . '.' . $a[$i] . '.' . $a[$i + 1];

        return $a[$i] . '.' . $a[$i + 1];

    }



 }


?>
