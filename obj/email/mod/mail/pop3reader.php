<?php


// POP3 reader via MLemos pop3class code

require_once (SYS_ROOT . '/lib/pop3class/pop3.php');



class pop3reader
{

    
    var $pop3 = null;    // connection
    
    var $iMessages = 0;  // nr messages in box
    
    var $iTotalSize = 0; // Total size of messages in box
    
    var $iCnt = 0;       // Message cursor
    
    var $headers = '';      // Current message headers
    
    var $body = '';         // Currente message body
    
    
    
    function connect($s_hostname, $s_username, $s_password)
    {
        
  	// Uncomment when using SASL authentication mechanisms
        
	// require("sasl.php");
        
        

	// MLemos test_pop3class code, adapted: ---
	
	$this->pop3 = new pop3_class;
	
	$this->pop3->hostname = $s_hostname; 		   // POP 3 server host name                      
	
	$this->pop3->port = 110;                         // POP 3 server host port, usually 110 but some servers use other ports. Gmail uses 995
	
	$this->pop3->tls = 0;                            // Establish secure connections using TLS      
	
	$this->pop3->user = $s_username; 			   // Authentication user name                    
	
	$this->pop3->password = $s_password;		   // Authentication password                     
        
	$this->pop3->realm = "";                         // Authentication realm or domain              
	
	$this->pop3->workstation = "";                   // Workstation for NTLM authentication         
	
	$this->pop3->apop=0;                                   // Use APOP authentication                     
	
	$this->pop3->authentication_mechanism = "USER";  // SASL authentication mechanism               
	
	$this->pop3->debug = 0;                          // Output debug information                    
	
	$this->pop3->html_debug = 0;                     // Debug information is in HTML                
	
	$this->pop3->join_continuation_header_lines=1;   // Concatenate headers split in multiple lines 

	
        
        // --- Connect ---
	if(($error = $this->pop3->Open()) != "")
        {
            print HtmlSpecialChars($error) . "\n";
            return false;
        }
        
        echo "Connected to the POP3 server &quot;".$this->pop3->hostname."&quot;.\n";
        
        
        
        // --- Login ---
        if(($error = $this->pop3->Login($s_username,$s_password,$this->pop3->apop)) != "")
        {
            print "Error in login: " . htmlspecialchars($error) . "\n";
            return false;
        }
        
        echo "User &quot;$s_username&quot; logged in.\n";
        
        

        // --- Stats ---
        if(($error = $this->pop3->Statistics($messages,$size)) != "")
        {
            print "Error calling Statistics... " . htmlspecialchars($error) . "\n";
            return false;
        }
        
        echo 'There are ' . $messages . ' messages in the mail box with a total of ' . number_format($size / 1204 / 1024, 1) . "MB.\n";
        
        $this->iMessages = $messages;
        
        $this->iTotalSize = $size;
        
        
       
        // --- Messages ---
        
        if ($messages == 0)
        {
            print "No messages found.\n";
            return false;
        }

	print "Connected...\n";
            
    }
    
    
   
   function deleteMessage()
   {
      $this->pop3->DeleteMessage($this->iMessage);
   }
 
    
    
    function retrieveMessage()
    {

       print "\nMessage " . $this->iCnt . "\n";

        if(($error = $this->pop3->RetrieveMessage($this->iCnt, $this->headers, $this->body)) != "")
        {
            print "Error: " . $error . "\n";
            
            return false;
        }
        
        $this->iCnt++;
        
    }
    
    
    
    
    // returns raw message
    
    function getRawMessage()
    {
       
       //print "\ngetRawMessage " . $this->iCnt . "\n";

       $this->pop3->OpenMessage($this->iCnt);
       
       $this->iCnt++;
       
       $this->pop3->GetMessage(20000000, $rawMessage, $bEnd);

	// $bEnd tells that the whole message has been returned
       
       return $rawMessage;
    }

    
    

    function connectionClose()
    {
        $error = $this->pop3->Close();

        if ($error == "") echo "Disconnected from the POP3 server &quot;".$this->pop3->hostname."&quot;.\n";

        else print $error . "\n";
    }

    
    
}
    
?>
