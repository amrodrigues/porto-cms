<?php

// Outgoing mail management

require_once ('iarray.php');
require_once ('iconf.php');
require_once('simplemail.php'); // this is a gateway to lib/swift

class sender {

    // all message data to merge, etc.
    // Each var is a sys_mailqueue column


    var $dat = array();
    var $bVerbose = false; // output stuff

    // constructor:

    function sender($bVerbose = false) {

        if ($bVerbose)
            $this->bVerbose = true;
    }

    /*
     * Create a new message
     */

    function createNewMessage($s_subject, $s_sender_name, $s_sender_email, $startsend, $s_smtp_host, $s_localhost = 'localhost.localdomain') {

        // verify data:

        if (empty($s_subject))
            die("sender: sender: message subject?");

        if (empty($s_sender_name))
            die("sender: sender: message sender_name?");

        if (empty($s_sender_email))
            die("sender: sender: message sender_email?");



        if (!empty($startsend))
            $this->dat['startsend'] = $startsend;

        else
            $this->dat['startsend'] = date('Y-m-d H:i:s');


        $this->dat['subject'] = $s_subject;



        // fill dat array for serialization:

        $this->dat['site_uri'] = $GLOBALS['conf']->siteurl;

        $this->dat['site_name'] = $GLOBALS['conf']->sitename;

        $this->dat['sender_name'] = $s_sender_name;

        $this->dat['sender_email'] = $s_sender_email;

        $this->dat['smtp_host'] = $s_smtp_host;

        $this->dat['localhost'] = $s_localhost;

        $this->dat['filename'] = date('YmdHis') . '_' . $_SERVER["HTTP_HOST"] . '_' . rand(1, 99999999);

        $this->dat['base_path'] = $_SERVER['DOCUMENT_ROOT'];

        $this->dat['base_uri'] = $GLOBALS['conf']->siteurl;

        $this->dat['smtp_user'] = $GLOBALS['conf']->smtpuser;

        $this->dat['smtp_pass'] = $GLOBALS['conf']->smtppass;
    }

    // sets oids of data objects (forms, etc.) as recipient lists:

    function setRecipients($aRecipientOids) {
        if (!is_array($aRecipientOids) || empty($aRecipientOids))
            die("sender: sender: set aRecipientOids");

        $this->dat['recipients'] = $aRecipientOids;
    }

    /*
     * set html message template using a html base template with a {[1]} tag
     * and a pid for fetching a folder content
     */

    function setTemplate($oidFolder) {

        global $ob;


        // get message folder

        $udFolder = $ob->get($oidFolder); // message folder. Property layout has layout oid

        if (empty($udFolder['br']['class']) || $udFolder['br']['class'] != 'folder')
            die("Posicione o gestor de objetos numa pasta com conte&uacute;do a enviar");

        $oidLay = $udFolder['br']['layout'];

        if (empty($oidLay))
            die("Antes de enviar esta mensagem, edite as propriedades da pasta e seleccione um layout.");


        // get layout contents, inserting CSS in <head>

        $udLay = $ob->get($oidLay);

        $sLay = $udLay['content']['template']['value'];

        $sLay = str_replace('<head>', '<head><style type="text/css">' . $udLay['content']['css']['value'] . '</style>', $sLay);


        // serialize objects below folder

        $a = $ob->children($oidFolder);

        $html = '';

        if (!empty($a))
            foreach ($a as $aa)
                $html .= $ob->get_html($aa['oid']);


        // set template inserting html into sLay:

        $this->dat['template'] = str_replace('{[1]}', $html, $sLay);


        // links
        //TODO: improve
        //$this->dat['template'] = str_replace(' src="/', ' src="' . $GLOBALS['conf']->siteurl . '/', $this->dat['template']);
        //$this->dat['template'] = str_replace(' href="/', ' href="' . $GLOBALS['conf']->siteurl . '/', $this->dat['template']);
    }

    /*
     * template getter for message preview
     */

    function getTemplate() {
        return $this->dat['template'];
    }

    /*
     * Having all data collected, let's serialize $this->dat and save it to a message queue
     */

    function queueInsert() {

        global $ob, $db;


        // prepare vars for sys_mailqueue:

        $dat = $this->dat;

        $subject = $dat['subject'];
        unset($dat['subject']);

        $startsend = $dat['startsend'];
        unset($dat['startsend']);

        $template = $dat['template'];
        unset($dat['template']);




        // add to sys_mailqueue:

        $sql = "insert into sys_mailqueue (startsend, subject, template, dat) values ('$startsend', '$subject', '" . mysql_real_escape_string($template) . "', '" . serialize($dat) . "')";

        $db->exec($sql);

        print_r(mysql_error());

        unset($this->dat);
    }

    function getMergedMessage($template, $aUserData = array()) { // a row data is merged with a template copy
        // template is the full html message with {{ }}
        // Merge message data:
        foreach ($this->dat as $k => $v)
            $template = str_replace('{{' . $k . '}}', $v, $template);

        // Merge $aUserData (row data):

        if (!empty($aUserData))
            foreach ($aUserData as $k => $v)
                $template = str_replace('{{' . $k . '}}', $v, $template);

        return $template;
    }

    /*
     * Check system load average
     */

    function checkLA() {
        $s_raw = shell_exec("cat /proc/loadavg");

        $i = strpos($s_raw, ' ');

        $f_load_average = (float) substr($s_raw, 0, $i);

        if ($f_load_average >= 7)
            return false;

        return true;
    }

    function partialSend() {

        global $db;


        try {



            if (!$this->checkLA()) {
                if ($this->bVerbose)
                    print "Load Average too high... returning later...\n";

                return;
            }






            // get sys_mailqueue row:

            $queueRow = $db->select_row('select * from sys_mailqueue where closedjob <> "1" and startsend < now() order by oid limit 1');

            if (empty($queueRow)) {
                if ($this->bVerbose)
                    print "queueRow is empty... nothing to send\n";

                return false; // no messages to send now or all messages already sent...
            }




            $queueId = $queueRow['oid'];

            $dat = unserialize($queueRow['dat']); // message data
            // init send data?

            if (empty($dat['curoid'])) {

                if ($this->bVerbose)
                    print_r($dat['recipients']);


                // if no recipients, close job and finish:

                if (empty($dat['recipients'])) {
                    if ($this->bVerbose)
                        print "Init: No dat[recipients] found at start... Closing Job.\n";

                    $this->closeJob($queueRow['oid']);

                    return;
                } else {

                    // Init: start with the first recipient object:

                    if ($this->bVerbose)
                        print "Init: start with the 1st rec. object\n";

                    $rec = array_values($dat['recipients']);

                    $dat['curoid'] = $rec[0];

                    $dat['curid'] = 0; // current id for fetching recipients at id+1

                    if ($this->bVerbose)
                        print "Init: first data oid is " . $rec[0] . "\n";


                    // create a temporary table for duplicate testing:

                    $sql = 'create table sys_mail' . $queueId . ' (em char(90))';

                    if ($this->bVerbose)
                        print 'Init: ' . $sql . "\n";

                    $db->exec($sql);
                }
            }


            // data table is:

            $table = 'form_data_' . $dat['curoid'];


            // get next few rows of recipient data:

            $sql = 'select * from ' . $table . ' where oid > ' . $dat['curid'] . ' and (sys_optout is null or sys_optout = \'0\' or sys_optout like \'\' or sys_optout = \'\') order by oid limit 200';

            if ($this->bVerbose)
                print $sql . "\n";

            $aRecipients = $db->select($sql);

            if ($this->bVerbose)
                print "Found " . count($aRecipients) . " recipients.\n";



            // jump to next data object?

            if (empty($aRecipients)) {
                if ($this->bVerbose)
                    print "No recipients found... trying to jump to next recipient list...\n";

                $rec = array_values($dat['recipients']);

                for ($f = 0; $f < count($rec); $f++) {
                    if ($rec[$f] == $dat['curoid']) {
                        if (!empty($rec[$f + 1])) {
                            // set next data object

                            $dat['curoid'] = $rec[$f + 1];

                            $dat['curid'] = 0; // next time, we will fetch id+1 and beyond

                            $this->persistDat($queueRow['oid'], $dat);

                            if ($this->bVerbose)
                                print "Setting current data object to " . $dat['curoid'] . "\n";

                            return;
                        } else {

                            // nothing more to send...

                            $this->closeJob($queueRow['oid']);

                            $this->persistDat($queueRow['oid'], $dat);

                            if ($this->bVerbose)
                                print "Next rec. list not found. Nothing more to send. Closing job...\n";

                            return;
                        }
                    }
                }
            }


            $iSent = 0;


            // for each recipient row, look for emails and send a message:





            foreach ($aRecipients as $k => $aRow) {

                if ($this->bVerbose)
                    print "Processing recipient row #" . $k . "\n";


                // collect all emails from user row:

                $aEmails = array();

                foreach ($aRow as $s) {
                    $s = trim($s);

                    $pattern = "/([\s]*)([_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*([ ]+|)@([ ]+|)([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,}))([\s]*)/i";

                    preg_match_all($pattern, $s, $matches);

                    if (!empty($matches[2]))
                        foreach ($matches[2] as $s_email)
                            $aEmails[$s_email] = $s_email;
                }


                if ($this->bVerbose)
                    print "@rowId: " . $dat['curid'] . "\n";


                // prepare send:

                if (!empty($aEmails))
                    foreach ($aEmails as $email) {

                        // get merged message with user row:

                        $s_body = $this->getMergedMessage($queueRow['template'], $aRow);

                        $s_body = str_replace('{{sys_email}}', $email, $s_body);


                        // Duplicate? :

                        $sql = 'select em from sys_mail' . $queueId . ' where em="' . $email . '"';

                        $atmp = $db->select($sql);


                        if (empty($atmp)) {
                            if ($this->bVerbose)
                                print "Sending to " . $email . "\n";

                            $oMail = new simpleMail();


                            $result = $oMail->send($dat['sender_name'], $dat['sender_email'], $email, $email, $dat['sender_email'], $queueRow['subject'], 'Mensagem em HTML', $s_body, $dat['smtp_host'], $dat['smtp_user'], $dat['smtp_pass'], $dat['base_path']);

                            //TODO: manage $result
                            // register for duplicates:

                            $sql = 'insert into sys_mail' . $queueId . ' (em) values("' . $email . '")';

                            $db->exec($sql);


                            $iSent++;
                        } else {

                            if ($this->bVerbose)
                                print "$email is duplicate... skipping\n";
                        }
                    }

                $dat['curid'] = $aRow['oid'];
            }


            // update db state:

            $this->addToSentCounter($queueRow['oid'], $iSent);

            $this->persistDat($queueRow['oid'], $dat);
        } catch (Exception $e) {
            var_dump($e);
        }
    }

    /*
     * Saves dat
     */

    function persistDat($queueId, $dat) {
        global $db;

        $sql = "update sys_mailqueue set dat='" . serialize($dat) . "' where oid=" . $queueId;

        if ($this->bVerbose)
            print $sql . "\n";

        $db->exec($sql);

        if ($this->bVerbose)
            print mysql_error();
    }

    /*
     * Adds a value to message sent counter of sys_mailqueue
     */

    function addToSentCounter($queueId, $valueToAdd) {

        global $db;


        $sql = 'select cnt from sys_mailqueue where oid=' . $queueId;

        $i = $db->select_cell($sql);

        $i = (int) $i;


        $sql = 'update sys_mailqueue set cnt=' . ($i + $valueToAdd) . ' where oid=' . $queueId;

        if ($this->bVerbose)
            print $sql . "\n";

        $db->exec($sql);

        if ($this->bVerbose)
            print mysql_error();
    }

    //TODO: dev

    function set_body_by_folder($id_folder) {

        global $db;


        // Get layout data --------------------

        $s_sql = 'select layout from broker where oid = ' . $id_folder;

        $id_layout = $db->select_cell($s_sql);

        if (empty($id_layout))
            die("Por favor associe um objecto de layout nas propriedade da pasta que deseja enviar por email.</body></html>");


        $a_layout = $db->select_row('select * from layout where oid =' . $id_layout);



        // Set body with layout data ----------

        $s_body = '<html><head><style type="text/css">' . $a_layout['css'] . '</style><title></title></head><body>' . $a_layout['template'] . '</body>';

        $this->s_body = $s_body;
    }

    function setUnsubscribeLink() {
        $sUnsub = '<div class="optout"><a href="' . $GLOBALS['conf']->siteuri . '/portal/unsubscribe.php?email={{sys_email}}&amp;sys_uid={{sys_uid}}&amp;oid={{oid}}&amp;id={{id}}"><u>Para Cancel futuras mensagens, clique aqui</u></a>.</div>';

        $this->dat['template'] = str_replace('</body>', $sUnsub . '</body>', $this->dat['template']);
    }

    /*
     *
     */

    function closeJob($queueOid) {
        global $db;

        try {

            // set closed job:

            $sql = 'update sys_mailqueue set closedjob="1" where oid=' . $queueOid;

            if ($this->bVerbose)
                print $sql . "\n";

            $db->exec($sql);


            // delete duplicates table:
            //TODO:uncomment
            //$sql = 'drop table sys_mail' . $queueOid;
            //$db->exec($sql);
        } catch (Exception $e) {
            if ($this->bVerbose)
                var_dump($e);
        }
    }

    //TODO: review
    function email_clean($s_email) {

        require_once('itype.php');

        $s_email = strtolower($s_email);

        $s_email = istr::noaccent($s_email);

        $s_email = str_replace('mailto:', '', $s_email);

        if (strpos($s_email, '/') !== false) {
            $a = explode('/', $s_email);

            $s_email = $a[0];
        }

        $s_email = trim($s_email, " \0\x0B\r\n\t.;\"'+ºª<>~^/*#$%&()=}][{§½£@«»:,-!_");

        $s_email = trim($s_email);

        if (itype::valid($s_email, 'email'))
            return $s_email;

        return false;
    }

}

?>
