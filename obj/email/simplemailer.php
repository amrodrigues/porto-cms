<?php

class simplemailer {

    function send($subject, $body, $a_emails) {

        //TODO:implement
    }

    /*
     * Sends $ud content text
     */

    function sendUd($a_emails, $subject, $ud, $addRef = '') {


        global $conf, $ob;

        $ob->log('simplemailer: sendUd enter');

        $a_emails = $this->parseEmails($a_emails);

        if (empty($a_emails))
            $ob->log('No emails found...');

        $ob->log('sending to ' . print_r($a_emails, true));

        // Build message body

        $s_message = '<html><head></head><body>';

        // Check if form has email type

        $reply_to = ''; // holds email for replies

        foreach ($ud['content'] as $a_prop) {
            $s_message .= '<h3>' . $a_prop['desc'] . '</h3>';

            if ($a_prop['type'] == 'fradio' || $a_prop['type'] == 'fselect')
                $s_message .= $a_prop['xselect'][$a_prop['value']];
            else
                $s_message .= $a_prop['value'];

            $s_message .= '<br /><br />';

            if (empty($reply_to) && $a_prop['type'] == 'email')
                $reply_to = $a_prop['value'];
        }

        // include new record oid:

        if (strlen($addRef) > 0)
            $s_message .= 'Ref: ' . $addRef . '<br />';

        $s_message .= '</body></html>';


        // Email sending ------------------------------

        $iSent = 0;

        foreach ($a_emails as $s_email) {

            $ob->log("sending to: $s_email");

            $s_email = trim($s_email);

            if (empty($reply_to))
                $s_reply_address = $s_email;
            else
                $s_reply_address = $reply_to;

            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= 'To: ' . $s_email . "\r\n";
            $headers .= 'From: "' . $conf->sitename . '" <' . $conf->adminemail . ">\r\n";
            $headers .= 'Reply-To: ' . $s_reply_address . "\r\n";

            if (mail($s_email, $subject, $s_message, $headers))
                $iSent++;
        }

        if ($iSent == 0) {

            $ob->log('error sending mail.');
            return false;
        }

        return true;
    }

    /*
     * Take email list and organize it
     * TODO: regex from mail import menu option
     */

    function parseEmails($a_emails) {

        // explode recipient addresses:

        if (!is_array($a_emails)) {
            if (strpos($a_emails, ',') !== false)
                $sep = ',';
            elseif (strpos($a_emails, ';') === false)
                $sep = ';';

            $a_emails = explode($sep, $a_emails);
        }

        if (!empty($a_emails))
            foreach ($a_emails as $k => $v)
                $a_emails[$k] = trim($v);

        return $a_emails;
    }

}

?>
