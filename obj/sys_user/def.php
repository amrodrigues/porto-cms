<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'sys_user',
        'easyname' => 'Users',
        'cache' => false
      ),
      'content' => array(
        'idprofile' => array(
          'name' => 'idprofile',
          'type' => 'dbselect',
          'desc' => 'User profile',
          'xsql' => 'select oid,title from sys_userprofile order by title'
        ),
        'name' => array(
          'name' => 'name',
          'type' => 'textline',
          'desc' => 'Name',
          'show' => true
        ),
        'email' => array(
          'name' => 'email',
          'type' => 'email',
          'desc' => 'Email',
          'show' => true
        ),
        'pass' => array(
          'name' => 'pass',
          'type' => 'smalltext',
          'desc' => 'Password'
        ),
        'debugmode' => array(
          'name' => 'debugmode',
          'type' => 'bool',
          'desc' => 'Debug mode'
        )
      )
    );
?>