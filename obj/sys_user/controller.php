<?php

class sys_user {

    function get_rows($id_start, $nr_lines) {
        global $db;

        $s_sql = 'select * from sys_user where oid >= ' . $id_start . ' order by oid limit ' . $nr_lines;

        $a_ret = $db->select($s_sql);


        // Check uids

        $b = false;

        foreach ($a_ret as $k => $v) {
            if (empty($v['sys_uid'])) {
                if ($b == false) {
                    $this->check_sys_uid_column();

                    $b = true;
                }

                $uid = $this->insert_uid($v['oid']);

                $a_ret[$k]['sys_uid'] = $uid;
            }
        }

        return $a_ret;
    }

    function optout($s_email, $sys_uid) {
        global $db;


        // Sanitize

        $s_email = str_replace('"', '', str_replace("'", "", $s_email));

        $sys_uid = str_replace('"', '', str_replace("'", "", $sys_uid));


        $s_sql = "update sys_user set sys_optout=\"1\" where sys_uid=\"$sys_uid\" and email=\"$s_email\"";

        $db->exec($s_sql);

        return $db->error();
    }

    function update_email($s_email, $sys_uid, $s_new_email) {
        global $db;


        // sanitize

        $s_email = str_replace('"', '', str_replace("'", "", $s_email));

        $sys_uid = str_replace('"', '', str_replace("'", "", $sys_uid));

        $s_new_email = str_replace('"', '', str_replace("'", "", $s_new_email));


        $s_sql = "update sys_user set $s_col=\"$s_new_email\" where sys_uid=\"$sys_uid\" and email=\"$s_email\"";

        $db->exec($s_sql);

        return $db->error();
    }

    function insert_uid($oid) {
        global $db;

        require_once($GLOBALS['conf']->base . '/core/uz/uzstr.php');

        $new_uid = uzstr::uid();

        $s_sql = "update sys_user set sys_uid='" . $new_uid . "' where oid=" . $oid;

        $db->exec($s_sql);

        return $new_uid;
    }

    function check_sys_uid_column() {
        global $db;

        if ($db->field_exists('sys_uid', 'sys_user') == false) {
            $s_sql = 'alter table sys_user add sys_uid char(255)';

            $db->exec($s_sql);
        }
    }

}
