<?php

$a_def = array(
    'classes' => false,
    'br' => array(
        'class' => 'template',
        'easyname' => 'Template',
        'cache' => true
    ),
    'content' => array(
        'html' => array(
            'name' => 'html',
            'type' => 'html',
            'desc' => 'HTML/Polymer',
            'show' => true
        )
    )
);
