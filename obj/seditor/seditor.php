<?php

class seditor
{

    function asHtml($ud)
    {
        
        global $db;

	require_once ($GLOBALS['conf']->root . '/metaform.php');

	$lay = '';
	

	if (empty($ud['content']['rid']['value'])) die ("Please specify rid");

	$o_meta = new metaform($ud['br']['oid']);
	



	// Get class
	
	$sql = 'select class from broker where oid=' . $ud['content']['rid']['value'];
	
	$s_class = $db->select_cell($sql);
	
	

	// Get def

	require($GLOBALS['conf']->root . '/obj/' . $s_class . '/def.php');
	
	$a_propnames = explode(',', $ud['content']['prop']['value']);
	
	
	// Remove unused prop:
	
	foreach($a_def['content'] as $k => $v)
	{
		$b_exists = false;
		
		foreach ($a_propnames as $s)
		{
			if ($s == $k)
			{
				$b_exists = true;
				
				break;	
			}
		}
		
		if ($b_exists === false) unset($a_def['content'][$k]);
	}
	
	$o_meta->metadata($a_def['content']);
	
	$o_meta->files_basedir($_SERVER['DOCUMENT_ROOT']);


	
	
	// Edit form
	
	if (empty($_POST))
	{

                $lay .= $o_meta->form('', '', null, 'Atualizar', false, 'update', '/index.php?oid=' . $ud['br']['oid'], null, null, false, true, '');

	} else {
		
		// Post data to DB	
		
		$o_meta->update_row ($s_class, $ud['content']['rid']['value'], $ud['content']['rid']['value'], 'oid', $_POST);
		
                $lay .= 'Os dados foram atualizados.';

                if (!empty($o_meta->s_errors)) $lay .= ' ERRO: ' . $o_meta->s_errors;

	}


        return $lay;
		
    }

}


?>