<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'seditor',
        'easyname' => 'Data editor',
        'cache' => false
      ),
      'content' => array(
        'rid' => array(
          'name' => 'rid',
          'type' => 'integer',
          'desc' => 'Object to edit',
          'show' => false
        ),
        'prop' => array(
          'name' => 'prop',
          'type' => 'textline',
          'desc' => 'Properties to edit',
          'optional' => true,
          'show' => false
        )
      )
    );
?>
