<?php

    /*
     * xtypes:
     * 1: Login via objeto de login com êxito
     * 2: Login via objeto de login falhado
     */

//TODO: permission-based log

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'sys_stats',
        'easyname' => 'Statistics',
        'cache' => false
      ),
      'content' => array(
        'xtype' => array(
          'name' => 'xtype',
          'type' => 'integer',
          'desc' => 'Register type'
        ),
        'ts' => array(
          'name' => 'ts',
          'type' => 'datetime',
          'desc' => 'Timestamp'
        ),
        'xuser' => array(
          'name' => 'xuser',
          'type' => 'textline',
          'desc' => 'User name',
          'optional' => true
        ),
        'rid' => array(
          'name' => 'rid',
          'type' => 'integer',
          'desc' => 'Object id',
          'optional' => true
        ),
        'xlog' => array(
          'name' => 'xlog',
          'type' => 'textline',
          'desc' => 'Specific data',
          'optional' => true
        )
      )
    );
?>