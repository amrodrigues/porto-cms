<?php

/*
 * Defines a user.
 * A user is associated with a role
 */

$a_def = array(

    'classes' => false,

    'br' => array(
        'class' => 'user',
        'easyname' => 'User',
        'cache' => false
    ),
    'content' => array(
        'idrole' => array(
            'name' => 'idrole',
            'type' => 'dbselect',
            'desc' => 'Role',
            'xsql' => 'select oid,title from role order by title'
        ),
        'name' => array(
            'name' => 'name',
            'type' => 'textline',
            'desc' => 'Name',
            'show' => true
        ),
        'email' => array(
            'name' => 'email',
            'type' => 'email',
            'desc' => 'Email',
            'show' => true
        ),
        'pass' => array(
            'name' => 'pass',
            'type' => 'smalltext',
            'desc' => 'Password'
        ),
        'debugmode' => array(
            'name' => 'debugmode',
            'type' => 'bool',
            'desc' => 'Debug mode'
        )
    )
);
?>