<?php

class user {

    function asHtml($ud) {
        return '';
    }

    function get_rows($id_start, $nr_lines) {

        global $ob;
        $s_sql = 'select * from user where oid >= ' . $id_start . ' order by oid limit ' . $nr_lines;
        $a_ret = $ob->select($s_sql);


        // Check uids

        $b = false;

        foreach ($a_ret as $k => $v) {
            if (empty($v['sys_uid'])) {
                if ($b == false) {
                    $this->check_sys_uid_column();
                    $b = true;
                }
                $uid = $this->insert_uid($v['oid']);
                $a_ret[$k]['sys_uid'] = $uid;
            }
        }

        return $a_ret;
    }

    function optout($s_email, $sys_uid) {
        global $ob;

        $s_email = str_replace('"', '', str_replace("'", "", $s_email));
        $sys_uid = str_replace('"', '', str_replace("'", "", $sys_uid));

        $s_sql = "update user set sys_optout=\"1\" where sys_uid=\"$sys_uid\" and email=\"$s_email\"";
        $ob->exec($s_sql);

        return; //$ob->error();
    }

    function update_email($s_email, $sys_uid, $s_new_email) {
        global $ob;

        // sanitize

        $s_email = str_replace('"', '', str_replace("'", "", $s_email));
        $sys_uid = str_replace('"', '', str_replace("'", "", $sys_uid));
        $s_new_email = str_replace('"', '', str_replace("'", "", $s_new_email));

        $s_sql = "update user set email=\"$s_new_email\" where sys_uid=\"$sys_uid\" and email=\"$s_email\"";
        $ob->dbExec($s_sql);
        return $ob->error();
    }

    function insert_uid($oid) {

        global $ob;
        require_once($GLOBALS['conf']->base . '/core/uz/uzstr.php');
        $new_uid = uzstr::uid();
        $s_sql = "update user set sys_uid='" . $new_uid . "' where oid=" . $oid;
        $ob->dbExec($s_sql);
        return $new_uid;
    }

    function check_sys_uid_column() {
        global $db;
        if ($db->field_exists('sys_uid', 'user') == false) {
            $s_sql = 'alter table user add sys_uid char(255)';
            $db->exec($s_sql);
        }
    }

}
