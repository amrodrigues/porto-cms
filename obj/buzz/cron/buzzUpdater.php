<?php

require_once($conf->root . '/text.php');
require_once($conf->root . '/collections.php');
require_once($conf->root . '/obj/datatable/datatable.php');

class buzzUpdater {
    /*
     * Gets list of buzz objects
     */

    private function buzzObjectsOid() {
        global $ob;
        $aBuzz = $ob->dbSelectColumn('select oid from broker where class="buzz"');
        if (empty($aBuzz)) {
            return false;
        }
        return $aBuzz;
    }

    /*
     * Execute 2 times a day
     */

    public function daily2x() {
        return $this->exec();
    }

    /*
     * Updates all buzz objects
     */

    public function exec() {
        global $ob;
        $ob->log("At buzzUpdater::exec()");
        $a = $this->buzzObjectsOid();
        if (empty($a)) {
            return false;
        }
        foreach ($a as $oid) {
            $this->updateBuzz($oid);
        }
    }

    /*
     * Updates a single buzz object
     */

    public function execSingle($oid) {
        $this->updateBuzz($oid);
    }

    /*
     *
     */

    public function updateBuzz($oid) {
        global $ob;
        //$ob->debugMode(true);
        if (!is_a($ob, 'dob')) {
            throw new Exception('ob is not an object');
        }
        $odt = new datatable();
        $ud = $ob->get($oid);

        /*
         *  1. get serp links
         */

        if (empty($ud['content']['urls']['value'])) {
            return;
        }

        $stSearchUrls = TextPreprocessor::linesToSet($ud['content']['urls']['value']);
        if (empty($stSearchUrls)) {
            throw new Exception("No search urls found");
        }

        $cr = new crawlerHelper;
        $aRxDiscard = $cr->explodeRegexes($ud['content']['urlfilter']['value'], false);
        $setSerpLinks = $cr->getSerpLinks($stSearchUrls, $aRxDiscard);
        $aPermanentPages = $cr->linesToArray($ud['content']['addpages']['value']);

        foreach ($aPermanentPages as $sp) {
            $setSerpLinks->add($sp);
        }
        if ($setSerpLinks->count() == 0) {
            return 'No SERP links';
        }

        /*
         * 2. Fetch each page and save results
         */

        $keywordCounter = new Counter();
        $cr->registerCounter($keywordCounter);
        $aRxPageMustHave = $cr->explodeRegexes($ud['content']['rxpage']['value']);
        $aRxNegative = $cr->explodeRegexes($ud['content']['norx']['value']);
        $aaRxPositive = $cr->explodeRegexesWithKey($ud['content']['rx']['value'], '::');

        foreach ($setSerpLinks as $sSerpLink) {
            $ob->log("Loading SERP link $sSerpLink");
            $cr->loadUrl($sSerpLink);
            if ($cr->rawTextIsEmpty()) {
                continue;
            }
            $cr->purifyToSimpleText();
            if ($cr->purifiedTextIsEmpty()) {
                continue;
            }
            if ($cr->matchesAnyInPurifiedText($aRxPageMustHave) == false) {
                continue;
            }
            if ($cr->matchesAnyInPurifiedText($aRxNegative)) {
                continue;
            }
            $aIdeas = $cr->countPatternMatchesWithArraysOfRegexes($aaRxPositive);

            // --- save html doc

            $aSave = array(
                'title' => $title = $cr->extractHtmlTitle(),
                'purified' => $cr->getPurifiedText(),
                'url' => $sSerpLink,
                'html' => $cr->getRawText(),
                'ideas' => ' ::' . implode(' :: ', $aIdeas) . ':: ',
                'ts' => date('Y-m-d H:i:s')
            );
            $ob->log("Preparing to insert to " . $ud['content']['dtpageoid']['value']);
            $odt->insertData($aSave, $ud['content']['dtpageoid']['value']);
        }


        /*
         * 3. save to db
         * Drinking Water idea will be in all web pages (100%)
         * The value of other ideas will be a percentage relative to the set
         */

        $a = $keywordCounter->asArray();
        if (!empty($a)) {
            $max = max($a);
            $ts = date('Y-m-d H:i:s');
            $aData = array();
            foreach ($a as $k => $v) {
                $aData[] = array('ts' => $ts, 'k' => $k, 'v' => $v, 'pc' => (($v / $max) * 100));
            }
            $ob->log($aData);
            $odt->insertData($aData, $ud['content']['dtoid']['value']);
        } else {
            $ob->log('No data to save to db.');
        }
    }

}
