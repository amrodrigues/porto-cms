<?php

$a_def = array(
    'classes' => array('image' => 1, 'imageslider' => 1, 'album' => 1, 'box' => 1),
    'br' => array(
        'class' => 'buzz',
        'easyname' => 'Buzz',
        'cache' => true
    ),
    'content' => array(
        'urls' => array(
            'name' => 'urls',
            'type' => 'code',
            'desc' => 'URL\'s to fetch links',
        ),
        'urlfilter' => array(
            'name' => 'urlfilter',
            'type' => 'code',
            'desc' => 'Remove URL\'s having the following regexes',
            'optional' => true,
            'show' => true
        ),
        'rxpage' => array(
            'name' => 'rxpage',
            'type' => 'code',
            'desc' => 'Each content page must match at least on the following regexes',
            'optional' => true,
            'show' => true
        ),
        'norx' => array(
            'name' => 'norx',
            'type' => 'code',
            'desc' => 'Discard pages having the following regexes',
            'optional' => true
        ),
        'addpages' => array(
            'name' => 'addpages',
            'type' => 'code',
            'desc' => 'Add the following pages for permanent monitorization',
            'optional' => true
        ),
        'rx' => array(
            'name' => 'rx',
            'type' => 'code',
            'desc' => 'Monitor the folowing idea::regex::regex... list on each content page',
            'optional' => true,
            'show' => true
        ),
        'dtoid' => array(
            'name' => 'dtoid',
            'type' => 'dbselect',
            'desc' => 'Save to data table. Needs ts(datetime),k(textline),v(float) properties.',
            'xsql' => 'select oid, easyname from broker where class="datatable" ' . $GLOBALS['oAuth']->sqlRestrict('view') . ' order by easyname'
        ),
        'dtpageoid' => array(
            'name' => 'dtpageoid',
            'type' => 'dbselect',
            'desc' => 'Save web docs to data table. Needs url(url),title(textline),html(code),purified(code) properties.',
            'xsql' => 'select oid, easyname from broker where class="datatable" ' . $GLOBALS['oAuth']->sqlRestrict('view') . ' order by easyname'
        )
    )
);
