<?php


/*
 * Under development
 */
class multibanco {
    
    var $aEntityId;
    
    /*
     * This method receives a payment confirmation from Ifthen
     * 
     * You must supply Ifthen Software the url that runs this method. Example:
     * https://www.example.com/index.php?oid=[oid]&chave=[ANTI_PHISHING_KEY]&entidade=[ENTITY]&referencia=[REFERENCE]&valor=[VALUE] 
     * [oid] is the id of multibanco object
     */
    
    function asHtml($ud) {

        if (isset($_REQUEST['chave']) && isset($_REQUEST['entidade']) && isset($_REQUEST['referencia']) && isset($_REQUEST['valor'])) {

            $chave = $_REQUEST['chave'];
            $entidade = $_REQUEST['entidade'];
            $referencia = $_REQUEST['referencia'];
            $valor = $_REQUEST['valor'];

            if ($chave !== trim($ud['content']['antiphishingkey']['value']))
                die("");

            // --- update payments table
            
            
            
            // --- send notification to customer and supplier

            die();
            
        } else {

            die("ERROR");
        }
    }


    /*
     * 
     */
    function save($ref, $value) {
       $a = array('ref' => $ref, 'val' => $value);
       
    }
    
    
    /*
     *  Note: payId is generated taking the last 4 digits of the oid column
     */
    function createTable($oid) {
        global $ob;
        $sql = 'create table multibanco' . (int) $oid . ' (oid int unsigned not null auto_increment, em char(110), descr char(255), idpay int unsigned, tscreate datetime, tspay datetime, ref char(20), val float, primary key(oid))';
        $ob->exec($sql);
    }
    
    
    /*
     * 
     */
    function getEntityId() {
        if (empty($this->aEntityId)) {
            $this->aEntityId = array('entityid' => "12345", 'subentityid' => '678');
            //$GLOBALS['ob']->dbSelectRow('select entityid, subentityid from multibanco order by oid limit 1');
        }
        return $this->aEntityId;
    }

    
    
    /*
     * Creates a new order id (or payment id) (0001 to 9999)
     * Return the new payment reference
     */
    
    function getNewPayReference($descr, $value) {

        $value = str_replace('.', '', $this->filterValue($value));

        // get new pay id
        global $ob;

        //TODO: db queries
        $a = array();
        $paymentid = "1456"; //TODO:remove
        $a = $this->getEntityId();
        $payref = $this->generatePayReference($a['entityid'], $a['subentityid'], $paymentid, $value);

        //TODO: save ref to db

        return $payref;
    }

    
   
    /*
     * 
     */

    function generatePayReference($entity, $subentity, $payId, $value) {

        if ($this->testEntity($entity) === false)
            die('multibanco:Entity not good');
        
        if ($this->testSubentity($subentity) === false)
            die('multibanco:Subentity not good');
        
        $payId = $this->filterPayId($payId);
        
        if ($payId === false)
            die('multibanco:Payment id not good');

        $s = $entity . $subentity . $payId . str_pad($value, 8, '0', STR_PAD_LEFT);

        // calculate control digits:

        $at = array(51, 73, 17, 89, 38, 62, 45, 53, 15, 50, 5, 49, 34, 81, 76, 27, 90, 9, 30, 3);
        $v = 0;
        for ($ix = 0; $ix < strlen($s); $ix++)
            $v = $v + ($at[$ix] * substr($s, $ix, 1));
        $ctrlDigits = 98 - ($v % 97);
        if (strlen($ctrlDigits) == 1)
            $ctrlDigits = '0' . $ctrlDigits;

        if (strlen($ctrlDigits) !== 2) die ("multibanco: error, ctrlDigits have more than 2 digits: $ctrlDigits");
        return $subentity . $payId . $ctrlDigits;
    }

    
    /*
     * 
     */
    function testEntity($s) {
        if (preg_match('/^\d{5}$/', "$s") == 1)
            return true;
        else
            return false;
    }

    
    /*
     * 
     */
    function testSubentity($s) {
        if (preg_match('/^\d{3}$/', $s) == 1)
            return true;
        else
            return false;
    }
    
    
    /*
     * Pads and truncates for a 1 to 9999 value
     * Note: payId can be 0 (at oid 10000, 20000, etc.)
    */
    function filterPayId($s) {
        if (preg_match('/^\d{4}$/', $s) !== 1) return false;
        $s = substr(str_pad($s, 4, STR_PAD_LEFT), -4);
        return $s;
    }
    
    
    
    /*
     * 
     */
    function filterValue($value) {
        $value = str_replace(',', '.', $value);
        $i = strrpos($value, '.');
        $value = substr($value, 0, $i) . '%%sys_separator%%' . substr($value, $i+1);
        $value = str_replace(array('.', '€', '&euro;', ' ', '$'), '', $value);
        $value = str_replace('%%sys_separator%%', '.', $value);
        return $value;
    }

}

?>