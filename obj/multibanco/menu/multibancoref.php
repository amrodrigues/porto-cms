<?php

class multibancoref {

    var $group = "payments";
    var $descr = "Multibanco";
    var $show = true;
    var $public = false;

    function exec() {

        global $ob, $conf;

$ob->debugMode(true);

        // --- present form:

        if (empty($_POST['val']) || empty($_POST['descr']))
            return '<h2>Generate Multibanco Payment Reference</h2>' . $this->form();

        // --- process form:
        
        require_once($conf->root . '/obj/multibanco/multibanco.php');
        $om = new multibanco();
        
        $val = $om->filterValue($_POST['val']);
        $descr = trim($_POST['descr']);

        if (!is_numeric($val)) return '<div class="error">multibancoref: value is not numeric</div>';

        $ref = $om->getNewPayReference($descr, $val);

        if ($ref !== false) {
            
            $aEntity = $om->getEntityId();
            
            $s = '<h2>Your payment reference for "' . $descr . '":</h2>';
            $s .= '<div style="border: 1px solid #999;padding:30px">';
            $s .= 'Entidade: <b>' . $aEntity['entityid'] . '</b><br><br>';
            $s .= 'Referência: <b>' . substr($ref, 0, 3) . ' ' . substr($ref, 3, 3) . ' ' . substr($ref, 6) . '</b><br><br>';
            $s .= 'Valor: <b>' . $val . '</b></div>';
        } else {

            $s = '<div class="error">There was an error generating your reference</div>';
        }

        return $s;
    }



/*
 * 
 */

function form() {

    return '
    <form method="post" enctype="multipart/form-data" action="/do/?m=multibanco.multibancoref">

     <label>Description</label><br>
     <input class="form-text" type="text" name="descr"><br> <br>
     
     <label>Value</label><br>
     <input class="form-text" type="text" name="val"><br> <br>
     
     <label>Send payment reference to email address: <b>[optional]</b></label><br>
     <input class="form-text" type="text" name="em"><br> <br>
     
     <input type="submit" value="Generate...">
     
    </form>';
}


}
?>