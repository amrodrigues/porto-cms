<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'multibanco',
        'easyname' => 'Multibanco',
        'cache' => true
      ),
      'content' => array(
        'entityid' => array(
          'name' => 'entityid',
          'type' => 'integer',
          'desc' => 'Entity ID',
          'show' => true
        ),
        'subentityid' => array(
          'name' => 'subentityid',
          'type' => 'integer',
          'desc' => 'Sub-entity ID',
          'show' => true
        ),
        'antiphishingkey' => array(
          'name' => 'antiphishingkey',
          'type' => 'textline',
          'validate' => '/[\dabcdefghijklmnopqrstuvwxyz]{50}/i',
          'desc' => 'Anti-phishing key (letters and numbers)',
          'show' => true
        )
      )
    );
?>