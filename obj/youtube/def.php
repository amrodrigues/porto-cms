<?php

$a_def = array(
    'classes' => false,
    'br' => array(
	'class' => 'youtube',
	'easyname' => 'Youtube video',
	'cache' => true
    ),
    'content' => array(
	'url' => array(
	    'name' => 'url',
	    'type' => 'textline',
	    'desc' => 'URL of video page',
	    'optional' => true
	),
	'align' => array(
	    'name' => 'align',
	    'type' => 'fradio',
	    'desc' => 'Align',
	    'optional' => 'yes',
	    'xselect' => array(
		1 => 'Left',
		2 => 'Right',
		3 => 'Center'
	    )
	),
	'width' => array(
	    'name' => 'width',
	    'type' => 'integer',
	    'optional' => 'no',
	    'desc' => 'Max. width',
	    'show' => true
	),
	'height' => array(
	    'name' => 'height',
	    'type' => 'integer',
	    'optional' => 'no',
	    'desc' => 'Max. height',
	    'show' => true
	),
	'autoplay' => array(
	    'name' => 'autoplay',
	    'type' => 'bool',
	    'desc' => 'Autoplay',
	    'optional' => true
	)
    )
);
?>