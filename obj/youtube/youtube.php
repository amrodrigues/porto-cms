<?php

/*
 * Serves a youtube video
 */
class youtube {

    function asHtml($ud) {

	$lay = '';

	$w = $ud['content']['width']['value'];
	$h = $ud['content']['height']['value'];

	if (empty($ud['content']['url']['value']))
	    return 'Please specify the URL of the youtube video page.';

	$url = str_replace('watch?v=', 'v/', $ud['content']['url']['value']);

	$lay = '
	<object width="' . $w . '" height="' . $h . '">
	<param name="movie" value="' . $url . '&hl=en&fs=1&"></param>
	<param name="allowFullScreen" value="true"></param>
	<param name="wmode" value="transparent"></param>
	<param name="allowscriptaccess" value="always"></param>
	<embed src="' . $url . '&hl=en&fs=1&" type="application/x-shockwave-flash" wmode="transparent" allowscriptaccess="always" allowfullscreen="true" width="' . $w . '" height="' . $h . '"></embed></object>
	';

	if ($ud['content']['align']['value'] > 0) {
	    switch ($ud['content']['align']['value']) {
		case '1':
		    $lay = '<div class="youtube-left">' . $lay . '</div>';
		    break;
		case '2':
		    $lay = '<div class="youtube-right">' . $lay . '</div>';
		    break;
		case '3':
		    $lay = '<div class="youtube-center">' . $lay . '</div>';
		    break;
	    }
	} else {

	    $lay = '<div class="youtube">' . $lay . '</div>';
	}


	return $lay;
    }




}

?>