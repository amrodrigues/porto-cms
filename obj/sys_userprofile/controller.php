<?php

class sys_userprofile {

    var $a_data;

    function sys_userprofile($p_db = null) {
        global $db;

        $s_sql = 'select oid,title from sys_userprofile order by (oid=1), title';

        $this->a_data = $db->select_keyvalue($s_sql);
    }

    function xhtml_select($s_element_name = "rlevel", $key_selected = 0) {
        $s = '<select name="' . $s_element_name . '">';

        foreach ($this->a_data as $k => $v) {
            $s .= '<option value="' . $k . '"';

            if ($key_selected == $k)
                $s .= ' selected="selected"';

            $s .= '>' . $v . '</option>';
        }

        $s .= '</select>';

        return $s;
    }

    function value($key) {
        if (!empty($this->a_data[$key]))
            return $this->a_data[$key];

        return '';
    }

}
