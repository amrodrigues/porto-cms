<?php

    /*
     * User profiles
     * Define access to all resources
     */

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'sys_userprofile',
        'easyname' => 'User profiles',
        'cache' => false
      ),
      'content' => array(
        'title' => array(
          'name' => 'title',
          'type' => 'textline',
          'optional' => false,
          'desc' => 'Profile name',
          'show' => true
        ),
        'perm' => array(
          'name' => 'perm',
          'type' => 'code',
          'optional' => true,
          'desc' => 'Permissions'
        ),
        'homeurl' => array(
          'name' => 'homeurl',
          'type' => 'textline',
          'desc' => 'After login jumps to URL...'
        ),
        'startobject' => array(
          'name' => 'startobject',
          'type' => 'integer',
          'desc' => 'Start object id'
        ),
        'logouturl' => array(
          'name' => 'logouturl',
          'type' => 'textline',
          'desc' => 'After logout jumps to URL...'
        ),
        'debugmode' => array(
          'name' => 'debugmode',
          'type' => 'bool',
          'desc' => 'Debugging mode',
          'optional' => true,
          'default' => '0'
        )
      )
    );
?>