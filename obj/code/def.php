<?php

$a_def = array(

	'classes' => '*',

	'br' => array(
		'class' => 'code',
		'easyname' => 'Code',
          'cache' => true
	),
	'content' => array(
		'code' => array(
                    'name' => 'code',
                    'type' => 'code',
                    'desc' => 'Client side code',
                    'optional' => false
		)
	)

);


?>