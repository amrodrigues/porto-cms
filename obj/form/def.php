<?php

$a_def = array(
    'classes' => false,
    'br' => array(
        'class' => 'form',
        'easyname' => 'Form',
        'cache' => true
    ),
    'content' => array(
        'dtoid' => array(
            'name' => 'dtoid',
            'type' => 'dbselect',
            'desc' => 'Save to data table',
            'xsql' => 'select oid, easyname from broker where class="datatable" ' . $GLOBALS['oAuth']->sqlRestrict('view') . ' order by easyname'
        ),
        'submitcaption' => array(
            'name' => 'submitcaption',
            'type' => 'smalltext',
            'desc' => 'Send button text',
            'optional' => true,
            'default' => 'OK'
        ),
        'reclist' => array(
            'name' => 'reclist',
            'type' => 'textline',
            'desc' => 'Send to the following email address(es)',
            'optional' => true
        ),
        'oktitle' => array(
            'name' => 'oktitle',
            'type' => 'textline',
            'desc' => 'Sent title',
            'optional' => true
        ),
        'oktxt' => array(
            'name' => 'oktxt',
            'type' => 'textline',
            'desc' => 'Sent message',
            'optional' => true
        ),
        'errortitle' => array(
            'name' => 'errortitle',
            'type' => 'textline',
            'desc' => 'Error title',
            'optional' => true
        ),
        'errortxt' => array(
            'name' => 'errortxt',
            'type' => 'textline',
            'desc' => 'Error text',
            'optional' => true
        ),
        'notsenttitle' => array(
            'name' => 'notsenttitle',
            'type' => 'textline',
            'desc' => 'Not sent title',
            'optional' => true
        ),
        'notsenttxt' => array(
            'name' => 'notsenttxt',
            'type' => 'textline',
            'desc' => 'Not sent message',
            'optional' => true
        ),
        'html' => array(
            'name' => 'html',
            'type' => 'html',
            'desc' => 'Custom HTML for this form',
            'optional' => true
        )
    )
);
