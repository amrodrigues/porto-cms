<?php

/*
 * Displays a standard form
 * Customizable by copying/pasting generated html in a code object
 */

class form {
    /*
     * Present a standard generated form or a customized version
     */

    function asHtml($ud) {

        global $conf;

        // at this point, if there's a $_POST, it may not be from the form we are trying to render
        // This happens if a page has multiple forms
        // So a form identified by oid hidden parameter.

        if (!empty($_POST['oid'])) {

            $oid = (int) $_POST['oid'];
            if ($oid == $ud['br']['oid'])
                $this->save($ud);
            else
                return '';
        }

        $formId = 'i' . $ud['br']['oid'];

        if (!empty($ud['content']['html']['value'])) {

            // --- custom form ----------------------

            require_once($conf->root . '/editor.php');
            editor::setAjaxHeaders($formId);

            $customform = str_replace('<form ', '<form id="' . $formId . '" ', $ud['content']['html']['value']);

            return str_replace('</form>', '
                <input type="hidden" name="oid" value="' . $ud['br']['oid'] . '">
            </form>
            <div id="i' . $ud['br']['oid'] . '-result" class="alert">', $customform
            );
        } else {

            // --- generated form --------------------

            require_once($conf->root . '/editor.php');

            // A form registers a datatable oid
            $datatableOid = $ud['content']['dtoid']['value'];

            require_once($conf->root . '/obj/datatable/datatable.php');
            $odt = new datatable();

            // A datatable is associated with an entity oid. This entity gives structure to a datatable db table
            // Entities keep properties, allowing to create db columns. Properties are kept in sys_property table

            $entityOid = $odt->entityOid($datatableOid);
            $aProperties = $odt->getProperties($entityOid);

            // editor generates a form:

            $ed = new editor();

            $ed->setSubmitCaption($ud['content']['submitcaption']['value']);
            $ed->setAction('/' . $ud['br']['oid']);
            $ed->setFormType('meta');
            $ed->setStoreOid($ud['content']['dtoid']);
            $ed->setDatatableOid($datatableOid);
            $ed->setFormOid($ud['br']['oid']);
            $ed->setAjax(true);
            $ed->setTopButtons(false);

            // properties from the entity associated to datatable are used to recreate a $ud
            // $ud is then passed to give structure to the HTML form

            $ud_fo = $odt->createSkel($aProperties, 'datatable' . $ud['br']['oid']);
            $ud_fo['br']['oid'] = $ud['br']['oid'];

            return $ed->asHtml($ud_fo);
        }
    }

    /*
     * save HTTP post to datatable associated with form
     */

    function save($ud_form) {

        global $ob, $conf;

        $oid = $ud_form['br']['oid'];  //$ob->param('oid', 'digits', false);  // oid is form oid
        if (!$oid) {
            throw new Exception('no oid');
        }
        $dt_oid = $ud_form['content']['dtoid']['value'];
        if (!$dt_oid) {
            throw new Exception('no dtoid');
        }

        // --- check if database table is OK
        require_once($GLOBALS['conf']->root . '/obj/datatable/datatable.php');
        $odt = new datatable();
        $dt_ud = $odt->checkIntegrity($dt_oid);

        // --- insert post data into table

        if (empty($dt_ud['content'])) {
            throw new Exception('no ud content');
        }

        // --- test for junk
        require_once($conf->root . '/junkfilter.php');
        $ojf = new junkfilter();
        foreach ($_POST as $v) {
            if ($ojf->scores($v) !== false || $ojf->isUpperLowercaseGibberish($v) === true) {
                $this->jsonResponse(0, $ud_form['content']['errortitle']['value'], $ud_form['content']['errortxt']['value']);
            }
        }

        $res = $ob->insertData($dt_ud, $_POST); // post is already filtered
        $newOid = 0;

        if ($res === true) { // data is valid
            // collect:
            $data = array();

            foreach ($dt_ud['content'] as $k => $meta) {
                $data[$k] = $meta['value'];
            }
            $newOid = $ob->dbInsert($odt->tableName($dt_oid), $data);
            $ob->log('form: new datatable oid is ' . $newOid);

            // --- send by email
            require_once($conf->root . '/obj/email/simplemailer.php');
            $ob->log('form: sending email(s)...');
            $o_email = new simplemailer();

            // extract easyname from referer
            $ref = $_SERVER['HTTP_REFERER'];
            $matches = null;
            preg_match("/\/([a-zA-Z0-9\-]+)$/", $ref, $matches);

            if (!empty($matches[1])) {

                $urlpart = $matches[1];

                if (!is_numeric($urlpart)) {
                    $aBr = $ob->dbSelectRow('select oid, easyname from broker where easyname=replace("' . $urlpart . '",\'-\',\' \') order by oid limit 1');
                } else {
                    $aBr = $ob->dbSelectRow('select oid, easyname from broker where oid=' . (int) $urlpart);
                }
            }

            if (!empty($aBr)) {
                $reasyname = $aBr['easyname'];
                $roid = $aBr['oid'];
            } else {
                $reasyname = $roid = 'Unknown';
            }

            $addRef = '<br><p>Message posted at: <a href="' . $ref . '">' . $reasyname . ' (id ' . $roid . ')</a></p>';

            if (!empty($ud_form['content']['reclist']['value'])) {
                $sendResult = $o_email->sendUd($ud_form['content']['reclist']['value'], $ud_form['br']['easyname'] . ': ' . $reasyname, $dt_ud, $addRef);
            } else {
                $sendResult = true;
            }
        }

        // --- respond

        if ($newOid > 0) { // valid data:
            if ($sendResult === true) { // was sent or not to send
                $this->jsonResponse(1, $ud_form['content']['oktitle']['value'], $ud_form['content']['oktxt']['value']);
            } else { // was not sent:
                $this->jsonResponse(0, $ud_form['content']['notsenttitle']['value'], $ud_form['content']['notsenttxt']['value']);
            }
        } else { // data not valid:
            $this->jsonResponse(0, $ud_form['content']['errortitle']['value'], $ud_form['content']['errortxt']['value'] . ' ' . $res);
        }

        die();
    }

    /*
     * respond to user submit
     */

    function jsonResponse($code, $title, $txt) {
        $js = json_encode(
                array('code' => $code,
                    'title' => $title,
                    'txt' => $txt
                )
        );
        print $js;
        die();
    }

}
