<?php

class product {

    function asHtml($ud) {

        $lay = '<div class="product-wrap">';
        $lay .= '<h1>' . $ud['content']['title']['value'] . '</h1>';
        $lay .= '{[1]}';
        
        if (!empty($ud['content']['descr']['value'])) {
            $lay .= '<div class="product-descr">' . $ud['content']['descr']['value'] . '</div>';
        }
        
        // Consumer price + tax percent:
        
        $cprice = (float) $ud['content']['cprice']['value'];
        $finalPrice = $cprice;
        
        if (!empty($ud['content']['pctax']['value'])) {
            $ratioTax = number_format($ud['content']['pctax']['value'] / 100, 2);
            $finalPrice = number_format($cprice + ($cprice * $ratioTax), 2);
        }
        
        if (!empty($ud['content']['cprice']['value']))
            $lay .= '<div class="product-price">' . $finalPrice . '&euro;</div>';

        if (!empty($ud['content']['specs']['value'])) {
            $lay .= '<div class="product-specs">' . $ud['content']['specs']['value'] . '</div>';
        }
        
        $lay .= '</div>';

        return $lay;
    }

}

?>