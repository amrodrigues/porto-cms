<?php

$a_def = array(
    'classes' => array('image' => '*', 'xfile' => '*', 'youtube' => 2, 'flexslider' => '*'),
    'br' => array(
        'class' => 'product',
        'easyname' => 'Product',
        'cache' => true
    ),
    'content' => array(
        'stock' => array(
            'name' => 'stock',
            'type' => 'integer',
            'desc' => 'Stock available',
            'optional' => true,
            'default' => '0'
        ),
        'title' => array(
            'name' => 'title',
            'type' => 'textline',
            'desc' => 'Title',
            'show' => true
        ),
        'mref' => array(
            'name' => 'mref',
            'type' => 'textline',
            'desc' => 'Supplier ref.',
            'optional' => true,
            'show' => true
        ),
        'iref' => array(
            'name' => 'iref',
            'type' => 'textline',
            'desc' => 'Internal ref.',
            'optional' => true,
            'show' => true
        ),
        'descr' => array(
            'name' => 'descr',
            'type' => 'richtext',
            'desc' => 'Brief marketing description',
            'optional' => true
        ),
        'specs' => array(
            'name' => 'specs',
            'type' => 'richtext',
            'desc' => 'Technical specs.',
            'optional' => true
        ),
        'cprice' => array(
            'name' => 'cprice',
            'type' => 'float',
            'desc' => 'Consumer price without tax',
            'optional' => true
        ),
        'pctax' => array(
            'name' => 'pctax',
            'type' => 'float',
            'desc' => 'Tax %',
            'optional' => true
        ),
        'weight' => array(
            'name' => 'weight',
            'type' => 'float',
            'desc' => 'Weight',
            'optional' => true
        ),
        'length' => array(
            'name' => 'length',
            'type' => 'float',
            'desc' => 'Length',
            'optional' => true
        ),
        'width' => array(
            'name' => 'width',
            'type' => 'float',
            'desc' => 'Width',
            'optional' => true
        ),
        'height' => array(
            'name' => 'height',
            'type' => 'float',
            'desc' => 'Height',
            'optional' => true
        )
    )
);
?>