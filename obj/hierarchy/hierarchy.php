<?php

/*
 * Hierarchic content generator
 * Allows to set templates for items and sub-items
 * Merges hierarchical data from a SQL select query.
 */

class hierarchy {

    private $itempc = '';  // item template for items with descendants
    private $itempn = '';  // item template for items WITHOUT descendants
    private $sitemp = ''; // subitem template
    private $a = array(); // all items
    private $cnt = 0;     // $a count

    function asHtml($ud) {

        $this->a = $GLOBALS['ob']->dbSelect($ud['content']['xsql']['value']);
        if (empty($this->a))
            return '';

        $this->itempc = $ud['content']['itempc']['value'];
        $this->itempn = $ud['content']['itempn']['value'];
        $this->sitemp = $ud['content']['sitemp']['value'];

        $this->cnt = count($this->a);
        $rootId = $ud['content']['rootid']['value'];

        for ($f = 0; $f < $this->cnt; $f++) {
            // --- find root data, get child and render each child:
            if ($this->a[$f]['oid'] == $rootId) {
                $ac = ''; // accumulate each item html
                $aCh = $this->child($rootId);
                if (!empty($aCh)) {
                    foreach ($aCh as $aChItem) {
                        $ac .= $this->get($aChItem, $this->child($aChItem['oid'])) . "\n";
                    }
                    $final = str_replace('{[1]}', $ac, $ud['content']['ttemp']['value']);
                    return $final;
                }
            }
        }

        return 'hierarchy: your SQL select must include top id. Item count is ' . $this->cnt;
    }

    /*
     * merges an item with the item template
     * merges sub-items with the sub-item template
     */

    function get($a, $aChild) { //a = current row
        // --- get subitems
        $subitems = '';

        if (count($aChild) > 0) {

            $item = $this->itempc;
            $sa = '';
            foreach ($aChild as $aCh) {
                $sa .= $this->get($aCh, $this->child($aCh['oid']));
            }

            if (strlen($sa) > 0)
                $subitems = str_replace('{[1]}', $sa, $this->sitemp) . "\n"; // insert items in subitem template
        } else {
            $item = $this->itempn; // template for items without descendants
        }

        // --- merge data

        if (strpos($item, '{{') !== false) {
            foreach ($a as $k => $v)
                $item = str_replace('{{' . $k . '}}', $v, $item);
        }

        return str_replace('{[1]}', $subitems, $item) . "\n"; // insert subitems in item template
    }

    /*
     * Find item's descendants
     */

    function child($oid) {
        $a = array();
        for ($f = 0; $f < $this->cnt; $f++) {
            if ($this->a[$f]['pid'] == $oid) {
                $a[] = $this->a[$f];
            }
        }
        return $a;
    }

}

?>