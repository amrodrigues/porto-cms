<?php

$a_def = array(
    
    'classes' => array('*'),
    
    'br' => array(
	'class' => 'hierarchy',
	'easyname' => 'Hierarchic Content'
    ),
    'content' => array(
        'rootid' => array(
	    'name' => 'rootid',
	    'type' => 'integer',
	    'desc' => 'Root id',
            'optional' => true
	),
        'xsql' => array(
	    'name' => 'xsql',
	    'type' => 'code',
	    'desc' => 'SQL Select, having "oid" and "pid" (parent id) columns',
            'optional' => true
	),
        'ttemp' => array(
	    'name' => 'ttemp',
	    'type' => 'code',
	    'desc' => 'Top template (use {[1]} as item placeholder)',
            'optional' => true
	),
        'itempc' => array(
	    'name' => 'itempc',
	    'type' => 'code',
	    'desc' => 'Item template when there are sub-items (use {[1]} as sub-item placeholder)',
            'optional' => true
	),
        'itempn' => array(
	    'name' => 'itempn',
	    'type' => 'code',
	    'desc' => 'Item template when there\'s NO sub-items',
            'optional' => true
	),
        'sitemp' => array(
	    'name' => 'sitemp',
	    'type' => 'code',
	    'desc' => 'Sub-item template (wraps sub-items)',
            'optional' => true
	)
    )
);
?>