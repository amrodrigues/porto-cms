<?php

$a_def = array(
    'classes' => array("image" => 80, 'text' => 80),
    'br' => array(
        'class' => 'cycle',
        'easyname' => 'Content Cycle'
    ),
    'content' => array(
        'w' => array(
            'name' => 'w',
            'type' => 'integer',
            'desc' => 'Width (pixels)'
        ),
        'h' => array(
            'name' => 'h',
            'type' => 'integer',
            'desc' => 'Height (pixels)'
        ),
        'fx' => array(
            'name' => 'fx',
            'type' => 'textselect',
            'desc' => 'Effect',
            'select' => array(
                'blindY' => 'blindY',
                'blindZ' => 'blindZ',
                'cover' => 'cover',
                'curtainX' => 'curtainX',
                'curtainY' => 'curtainY',
                'fade' => 'fade',
                'fadeZoom' => 'fadeZoom',
                'growX' => 'growX',
                'growY' => 'growY',
                'scrollUp' => 'scrollUp',
                'scrollDown' => 'scrollDown',
                'scrollLeft' => 'scrollLeft',
                'scrollRight' => 'scrollRight',
                'scrollHorz' => 'scrollHorz',
                'scrollVert' => 'scrollVert',
                'shuffle' => 'shuffle',
                'slideX' => 'slideX',
                'slideY' => 'slideY',
                'toss' => 'toss',
                'turnUp' => 'turnUp',
                'turnDown' => 'turnDown',
                'turnLeft' => 'turnLeft',
                'turnRight' => 'turnRight',
                'uncover' => 'uncover',
                'wipe' => 'wipe',
                'zoom' => 'zoom'
            )
        )
    )
);
?>