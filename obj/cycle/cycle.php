<?php

class cycle {

    function asHtml($ud) {

	global $ob;

        $aCh = $ob->children($ud['br']['oid']);
        if (empty($aCh)) return '';
        
        $ob->includeJquery();
        $ob->includeScriptTag('/obj/cycle/jquery.cycle.all.js');

        
	$lay = '<div class="cycle cycle' . $ud['br']['oid'] . '">';
            foreach ($aCh as $a) $lay .= $ob->getHtml($a['oid']);
        $lay .= '</div>';
        
        $lay .= '<script type="text/javascript">
            $(".cycle' . $ud['br']['oid'] . '").cycle({ 
                fx:    \'' . $ud['content']['fx']['value'] . '\', 
                sync:  false, 
                delay: -2000 
            });          
            </script>
        ';
        
	return $lay;
    }

}

?>