<?php

$a_def = array(
    'classes' => array('image' => '*'),
    'br' => array(
	'class' => 'album',
	'easyname' => 'Album',
	'cache' => true
    ),
    'content' => array(

	'label' => array(
	    'name' => 'label',
	    'type' => 'bool',
	    'desc' => 'Use image title as label'
	),
	'beh' => array(
	    'name' => 'beh',
	    'type' => 'fradio',
	    'desc' => 'Behavior',
	    'optional' => true,
	    'xselect' => array(
		1 => 'Use image links',
		2 => 'Zoom images'
	    )
	)
    )
);
?>