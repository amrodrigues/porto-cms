<?php

class album {

    function asHtml($ud) {

	global $ob, $conf;

	$lay = '<div class="album">';

	$aImages = $ob->childrenOfClass('image', $ud ['br']['oid']);

	if (empty($conf->behavior['album.width'])) $w = 990;
	else $w = $conf->behavior['album.width'];

	$tw = number_format (($w / 3) - 10);
	$th = number_format(($tw / 16) * 9);

	$uselinks = false;
	$zoom = false;

	if ($ud['content']['beh']['value'] == 1)
	    $uselinks = true;
	else
	    $zoom = true;

	if ($zoom) {
	    $ob->includeJquery();
	    $ob->includeNyromodal();
	}

	require_once($conf->root . '/thumb.php');
	$oth = new thumb();
	
	foreach ($aImages as $a) {

	    $lay .= '<div class="album-cell">';
	    
	    $thumb_href = $oth->thumbUrl($a['oid'], $a['userfile'], $tw, $th); //thumbUrl checks if thumb exists in fs
	    $regular_href = $ob->objectGetFilename($a['oid'], $a['userfile']);
	    $f = true;
	    
	    if ($zoom) {
		$lay .= '<a ';
		if ($f) { $lay .= 'id="imgFiche" '; $f = false; }
		$lay .= 'href="' . $regular_href . '"  class="nyroModal" rel="gal">';
	    }
	    elseif (!empty($ud['content']['linkto']['value']))
		$lay .= '<a href="' . $ud['content']['linkto']['value'] . '">';

	    $lay .= '<img class="album-img" alt="' . $a['alt'] . '" title="' . $a['title'] . '" src="' . $thumb_href . '">';

	    if ($zoom || $uselinks)
		$lay .= '</a>';

	    if ($ud['content']['label']['value'])
		$lay .= '<div class="album-label">' . $a['title'] . '</div>';

	    $lay .= '</div>';
	    

	}
	
	$lay .= '<script type="text/javascript">$(function() { $(".nyroModal").nyroModal();});</script>';
	
	
	
	return $lay . '</div>';
	    
	   
    }

}
?>