<?php

class news {

    function asHtml($ud) {

	global $ob;


	if (!empty($_GET['op']))
	    $showAll = true;
	else
	    $showAll = false;


	$lay = '<div class="news">';



	// --- news header with titles

	$lay .= '<div class="news-head">';

	if (!empty($ud['content']['pretitle']['value']))
	    $lay .= '<div class="news-pretitle">' . $ud['content']['pretitle']['value'] . '</div>';

	$lay .= '<h1>' . $ud['content']['title']['value'] . '</h1>';

	if (!empty($ud['content']['subtitle']['value']))
	    $lay .= '<h2>' . $ud['content']['subtitle']['value'] . '</h2>';

	$lay .= '<div class="news-date">' . $ud['content']['pdate']['value'] . '</div>';

	$lay .= '</div>';


	// --- lead

	$lay .= '<div class="news-lead">';


	// --- children: process folder

	$aChild = $ob->children($ud['br']['oid']);

	$afterLead = '';

	if (!empty($aChild))
	    foreach ($aChild as $a) {
	    
		// --- images go top:

		if ($a['class'] == 'image' ||  $a['class'] == 'album') {
		    if ($a['laypos'] == 1)
			$lay .= $ob->getHtml($a['oid']);

		    // --- text parts go after lead text
		} elseif ($a['class'] == 'text') {

		    $afterLead .= $ob->getHtml($a['oid']);

		    // --- folder contents is listed below lead + text parts:
		    
		} elseif ($a['class'] == 'folder') {

		    $aList = $ob->children($a['oid']);

		    $i = 0;

		    $linkList = '';

		    foreach ($aList as $aa) {
			if ($showAll) {
			    $afterLead .= $ob->getHtml($aa['oid']);
			} else {

			    $linkList .= '<div class="news-attach-item"><a href="/index.php?oid=' . $aa['oid'] . '">' . $aa['easyname'] . '</a></div>';

			    $i++;
			}
		    }

		    if ($i > 0)
			$afterLead .= '<div class="news-attach">' . $linkList . '</div>';
		}
	    }

	if (!empty($ud['content']['txt']['value']))
	    $lay .= $ud['content']['txt']['value'];

	$lay .= '</div>';

	$lay .= $afterLead . '</div>';


	return $lay;
    }

}

?>
