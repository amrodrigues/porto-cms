<?php

$a_def = array(
    'classes' => array('image' => 1, 'text' => '*'),
    'br' => array(
        'class' => 'news',
        'easyname' => 'News',
        'cache' => true
    ),
    'content' => array(
        'title' => array(
            'name' => 'title',
            'type' => 'textline',
            'desc' => 'Title',
            'show' => true
        ),
        'subtitle' => array(
            'name' => 'subtitle',
            'type' => 'textline',
            'optional' => true,
            'desc' => 'Subtitle'
        ),
        'pdate' => array(
            'name' => 'pdate',
            'type' => 'date',
            'optional' => true,
            'desc' => 'Publish date',
            'default' => date('Y-m-d')
        ),
        'txt' => array(
            'name' => 'txt',
            'type' => 'richtext',
            'desc' => 'Lead (for article sections, use text objects inside)'
        )
    )
);
