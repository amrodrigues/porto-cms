<?php

$a_def = array(
    'classes' => array('image' => 1, 'imageslider' => 1, 'album' => 1, 'box' => 1),
    'br' => array(
        'class' => 'text',
        'easyname' => 'Text',
        'cache' => true
    ),
    'content' => array(
        'hx' => array(
            'name' => 'hx',
            'type' => 'fselect',
            'desc' => 'Heading',
            'xselect' => array(
                1 => 'h1',
                2 => 'h2',
                3 => 'h3',
                4 => 'h4',
                5 => 'h5',
                6 => 'h6'
            )
        ),
        'title' => array(
            'name' => 'title',
            'type' => 'textline',
            'desc' => 'Title',
            'optional' => true,
            'show' => true
        ),
        'txt' => array(
            'name' => 'txt',
            'type' => 'richtext',
            'desc' => 'Text',
            'optional' => true
        )
    )
);
