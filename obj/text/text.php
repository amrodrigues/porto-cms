<?php

/*
 * Text has an inner div (text-content) that enables easy alignment of child objects having its own divs
 */

class text {

    function asHtml($ud) {

        $lay = '<div class="text">{[1]}<div class="text-content">';

        if (!empty($ud['content']['title']['value'])) {

            $h = $ud['content']['hx']['value'];
            $lay .= '<h' . $h . '>' . $ud['content']['title']['value'] . '</h' . $h . '>';
        }

        $lay .= $ud['content']['txt']['value'] . '</div>{[2]}</div>';

        return $lay;
    }

}
