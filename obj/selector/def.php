<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'selector',
        'easyname' => 'Object selector',
        'cache' => false
      ),
      'content' => array(
        'sqls' => array(
          'name' => 'sqls',
          'type' => 'code',
          'desc' => 'SQL select expression having an "oid" list'
        )
      )
    );
?>