<?php
	
class random
{
	
	function asHtml($ud)
	{	
		global $ob;
		
		$lay = '';
		
		$id = $ud['br']['oid'];
	
		$a_random_ids = array();
		
		$n = $ud['content']['n']['value'];
	
	
		if ($ud['content']['persist']['value'] && !empty($_SESSION['random'][$id]))
		{
			
			if ($n != count($_SESSION['random'][$id]))
			{
				$a_to_randomize = $ob->children_ids($id);
				
				if ($n > 0) $a = array_rand($a_to_randomize, $n);
				
				else $a = array_rand($a_to_randomize);
				
				if (!is_array($a)) $a = array($a);
	
				foreach ($a as $k) $a_random_ids[] = $a_to_randomize[$k];
				
			} else {
				
				$a_random_ids = $_SESSION['random'][$id];
			}
			
		} else {
			
			$a_to_randomize = $ob->children_ids($id);
			
			if ($n > 0) $a = array_rand($a_to_randomize, $n);
			
			else $a = array_rand($a_to_randomize);
			
			if (!is_array($a)) $a = array($a);
			
			foreach ($a as $k) $a_random_ids[] = $a_to_randomize[$k];
		}
		
		foreach ($a_random_ids as $id) $lay .=  $ob->get_html($id);
		
		return $lay;
	}
}
?>