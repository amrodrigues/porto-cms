<?php

    $a_def = array(
      'classes' => '*',
      'br' => array(
        'class' => 'random',
        'easyname' => 'Random content',
        'cache' => false
      ),
      'content' => array(
        'n' => array(
          'name' => 'n',
          'type' => 'integer',
          'optional' => 'no',
          'desc' => 'Number of objects serving at the same time',
          'show' => true
        ),
        'persist' => array(
          'name' => 'persist',
          'type' => 'bool',
          'optional' => 'yes',
          'desc' => 'Keep the same throughout the session',
          'show' => true
        )
      )
    );
?>