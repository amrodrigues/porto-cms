<?php

/*
 * A behavior object is placed inside layouts or folders to modify objects behavior.
 * Objects will test $conf->behavior for keys and values to use, deciding how to react.
 * A behavior object keeps just a key-value list encoded in JSON
 */
class behavior {

    function asHtml($ud) {

        $GLOBALS['conf']->behavior = json_decode($ud['content']['jsondata']['value'], true);
    }

}

?>