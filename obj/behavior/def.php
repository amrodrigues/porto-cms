<?php

    $a_def = array(
      'classes' => false,
      'br' => array(
        'class' => 'behavior',
        'easyname' => 'Behavior',
        'cache' => true
      ),
      'content' => array(
        'jsondata' => array(
          'name' => 'jsondata',
          'type' => 'code',
          'desc' => 'JSON { "key" : "value", "key" : "value", ... } data',
          'show' => true
        )
      )
    );
?>