<?php

class geochart {

    function asHtml($ud) {

        global $ob;
        $ob->includeScriptTag('https://www.google.com/jsapi');
        $data = $ob->dbSelect($ud['content']['xsql']['value']);
        if (empty($data)) {
            return 'No data available';
        }
        $scr = "
        <script type='text/javascript'>

                google.load('visualization', '1', {'packages': ['geochart']});
                google.setOnLoadCallback(drawMarkersMap);

                function drawMarkersMap() {
                    var data = google.visualization.arrayToDataTable([";

        $scr .= '["' . implode('","', array_keys($data[0])) . '"],';

        foreach ($data as $a) {
            $scr .= '[' . implode(',', $a) . '],';
        }
        $scr = substr($scr, 0, strlen($scr) - 1) . "
            ]);
            var options = {
            region: '";

        if (empty($ud['content']['region']['value'])) {
            $scr .= 'world';
        } else {
            $scr .= $ud['content']['region']['value'];
        }
        $scr .= "',
             displayMode: 'markers',
             colorAxis: {colors: ['#555555', '#FF0000']}
            };
            var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
            chart.draw(data, options);
            };

        </script>
        ";

        $ob->addBottom('geochart', $scr);

        return '
        <div id="chart_div"></div>
';
    }

}
