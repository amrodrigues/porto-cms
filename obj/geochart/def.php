<?php

$a_def = array(
    'classes' => false,
    'br' => array(
        'class' => 'geochart',
        'easyname' => 'GeoChart',
        'cache' => true
    ),
    'content' => array(
        'xsql' => array(
            'name' => 'xsql',
            'type' => 'code',
            'desc' => 'SQL select'
        ),
        'region' => array(
            'name' => 'region',
            'type' => 'textline',
            'desc' => 'Region (see Google Visualization/Geomap docs)',
            'optional' => true
        )
    )
);
