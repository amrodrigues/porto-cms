<?php

class status_save {


    /*
     * Name is optional (visitor or user name)
    */

    public function savepost() {

        global $ob, $oAuth;

        $boid = (int) $_GET['boid'];
        if (empty($boid)) die("no boid");

        $bud = $ob->get($boid);

        if (empty($_POST['txt']))
            $this->jsonResponse(0, $ud['content']['errortitle']['value'], $ud['content']['errorempty']['value']);

        $ob->log('status_save: savepost');


        if ($oAuth->userIsLoggedIn()) {

            $name = $oAuth->userName();
            $email = $oAuth->userEmail();
            $iduser = $oAuth->userId();

            $ob->log('status_save: user is logged in');

        } else {

            $name = $ob->postParam('name', 'any', 'Anonymous');
            $email = $ob->postParam('email', 'any', '');

            if (strpos($email, '@') === false)
                $this->jsonResponse(0, $bud['content']['errortitle']['value'], $bud['content']['erroremail']['value']);

            $iduser = 0;

            $ob->log('sys_status_save: anonymous user');
        }

        $rid = $ob->param('rid', 'digits', 0);

        $aData = array(
            'rid' => $rid,
            'xname' => $name,
            'iduser' => $iduser,
            'email' => $email,
            'txt' => $_POST['txt'],
            'ts' => date('Y-m-d H:i:s')
        );

        $newOid = $ob->dbInsert('sys_status', $aData);

        $ob->log('status_save: new is is ' . $newOid);

        if ($newOid > 0)
            $this->jsonResponse(1, $bud['content']['oktitle']['value'], $bud['content']['oktxt']['value']);
        else
            $this->jsonResponse(0, $ud['content']['errortitle']['value'], $bud['content']['errordb']['value']);
    }




    /*
     * respond status codes for modal
     */

    private function jsonResponse($code, $title, $txt) {

        $js = json_encode(
                array('code' => $code,
                    'title' => $title,
                    'txt' => $txt
                )
        );
        print $js;

        die();
    }

}

?>
