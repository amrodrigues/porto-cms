<?php

class status
{

    function asHtml($ud) {

        /* This is a behavior object
         * We won't display html,
         * just set vars for other objects
         */
        global $ob;

        $ob->registerBehavior($ud);
    }


    /*
     * return a status form
     */

    function statusForm($rid) {

        global $oAuth, $ob;

        $beh = $ob->behavior('status');
        if ($beh === false) return '';


        $cssId = 'statusform' . $rid;
        $isUser = $oAuth->userIsLoggedIn();
        $action = '/?go=status-save-savepost&boid=' . $beh['oid'] . '&rid=' . $rid;

        // --- create

        $s = '<div class="status-form">
            <form id="' . $cssId . '" method="post" action="/NotUsed" type="multipart/form-data">';

        if ($isUser === false) {

            $s .= '<label for="name" accesskey="n">' . $beh['labelname'] . '</label> <input name="name" type="text" id="status-name" placeholder="name" />
            <label for="email" accesskey="e">' . $beh['labelemail'] . '</label> <input type="text" name="email" id="status-email" placeholder="email" />';
        }

        $s .= '<label for="status" accesskey="s">' . $beh['labelstatus'] . '</label> <textarea name="txt" id="status-txt" placeholder="..." ></textarea>
	    <input type="submit" value="' . $beh['labelsubmit'] . '" />
            ';

        $s .= '</form>';

        $this->ajaxify($cssId, $action);

        $s .= '</div>';

        return $s;
    }




    /*
     * Controls form submit by jquery for spam
     */

    //TODO: merge with form equivalent method

    private function ajaxify($formCssId, $action) {

        global $ob;

        $ob->includeCss('/mod/reveal/reveal.css');
        $ob->includeScriptTag('/mod/reveal/jquery.reveal.js');

        $ob->includeScript($formCssId, '

            $(function(){

                $("#' . $formCssId . '").submit(

                     function(){

                        $.ajax({
                            type: "POST",
                            url: "' . $action . '",
                            data: $(this).serialize(),
                            success: function(resp) {
                                    $(\'<div class="reveal-modal" id="' . $formCssId . 'modal"><h1>\' + resp.title + "</h1><p>" + resp.txt + \'</p><a class="close-reveal-modal">&#215;</a></div></div>\').insertAfter(\'body\');
                                    $("#' . $formCssId . 'modal").reveal();
                                    if (resp.code == 1) $("#' . $formCssId . '").each(function(){ this.reset();});
                                },
                            dataType: "json"
                            });

                            return false;
                     }
                 );

                return false;
             });

         </script>
            ');

    }



    /*
     * Create a list of status items for a specific object
     */
    function statusList($oid) {

        global $ob;

        // --- data

        $sql = 'select xname, txt, ts from sys_status where sys_status.rid=' . $oid . ' order by oid';

        $a = $ob->dbSelect($sql);


        // --- list

        $s = '<div class="status-list">';

        if (!empty($a)) foreach ($a as $aa) {

                $s .= '
                    <div class="status-item">

                        <div class="status-name">' . $aa['xname'] . ' <br /><small>' . $aa['ts'] . '</small></div>

                        <div class="status-txt">' . $aa['txt'] . '</div>
                    </div>

                    ';
            }

        $s .= '</div>';

        // may return empty div for jquery to fill a new status

        return $s;
    }



}

?>