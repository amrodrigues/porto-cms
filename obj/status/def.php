<?php

$a_def = array(
    'classes' => '*',
    'br' => array(
        'class' => 'status',
        'easyname' => 'Status Behavior',
        'cache' => true
    ),
    'content' => array(
        'classes' => array(
            'name' => 'classes',
            'type' => 'fcheck',
            'desc' => 'Display status box on',
            'xselect' => array(1 => 'blogposts', 2 => 'news', 3 => 'events'),
            'optional' => true
        ),
        'restr' => array(
            'name' => 'restr',
            'type' => 'bool',
            'desc' => 'Restrict to registered users',
            'optional' => true
        ),
        'em' => array(
            'name' => 'em',
            'type' => 'bool',
            'desc' => 'Ask for email',
            'optional' => true
        ),
        'labellist' => array(
            'name' => 'labellist',
            'type' => 'textline',
            'desc' => 'Label for listing',
            'optional' => true
        ),
        'labelname' => array(
            'name' => 'labelname',
            'type' => 'textline',
            'desc' => 'Label for Name',
            'optional' => true
        ),
        'labelemail' => array(
            'name' => 'labelemail',
            'type' => 'textline',
            'desc' => 'Label for Email',
            'optional' => true
        ),
        'labelstatus' => array(
            'name' => 'labelstatus',
            'type' => 'textline',
            'desc' => 'Label for Status/Comment',
            'optional' => true
        ),
        'labelsubmit' => array(
            'name' => 'labelsubmit',
            'type' => 'textline',
            'desc' => 'Label for Submit button',
            'optional' => true
        ),
        'oktitle' => array(
            'name' => 'oktitle',
            'type' => 'textline',
            'desc' => 'Title for successful post',
            'optional' => true
        ),
        'oktxt' => array(
            'name' => 'oktxt',
            'type' => 'textline',
            'desc' => 'Message for successful post',
            'optional' => true
        ),
        'errortitle' => array(
            'name' => 'errortitle',
            'type' => 'textline',
            'desc' => 'Error title',
            'optional' => true
        ),
        'erroremail' => array(
            'name' => 'erroremail',
            'type' => 'textline',
            'desc' => 'Error message for email not found',
            'optional' => true
        ),
        'errorempty' => array(
            'name' => 'errorempty',
            'type' => 'textline',
            'desc' => 'Error message for empty post',
            'optional' => true
        ),
        'errordb' => array(
            'name' => 'errordb',
            'type' => 'textline',
            'desc' => 'Error message if database error',
            'optional' => true
        )
    )
);
?>