<?php

$a_def = array(
    'classes' => '*',
    'br' => array(
        'class' => 'layout',
        'easyname' => 'Layout',
        'cache' => true
    ),
    'content' => array(
        'framework' => array(
            'name' => 'framework',
            'type' => 'textline',
            'desc' => 'Use bootstrap (e.g. 3-6-3,8-4)',
            'optional' => true,
            'show' => true
        ),
        'template' => array(
            'name' => 'template',
            'type' => 'html',
            'optional' => true,
            'desc' => 'HTML template having {[1]}, {[2]}... placeholders for content insertion'
        ),
        'idcss' => array(
            'name' => 'idcss',
            'type' => 'dbselect',
            'optional' => true,
            'xsql' => 'select oid, easyname from broker where class="css" order by easyname',
            'desc' => 'Use CSS object'
        ),
        'mjquery' => array(
            'name' => 'mjquery',
            'type' => 'bool',
            'optional' => true,
            'desc' => 'Manual Jquery include'
        )
    )
);
