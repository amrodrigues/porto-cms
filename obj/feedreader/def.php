<?php

$a_def = array(
    
    'classes' => false,
    
    'br' => array(
	'class' => 'feedreader',
	'easyname' => 'Feedreader'
    ),
    'content' => array(
        'url' => array(
	    'name' => 'url',
	    'type' => 'textline',
	    'desc' => 'Atom or RSS feed URL',
            'optional' => true
	)
    )
);
?>