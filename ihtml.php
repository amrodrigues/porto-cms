<?php

class ihtml {
    /*
     *  Simple box with title, description and content
     */

    static function titleDescContentBox($title, $desc, $content) {
        return "<div class=\"titleDescContentBox-wrapper\"><div style=\"titleDescContentBox-intro\"><h1>$title</h1><p>$desc</p></div><div class=\"titleDescContentBox-content\">$content</div></div>";
    }

    /*
     * List of items to click and jump to a url
     *
     * $aaKeyValue is an array of arrays with at least two keys: an id, and a description/title/value
     *
     * Like: array( 0 => array('oid' => 123, 'name' => 'Peter'), 1 => ... etc)
     *
     * $hrefTemplate is the url template to click in each item
     *
     * Like '/do/m=broker.menuclass&rid={{oid}}', where {{oid}} is a property (key) of an $aaKeyValue array item
     *
     * $valuePropertyName is the array key with text to display as description (db column name)
     *
     */

    static function linkedList($aaKeyValue, $hrefTemplate, $valuePropertyName) {

        if (empty($aaKeyValue))
            return '';

        $s = '<ul class="linkedList-ul">';

        foreach ($aaKeyValue as $row) {
            $s .= ihtml::mergeProperties('<li><a href="' . $hrefTemplate . '">{{' . $valuePropertyName . '}}</a></li>', $row);
        }

        $s .= '</ul>';

        return $s;
    }

    /*
     * Replaces each {{PROPERTYNAME}} by it's value
     */

    static function mergeProperties($sTemplate, $aData) {
        foreach ($aData as $k => $v)
            $sTemplate = str_replace('{{' . $k . '}}', $v, $sTemplate);

        return $sTemplate;
    }

    /*
     * Create a rectangle to include buttons and other controls
     */

    static function toolbar($content) {
        return '<div class="toolbar-div">' . $content . '</div>';
    }

    /*
     * A button to click and jump to a url
     */

    static function button($sCaption, $href) {
        return '<input type="button" class="button-input" value="' . $sCaption . '" onclick="document.location=\'' . $href . '\';" />';
    }

    static function submitButton($buttonLabel) {
        return '<input type="submit" class="button-submit" value="' . $buttonLabel . '" />';
    }

    static function textLine($name, $value = '') {
        $s = '<input type="text" name="' . $name . '" class="textLine-text" ';
        if ($value != '')
            $s .= 'value="' . $value . '"';
        $s .= ' />';

        return $s;
    }

    static function textLineAndSubmitButton($textName, $buttonValue) {
        return '<div class="textLineAndSubmitButton-div">' . ihtml::textLine($textName) . ihtml::submitButton($buttonValue) . '</div>';
    }

    static function form($action, $content) {
        return '<form method="post" enctype="multipart/form-data" action="' . $action . '">' . $content . '</form>';
    }

    static function getIntegerParam($urlParameterName) {

        if (is_numeric($_GET[$urlParameterName])) {
            $v = (int) $_GET[$urlParameterName];

            return $v;
        }

        return false;
    }

    /*
     * Takes data of a form POST by key
     * Acts as a key/parameter filter to exclude data that otherwise might be sent to an sql building method in db class
     */

    static function postData($asKeys) {
        if (!is_array($asKeys)) {
            $asKeys = array($asKeys);
        }

        $aPostData = array();

        foreach ($_POST as $k => $v) {
            if (array_search($k, $asKeys) !== false) {
                $aPostData[$k] = $v;
            }
        }

        return $aPostData;
    }

    /*
     *
     */

    static function ieditor($menuclass, $rid) {

        // This script calls a data url and populates the empty table:

        $s = '
                    <div id="ieditor"> </div>
                    <script type="text/javascript" src="/mod/jquery/jquery.tablesorter.min.js"></script>
                    <script>
                        $("document").ready(function() {
                            top.left.ieditor.createTable("' . $menuclass . '",' . $rid . ');
                        });
                    </script>
            ';

        return $s;
    }

    /*
     *
     */

    static function backofficeHeader($print = true) {
        header('Content-Type: text/html; charset=UTF-8');

        $s = '<!DOCTYPE HTML>
                <html>
                <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <link rel="stylesheet" type="text/css" href="/do/style.css" />
                        <script type="text/javascript" src="/mod/jquery/jquery-1.8.3.min.js"></script>
                        <script type="text/javascript" src="/mod/jquery/jquery-ui-1.9.2.custom.min.js"></script>
                        <link type="text/css" href="/mod/jquery/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" />
                        <title>Backoffice</title>
                </head>
                <body>
                <div class="wrapper">
                ';

        if ($print)
            print $s;
        else
            return $s;
    }

    /*
     *
     */

    static function backofficeFooter($print = true) {
        $s = "</div></body></html>";

        if ($print) {
            print $s;
        } else {
            return $s;
        }
    }

    //TODO: old stuff - review:


    /*
     *
     */
    function linked_list($s_title, $a_data, $s_url_prefix, $s_bullet = '', $s_separator = '') {

        if (empty($a_data)) {
            return '';
        }
        $s = '<h1>' . $s_title . '</h1>';
        $a_keys = array_keys($a_data);
        $m_key = $a_keys[0];
        $a_keys2 = array_keys($a_data[$m_key]);
        $id_field = $a_keys2[0];

        foreach ($a_data as $a_row) {
            $s .= $s_bullet;
            $i_oid = $a_row[$id_field];
            unset($a_row['oid']);
            foreach ($a_row as $s_col) {
                $s .= '<a href="' . $s_url_prefix . $i_oid . '">' . $s_col . '</a> ' . $s_separator;
            }
            $s = substr($s, 0, strlen($s) - strlen($s_separator));
            $s .= '<br>';
        }

        $s .= "<br />\n";

        return $s;
    }

    function note($s_text) {
        return '<blockquote>' . $s_text . '</blockquote>';
    }

    function openform($s_action, $s_target, $s_method) {
        $s = '<form method="' . $s_method . '" ';

        if ($s_target)
            $s .= 'target="' . $s_target . '" ';

        $s .= 'enctype="multipart/form-data" action="' . $s_action . '">';

        return $s;
    }

    function closeform() {
        return '</form>' . "\n\n";
    }

    function spacer($i_lines = 1) {
        $s = '';

        for ($f = 0; $f < $i_lines; $f++)
            $s .= '<br />';

        return $s;
    }

    function hidden($s_name, $s_value) {
        return '<input type="hidden" name="' . $s_name . '" value="' . $s_value . '" />' . "\n";
    }

    function input_text($s_name, $s_css) {
        return '<input type="text" name="' . $s_name . '" style="' . $s_css . '" />' . "\n";
    }

    function file($s_element_name) {
        return '<input type="file" name="' . $s_element_name . '" />';
    }

    function submit($s_caption = ' OK ', $s_name = 'submit') {
        return '<input type="submit" name="' . $s_name . '" value="' . $s_caption . '" />';
    }

    function br($i = 1) {
        return str_repeat('<br />', $i) . "\n";
    }

    function title($s) {
        return '<h1>' . $s . '</h1>' . "\n";
    }

    function subtitle($s) {
        return '<h2>' . $s . '</h2>' . "\n";
    }

    function p($s) {
        return '<p>' . $s . '</p>' . "\n";
    }

    function table($a_data, $a_column_titles = array(), $a_links = array(), $b_noflines = false) {

        $i_cols = count($a_column_titles);

        $s = '<table class="table" cellspacing="0" cellpadding="0">' . "\n";

        if ($b_noflines)
            $s .= '<tr><td colspan="99" style="font-weight:bold">' . (count($a_data) - 1) . ' linhas.</td></tr>';

        if (is_array($a_column_titles)) {
            $s .= "\t<tr>";

            foreach ($a_column_titles as $s_col)
                $s .= "<th class=\"th\">$s_col</th>";

            $s .= "</tr>\n";
        }

        $r = 1;

        foreach ($a_data as $a_row) {
            $s .= "\t<tr>\n";

            $c = 1;

            foreach ($a_row as $k => $s_col) {
                if (!$s_col)
                    $s_col = '&nbsp;';

                $s .= '<td class="td">';

                if (!empty($a_links[$k])) {
                    $s .= ihtml::merge($a_links[$k], $a_row);
                } else {

                    $s .= $s_col;
                }

                $s .= '</td>';

                $c++;
            }

            $s .= "\n\t</tr>\n";

            $r++;
        }

        $s .= "</table>\n\n";



        return $s;
    }

    function merge($s_template, $a_data, $s_start_delimiter = '#', $s_end_delimiter = '#') {
        foreach ($a_data as $k => $v) {
            $s_template = str_replace($s_start_delimiter . $k . $s_end_delimiter, $v, $s_template);
        }

        return $s_template;
    }

    function ttable($a_data, $i_columns, $s_template = '', $s_css_class = 'ttable') {

        $a_keys = array_keys($a_data);

        if (empty($s_template)) {
            $s_template = '';

            foreach ($a_data[$a_keys[0]] as $k => $v)
                $s_template .= '{[' . $k . ']}<br />';
        }

        $s = '<table class="' . $s_css_class . '" cellspacing="0" cellpadding="0" width="100%">' . "\n";

        $c = 0; // Contador de colunas

        foreach ($a_keys as $s_key) {

            if ($c == ($i_columns)) {
                $s .= "\t<tr>\n";

                $c = 0;
            }

            $s .= '<td class="ttable-td">';

            $s_temp = $s_template;

            foreach ($a_data[$s_key] as $k => $v)
                $s_temp = str_replace('{[' . $k . ']}', $v, $s_temp);

            $s .= $s_temp;

            $s .='</td>';

            $c++;

            if ($c == ($i_columns)) {
                $s .= "\n\t</tr>\n";
            }
        }


        $s .= "</table>\n\n";

        return $s;
    }

    function sequence($a_data, $s_template = '') {

        $a_keys = array_keys($a_data);

        if (empty($s_template)) {
            $s_template = '';

            foreach ($a_data[$a_keys[0]] as $k => $v)
                $s_template .= '{[' . $k . ']} ';
        }

        $s = '';

        foreach ($a_keys as $s_key) {

            $s_temp = $s_template;

            foreach ($a_data[$s_key] as $k => $v)
                $s_temp = str_replace('{[' . $k . ']}', $v, $s_temp);

            $s .= $s_temp;
        }

        return $s;
    }

    function selective_htmlentities($s) {

        static $a = array(
            array('&Aacute;', 'Á'), array('&aacute;', '�'), array('&Acirc;', '�'), array('&acirc;', '�'),
            array('&AElig;', ''), array('&aelig;', ''), array('&Agrave;', '�'), array('&agrave;', '�'),
            array('&Aring;', ''), array('&aring;', ''), array('&Atilde;', '�'), array('&atilde;', '�'),
            array('&Auml;', '�'), array('&auml;', '�'), array('&Ccedil;', '�'), array('&ccedil;', '�'),
            array('&Eacute;', '�'), array('&eacute;', '�'), array('&Ecirc;', '�'), array('&ecirc;', '�'),
            array('&Egrave;', '�'), array('&egrave;', '�'), array('&ETH;', ''), array('&eth;', ''),
            array('&Euml;', '�'), array('&euml;', '�'), array('&Iacute;', '�'), array('&iacute;', '�'),
            array('&Icirc;', '�'), array('&icirc;', '�'), array('&Igrave;', '�'), array('&igrave;', '�'),
            array('&Iuml;', '�'), array('&iuml;', ''), array('&Ntilde;', ''), array('&ntilde;', ''),
            array('&Oacute;', '�'), array('&oacute;', '�'), array('&Ocirc;', '�'), array('&ocirc;', '�'),
            array('&Ograve;', '�'), array('&ograve;', '�'), array('&Oslash;', ''), array('&oslash;', ''),
            array('&Otilde;', '�'), array('&otilde;', '�'), array('&Ouml;', '�'), array('&ouml;', ''),
            array('&szlig;', ''), array('&THORN;', ''), array('&thorn;', ''), array('&Uacute;', '�'),
            array('&uacute;', '�'), array('&Ucirc;', '�'), array('&ucirc;', '�'), array('&Ugrave;', '�'),
            array('&ugrave;', '�'), array('&Uuml;', '�'), array('&uuml;', '�'), array('&Yacute;', '�'),
            array('&yacute;', '�'), array('&yuml;', '�')
        );

        foreach ($a as $aa)
            $s = str_replace($aa[1], $aa[0], $s);

        return $s;
    }

    /*
     *
     */

    public function select($kvData, $sElementName, $sOnselect = false) {

        $s = '<select ';
        if (!empty($sOnselect)) {
            $s .= 'onchange="' . $sOnselect . '"';
        }
        $s .= ' name="' . $sElementName . '">';

        $s .= '<option value="">...</option>';
        foreach ($kvData as $k => $desc) {
            $s .= '<option value="' . $k . '">' . $desc . '</option>' . "\n";
        }
        $s .= '</select>';
        return $s;
    }

    /*
     *
     */

    function radio($kv_data, $s_element_name) {

        $s_ret = '';

        foreach ($kv_data as $k => $s) {
            $s_ret .= '<input type="radio" name="' . $s_element_name . '" value="' . $k . '" />' . $s . "<br />\n";
        }

        return $s_ret;
    }

    function checkbox($kv_data, $s_element_name) {

        $s_ret = '<div class="ihtml-checkbox-div">';

        foreach ($kv_data as $k => $s) {
            $s_ret .= '<input class="ihtml-checkbox-input" type="checkbox" name="' . $s_element_name . '[]" value="' . $k . '" />&nbsp;' . $s . "<br />\n";
        }

        return $s_ret . '</div>';
    }

    static function getParam($s_var, $s_type = null, $b_optional = false, $a_options = null) {

        $m_value = null;

        if (!empty($_GET[$s_var])) {
            $m_value = $_GET[$s_var];
        } elseif (!empty($_POST[$s_var])) {
            $m_value = $_POST[$s_var];
        }

        if (is_string($m_value)) {
            $m_value = trim($m_value);

            if ($s_type != 'code' && $s_type != 'richtext' && $s_type != 'html') {
                $m_value = strip_tags($m_value);
                $m_value = nl2br($m_value);
            }
        }

        if (($s_type == 'integer' || $s_type == 'float') && $m_value == '0')
            return 0;

        if (empty($m_value))
            return null;

        if (empty($s_type))
            return $m_value;

        require_once ('itype.php');

        if (itype::valid($m_value, $s_type, $b_optional, $a_options)) {
            return $m_value;
        }

        return false;
    }

}
